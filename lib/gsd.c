#include "headers/gsd.h"

#define NODES_MAX 20000
#define SPHERES_MAX 500
#define TRUE 1
#define FALSE 0

void gsd_trans(M_gsd *gsd, M_point p) {
    M_vector v = m_displacement(p, gsd->center);
    int i;
    for ( i = 0; i < gsd->size; i++ ) {
        m_point_trans(&gsd->nodes[i].pos, v);
    }
    gsd->center = p;
}

void gsd_reset(M_gsd *gsd) {
    int i;
    for( i =0; i< gsd->size; i++ ) {
        gsd->nodes[i].valid = TRUE;
    }
}


prec m_gsd_sasa(M_molec *molec, M_uint n, prec solvent ) {
    DEBUG("Using %d points per atom\n",n);
    M_point origin = (M_point){.x = 0.0, .y = 0.0, .z = 0.0 };
    M_atom *atom = molec->atoms->data[0];
    origin.dims = atom->pos.dims;
    M_gsd s;
    M_uint i, j, z, y;

    M_uint sasa = 0;
    M_uint max_sasa = molec->atoms->count * n;

    M_atom *a, *b;
    a = molec->atoms->data[0];
    s = m_gsd_new ( n, a->spec->radius + solvent, origin ); 
    for ( i = 0; i < molec->atoms->count; i++ ) {
        a = molec->atoms->data[i];
        gsd_trans(&s,a->pos);
        gsd_reset(&s);
        // Build a gsd sphere at 'a->pos' with radius 'a->radius' 
        DEBUG("Generated GSD for atom %d with radius %lf\n",i,a->spec->radius);
        // Find all the atoms which intersect b
        M_parray *intersect = m_parray_new(0);
        for ( j = 0; j < molec->atoms->count; j++ ) {
            if ( j != i ) {
                b = molec->atoms->data[j];
                // Check if a & b intersect
                double  max_intersect_dist = a->spec->radius + b->spec->radius + solvent + solvent;
                if ( m_vector_mag(m_displacement(a->pos, b->pos)) < max_intersect_dist ) {
                    m_parray_push(intersect,b);
                }
            }
        }
        DEBUG("%d atoms intersect\n", intersect->count);
        // For all intersecting spheres, find out how many points are involved.
        for ( z = 0; z < s.size; z++ ) {
            if ( s.nodes[z].valid == TRUE ) {
                for ( y = 0; y < intersect->count; y++ ) {
                    b = intersect->data[y];
                    // if the point is within the other sphere, sasa++
                    if (m_vector_mag(m_displacement(s.nodes[z].pos, b->pos)) < b->spec->radius + solvent ) {
                        sasa++;
                        s.nodes[z].valid = FALSE;
                        break;
                    }
                }
            }
        }
        m_parray_free(intersect);
    }
    free(s.nodes);
    DEBUG("Sasa / max:  %d / %d\n",sasa,max_sasa);
    return (prec)(max_sasa-sasa)/(prec)max_sasa;
}

M_uint octrant ( M_parray *oct, M_atom *b, prec solvent ) { 
    int z, f;
    M_uint ret = 0;
    M_node *node;
    prec r, r2;
    int bad;
    for ( z = 0; z < oct->count; z++ ) {
        node = (M_node*)oct->data[z];
        bad = 0;
        if (node->valid == TRUE ) { 
            bad = 1;
            // if the point is within the other sphere, sasa++
            r = b->spec->radius + solvent;
            r2 = r*r;
            if (m_vector_mag2(m_displacement(node->pos, b->pos)) < r2) {
                ret++;
                node->valid = FALSE;
            }
        }
    }
    return ret;
}


prec m_gsd_sasa2(M_molec *molec, M_uint n, prec solvent ) {
    DEBUG("Using %d points per atom\n",n);
    M_point origin = (M_point){.x = 0.0, .y = 0.0, .z = 0.0 };
    M_atom *atom = molec->atoms->data[0];
    origin.dims = atom->pos.dims;
    M_gsd s;
    M_uint i, j, z, y, w;

    M_uint sasa = 0;
    M_uint max_sasa = molec->atoms->count * n;
    M_vector disp;

    M_atom *a, *b;
    a = molec->atoms->data[0];
    s = m_gsd_new ( n, a->spec->radius + solvent, origin ); 
    prec bdx, bdy, bdz, nbdx, nbdy, nbdz;
    for ( i = 0; i < molec->atoms->count; i++ ) {
        a = molec->atoms->data[i];
        gsd_trans(&s, a->pos);
        gsd_reset(&s);
        //s = m_gsd_new ( n, a->spec->radius + solvent, a->pos); 
        // Build a gsd sphere at 'a->pos' with radius 'a->radius' 
        
        // Narrow the list down to atoms which actually intersect
        // atom a
        M_parray *intersect = m_parray_new(0);
        for ( j = 0; j < molec->atoms->count; j++ ) {
            if ( j != i ) {
                b = molec->atoms->data[j];
                // Check if a & b intersect
                double  max_intersect_dist = a->spec->radius + b->spec->radius + solvent + solvent;
                if ( m_vector_mag(m_displacement(a->pos, b->pos)) < max_intersect_dist ) {
                    m_parray_push(intersect,b);
                }
            }
        }
        
        int quads;
        // Go through each quadrant
        // XpYpZp

        for ( j = 0; j < intersect->count; j++ ) {
            quads = 0;
            b = intersect->data[j];
            prec brad = b->spec->radius + solvent + solvent;
            disp = m_displacement(b->pos, a->pos);
            bdx = brad + disp.x;
            bdy = brad + disp.y;
            bdz = brad + disp.z;

            nbdx = -brad + disp.x;
            nbdy = -brad + disp.y;
            nbdz = -brad + disp.z;
            if  ( ( bdx > 0 ) &&
                  ( bdy > 0 ) &&
                  ( bdz > 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XpYpZp, b, solvent);
            }

            // XpYpZn
            if  ( ( bdx > 0 ) &&
                  ( bdy > 0 ) &&
                  ( nbdz < 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XpYpZn, b, solvent);
            }

            // XpYnZp
            if  ( ( bdx > 0 ) &&
                  ( nbdy < 0 ) &&
                  ( bdz > 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XpYnZp, b, solvent);
            }
            // XpYnZn
            if  ( ( bdx > 0 ) &&
                  ( nbdy < 0 ) &&
                  ( nbdz < 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XpYnZn, b, solvent);
            }

            // XnYpZp
            if  ( ( nbdx < 0 ) &&
                  ( bdy > 0 ) &&
                  ( bdz > 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XnYpZp, b, solvent);
            }

            // XnYpZn
            if  ( ( nbdx < 0 ) &&
                  ( bdy > 0 ) &&
                  ( nbdz < 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XnYpZn, b, solvent);
            }

            // XnYnZp
            if  ( ( nbdx < 0 ) &&
                  ( nbdy < 0 ) &&
                  ( bdz > 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XnYnZp, b, solvent);
            }

            // XnYnZn
            if  ( ( nbdx < 0 ) &&
                  ( nbdy < 0 ) &&
                  ( nbdz < 0 ) ) {
                  // loop over points in quad
                  sasa += octrant(s.XnYnZn, b, solvent);
            }
        }
        m_parray_free(intersect);
        for ( w = 0; w < s.XpYpZp->count; w++ ) {
            M_node node = *(M_node*)s.XpYpZp->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XpYpZn->count; w++ ) {
            M_node node = *(M_node*)s.XpYpZn->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XpYnZn->count; w++ ) {
            M_node node = *(M_node*)s.XpYnZn->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XpYnZp->count; w++ ) {
            M_node node = *(M_node*)s.XpYnZp->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XnYpZp->count; w++ ) {
            M_node node = *(M_node*)s.XnYpZp->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XnYpZn->count; w++ ) {
            M_node node = *(M_node*)s.XnYpZn->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XnYnZp->count; w++ ) {
            M_node node = *(M_node*)s.XnYnZp->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
        for ( w = 0; w < s.XnYnZn->count; w++ ) {
            M_node node = *(M_node*)s.XnYnZn->data[w];
            if (node.valid == TRUE) {
                printf("%f\t%f\t%f\n",node.pos.x, node.pos.y, node.pos.z);
            }
        }
    }
    DEBUG("Sasa / max:  %d / %d\n",sasa,max_sasa);
    return (prec)(max_sasa-sasa)/(prec)max_sasa;
}
/*
        */

M_gsd m_gsd_new( M_uint nodes, prec scale, M_point c) {
    DEBUG("Building GSD sphere with %d points\n", nodes);
    M_gsd s;
    M_point p, w;
    s.XpYpZp = m_parray_new(0);
    s.XpYpZn = m_parray_new(0);
    s.XpYnZp = m_parray_new(0);
    s.XpYnZn = m_parray_new(0);
    s.XnYpZp = m_parray_new(0);
    s.XnYpZn = m_parray_new(0);
    s.XnYnZp = m_parray_new(0);
    s.XnYnZn = m_parray_new(0);
    s.nodes = malloc(sizeof(M_node)*nodes);
    prec inc = 3.1415 * ( 3 - sqrt(5) );
    prec off = 2.0/((prec)nodes);
    int i;
    prec y,phi,r;
    for ( i = 0; i < nodes; i++ ) {
        y = i * off - 1 + (off/2.0);
        r = sqrt( 1 - y*y ) ;
        phi = i * inc;
        w.x = cos(phi)*r*scale; 
        w.y = sin(phi)*r*scale;
        w.z = y*scale;
        p.x = w.x + c.x; 
        p.y = w.y + c.y;
        p.z = w.z + c.z;
        p.dims = c.dims;
        s.nodes[i].pos = p;
        s.nodes[i].valid = TRUE;
        M_node *t = &s.nodes[i];
        if ( w.x >= 0 ) {
            if ( w.y >= 0 ) {
                if ( w.z >= 0 ) {
                    m_parray_push (s.XpYpZp, t);
                } else {
                    m_parray_push (s.XpYpZn, t);
                }
            } else {
                if ( w.z >= 0 ) {
                    m_parray_push (s.XpYnZp, t);
                } else {
                    m_parray_push (s.XpYnZn, t);
                }
            }
        } else {
            if ( w.y >= 0 ) {
                if ( w.z >= 0 ) {
                    m_parray_push (s.XnYpZp, t);
                } else {
                    m_parray_push (s.XnYpZn, t);
                }
            } else {
                if ( w.z >= 0 ) {
                    m_parray_push (s.XnYnZp, t);
                } else {
                    m_parray_push (s.XnYnZn, t);
                }
            }
        }
    }
    DEBUG("XpYpZp %d\n", s.XpYpZp->count);
    DEBUG("XpYpZn %d\n", s.XpYpZn->count);
    DEBUG("XpYnZp %d\n", s.XpYnZp->count);
    DEBUG("XpYnZn %d\n", s.XpYnZn->count);
    DEBUG("XnYpZp %d\n", s.XnYpZp->count);
    DEBUG("XnYpZn %d\n", s.XnYpZn->count);
    DEBUG("XnYnZp %d\n", s.XnYnZp->count);
    DEBUG("XnYnZn %d\n", s.XnYnZn->count);
    s.size = nodes;
    s.scale = scale;
    s.center = c;
    DEBUG("Finished GSD sphere with %d points\n", nodes);
    return s;
}
