#include "headers/forcefield.h"

void calc_rcut(M_system *sys) {

    // get the shortest dimension
    prec s = sys->dims.x;
    if ( s > sys->dims.y) {
        s = sys->dims.y;
    }
    if ( s > sys->dims.z) {
        s = sys->dims.z;
    }
    s = s/2.0;
    if (sys->rcut) {
        if (sys->rcut > s) {
            sys->rcut = s;
        }
    } else {
        sys->rcut = s;
    }   

    sys->rcut2 = sys->rcut * sys->rcut;

}

M_vector m_lj_force( M_atom *a, M_atom *b ) {
    //                 1     /  dU  \.
    // f_mn = - r_mn ------ |  ----  |
    // ~        ~    |r_mn|  \  dr  /

    // U = (smallsig/r)^12 - c6/r^6

    // dU/dr = -12(smallsig)^12/r^13 + 6 c6/r^7

    // f_mn = - r_mn ( -12(smallsig)^12/r^14 + 6 c6/r^8 )
    // ~        ~

    // if both the interaction parameter is zero
    if (  a->spec->smallsig[b->spec->id] == 0 
          && a->spec->C6[b->spec->id] == 0 ) {
        return M_ZERO_VEC;
    }

    M_vector f = M_ZERO_VEC;
    prec ss12 = a->spec->ss12[b->spec->id];
    prec c6 = a->spec->C6[b->spec->id];

    // dr is in the direction of a->b : dr = b - a
    M_vector dr = m_disp(b->pos, a->pos);
    prec r2 = m_vector_mag2(dr);
    prec r_r8 = pow(r2,-4.0);
    prec r_r14 = r_r8*r_r8*r2;

    f = m_vector_scale(dr, (prec) -1.0*(-12.0*ss12*r_r14 + 6.0*c6*r_r8));
    return f;
}


void m_forcefield_load_config(M_system *sys, dictionary *dict) {
    // constants relating to the forcefield
    sys->rcut         = iniparser_getdouble(dict,"system:rcut", 0);
    sys->no_cutoff    = iniparser_getboolean(dict,"system:no_cutoff",0);

    // read in values for interaction parameters
    int i;
    char atom_smallsig_key[40];
    char atom_C6_key[40];
    char atom_key[40];
    char *p;
    for ( i=0; i<sys->atom_specs->count; i++ ) {
        M_atom_spec *aspec = m_sys_atom_spec(sys,i);
        M_molec_spec *mspec = aspec->molec_spec;

        strcpy(atom_key,mspec->name);
        strcat(atom_key,"#");
        strcat(atom_key,aspec->name);

        strcpy(atom_smallsig_key,atom_key);
        strcat(atom_smallsig_key,":smallsig");

        aspec->smallsig = malloc(sizeof(prec)*sys->atom_specs->count); 
        clear_farray(aspec->smallsig, sys->atom_specs->count);
        p = iniparser_getstring(dict, atom_smallsig_key, NULL); 
        if ( p != NULL ) {
            parse_csv_farray( aspec->smallsig, p, sys->atom_specs->count );
        }

        strcpy(atom_C6_key,atom_key);
        strcat(atom_C6_key,":C6");

        aspec->C6 = (prec *) malloc(sizeof(prec) * sys->atom_specs->count); 
        clear_farray(aspec->C6, sys->atom_specs->count);
        p = iniparser_getstring(dict, atom_C6_key, NULL); 
        if ( p != NULL ) {
            parse_csv_farray( aspec->C6, p, sys->atom_specs->count );
        }
        
        int j;
        // make sure the interaction matrix is symmetric
        for ( j=0; j<i; j++ ) {
            M_atom_spec *aspec_j = m_sys_atom_spec(sys,j);
            aspec->smallsig[j] = aspec_j->smallsig[i];
            aspec->C6[j] = aspec_j->C6[i];
        }

        aspec->ss12 = (prec *) malloc(sizeof(prec)*sys->atom_specs->count);
        for ( j=0; j<sys->atom_specs->count; j++ ) {
            aspec->ss12[j] = pow(aspec->smallsig[j], 12.0);
        }
    }
}
