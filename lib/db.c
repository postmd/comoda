#include "headers/db.h"

M_system * m_system_new() {
    M_system *new       = malloc(sizeof(M_system));
    new->atoms          = m_array_new(0, sizeof(M_atom),
                                      (M_callback) m_atom_free_children);
    new->atom_specs     = m_array_new(0, sizeof(M_atom_spec),
                                      (M_callback) m_atom_spec_free_children);
    new->molecs         = m_array_new(0, sizeof(M_molec),
                                      (M_callback) m_molec_free_children);
    new->molec_specs    = m_array_new(0, sizeof(M_molec_spec),
                                      (M_callback) m_molec_spec_free_children);
    new->has_charges    = 0;
    new->net_charge     = 0;

    new->rcut           = 0.0;
    new->rcut2          = 0.0;
    new->no_cutoff      = 0;
    new->volume         = 0;
    new->dims           = (M_vector) {.x = 0, .y = 0, .z = 0};
    new->wrap_coords    = 1;
    new->constant_dims  = 0;

    new->calc_dipoles   = 0;
    new->calc_centers   = 0;

    new->read_dip       = 0;
    new->read_vel       = 0;
    new->read_force     = 0;

    clear_uiarray(new->cell_ngrid,3);
    new->pop_atom_cell  = 0;
    new->pop_mol_cell   = 0;
    new->cell_atoms     = NULL;
    new->cell_molecs    = NULL;

    clear_uiarray(new->nk,3);
    new->eta            = 0;
    new->ewald          = NULL;
    new->calc_ewald     = 0;

    new->calc_stress    = 0;
    new->stress_lj_lr   = 0;
    new->stress_toymd   = 0;
    new->stress         = NULL;

    new->current_frame_id = -1; // this is ticked to id=0 on the first read
    new->frames_processed = 0;
    new->start_frame      = 1;
    new->end_frame        = 0;
    new->use_end          = 0;  // do not terminate at the "end_frame"
    new->skip             = 0;

    return new;
};

void m_system_free(M_system *sys) {
    if ( sys != NULL ) {
        m_array_free(sys->atoms);
        m_array_free(sys->atom_specs);
        m_array_free(sys->molecs);
        m_array_free(sys->molec_specs);

        m_cells_free(sys->cell_atoms);
        m_cells_free(sys->cell_molecs);
        m_ewald_free(sys->ewald);

        free(sys);
        sys = NULL;
    }
}

M_atom *m_atom_new() {
    M_atom *new = malloc(sizeof(M_atom));

    new->id = 0;
    new->id_in_spec = 0;
    new->id_in_molec = 0;

    new->spec = NULL;
    new->molec = NULL;

    new->pos = (M_point) {.x = 0, .y = 0, .z = 0, .dims = NULL };
    new->vel = (M_vector) {.x = 0, .y = 0, .z = 0 };
    new->force = (M_vector) {.x = 0, .y = 0, .z = 0 };
    new->dip = (M_vector) {.x = 0, .y = 0, .z = 0 };

    return new;
}

void m_atom_free_children(M_atom *atom) {
}

void m_atom_free(M_atom *atom) {
    m_atom_free_children(atom);
    free(atom);
    atom = NULL;
}

M_atom *m_atom_copy(M_atom *atom) {
    M_atom *new = m_atom_new();

    new->spec = atom->spec;

    new->pos = m_point_copy(&atom->pos);
    new->vel = m_vector_copy(&atom->vel);
    new->force = m_vector_copy(&atom->force);
    new->dip = m_vector_copy(&atom->dip);

    return new;
}

M_atom_spec *m_atom_spec_new ( char *name, char *symbol, M_uint count,
                               prec mass, prec radius, prec charge) {
    M_atom_spec *new = malloc(sizeof(M_atom_spec));
    strcpy(new->name, name);
    strcpy(new->symbol,symbol);
    new->count = count;
    new->mass = mass;
    new->atoms = m_parray_new(0);
    new->charge = charge;
    new->radius = radius;
    new->smallsig = NULL;
    new->ss12 = NULL;
    new->C6 = NULL;
    return new;
}

void m_atom_spec_free_children ( M_atom_spec *spec ) {
    m_parray_free(spec->atoms);
    if ( spec->smallsig != NULL ) {
        free(spec->smallsig);
    }
    if ( spec->ss12 != NULL ) {
        free(spec->ss12);
    }
    if ( spec->C6 != NULL ) {
        free(spec->C6);
    }
}
void m_atom_spec_free ( M_atom_spec *spec ) {
    if ( spec != NULL ) {
        m_atom_spec_free_children(spec);
        free(spec);
        spec = NULL;
    }
}

M_molec_spec *m_molec_spec_new ( char *name, int count ) {
    M_molec_spec *new = malloc(sizeof(M_molec_spec));
    new->atom_specs = m_parray_new(0);
    new->molecs = m_parray_new(0);
    strcpy(new->name,name);
    new->count = count;
    new->atoms = 0;
    new->position = TOGETHER;
    new->center = 0;
    new->mass = -1.0;
    new->charge = 0.0;
    return new;
}

void m_molec_spec_free_children (M_molec_spec *spec) {
    m_parray_free(spec->atom_specs);
    spec->atom_specs = NULL;
    m_parray_free(spec->molecs);
    spec->molecs = NULL;
}
void m_molec_spec_free (M_molec_spec *spec) {
    if ( spec != NULL ) {
        m_molec_spec_free_children(spec);
        free(spec);
        spec = NULL;
    }
}

M_molec *m_molec_new() {
    M_molec *new = malloc(sizeof(M_molec));
    new->id = 0;
    new->id_in_spec = 0;

    new->atoms = m_parray_new(0);
    new->spec = NULL;
    new->atoms_array = NULL;

    new->dipole = (M_vector) {.x = 0, .y = 0, .z = 0};
    new->center = (M_point) {.x = 0, .y = 0, .z = 0, .dims = NULL };
    new->vel    = (M_vector) {.x = 0, .y = 0, .z = 0};

    return new;
}

void m_molec_free_children(M_molec *molec) {
    m_parray_free(molec->atoms);
    molec->atoms = NULL;
    if(molec->atoms_array != NULL ) {
        //DEBUG("Has own atoms array %d\n", 0);
        m_array_free(molec->atoms_array);
        molec->atoms_array = NULL;
    }
}

void m_molec_free(M_molec *molec) {
    if ( molec != NULL ) {
        m_molec_free_children(molec);
        free(molec);
        molec = NULL;
    }
}

M_molec *m_molec_copy(M_molec *molec) {
    M_molec *new = m_molec_new();

    new->id = 0;
    new->id_in_spec = 0;
    new->spec = molec->spec;

    new->dipole = m_vector_copy(&molec->dipole);
    new->center = m_point_copy(&molec->center);
    new->vel    = m_vector_copy(&molec->vel);

    new->atoms_array = m_array_new(0, sizeof(M_atom),
                                   (M_callback) m_atom_free_children);

    int i;
    M_atom *new_atom, *new_atom_p;
    for ( i=0; i<molec->atoms->count; i++ ) {
        new_atom=m_atom_copy(m_molec_atom(molec,i));

        new_atom_p = m_array_push_nofree(new->atoms_array,new_atom,1);
        m_parray_push(new->atoms, new_atom_p);
    }

    return new;
}

/* M_frame : not used at the moment

M_frame *m_frame_new() {
    M_frame *new = malloc(sizeof(M_frame));
    return new;
}

void m_frame_free_children(M_frame *frame) {
}

void m_frame_free(M_frame *frame) {
    if ( frame != NULL ) {
        m_frame_free_children(frame);
        free(frame);
        frame = NULL;
    }
}
*/
