#include "headers/env.h"

int get_num_atoms(int *a) {
  char *atoms = getenv("ATOMS");
  if (atoms != NULL ) {
    *a = atoi(atoms);
    return 1;
  } else {
    fprintf(stderr, "Error: ATOMS Environment Variable must be set\n");
    return 0;
  }
}

int get_num_mols(int *a) {
  char *atoms = getenv("MOLECULES");
  if (atoms != NULL ) {
    *a = atoi(atoms);
    return 1;
  } else {
    fprintf(stderr, "Error: MOLECULES Environment Variable must be set\n");
    return 0;
  }
}

int get_num_objects(int *a) {
  char *atoms = getenv("OBJECTS");
  if (atoms != NULL ) {
    *a = atoi(atoms);
    return 1;
  } else {
    fprintf(stderr, "Error: OBJECTS Environment Variable must be set\n");
    return 0;
  }
}

int get_bin_size(double *a) {
  char *bin_s = getenv("BIN_SIZE");
  if (bin_s != NULL ) {
    *a = atof(bin_s);
    return 1;
  } else {
    fprintf(stderr, "Error: BIN_SIZE Environment Variable must be set\n");
    return 0;
  }
}
  
int get_r_cut(double *a) {
  char *r_cut = getenv("R_CUT");
  if (r_cut != NULL ) {
    *a = atof(r_cut);
    return 1;
  } else {
    fprintf(stderr, "Error: R_CUT Environment Variable must be set\n");
    return 0;
  }
}

int get_box_dim(vector *v) {
  double x,y,z;
  char *box_dim = getenv("BOX_DIMENSIONS");
  if (box_dim != NULL ) {
    int matched = sscanf(box_dim, "%lf,%lf,%lf",&x,&y,&z);
    if (matched ==3 ) {
      v->x = x;
      v->y = y;
      v->z = z;
    }
    return 1;
  } else {
    fprintf(stderr, "Error: BOX_DIMENSIONS Environment Variable must be set\n");
    return 0;
  }
}

int get_dipole_matrix(dipole_vector *dm) {
  char *dipole_matrix = getenv("DIPOLE_MATRIX");
  char sep[] = " ";
  char *token;
  int i = 0;
  if ( dipole_matrix != NULL ) {
    token = strtok(dipole_matrix,sep);
    while ( token != NULL ) {
      dipole_vector d;
      int a,b;
      double scale;
      int matched = sscanf(token, "%d,%d,%lf", &a, &b,&scale);
      if ( matched == 3 ) {
        d.a = a;
        d.b = b;
        d.scale = scale;
        dm[i++] =d;
      } else {
        fprintf(stderr,"DIPOLE_MATRIX was not properly formatted");
        return 0;
      }
      token = strtok(NULL,sep);
    }
    return i;
  } else {
    fprintf(stderr,"DIPOLE_MATRIX was not properly formatted");
    return 0;
  }
}
/*
int main() {
  int num_atoms;
  vector box_dim;
  dipole_vector dipole_matrix[100];
  if ( !get_num_atoms(&num_atoms) ) { return 1; }
  if ( !get_box_dim(&box_dim) ) { return 1; }
  if ( !get_dipole_matrix(dipole_matrix) ) { return 1; }
  
  printf("%d\n", num_atoms);
  printf("%f\n", box_dim.x);
  printf("%d\n", dipole_matrix[0].a); 
}
*/
