#include "headers/vector.h"

M_vector m_vector_add( M_vector v1, M_vector v2 ) {
    M_vector v;  
    v.x = v1.x + v2.x;
    v.y = v1.y + v2.y;
    v.z = v1.z + v2.z;
    return v;
}

M_vector m_vector_subtract( M_vector v1, M_vector v2 ) {
    M_vector v;  
    v.x = v1.x - v2.x;
    v.y = v1.y - v2.y;
    v.z = v1.z - v2.z;
    return v;
}

M_vector m_vector_scale( M_vector v1, prec k ) {
    M_vector v;
    v.x = v1.x * k;
    v.y = v1.y * k;
    v.z = v1.z * k;
    return v;
}

M_vector m_vector_unit( M_vector v1 ) {
    M_vector v;
    prec r;
    r = m_vector_length(v1);
    v = m_vector_scale(v1,1/r);
    return v;
}

M_vector m_vector_invert( M_vector v1 ) {
    M_vector v;
    v.x = -v1.x;
    v.y = -v1.y;
    v.z = -v1.z;
    return v;
}

M_vector m_point_to_vector( M_point *p ) {
    M_vector v;
    v.x = p->x;
    v.y = p->y;
    v.z = p->z;
    return v;
}

prec m_vector_dot( M_vector v1, M_vector v2 ) {
    return ( v1.x*v2.x + v1.y*v2.y + v1.z*v2.z );
}

void m_point_wrap( M_point *p ) {
    // wraps a point into its dims
    p->x = fmod(p->x, p->dims->x);
    p->y = fmod(p->y, p->dims->y);
    p->z = fmod(p->z, p->dims->z);

    if ( p->x < 0 ) {
      p->x += p->dims->x;
    }
    if ( p->y < 0 ) {
      p->y += p->dims->y;
    }
    if ( p->z < 0 ) {
      p->z += p->dims->z;
    }
}

void m_point_trans( M_point *p, M_vector v ) {
    p->x += v.x;
    p->y += v.y;
    p->z += v.z;

    m_point_wrap(p);
}

void m_puts_point(M_point v) {
  printf("%.10E\t%.10E\t%.10E\n", v.x,v.y,v.z);
}

void m_puts_vector(M_vector v) {
  printf("%.10E\t%.10E\t%.10E\n", v.x,v.y,v.z);
}

void m_puts_matrix(M_matrix m) {
  printf("%.10E\t%.10E\t%.10E\n", m.xx, m.xy, m.xz);
  printf("%.10E\t%.10E\t%.10E\n", m.yx, m.yy, m.yz);
  printf("%.10E\t%.10E\t%.10E\n", m.zx, m.zy, m.zz);
}

void m_puts_matrix_oneline(M_matrix m) {
  printf("%.10E\t%.10E\t%.10E\t%.10E\t%.10E\t%.10E\t%.10E\t%.10E\t%.10E\n",
                                                          m.xx, m.xy, m.xz,
                                                          m.yx, m.yy, m.yz,
                                                          m.zx, m.zy, m.zz);
}

void m_puts_trace(M_matrix m) {
  printf("%.10E\n",m_matrix_trace(m));
}

void m_puts_diag(M_matrix m) {
  printf("%.10E\t%.10E\t%.10E\n",m.xx,m.yy,m.zz);
}

M_vector m_read_dims_stdin() {
    M_vector dims;
    if (scanf("%lf,%lf,%lf", &dims.x, &dims.y, &dims.z) == 3 ){
        return dims;
    } else {
        exit(1);
    }
}

// M_vector m_disp(M_point a, M_point b) {
//     M_vector new;
//     prec dx, dy, dz;
//     dx = a.x - b.x;
//     dy = a.y - b.y;
//     dz = a.z - b.z;
//     new = (M_vector){.x = dx, .y = dy, .z = dz};
//     return new;
// }

M_vector m_disp(M_point a, M_point b) {
    prec dx, dy, dz;
    dx = a.x - b.x;
    dy = a.y - b.y;
    dz = a.z - b.z;
    M_vector *dims;

    if ( a.dims->x == b.dims->x
         && a.dims->y == b.dims->y 
         && a.dims->z == b.dims->z ) {
        dims = a.dims;
    } else {
        FATAL("points %c and %c are in different boxes\n", 'a', 'b');
        FATAL("point a is in box [ %f, %f, %f ]\n", a.dims->x, a.dims->y,
                                                    a.dims->z);
        FATAL("point b is in box [ %f, %f, %f ]\n", b.dims->x, b.dims->y,
                                                    b.dims->z);
        exit(1);
    }

    M_vector new;

    if (dx >= dims->x/2) {
        dx -= dims->x;
    } else if ( dx < -dims->x/2) {
        dx += dims->x;
    }

    if (dy >= dims->y/2) {
        dy -= dims->y;
    } else if ( dy < -dims->y/2) {
        dy += dims->y;
    }

    if (dz >= dims->z/2) {
        dz -= dims->z;
    } else if ( dz < -dims->z/2) {
        dz += dims->z;
    }

    new = (M_vector){.x = dx, .y = dy, .z = dz};
    return new;
}

prec m_vector_mag(M_vector v) {
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

prec m_vector_mag2(M_vector v) {
    return v.x*v.x + v.y*v.y + v.z*v.z;
}

M_point m_vector_to_point(M_vector *v, M_vector *dims) {
    M_point new;

    new.x = v->x;
    new.y = v->y;
    new.z = v->z;

    m_point_bound( &new, dims );

    return new;
}

void m_point_bound(M_point *point, M_vector *dims) {
    if (point->dims != NULL ) {
        FATAL("Point is already bounded by [%e %e %e].\n", point->dims->x,
                                                           point->dims->y,
                                                           point->dims->z);
        FATAL("Can't bound a point which is already bounded!\n");
        exit(1);
    }
    if(point->x < 0) {
        point->x = dims->x + point->x;
    }
    if(point->y < 0) {
        point->y = dims->y + point->y;
    }
    if(point->z < 0) {
        point->z = dims->z + point->z;
    }
    if(point->x > dims->x) {
        point->x = - dims->x + point->x;
    }
    if(point->y > dims->y) {
        point->y = - dims->y + point->y;
    }
    if(point->z > dims->z) {
        point->z = - dims->z + point->z;
    }
    point->dims = dims;
}

M_vector m_vector_copy(M_vector *v) {

    M_vector new;

    new.x = v->x;
    new.y = v->y;
    new.z = v->z;

    return new;

}

M_point m_point_copy(M_point *p) {

    M_point new;

    new.x = p->x;
    new.y = p->y;
    new.z = p->z;

    new.dims = p->dims;

    return new;

}

M_matrix m_matrix_copy(M_matrix *m) {
    M_matrix new;
    new.xx = m->xx;
    new.xy = m->xy;
    new.xz = m->xz;
    new.yx = m->yx;
    new.yy = m->yy;
    new.yz = m->yz;
    new.zx = m->zx;
    new.zy = m->zy;
    new.zz = m->zz;

    return new;
}

M_matrix m_matrix_add(M_matrix m1, M_matrix m2) {

    M_matrix new;

    new.xx = m1.xx + m2.xx;
    new.xy = m1.xy + m2.xy;
    new.xz = m1.xz + m2.xz;
    new.yx = m1.yx + m2.yx;
    new.yy = m1.yy + m2.yy;
    new.yz = m1.yz + m2.yz;
    new.zx = m1.zx + m2.zx;
    new.zy = m1.zy + m2.zy;
    new.zz = m1.zz + m2.zz;

    return new;
}

M_matrix m_matrix_subtract(M_matrix m1, M_matrix m2) {

    M_matrix new;

    new.xx = m1.xx - m2.xx;
    new.xy = m1.xy - m2.xy;
    new.xz = m1.xz - m2.xz;
    new.yx = m1.yx - m2.yx;
    new.yy = m1.yy - m2.yy;
    new.yz = m1.yz - m2.yz;
    new.zx = m1.zx - m2.zx;
    new.zy = m1.zy - m2.zy;
    new.zz = m1.zz - m2.zz;

    return new;
}

M_matrix m_matrix_scale(M_matrix m, prec k) {

    M_matrix new;

    new.xx = m.xx * k;
    new.xy = m.xy * k;
    new.xz = m.xz * k;
    new.yx = m.yx * k;
    new.yy = m.yy * k;
    new.yz = m.yz * k;
    new.zx = m.zx * k;
    new.zy = m.zy * k;
    new.zz = m.zz * k;

    return new;
}

prec m_matrix_trace(M_matrix m) {
    return m.xx + m.yy + m.zz;
}

M_matrix m_matrix_multiply(M_matrix m1, M_matrix m2) {

    M_matrix new;

    new.xx = m1.xx * m2.xx + m1.xy * m2.yx + m1.xz * m2.zx;
    new.xy = m1.xx * m2.xy + m1.xy * m2.yy + m1.xz * m2.zy;
    new.xz = m1.xx * m2.xz + m1.xy * m2.yz + m1.xz * m2.zz;
    new.yx = m1.yx * m2.xx + m1.yy * m2.yx + m1.yz * m2.zx;
    new.yy = m1.yx * m2.xy + m1.yy * m2.yy + m1.yz * m2.zy;
    new.yz = m1.yx * m2.xz + m1.yy * m2.yz + m1.yz * m2.zz;
    new.zx = m1.zx * m2.xx + m1.zy * m2.yx + m1.zz * m2.zx;
    new.zy = m1.zx * m2.xy + m1.zy * m2.yy + m1.zz * m2.zy;
    new.zz = m1.zx * m2.xz + m1.zy * m2.yz + m1.zz * m2.zz;

    return new;
}

M_matrix m_matrix_square(M_matrix m) {
    return m_matrix_multiply(m, m);
}

M_vector m_matrix_vector_multiply(M_matrix m, M_vector v) {
    M_vector new;

    new.x = m.xx * v.x + m.xy * v.y + m.xz * v.z;
    new.y = m.yx * v.x + m.yy * v.y + m.yz * v.z;
    new.z = m.zx * v.x + m.zy * v.y + m.zz * v.z;

    return new;
}

void m_matrix_zero(M_matrix *m) {
    m->xx = 0;
    m->xy = 0;
    m->xz = 0;
    m->yx = 0;
    m->yy = 0;
    m->yz = 0;
    m->zx = 0;
    m->zy = 0;
    m->zz = 0;
}

M_matrix m_vector_outer(M_vector a, M_vector b) {
    M_matrix m;
    m.xx = a.x*b.x;
    m.xy = a.x*b.y;
    m.xz = a.x*b.z;
    m.yx = a.y*b.x;
    m.yy = a.y*b.y;
    m.yz = a.y*b.z;
    m.zx = a.z*b.x;
    m.zy = a.z*b.y;
    m.zz = a.z*b.z;
    return m;
}

M_vector rand_sphere1() {
    // Generate random point on a unit sphere by the method of Cook (1957)
    // see http://mathworld.wolfram.com/SpherePointPicking.html

    prec x0, x1, x2, x3;
    prec mod;

    while (true) {
        x0 = rand_1()*2.0-1.0;
        x1 = rand_1()*2.0-1.0;
        x2 = rand_1()*2.0-1.0;
        x3 = rand_1()*2.0-1.0;

        mod = x0*x0 + x1*x1 + x2*x2 + x3*x3;

        if ( mod < 1 ) break;
    }

    M_vector new;
    new.x = 2.0*(x1*x3 + x0*x2)/mod;
    new.y = 2.0*(x2*x3 - x0*x1)/mod;
    new.z = (x0*x0 + x3*x3 - x1*x1 - x2*x2)/mod;

    return new;

}
