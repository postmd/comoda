#include "headers/arithmetic.h"

int pmod ( int a, int b ) {
  if ( b > 0 ) {
    int ret = a % b;
    if(ret < 0)
      ret+=b;
    return ret;
  }
  else if ( b < 0 ) {
    return pmod(-a,-b);
  }
  else {
    FATAL("Attempting to divide by zero.\n");
    exit(1);
  }
}

//              { 0,    if a<0
// pspan(a,b) = { a,    if 0<a<|b|
//              { |b|,  if a>=|b|
int pspan ( int a, int b ) {
  if ( b < 0 ) {
    return pspan(a,-b);
  } else if ( a < 0 ) {
    return 0;
  } else if ( a < b ) {
    return a;
  } else {
    return b;
  } 
}

M_uint bin_id( prec val, prec min, prec width ) {
  M_uint id;

  if ( val >= min ) {
    
    id = (M_uint) floor( ( val - min ) / width );
    return id;

  } else {
    FATAL("Key value %f less than minimum %f.\n",val,min);
    exit(1);
  }
}

prec rand_1() {
    return (double)rand() / (double)RAND_MAX;
}

