#include "headers/marray.h"

M_uint nearest_power(M_uint n);

int m_array_resize( M_array *array, M_uint length );

M_uint nearest_power(M_uint n) {
    M_uint m = 1;
    while ( m < n && n > 0 ) {
        m <<= 1;
    }
    return m ? m : n;
}

M_uint cheapo(M_uint n) {
    return n + 10;
}

M_uint rich(M_uint n) {
    return n + 40000;
}

int m_array_resize( M_array *array, M_uint length ) {
    M_uint size = M_RESERVE_MINIMUM;
    if (length > size ) {
        size = M_RESERVE_METHOD(length);
    } 
    array->data = realloc(array->data, size*array->e_size);
    //DEBUG("Requested %d  elements, Reserve function wants %d elements\n", length, size);
    array->length = size;
    return 0;
}

M_array *m_array_new(M_uint length, size_t size, M_callback myfree) {
    M_array *new = malloc(sizeof(M_array));
    new->e_size = size;
    new->length = length;
    new->free_func = myfree;
    new->data = NULL;
    m_array_resize(new, length);
    new->count  = 0;
    return new;
}

M_array *m_array_copy(M_array *array) {
  M_array *new = m_array_new( array->length, array->e_size, array->free_func );

  int i;
  for ( i = 0; i < array->count; i++ ) {
    m_array_push_nofree( new, m_array_elt_pos(array,i), 1 );
  }

  return new;
}

void m_array_free(M_array *array) {
    if ( array != NULL ) {
        int i;
        if ( array->free_func != NULL ) {
            //DEBUG("Using callback on %d elements\n", array->count);
            for ( i = 0; i < array->count; i++ ) {
                array->free_func(m_array_elt_pos(array,i));
            }
        }
        free(array->data);
        array->data = NULL;
        free(array);
        array = NULL;
    }
}

void *m_array_push(M_array *array, void* element, M_uint length) {

    void *r_element = m_array_push_nofree(array, element, length);
    free(element);
    return r_element;

} 

void *m_array_push_nofree(M_array *array, void* element, M_uint length) {
    if ( array->count + length > array->length ) {
        //DEBUG("Array is full (%d elements), resizing\n", array->length);
        if ( m_array_resize(array, array->count + length) ) {
            printf("Resized!\n");
        }
    }
    if (memcpy( m_array_elt_pos(array,array->count), element, 
                m_array_elt_len(array,length))) {
        //DEBUG("Appended %d elements to array\n", length);
        int u = array->count;
        array->count += length;
        return m_array_elt_pos(array, u);
    }
    return NULL;
} 

int m_parray_resize( M_parray *array, M_uint length) {
    M_uint size = M_RESERVE_MINIMUM;
    if (length >= size ) {
        size = M_RESERVE_METHOD(length + 1);
    } 
    //DEBUG("Requested %d  elements, Reserve function wants %d elements\n", length, size);
    array->data = realloc(array->data, size*sizeof(int*));
    array->length = size;
    return 0;
}

M_parray *m_parray_new(M_uint length) {
    M_parray *new = malloc(sizeof(M_parray));
    new->length = 0;
    new->data = NULL;
    m_parray_resize(new,length);
    new->count  = 0;
    return new;
}

void m_parray_free(M_parray *array) {
    if ( array != NULL ) {
        free(array->data);
        array->data = NULL;
        free(array);
        array = NULL;
    }
}

void m_parray_free_children(M_parray *array, M_callback myfree) {
    int i;
    if ( myfree != NULL ) {
        for (i=0; i<array->count; i++) {
            myfree(array->data[i]); 
        }
    }
    m_parray_free(array);
}

M_parray *m_parray_copy(M_parray *array) {
    M_parray *new = m_parray_new(array->length);

    int i;
    for ( i=0; i<array->count; i++ ) {
        m_parray_push( new, array->data[i] );
    }

    return new;
}

int m_parray_push(M_parray *array, void* element) {
    if ( array->count >= array->length) {
        //DEBUG("Array is full (%d elements), resizing\n",array->count);
        if ( m_parray_resize(array,array->length+1) != 0 ) {
            FATAL("array resize failed!\n");
            exit(1);
        }
    }
    array->data[array->count] = element;
    array->count += 1;
    //DEBUG("Appended %d elements to array\n", array->count);
    return 0;
} 

void m_parray_swap(M_parray *array, M_uint i, M_uint j) {
    void *temp;
    if ( i != j ) {
        temp = array->data[i];
        array->data[i] = array->data[j];
        array->data[j] = temp;
    }
}

void m_parray_reverse(M_parray *array) {
    int n = array->count;
    int i = 0;
    int j = n - 1;
    while ( i < j ) {
        m_parray_swap(array, i, j);
        i++;
        j--;
    }
}

void m_parray_insert(M_parray *array, void* element, M_uint i) {
    if ( i > array->count ) {
      FATAL("M_parray insertion at position %d failed. M_parray is only %d in length.",i,array->count);
      exit(1);
    } else if ( i == array->count ) {
      // inserting at the end of the parray
      // this is the same as push
      m_parray_push(array,element);
    } else {
      void *temp;
      // copy the last element, extend it to the end of the m_parray
      temp = array->data[array->count-1];
      m_parray_push(array,temp);
      
      int j;
      // shift elements up by 1
      for ( j=array->count-1; j>i; j--) {
        temp = array->data[j-1];
        array->data[j]=temp;
      }

      array->data[i] = element;
    }
}

void m_parray_delete(M_parray *array, M_uint i) {

    if ( i >= array->count ) {
      FATAL("M_parray deletion at position %d failed. M_parray is only %d in length.",i,array->count);
      exit(1);
    } else {

      void *temp;

      int j;
      for ( j=i; j<array->count-1; j++) {
        temp = array->data[j+1];
        array->data[j] = temp;
      }

      array->data[array->count] = NULL;
      array->count -= 1;

      m_parray_resize(array, array->count);

    }
}

M_array *m_array_map(M_array *array, size_t element, M_map_fn fn){
    M_array *new = m_array_new(0, element, NULL);
    int i;
    void *b = malloc(sizeof(element));
    for ( i = 0; i < array->count; i++ ) {
        void *elt = m_array_elt_pos(array, i);
        fn(b, elt);
        m_array_push_nofree(new, b, 1);
    }
    return new;
}

M_array *m_parray_map(M_parray *array, size_t element, M_map_fn fn){
    M_array *new = m_array_new(0, element, NULL);
    int i;
    void *b = malloc(sizeof(element));
    void *elt;
    for ( i = 0; i < array->count; i++ ) {
        elt = array->data[i];
        fn(b, elt);
        m_array_push_nofree(new, b, 1);
    }
    free(b);
    return new;
}

typedef struct _thpmap_params{
    M_uint id;
    M_parray *array;
    M_array *results;
    M_map_fn func;
} M_thpmap_params;

void *m_thparray_map_thread(void *p) {
    M_thpmap_params *params = p;
    M_uint start    = ceil(params->array->count*(float)params->id /(float)THREADS);
    M_uint end      = ceil((float)(params->id+1)*params->array->count/(float)THREADS);
    DEBUG("thread [%d/%d]: array[%d -> %d]\n", params->id + 1, THREADS, start, end - 1);

    int i;
    void *temp_ans = malloc(params->results->e_size);
    for (i = start; i < end; i++ ) {
        params->func(temp_ans,params->array->data[i]);
        m_array_push_nofree(params->results, temp_ans, 1);
    }
    free(temp_ans);

    pthread_exit(NULL);
}

M_array *m_thparray_map(M_parray *array, size_t element, M_map_fn fn){
    DEBUG("Mapping array of size %d\n",array->count);

    pthread_t *threads = malloc(sizeof(pthread_t)*THREADS);
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    M_array *new = m_array_new(0, element, NULL);
    M_thpmap_params *params = malloc(sizeof(M_thpmap_params)*THREADS);
    int pid;
    for ( pid = 0; pid < THREADS; pid++ ) {
        params[pid].id = pid;
        params[pid].func = fn;
        params[pid].array = array;
        params[pid].results = m_array_new(0, element, NULL);
        DEBUG("Created thread params for %d\n", pid + 1);
    }

    int t, rc;
    for ( t = 0; t < THREADS; t++ ) {
        rc = pthread_create( &threads[t], &attr, m_thparray_map_thread, (void *) &params[t] );
        if(rc) {
            printf("thread exited with %d :-(\n",rc);
        }
    }

    pthread_attr_destroy(&attr);
    void *status;

    for ( t = 0; t < THREADS; t++ ) {
        rc = pthread_join(threads[t], &status);
        if(rc) {
            printf("thread %d exited with %d :-(\n",rc,t);
        }
        DEBUG("Thread %d complete!\n",t + 1);
    }

    int i;
    for ( pid = 0; pid < THREADS; pid++ ) {
        for ( i = 0; i < params[pid].results->count; i++ ) {
            m_array_push_nofree(new, m_array_elt_pos(params[pid].results,i), 1);
        }
        m_array_free(params[pid].results);
    }
    free(threads);
    free(params);

    return new;
}

void m_parray_qufree(M_parray* p) {
    p->count = 0;
}
