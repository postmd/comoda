#include "headers/maggregate.h"

/* Used internally to pass parameters to a thread */
typedef struct {
    short id;
    M_array *array;
    M_aggregate *agg;
    M_aggregate_fn func;
} M_thaggregate_params;

/* Used internally to pass parameters to a pair aggregater thread */
typedef struct {
    short id;
    M_array *array;
    M_aggregate *agg;
    M_aggregate_pair_fn func;
} M_thaggregate_pair_params;

/* This is a thread for a threaded aggregate */
void *m_thaggregate_thread(void *p);

/* This is a thread for a threaded pair aggregate */
void *m_thaggregate_pair_thread(void *p);

M_n_aggregate *m_n_aggregate_new( M_uint n, prec *min, prec *max, prec *width, M_uint dims) {

  if ( n > 0 ) {
    
    M_n_aggregate *new = malloc(sizeof(M_n_aggregate));
    new->n = n;
    new->dims = dims;

    new->min = malloc(sizeof(prec) * n);
    new->max = malloc(sizeof(prec) * n);
    new->width = malloc(sizeof(prec) * n);
    new->length = malloc(sizeof(M_uint) * n);
    new->total_length = 1;

    new->keys = malloc(sizeof(prec *) * n);
    new->keys_count = malloc(sizeof(M_uint *) * n);

    int i;
    for (i=0; i<n; i++ ) {

      // allocate the key space

      new->min[i] = min[i];
      new->width[i] = width[i];

      M_uint keyspace = ceil( ( max[i] - min[i] ) / width[i] );
      if ( keyspace < 1 ) {
        keyspace = 1;
      }

      new->length[i] = keyspace;
      new->max[i] = keyspace * new->width[i] + new->min[i];

      new->total_length *= keyspace;

      new->keys[i] = malloc(sizeof(prec) * keyspace);
      new->keys_count[i] = malloc(sizeof(M_uint) * keyspace);
      M_uint j;
      for (j=0; j<keyspace; j++) {
        new->keys[i][j] = new->min[i] + new->width[i]*( j + 0.5 );
        new->keys_count[i][j] = 0;
      }
    }

    new->count = malloc(sizeof(M_uint) * new->total_length);
    clear_uiarray(new->count, new->total_length);

    new->values = malloc(sizeof(prec) * new->total_length * new->dims);
    clear_farray(new->values, new->total_length * new->dims);

    new->key = malloc(sizeof(prec) * n);
    clear_farray(new->key, n);

    new->data = malloc(sizeof(prec) * dims);
    clear_farray(new->data, dims);

    new->print_col = malloc(sizeof(M_uint) * dims);
    m_n_aggregate_set_print_all(new);

    new->print_2d_values = 0;
    new->print_2d_count = 0;
    new->print_0_count = 0;

    new->auto_extend = 1;

    return new;

  } else {
    FATAL("cannot create a 0-dimensional aggregate object.\n");
    exit(1);
  }
  return NULL;
}

M_uint m_n_aggregate_get_count_id(M_n_aggregate *agg) {
  int i;
  M_uint id = 0;

  /* define i_n as the zero-centric id list
   * define n_n as the length of the list {i_n}
   *
   * id = i0 * n1 * n2 * n3 * .... +
   *      i1      * n2 * n3 * .... +
   *      i2           * n3 * .... +
   *      ...
   *      i_n-1             * n_n  +
   *      i_n
   *
   * id = i_n + n_n * (
   *            i_n-1 + n_n-1 * ( 
   *                    ...
   *                    i_2 + n_2 * ( i_1 + n_1 * i_0 ) ) )
   */

  for ( i = 0; i< agg->n; i++ ) {
    id *= agg->length[i];
    id += bin_id( agg->key[i], agg->min[i], agg->width[i] );
  }
  return id;
}

M_uint m_n_aggregate_get_values_id(M_n_aggregate *agg, M_uint dim) {
  M_uint id = dim * agg->total_length;
  id += m_n_aggregate_get_count_id(agg);
  return id;
}

M_uint *m_n_aggregate_get_id_list(M_n_aggregate *agg, M_uint id) {
  M_uint *id_list = malloc(sizeof(M_uint) * agg->n+1);
  M_uint product = id;

  // the last element is the dim
  id_list[agg->n] = id/agg->total_length;
  // the remainder is the id according to the key dimensions
  product = id % agg->total_length;

  int i;
  for ( i = agg->n-1; i >= 0; i-- ) {
    id_list[i] = product % agg->length[i];
    product /= agg->length[i]; // the remainder is discarded by integer operation
  }
  
  return id_list;
}


void _m_n_aggregate(M_n_aggregate *agg) {
  int i;
  int valid = 1;
  // loop over n keys to check for key validity
  for ( i = 0; i < agg->n; i++ ) {
    if ( agg->key[i] < agg->min[i] && agg->key[i] > agg->max[i] ) {
      // break if out of key is out of range
      DEBUG("key #%d : %f out of range of the aggregate, (%f,%f)\n",i,agg->key[i],agg->min[i],agg->max[i]);
      valid = 0;
    }
  }

  if (valid) {
    // if valid, increment count for each key
    for ( i = 0; i < agg->n; i++ ) {
      // increment the count for each key
      M_uint key_id = bin_id( agg->key[i], agg->min[i], agg->width[i] );
      agg->keys_count[i][key_id]++;
    }

    // increment the count for the key combination
    M_uint count_id = m_n_aggregate_get_count_id(agg);
    agg->count[count_id]++;

    // increment the data values
    for ( i = 0; i < agg->dims; i++ ) {
      M_uint values_id = m_n_aggregate_get_values_id(agg,i);
      agg->values[values_id] += agg->data[i];
    }
  }

  // clear the temporary data
  m_n_aggregate_clear_data(agg);
}

void m_n_aggregate(M_n_aggregate **agg_p) {
  M_n_aggregate *agg = *agg_p;
  int i;
  int extend = 0;
  // loop over n keys
  if ( agg->auto_extend ) {
    for ( i = 0; i < agg->n; i++ ) {
      if ( agg->key[i] < agg->min[i] || agg->key[i] > agg->max[i] ) {
        extend = 1;
        break;
      }
    }
  }
  
  if (extend) {
    agg=m_n_aggregate_extend(agg);
    *agg_p = agg;
  }
  _m_n_aggregate(agg);
}

void m_n_aggregate_overwrite_count(M_n_aggregate *agg, M_uint count) {
  int i;
  // loop over n keys
  for ( i = 0; i < agg->n; i++ ) {
    if ( agg->key[i] > agg->min[i] && agg->key[i] < agg->max[i] ) {
      // nothing to do here
    } else {
      // break if out of key is out of range
      FATAL("key #%d : %f out of range of the aggregate, (%f,%f)\n",i,agg->key[i],agg->min[i],agg->max[i]);
      exit(1);
    }
  }

  // increment the count for the key combination
  M_uint count_id = m_n_aggregate_get_count_id(agg);
  agg->count[count_id] = count;
}

void m_n_aggregate_overwrite_keys_count(M_n_aggregate *agg, M_uint n, M_uint count) {
  if ( n < agg->n ) {
    if ( agg->key[n] > agg->min[n] && agg->key[n] < agg->max[n] ) {
      M_uint key_id = bin_id( agg->key[n], agg->min[n], agg->width[n] );
      agg->keys_count[n][key_id] = count;
    } else {
      // break if out of key is out of range
      FATAL("key #%d : %f out of range of the aggregate, (%f,%f)\n",n,agg->key[n],agg->min[n],agg->max[n]);
      exit(1);
    }
  } else {
    // break if key_id is out of range
    FATAL("there is no key with id=%d, there are %d keys\n",n,agg->n);
    exit(1);
  } 
}

void m_n_aggregate_clear_data(M_n_aggregate *agg) {
  clear_farray(agg->key,agg->n);
  clear_farray(agg->data,agg->dims);
}

void m_n_aggregate_reset(M_n_aggregate *agg) {
  m_n_aggregate_clear_data(agg);
  clear_farray(agg->values,agg->total_length*agg->dims);
  clear_uiarray(agg->count,agg->total_length);
  int i;
  for ( i=0; i<agg->n; i++ ) {
    clear_uiarray(agg->keys_count[i],agg->length[i]);
  }
}

void m_n_aggregate_free(M_n_aggregate *agg) {
    if ( agg != NULL ) {
        free(agg->min);
        free(agg->max);
        free(agg->width);
        free(agg->length);
        free(agg->count);
        free(agg->values);
        free(agg->key);
        free(agg->data);
        free(agg->print_col);
        int i;
        for ( i=0; i<agg->n; i++ ) {
          free(agg->keys[i]);
          free(agg->keys_count[i]);
        }
        free(agg->keys);
        free(agg->keys_count);

        free(agg);
        agg = NULL;
    }
}

M_n_aggregate *m_n_aggregate_extend(M_n_aggregate *agg) {

  int expand = 0;
  int i;
  prec *min = malloc(sizeof(prec)*agg->n);
  prec *max = malloc(sizeof(prec)*agg->n);
  prec *width = malloc(sizeof(prec)*agg->n);

  for ( i=0; i<agg->n; i++ ) {

    width[i] = agg->width[i];

    if ( agg->key[i] < agg->min[i] ) {
      min[i] = agg->min[i] - ceil( ( agg->min[i] - agg->key[i] ) / width[i] ) * width[i];
      expand = 1;
    } else {
      min[i] = agg->min[i];
    }

    if ( agg->key[i] > agg->max[i] ) {
      max[i] = agg->key[i];
      expand = 1;
    } else {
      max[i] = agg->max[i];
    }

  }

  if ( expand ) {
    M_n_aggregate *new = m_n_aggregate_new( agg->n, min, max, width, agg->dims );
    free(min);
    free(max);
    free(width);

    // copy static values 
    new->print_count = agg->print_count;
    new->print_empty = agg->print_empty;
    for ( i=0; i<agg->dims; i++ ) {
      new->print_col[i] = agg->print_col[i];
    }
    new->print_2d_values = agg->print_2d_values;
    new->print_2d_count = agg->print_2d_count;
    new->print_0_count = agg->print_0_count;


    // save a copy of the key & data value of agg
    prec *save_key = farray_copy(agg->key, agg->n);
    prec *save_data = farray_copy(agg->data, agg->dims);

    int j;
    M_uint old_count_id,old_value_id;
    M_uint *key_id = malloc( sizeof(M_uint) * agg->n);
    clear_uiarray(key_id, agg->n);
    // loop over all key combinations
    do {
      // copy the value of the key combination
      for ( j=0; j<agg->n; j++ ) {
        agg->key[j] = agg->keys[j][key_id[j]];
        new->key[j] = agg->keys[j][key_id[j]];
      }

      // copy the values of the key combination
      for ( j=0; j<agg->dims; j++ ) {
        old_value_id = m_n_aggregate_get_values_id(agg,j);
        new->data[j] = agg->values[old_value_id];
      }
      _m_n_aggregate(new); // safely add this to new
      
      // copy the value of the key combination again
      for ( j=0; j<agg->n; j++ ) {
        new->key[j] = agg->keys[j][key_id[j]];
      }

      // copy the count to new
      old_count_id = m_n_aggregate_get_count_id(agg);
      m_n_aggregate_overwrite_count(new, agg->count[old_count_id]); // safely overwrite

    // increment to next key id combination
    } while( increment_id_list( key_id, agg->length, agg->n ) );
    free(key_id);

    // copy the keys_count to new
    clear_farray(new->key,new->n);
    for (j=0; j<agg->n; j++) {
      for (i=0; i<agg->length[j]; i++) {
        new->key[j] = agg->keys[j][i];
        m_n_aggregate_overwrite_keys_count( new, j, agg->keys_count[j][i] ); // safely overwrite
        new->key[j] = 0;
      }
    }

    // copy the saved key & data to new
    free(new->key);
    free(new->data);
    new->key = save_key;
    new->data = save_data;

    // free old aggregator
    m_n_aggregate_free( agg );

    return new;

  } else {

    free(min);
    free(max);
    free(width);
    return agg;
  }
}

M_aggregate *m_aggregate_new( prec min, prec max, prec width, M_uint dims) {

    if ( width > 0  && dims > 0 ) {

      M_aggregate *new = malloc(sizeof(M_aggregate));

      M_uint keyspace = ceil(( max - min ) / width);
      if (keyspace<1) {
        keyspace = 1;
      }

      new->length = keyspace;
      new->dims = dims;
      new->count  = (M_uint*) malloc(sizeof(M_uint)*keyspace);
      new->values = (prec**) malloc(sizeof(prec*)*keyspace);
      new->min    = min;
      new->max    = width*keyspace + min;
      new->width  = width;
      new->row    = (prec * ) malloc(sizeof(prec) * dims);
      new->print_col = (M_uint*) malloc(sizeof(M_uint) * dims);
      new->auto_extend = 1;

      int i,j;
      for ( i = 0; i < keyspace; i++ ) {
          new->values[i] = (prec*) malloc(sizeof(prec)*dims);
          for ( j = 0; j < dims; j++ ){
            new->values[i][j] = 0.0;
          }
          new->count[i] = 0;
          new->values[i][0] = (i * width) + (width * 0.5) + min;
      }

      m_aggregate_clear_row(new);
      m_aggregate_set_print_all(new);

      return new;

  } else {

      FATAL("Require a positive width and a positive integer dims.\n");
      FATAL("width supplied: %f\t dims supplied: %d\n",width,dims);
      exit(1);

  }
}

void m_aggregate_reset(M_aggregate *agg) {
    int i, j;
    for ( i = 0; i < agg->length; i++ ) {
        for ( j = 1; j < agg->dims; j++ ) {
            agg->values[i][j] = 0.0;
        }
        agg->count[i] = 0;
    }
    m_aggregate_clear_row(agg);
}

void m_aggregate_free(M_aggregate *agg) {
    if ( agg != NULL ) {
        free(agg->count);
        int i;
        for ( i = 0; i < agg->length; i++ ) {
            free(agg->values[i]);
        }
        free(agg->values);
        free(agg->row);
        free(agg->print_col);
        free(agg);
    }
}

void _m_aggregate(M_aggregate *aggregate) {
    int i;
    prec key = aggregate->row[0];
    int index;

    if ( key < aggregate->max && key > aggregate->min ) {
        index = (int) ((key - aggregate->min) / aggregate->width);
        for ( i = 1; i < aggregate->dims; i++ ) {
            aggregate->values[index][i] += aggregate->row[i];
        }
        aggregate->count[index] += 1;
    } else {
        DEBUG("Key %f out of range of the aggregate, (%f,%f), data discarded\n",key,aggregate->min,aggregate->max);
    }
    m_aggregate_clear_row(aggregate);

}

void m_aggregate(M_aggregate **aggregate_p) {
    M_aggregate *aggregate = *aggregate_p;
    prec key = aggregate->row[0];

    if ( aggregate->auto_extend && (key > aggregate->max || key < aggregate->min) ) {
        aggregate = m_aggregate_extend(aggregate, fmin(key, aggregate->min), fmax(key, aggregate->max));
        *aggregate_p = aggregate;
    }
    _m_aggregate(aggregate);
}

void m_aggregate_overwrite_count(M_aggregate *aggregate, prec key, M_uint count) {

    if ( key < aggregate->max && key > aggregate->min ) {
        M_uint index = ((key - aggregate->min) / aggregate->width);
        aggregate->count[index] = count;
    } else {
        FATAL("Key %f out of range of the aggregate, (%f,%f)\n",key,aggregate->min,aggregate->max);
        exit(1);
    }

}

M_aggregate *m_aggregate_extend(M_aggregate *agg, prec min, prec max) {

  if ( min<agg->min || max>agg->max ) {

    // set the minimum value to be an integer multiple of agg->width away from the original agg->min
    prec new_min = fmin(min, agg->min);
    if ( min<agg->min ) {
        new_min = agg->min - ceil( (agg->min - min)/agg->width ) * agg->width;
    }

    prec new_max = fmax(max, agg->max);
    
    M_aggregate *new = m_aggregate_new( new_min, new_max, agg->width, agg->dims );

    int i,j;

    // copy print quantities
    for ( i=0; i<agg->dims; i++ ) {
      new->print_col[i] = agg->print_col[i];
    }
    new->print_empty = agg->print_empty;
    new->print_count = agg->print_count;

    // copy values safely using the m_aggregate function
    for ( i=0; i<agg->length; i++ ) {
      for ( j=0; j<agg->dims; j++ ) {
        new->row[j] = agg->values[i][j];
      }
      _m_aggregate(new);
    }

    // copy the counts across safely using the m_aggregate_overwrite_count function
    for ( i=0; i<agg->length; i++ ) {
      m_aggregate_overwrite_count( new, agg->values[i][0], agg->count[i] );
    }

    // copy the row
    for ( i=0; i< agg->dims; i++ ) {
      new->row[i] = agg->row[i];
    }

    m_aggregate_free( agg );

    return new;

  } else {
    
    return agg;

  }

}

void m_aggregate_clear_row(M_aggregate *aggregate) {
    int i;
    for ( i = 0; i < aggregate->dims; i++ ) {
      aggregate->row[i] = 0;
    }
}

void m_aggregate_set_print_all(M_aggregate *aggregate) {
    int i;
    for ( i = 0; i < aggregate->dims; i++ ) {
      aggregate->print_col[i] = 1;
    }
    aggregate->print_count = 1;
    aggregate->print_empty = 0;
}

void m_n_aggregate_set_print_all(M_n_aggregate *aggregate) {
    int i;
    for ( i = 0; i < aggregate->dims; i++ ) {
      aggregate->print_col[i] = 1;
    }
    aggregate->print_count = 1;
    aggregate->print_empty = 0;
}

void m_aggregate_set_print_none(M_aggregate *aggregate) {
    int i;
    for ( i = 0; i < aggregate->dims; i++ ) {
      aggregate->print_col[i] = 0;
    }
    aggregate->print_count = 0;
    aggregate->print_empty = 0;
}

void m_n_aggregate_set_print_none(M_n_aggregate *aggregate) {
    int i;
    for ( i = 0; i < aggregate->dims; i++ ) {
      aggregate->print_col[i] = 0;
    }
    aggregate->print_count = 0;
    aggregate->print_empty = 0;
    aggregate->print_2d_values = 0;
    aggregate->print_2d_count = 0;
    aggregate->print_0_count = 0;
}

void m_aggregate_add(M_aggregate *aggregate1, M_aggregate *aggregate2) {
    int row, col;
    
    // first check if they are compatible 
    if (m_aggregate_chkdims(aggregate1,aggregate2)) {

        // add everything except the key
        for ( row = 0; row < aggregate1->length; row++) {
            for ( col = 1; col < aggregate1->dims; col++ ) { 
                aggregate1->values[row][col]+=aggregate2->values[row][col];
            }   
            aggregate1->count[row]+=aggregate2->count[row];
        }   
    } else {
        DEBUG("Not the same!\n");
    }
}

int m_aggregate_chkdims(M_aggregate *aggregate1, M_aggregate *aggregate2) {

    if (aggregate1->length==aggregate2->length) {
      if (aggregate1->width==aggregate2->width) {
        if (aggregate1->min==aggregate2->min) {
          if (aggregate1->dims==aggregate2->dims) {
            // the two aggregates are setup the same!
            return 1;
          }
        }
      }
    }
    return 0;
}

void m_aggregate_fprint(FILE *f, M_aggregate *aggregate) {
    int row, col;
    for ( row = 0; row < aggregate->length; row++) {
        if ( aggregate->count[row] != 0 || aggregate->print_empty ) {
            for ( col = 0; col < aggregate->dims; col++ ) {
                if ( aggregate->print_col[col] ) {
                  fprintf(f,"%15.10f\t",aggregate->values[row][col]);
                }
            }
            if ( aggregate->print_count ) {
              fprintf(f,"%d\t",aggregate->count[row]);
            }
            fprintf(f,"\n");
        }
    }
}

void m_aggregate_print(M_aggregate *aggregate) {
    m_aggregate_fprint(stdout,aggregate);
}

void m_n_aggregate_fprint(FILE *f, M_n_aggregate *agg) {
    // if we are printing a 2d matrix
    if  ( ( agg->n == 2 ) && ( ( agg->print_2d_values && (sum_uiarray(agg->print_col,agg->dims)==1) ) || agg->print_2d_count ) ) {
        // remember which column to print
        M_uint col;
        if ( agg->print_2d_values ) {
          int i;
          for ( i = 0; i < agg->dims; i++ ) {
            if ( agg->print_col[i] ) {
              col = i;
              break;
            }
          }
        }

        M_uint id, k0_id, k1_id;

        // print the column headings (key 1)
        fprintf(f, "keys\t");
        for ( k1_id = 0; k1_id < agg->length[1]; k1_id++ ) {
          fprintf(f, "%15.10f\t",agg->keys[1][k1_id]);
        }
        if ( agg->print_0_count ) {
          fprintf(f, "count\t");
        }
        fprintf(f,"\n");

        id = 0;
        M_uint temp_id;
        for ( k0_id = 0; k0_id < agg->length[0]; k0_id++ ) { 
          // print the row key (key 0)
          fprintf(f,"%15.10f\t",agg->keys[0][k0_id]); 

          // print all the columns (key 1)
          for ( k1_id = 0; k1_id < agg->length[1]; k1_id++ ) {
            if ( agg->print_2d_values ) {
              temp_id = col * agg->total_length + id;
              fprintf(f, "%15.10f\t",agg->values[temp_id]);
            } else {
              // must be print_2d_count
              fprintf(f,"%15d\t",agg->count[id]);
            }
            id++;
          }
          if ( agg->print_0_count ) {
            fprintf(f,"%15d\t",agg->keys_count[0][k0_id]);
          }
          fprintf(f,"\n");
        }
        
    } else {
        M_uint id, key, col, temp_id;
        M_uint *id_list;
        for ( id = 0; id < agg->total_length; id++) {
            if ( agg->count[id] != 0 || agg->print_empty ) {
                id_list = m_n_aggregate_get_id_list(agg,id);
                for ( key = 0; key < agg->n; key++ ) {
                    fprintf(f,"%15.10f\t",agg->keys[key][id_list[key]]);
                }
                for ( col = 0; col < agg->dims; col++ ) {
                    if ( agg->print_col[col] ) {
                      temp_id = id + col * agg->total_length;
                      fprintf(f,"%15.10f\t",agg->values[temp_id]);
                    }
                }
                if ( agg->print_count ) {
                  fprintf(f,"%d\t",agg->count[id]);
                }
                fprintf(f,"\n");
                free(id_list);
            }
        }
    }
}

void m_n_aggregate_print(M_n_aggregate *aggregate) {
    m_n_aggregate_fprint(stdout,aggregate);
}

void m_aggregate_print_all(M_aggregate *aggregate) {
    int row, col;
    for ( row = 0; row < aggregate->length; row++) {
        for ( col = 0; col < aggregate->dims; col++ ) {
            printf("%15.10f\t",aggregate->values[row][col]);
        }
        printf("%d\t",aggregate->count[row]);
        printf("\n");
    }
}

void m_aggregate_col_scale(M_aggregate *aggregate, int col_index, prec k) {
    int row;
    for ( row = 0; row < aggregate->length; row++) {
        aggregate->values[row][col_index] *= k;
    }
}

void m_aggregate_col_scale_dV(M_aggregate *aggregate, int col_index) {
    int row;
    prec r, dr, dV;
    for ( row = 0; row < aggregate->length; row++) {
        r = aggregate->values[row][0];
        dr = aggregate->width;
        dV = 4.0/3.0*pi*(pow(r+dr/2.0,3)-pow(r-dr/2.0,3));
        aggregate->values[row][col_index] /= dV;
    }
}

M_uint m_n_aggregate_lookup(M_n_aggregate *agg) {

    M_uint *id_list = malloc(sizeof(M_uint)*agg->n);
    int i;
    for ( i=0; i<agg->n; i++ ) {
        if ( agg->key[i] < agg->min[i] || agg->key[i] > agg->max[i] ) {
            FATAL("Supplied key %lf is out of bounds (%lf, %lf)\n",agg->key[i],
                                                                   agg->min[i],
                                                                   agg->max[i]);
            exit(1);
        } else {
            id_list[i] = bin_id( agg->key[i], agg->min[i], agg->width[i] );
        }
    }

    M_uint id = get_id(id_list, agg->length, agg->n);
    free(id_list);

    int j;
    for ( j=0; j<agg->dims; j++ ) {
        agg->data[j] = agg->values[id+(agg->total_length*j)];
    }
    return agg->count[id];
}

void m_n_aggregate_col_scale(M_n_aggregate *agg, int col_index, prec k) {
    M_uint id;
    for ( id = col_index*agg->total_length; id < (col_index+1)*agg->total_length; id++ ) {
        agg->values[id] *= k;
    }
}

void m_n_aggregate_key_scale(M_n_aggregate *agg) {

    bool *wildcard = malloc(sizeof(bool)*agg->n);
    M_uint *dest   = malloc(sizeof(M_uint)*agg->n);
    bool *cols_to_scale = malloc(sizeof(bool)*agg->dims);

    int i;
    for ( i=0; i<agg->n; i++ ) {
        if ( agg->key[i] < agg->min[i] || agg->key[i] > agg->max[i] ) {
            FATAL("Supplied key %lf is out of bounds (%lf, %lf)\n",agg->key[i],
                                                                   agg->min[i],
                                                                   agg->max[i]);
            exit(1);
        } else if ( isnan(agg->key[i]) ) {
            wildcard[i] = true;
            dest[i] = 0;
        } else {
            wildcard[i] = false;
            dest[i] = bin_id( agg->key[i], agg->min[i], agg->width[i] );
        }
    }

    int j;
    for ( j=0; j<agg->dims; j++ ) {
        if ( isnan(agg->data[j]) ) {
            cols_to_scale[j] = false;
        } else {
            cols_to_scale[j] = true;
        }
    }

    M_uint id;
    M_uint *id_list = malloc(sizeof(M_uint)*agg->n);
    bool match;
    clear_uiarray(id_list, agg->n);
    do {
        match = true;
        for ( i=0; i<agg->n; i++ ) {
            if ( dest[i] != id_list[i] && (! wildcard[i]) ) {
                match = false;
            }
        }

        if (match) {
            id = get_id(id_list, agg->length, agg->n);
            for ( j=0; j<agg->dims; j++ ) {
                agg->values[id+(agg->total_length*j)] *= agg->data[j];
            }
        }

    } while ( increment_id_list(id_list, agg->length, agg->n) );

    free(wildcard);
    free(dest);
    free(cols_to_scale);
    free(id_list);
    m_n_aggregate_clear_data(agg);
}
    
void m_n_aggregate_col_scale_dV(M_n_aggregate *agg, int col_index, int key_id ) {
    M_uint id;
    M_uint *id_list;
    prec r, dr, dV;
    for( id = col_index*agg->total_length; id < (col_index+1)*agg->total_length; id++ ) {
      id_list = m_n_aggregate_get_id_list(agg,id);

      r = agg->keys[key_id][id_list[key_id]];
      dr = agg->width[key_id];
      dV = 4.0/3.0*pi*(pow(r+dr/2.0,3)-pow(r-dr/2.0,3));
      agg->values[id] /= dV;
    
      free(id_list);
    }
}


int m_thaggregate(M_aggregate *agg, M_array* array, M_aggregate_fn fn) {
    pthread_t *threads;
    threads = (pthread_t*)malloc(sizeof(pthread_t)*THREADS);
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    M_thaggregate_params *params = malloc(sizeof(M_thaggregate_params) * THREADS);

    int rc;
    int t;
    void *status;

    // Initialize all aggregates
    for ( t = 0; t < THREADS; t++ ) {
        params[t].id = t;
        params[t].array = array;
        params[t].func = fn;
        params[t].agg = m_aggregate_new(agg->min, agg->max, agg->width, agg->dims);
    }

    // run threads
    DEBUG("Spawning %d threads\n",THREADS);

    for ( t = 0; t < THREADS; t++ ) {
        rc = pthread_create( &threads[t], &attr, m_thaggregate_thread, (void *) &params[t] );
        if(rc) {
            printf("thread exited with %d :-(\n",rc);
        }
    }

    pthread_attr_destroy(&attr);

    for ( t = 0; t < THREADS; t++ ) {
        rc = pthread_join(threads[t], &status);
        if(rc) {
            printf("thread %d exited with %d :-(\n",rc,t);
        }
        DEBUG("Thread %d complete!\n",t);
    }

        
    int i, d;
    for ( t = 0; t < THREADS; t++ ) {
        for ( i = 0; i < agg->length; i++ ) {
            for ( d = 1; d < agg->dims; d++ ) {
                agg->values[i][d] += params[t].agg->values[i][d];
            }
            agg->count[i] += params[t].agg->count[i];
        }
    }
    
    // free memory
    for ( t = 0; t < THREADS; t++ ) {
        m_aggregate_free(params[t].agg);
    }
    free(params);
    free(threads);

    return 0;
}

void *m_thaggregate_thread(void *p) {
    // Load up the params as a local variable
    M_thaggregate_params *params;
    params = (M_thaggregate_params *) p;

    // create a pointer to the aggregate
    // for convenience
    M_aggregate *agg = params->agg;

    int start, end, id, length;
    M_array *array;

    id              = params->id;
    array           = params->array;
    length          = params->array->count;

    start       = (int) ceil(length / THREADS) * id;
    end         = (int) start + ceil(length / THREADS);
    DEBUG("Thread[%d/%d]: array[%d -> %d]\n", id + 1, THREADS, start, end - 1);

    void *element;
    int i;
    for ( i = start + 1; i <= end; i++ ) {
        element = m_array_elt_pos(array, i ) ;
        params->func(agg,element);
    }
    //free(r);
    pthread_exit(NULL);
}

int m_thaggregate_pair(M_aggregate *agg, M_array* array, M_aggregate_pair_fn fn) {
    pthread_t *threads;
    threads = (pthread_t*)malloc(sizeof(pthread_t)*THREADS);
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    M_thaggregate_pair_params *params = malloc(sizeof(M_thaggregate_pair_params) * THREADS);

    int rc;
    int t;
    void *status;

    // Initialize all aggregates
    for ( t = 0; t < THREADS; t++ ) {
        params[t].id = t;
        params[t].array = array;
        params[t].func = fn;
        params[t].agg = m_aggregate_new(agg->min, agg->max, agg->width, agg->dims);
    }

    // run threads
    DEBUG("%d elements gives %d pairs\n",array->count, (int)(0.5 * (array->count - 1)*array->count));

    for ( t = 0; t < THREADS; t++ ) {
        rc = pthread_create( &threads[t], &attr, m_thaggregate_pair_thread, (void *) &params[t] );
        if(rc) {
            printf("thread exited with %d :-(\n",rc);
        }
    }

    pthread_attr_destroy(&attr);

    for ( t = 0; t < THREADS; t++ ) {
        rc = pthread_join(threads[t], &status);
        if(rc) {
            printf("thread %d exited with %d :-(\n",rc,t);
        }
        DEBUG("Thread %d complete!\n",t);
    }

        
    int i, d;
    for ( t = 0; t < THREADS; t++ ) {
        for ( i = 0; i < agg->length; i++ ) {
            for ( d = 1; d < agg->dims; d++ ) {
                agg->values[i][d] += params[t].agg->values[i][d];
            }
            agg->count[i] += params[t].agg->count[i];
        }
    }
    
    // free memory
    for ( t = 0; t < THREADS; t++ ) {
        m_aggregate_free(params[t].agg);
    }
    free(params);
    free(threads);

    return 0;
}

void *m_thaggregate_pair_thread(void *p) {
    // Load up the params as a local variable
    M_thaggregate_pair_params *params;
    params = (M_thaggregate_pair_params *) p;

    // create a pointer to the aggregate
    // for convenience
    M_aggregate *agg = params->agg;

    int start, end, id, length;
    M_array *array;

    id              = params->id;
    array           = params->array;
    length          = params->array->count;

    start           = (int)(round(sqrt((float)id/(float)THREADS)*length));
    end            = (int)(round(sqrt((float)(id+1)/(float)THREADS)*length));
    DEBUG("Thread[%d/%d]: array[%d -> %d]\n", id + 1, THREADS, start, end - 1);

    void *a, *b;
    int i, j;

    for ( i = start; i < end; i++ ) {
        a = m_array_elt_pos(array, i ) ;
        for ( j = 0; j < i; j++ ) {
            b = m_array_elt_pos(array, j ) ;
            params->func(agg, a, b);
        }
    }
    pthread_exit(NULL);
}
