#include "headers/mstring.h"

int chk_delim(char *in, const char *delims, int n_delims) {
    int i;
    char *p = (char *) delims;
    for (i=0; i<n_delims; i++ ) { 
        if ( *in == *p ) { 
            return 1;
        }   
        p++;
    }   
    return 0;
}

char *next_field(char *str, const char *delims, int n_delims) {

    // first check if it is null
    if ( str == NULL ) {
        return NULL;
    }

    char *p = str;

    // first, skip past any initial delimiters
    while ( chk_delim(p, delims, n_delims) ) { 
        p++;
    }   

    int prev_delim = 0;

    while ( *p != '\0' ) { 
        if ( chk_delim(p, delims, n_delims) ) { 
            prev_delim = 1;
        } else if ( prev_delim == 1 ) { 
            return p;
        }   
        p++;
    }   
    return NULL;
}

char *next_n_fields(char *str, const char *delims, int n_delims, int n) {
    int i;
    char *out = str;
    for (i=0; i<n; i++) {
        out = next_field(out, delims, n_delims);
        if ( out == NULL ) {
            return NULL;
        }
    }
    return out;
}

int count_n_fields(char *str, const char *delims, int n_delims) {
    // first check if it is null
    if ( str == NULL ) {
        return -1; // if so, return error
    }

    char *p = str;

    // skip past any initial delimiters
    while ( chk_delim(p, delims, n_delims) ) { 
        p++;
    }   
    if ( *p == '\0' ) {
        return 0; // 0 fields if we are already at the end
    }
    
    int i = 1; // we already have one field
    while ( ( p = next_field(p, delims, n_delims ) ) != NULL ) {
        i++;
    }

    return i;
}

char *trim_delims(char *str, const char *delims, int n_delims) {

    // first check if it is null
    if ( str == NULL ) {
        return NULL;
    }

    char *p = str;

    // first, skip past any initial delimiters
    while ( chk_delim(p, delims, n_delims) ) { 
        p++;
    }   
    
    int l = 0;
    int n = 16;
    char *out = malloc(sizeof(char)*n);

    while ( *p != '\0' ) { 
        if ( ! chk_delim(p, delims, n_delims) ) { 
            l++;
            if ( l>=n ) {
                n = 2*n;
                out = realloc(out, sizeof(char)*n);
            }
            if ( out != NULL ) {
                out[l-1] = *p;
            } else {
                FATAL("Error while reallocating string.\n");
                FATAL("Processing: %s\n",str);
                FATAL("Remaining:  %s\n",p);
                exit(1);
            }
        }   
        p++;
    }
    out[l] = '\0';
    return out;
}

char *trim_consec_delims(char *str, const char *delims, int n_delims, 
                         const char *new_delim) {

    // first check if it is null
    if ( str == NULL ) {
        return NULL;
    }

    char *p = str;

    // first, skip past any initial delimiters
    while ( chk_delim(p, delims, n_delims) ) { 
        p++;
    }   
    
    int l = 0;
    int n = 16;
    char *out = malloc(sizeof(char)*n);

    int is_delim = 0;
    int prev_delim = 0;
    int append = 1;

    while ( *p != '\0' ) { 
        is_delim = chk_delim(p, delims, n_delims);
        if ( prev_delim && is_delim ) {
            append = 0;
        } else if ( (!prev_delim) && is_delim ) {
            append = 1;
        } else {
            append = 1;
        }
        prev_delim = is_delim;
        if ( append ) {
            l++;
            if ( l>=n ) {
                n = 2*n;
                out = realloc(out, sizeof(char)*n);
            }
            if ( out != NULL ) {
                if ( is_delim ) {
                    out[l-1] = *new_delim;
                } else {
                    out[l-1] = *p;
                }
            } else {
                FATAL("Error while reallocating string.\n");
                FATAL("Processing: %s\n",str);
                FATAL("Remaining:  %s\n",p);
                exit(1);
            }
        } 
        p++;
    }
    out[l] = '\0';
    return out;
}

char *strcpy_e(char *dest, const char *src) {

    free(dest);
    


    return dest;

}
