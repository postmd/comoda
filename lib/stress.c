#include "headers/stress.h"
#include "headers/db.h"
#include "headers/forcefield.h"

M_stress* m_stress_new() {
    M_stress *new = malloc(sizeof(M_stress));
    new->electro = NULL;
    new->lj      = NULL;
    new->kinetic = NULL;
    new->total   = NULL;
    return new;
}
void m_stress_free(M_stress *stress) {
    if ( stress != NULL ) {
        free(stress->electro);
        free(stress->lj);
        free(stress->kinetic);
        free(stress->total);
        free(stress);
        stress = NULL;
    }
}
void m_stress_reset(M_stress *stress) {
    m_matrix_zero(stress->electro);
    m_matrix_zero(stress->lj);
    m_matrix_zero(stress->kinetic);
    m_matrix_zero(stress->total);
}
M_stress* m_stress_copy(M_stress *stress) {
    M_stress *new = m_stress_new();

    new->electro = (M_matrix *) malloc(sizeof(M_matrix));
    new->lj      = (M_matrix *) malloc(sizeof(M_matrix));
    new->kinetic = (M_matrix *) malloc(sizeof(M_matrix));
    new->total   = (M_matrix *) malloc(sizeof(M_matrix));

    *new->electro = m_matrix_copy(stress->electro);
    *new->lj      = m_matrix_copy(stress->lj);
    *new->kinetic = m_matrix_copy(stress->kinetic);
    *new->total   = m_matrix_copy(stress->total);

    return new;
}

// this routine follows the method used in toyMD
M_matrix* m_stress_calc_electro_toy(M_system *sys) {
    prec eta = sys->ewald->eta;

    //              1         /  /           1           \.
    // sig_k_ab = ----   sum |  | 1_ab - 2(-----) k_a k_b |
    //             2V     k   \  \         |k|^2  ~   ~  /
    //                                                          \.
    //                              * greenfn(k) * |rhohat(k)|^2 |
    //                                                          /
    M_matrix sig_k = M_ZERO_MAT; // reciprocal space contribution
    int k_id;
    for (k_id=0; k_id<sys->ewald->n_k; k_id++) {
        M_vector k = m_ewald_get_k(k_id, sys->ewald);

        prec k2 = m_vector_dot(k,k);

        if ( k2 > 0 ) {
            prec pre_k_factor = 2.0 * (1.0/(4.0*pow(eta,2.0)) + 1.0/k2);
            pre_k_factor = 2.0/k2;
            prec tail_factor = sys->ewald->greenfn[k_id]
                               * creal( sys->ewald->rhohat[k_id]
                                        * conj(sys->ewald->rhohat[k_id]) );
            M_matrix t = m_matrix_subtract(M_IDENT_MAT,
                                           m_matrix_scale(m_vector_outer(k, k),
                                                          pre_k_factor) );
            sig_k = m_matrix_add( sig_k, m_matrix_scale( t, tail_factor ) ); 
        }
    }
    sig_k = m_matrix_scale( sig_k, (prec) 1/(2*sys->volume) );
    
    //                q_i*q_j  /           \.
    // sig_a_ab = sum ------- | erfc(eta*r) | r_a r_b
    //            i<j   r^3    \           /  ~   ~
    // note: sum excludes pairs in the same molecule
    M_matrix sig_a = M_ZERO_MAT; // atomic contribution
    int i,j;
    for (i=0; i<sys->atoms->count; i++ ) {
        M_atom *atom_i = m_sys_atom(sys, i);
        
        // no need to do anything if the atom is neutral
        if ( atom_i->spec->charge == 0 ) continue;

        M_parray *pairs = m_cells_get_objs_pos( sys->cell_atoms, 
                                        &(atom_i->pos) );

        for ( j=0; j<pairs->count; j++ ) { 
            M_atom *atom_j = pairs->data[j];

            if ( atom_j->spec->charge == 0 ) continue;
            if ( atom_i->id >= atom_j->id ) continue;
            if ( atom_i->molec->id == atom_j->molec->id ) continue;

            M_vector dr = m_radius(atom_i->pos, atom_j->pos);
            prec r2 = m_vector_mag2(dr);

            if ( r2 < sys->rcut2 || sys->no_cutoff ) { 
                prec r = sqrt(r2);
                prec r3 = r*r2;
                prec er = eta*r;
      
                prec factor = atom_i->spec->charge * atom_j->spec->charge / r3;
                factor *= ( erfc(er) );
                sig_a = m_matrix_add( sig_a, 
                                      m_matrix_scale( m_vector_outer(dr, dr),
                                                      factor ) );
            } 
        }
        m_parray_free(pairs);
    }

    //                      q_i*q_j /          \.
    // sig_m_ab = - sum sum -------| erf(eta*r) | r_a r_b
    //               m  i<j   r^3   \          /  ~   ~
    // note: sum over previously excluded atoms
    M_matrix sig_m = M_ZERO_MAT; // molecular contribution
    int m;
    for (m=0; m<sys->molecs->count; m++ ) {
        M_molec *mol = m_sys_molec(sys, m);

        for (i=0; i<mol->atoms->count-1; i++) {
            M_atom *atom_i = m_molec_atom(mol, i);

            if ( atom_i->spec->charge == 0 ) continue;

            for (j=i+1; j<mol->atoms->count; j++) {
                M_atom *atom_j = m_molec_atom(mol, j);

                if ( atom_j->spec->charge == 0 ) continue;

                M_vector dr = m_radius(atom_i->pos, atom_j->pos);
                prec r2 = m_vector_mag2(dr);
                prec r = sqrt(r2);
                prec r3 = r*r2;
                prec er = eta*r;

                prec factor = atom_i->spec->charge * atom_j->spec->charge / r3;
                factor *= ( erf(er) );

                sig_m = m_matrix_add( sig_m,
                                      m_matrix_scale( m_vector_outer(dr, dr),
                                                      factor ) );
            }
        }
    }
    sig_m = m_matrix_scale(sig_m, -1.0);

    M_matrix sig_f = M_ZERO_MAT;
    for (m=0; m<sys->molecs->count; m++ ) {
        M_molec *mol = m_sys_molec(sys, m);

        for (i=0; i<mol->atoms->count; i++) {
            M_atom *atom_i = m_molec_atom(mol, i);

            if ( atom_i->spec->charge == 0 ) continue;

            M_vector f = M_ZERO_VEC;

            M_vector k_net = M_ZERO_VEC;

            for (k_id=0; k_id<sys->ewald->n_k; k_id++) {
                M_vector k = m_ewald_get_k(k_id, sys->ewald);

                prec k2 = m_vector_dot(k,k);
                if ( k2 == 0 ) continue;

                M_complex ikr = I * (k.x*atom_i->pos.x + k.y*atom_i->pos.y +
                                     k.z*atom_i->pos.z);

                prec factor = sys->ewald->greenfn[k_id];
                factor *= cimag( sys->ewald->rhohat[k_id] * cexp(-ikr) );

                k_net = m_vector_add( k_net, m_vector_scale( k, factor ) );
            }
            k_net = m_vector_scale(k_net, -.5*atom_i->spec->charge/sys->volume);


            M_vector q_net = M_ZERO_VEC;
            for ( j=0; j<sys->atoms->count; j++ ) {
                M_atom *atom_j = m_sys_atom(sys, j);
                if ( atom_j->spec->charge == 0 ) continue;
                if ( atom_j->molec->id == atom_i->molec->id ) continue;

                // direction: i-->j
                M_vector dr_ij = m_disp(atom_j->pos, atom_i->pos);
                prec r2 = m_vector_mag2(dr_ij);

                if ( r2 < sys->rcut2 || sys->no_cutoff ) { 

                    prec r = sqrt(r2);
                    prec r3 = r*r2;
                    prec er = eta*r;

                    prec factor = atom_j->spec->charge;
                    factor *= ( 2.0/sqrt_pi*er*exp(-er*er)+erfc(er) )/r3;
                    q_net = m_vector_add(q_net, m_vector_scale(dr_ij,factor));
                }
            }
            q_net = m_vector_scale(q_net, atom_i->spec->charge);

            M_vector m_net = M_ZERO_VEC;
            for ( j=0; j<mol->atoms->count; j++ ) {
                M_atom *atom_j = m_molec_atom(mol,j);
                if ( atom_j->spec->charge == 0 ) continue;
                if ( atom_j->id == atom_i->id ) continue;

                // direction: i-->j
                M_vector dr_ij = m_disp(atom_j->pos, atom_i->pos);
                prec r2 = m_vector_mag2(dr_ij);

                if ( r2 < sys->rcut2 || sys->no_cutoff ) { 

                    prec r = sqrt(r2);
                    prec r3 = r*r2;
                    prec er = eta*r;

                    prec factor = atom_j->spec->charge;
                    factor *= ( -2.0/sqrt_pi*er*exp(-er*er)+erf(er) )/r3;
                    m_net = m_vector_add(m_net, m_vector_scale(dr_ij,factor));
                }
            }
            m_net = m_vector_scale(m_net, -1.0*atom_i->spec->charge);

            f = m_vector_add(f, k_net);
            f = m_vector_add(f, q_net);
            f = m_vector_add(f, m_net);

            // direction: mol-->atom
            M_vector dr_mi = m_disp(atom_i->pos, mol->center);
            sig_f = m_matrix_add( sig_f, m_vector_outer(f, dr_mi));
        }
    }
    prec self_correct = sys->ewald->U_self/3;
    //DEBUG("Pexcsr:\t\t%.12E\n",m_matrix_trace(sig_a)/(3*sys->volume));
    //DEBUG("Pexclr:\t\t%.12E\n",m_matrix_trace(sig_k)/(3*sys->volume));
    //DEBUG("Pexcself:\t%.12E\n",self_correct/(sys->volume));
    //DEBUG("molcorrectlr:\t%.12E\n",m_matrix_trace(sig_m)/(3*sys->volume));
    //DEBUG("molcorrectlr2:\t%.12E\n",m_matrix_trace(sig_f)/(3*sys->volume));

    // sig_coulomb = sig_k + sig_a - sig_m - sig_f - I*self_correct
    M_matrix *mat = malloc(sizeof(M_matrix));

    *mat = m_matrix_add(sig_k, sig_a);
    *mat = m_matrix_add(*mat, sig_m);
    *mat = m_matrix_add(*mat, sig_f);
    *mat = m_matrix_add(*mat, m_matrix_scale(M_IDENT_MAT, self_correct));
    *mat = m_matrix_scale(*mat, (prec) 1.0/sys->volume);

    return mat;
}

// this routine follows the formulation given by Alejandre, Tildesley & Chapela
// this is a molecular virial
M_matrix* m_stress_calc_electro_mol(M_system *sys) {
    prec eta = sys->ewald->eta;

    //              1         /  /           1        1           \.
    // sig_k_ab = ----   sum |  | 1_ab - 2(------ + -----) k_a k_b |
    //             2V     k   \  \         4eta^2   |k|^2  ~   ~  /
    //                                                          \.
    //                              * greenfn(k) * |rhohat(k)|^2 |
    //                                                          /
    M_matrix sig_k = M_ZERO_MAT; // reciprocal space contribution
    int k_id;

    for (k_id=0; k_id<sys->ewald->n_k; k_id++) {
        M_vector k = m_ewald_get_k(k_id, sys->ewald);

        prec k2 = m_vector_dot(k,k);

        if ( k2 > 0 ) {
            prec pre_k_factor = 2.0 * (1.0/(4.0*pow(eta,2.0)) + 1.0/k2);
            // only the energy contribution
            // pre_k_factor = (2.0)/k2;
            // only the k^2 contribution
            //pre_k_factor = 3.0/k2 + 2.0 * (1.0/(4.0*pow(eta,2.0)));
            prec tail_factor = sys->ewald->greenfn[k_id]
                               * creal( sys->ewald->rhohat[k_id]
                                        * conj(sys->ewald->rhohat[k_id]) );
            M_matrix t = m_matrix_subtract(M_IDENT_MAT,
                                           m_matrix_scale(m_vector_outer(k, k),
                                                          pre_k_factor) );
            sig_k = m_matrix_add( sig_k, m_matrix_scale( t, tail_factor ) ); 
        }
    }
    sig_k = m_matrix_scale( sig_k, (prec) 1/(2*sys->volume) );

    //                q_i*q_j  /               2eta*r                 \.
    // sig_a_ab = sum ------- | erfc(eta*r) + -------- exp(-(eta*r)^2) | r_a r_b
    //            i,j  2r^3    \              sqrt(pi)                /  ~   ~
    // note: sum excludes pairs in the same molecule
    M_matrix sig_a = M_ZERO_MAT; // atomic contribution
    int i,j;
    for (i=0; i<sys->molecs->count; i++ ) {
        M_molec *mol_i = m_sys_molec(sys, i);

        M_parray *pairs = m_cells_get_objs_pos( sys->cell_molecs, 
                                        &(mol_i->center) );

        for (j=0; j<pairs->count; j++) {
            M_molec *mol_j = pairs->data[j];

            if ( mol_i->id >= mol_j->id ) continue;

            M_vector dr_ij = m_disp(mol_j->center, mol_i->center);
            
            if (m_vector_mag2(dr_ij) > sys->rcut2 && ! sys->no_cutoff) continue;

            int a,b;
            for (a=0; a<mol_i->atoms->count; a++) {
                M_atom *atom_a = m_molec_atom(mol_i, a);
                if (atom_a->spec->charge == 0) continue;

                for (b=0; b<mol_j->atoms->count; b++) {
                    M_atom *atom_b = m_molec_atom(mol_j, b);
                    if (atom_b->spec->charge == 0) continue;

                    M_vector dr_ab = m_disp(atom_b->pos, atom_a->pos);

                    prec r2 = m_vector_mag2(dr_ab);

                    if ( r2 < sys->rcut2 || sys->no_cutoff ) { 

                        prec r = sqrt(r2);
                        prec r3 = r*r2;
                        prec er = eta*r;

                        prec factor = atom_a->spec->charge*atom_b->spec->charge;
                        factor *= ( 2/sqrt_pi*er*exp(-er*er)+erfc(er) )/r3;

                        sig_a = m_matrix_add(sig_a, 
                                    m_matrix_scale( m_vector_outer(dr_ij,dr_ab),
                                                    factor ) );
                    }
                }
            }
        }

        m_parray_free(pairs);
    }

    //             1                            /
    // sig_m_ab = ---- sum sum r_mi(b) q_i sum |  greenfn[k] * k(a) *
    //             2V   m   i               k   \.
    //                                                                        \.
    //                                            imag(rhohat[k]*exp(-ik r_i)) |
    //                                                                        /
    // note: sum over atoms in a molecule
    M_matrix sig_m = M_ZERO_MAT; // molecular contribution
    int m;
    for (m=0; m<sys->molecs->count; m++ ) {
        M_molec *mol = m_sys_molec(sys, m);

        for (i=0; i<mol->atoms->count; i++) {
            M_atom *atom_i = m_molec_atom(mol, i);

            if ( atom_i->spec->charge == 0 ) continue;

            // direction: mol-->atom
            M_vector dr_mi = m_disp(atom_i->pos, mol->center);

            M_vector k_net = M_ZERO_VEC;

            for (k_id=0; k_id<sys->ewald->n_k; k_id++) {
                M_vector k = m_ewald_get_k(k_id, sys->ewald);

                prec k2 = m_vector_dot(k,k);
                if ( k2 == 0 ) continue;

                M_complex ikr = I * (k.x*atom_i->pos.x + k.y*atom_i->pos.y +
                                     k.z*atom_i->pos.z);

                prec factor = sys->ewald->greenfn[k_id];
                factor *= cimag( sys->ewald->rhohat[k_id] * cexp(-ikr) );

                k_net = m_vector_add( k_net, m_vector_scale( k, factor ) );
            }

            k_net = m_vector_scale(k_net, atom_i->spec->charge);

            sig_m = m_matrix_add( sig_m, m_vector_outer( k_net, dr_mi ) );
        }
    }
    sig_m = m_matrix_scale( sig_m, (prec) 1/(2*sys->volume) );

    M_matrix *mat = malloc(sizeof(M_matrix));

    *mat = m_matrix_add(sig_k, sig_a);
    *mat = m_matrix_add(*mat, sig_m);
    *mat = m_matrix_scale(*mat, (prec) 1.0/sys->volume);

    return mat;
}

M_matrix* m_stress_calc_lj(M_system *sys) {
    //               1  molecules   i    j
    // sigma(a,b) = ---    sum     sum  sum  r_ij(a) * f_mn(b)
    //               V     i<j      m    n   ~         ~

    M_matrix *mat = malloc(sizeof(M_matrix));
    m_matrix_zero(mat);

    int i,j,m,n;
    for (i=0; i<sys->molecs->count; i++) {
        M_molec *mol_i = m_sys_molec(sys,i);

        M_parray *pairs = m_cells_get_objs_pos(sys->cell_molecs,
                                               &(mol_i->center));
        for (j=0; j<pairs->count; j++) {
            M_molec *mol_j = pairs->data[j];

            if ( mol_i->id >= mol_j->id ) continue;

            // dr direction is i->j: dr = j - i
            M_vector dr = m_disp( mol_j->center, mol_i->center );
            prec r2 = m_vector_mag2(dr);
            if ( r2 < sys->rcut2 || sys->no_cutoff ) {
                for (m=0; m<mol_i->atoms->count; m++) {
                    M_atom *atom_m = m_molec_atom(mol_i,m);
                    for (n=0; n<mol_j->atoms->count; n++) {
                        M_atom *atom_n = m_molec_atom(mol_j,n); 

                        M_vector f = m_lj_force( atom_m, atom_n );

                        M_matrix t = m_vector_outer(dr,f);

                        if ( sys->stress_toymd ) {
                            // the LJ-part of toyMD uses an additional factor of
                            // sqrt(r_{ij}.r_{ij}/r_{mn}.r_{mn})
                            M_vector dr_mn = m_disp( atom_n->pos, atom_m->pos );
                            prec factor = sqrt( m_vector_mag2(dr) 
                                                / m_vector_mag2(dr_mn) );
                            t = m_matrix_scale(t, factor);
                        }
                        *mat = m_matrix_add(*mat,t);
                    }
                }
            }
        }
        m_parray_free(pairs);
    }

    *mat = m_matrix_scale( *mat, (prec) 1.0/sys->volume );

    return mat;
}

M_matrix m_stress_calc_lj_lr(M_system *sys) {
    // now do the long-range correction
    // sigma_lr.zz = sigma_lr.yy = sigma_lr.zz
    //                1   mol_spec  N_a N_b    /    delta(a,b) \.    /   8pi  \.
    //             = ---    sum     ------- * | 1 - ----------  | * | - -----  |
    //                V    a<=b        V       \        2      /     \  3rc^3 / 
    //                                           / a->atoms  b->atoms         \.
    //                                        * |    sum       sum    C6_{mn}  |
    //                                           \    m         n             /
    // off-diagonal elements are zero
    M_matrix sigma_lr = M_ZERO_MAT;
    
    prec diag = 0;
    int a,b,m,n;
    for ( a=0; a<sys->molec_specs->count; a++ ) {
        M_molec_spec *mspec_a = m_sys_molec_spec(sys,a);
        for ( b=a; b<sys->molec_specs->count; b++ ) {
            M_molec_spec *mspec_b = m_sys_molec_spec(sys,b);

            prec c6_sum = 0;
            for ( m=0; m<mspec_a->atom_specs->count; m++ ) {
                M_atom_spec *aspec_m = m_mspec_aspec(mspec_a, m);
                for ( n=0; n<mspec_b->atom_specs->count; n++ ) {
                    M_atom_spec *aspec_n = m_mspec_aspec(mspec_b, n);

                    c6_sum += aspec_m->C6[aspec_n->id] * aspec_m->count
                                                       * aspec_n->count;
                }
            }

            if ( a == b ) {
                c6_sum = c6_sum / 2;
            }

            diag += c6_sum * mspec_a->count * mspec_b->count;
        }
    }
    diag *= -8*pi/(3*pow(sys->rcut,3.0)*sys->volume);
    diag *= 1.0/sys->volume;

    sigma_lr.xx = diag;
    sigma_lr.yy = diag;
    sigma_lr.zz = diag;

    return sigma_lr;
}

M_matrix* m_stress_calc_kinetic(M_system *sys) {
    M_matrix *mat = (M_matrix *) malloc(sizeof(M_matrix));
    m_matrix_zero(mat);

    // Note that the sigma=-P, where sigma is the stress tensor
    // and P the pressure tensor
    //                1
    // sigma(a,b) =  ---    sum     m_i * v_i(a) * v_i(b)
    //                V  molecules

    int i;
    for (i=0; i<sys->molecs->count; i++) {
        M_molec *mol = m_sys_molec(sys, i);
        *mat = m_matrix_add( *mat, 
                             m_matrix_scale( m_vector_outer(mol->vel, mol->vel),
                                             mol->spec->mass ) );
    }

    *mat = m_matrix_scale( *mat, (prec) 1.0/sys->volume );

    return mat;
}

M_stress* m_stress_calc_frame(M_system *sys) {
    M_stress *new = m_stress_new();
    new->total = (M_matrix *) malloc(sizeof(M_matrix));
    m_matrix_zero(new->total);

    if ( sys->has_charges && sys->calc_ewald ) {
        if ( sys->stress_toymd ) {
            new->electro = m_stress_calc_electro_toy(sys);
        } else {
            new->electro = m_stress_calc_electro_mol(sys);
        }
        *new->total = m_matrix_add(*new->total, *new->electro);
    }

    new->lj      = m_stress_calc_lj(sys);
    if ( sys->stress_lj_lr ) {
        *new->lj = m_matrix_add(*new->lj, m_stress_calc_lj_lr(sys));
    }
    *new->total = m_matrix_add(*new->total, *new->lj);

    if ( sys->read_vel ) {
        new->kinetic = m_stress_calc_kinetic(sys);
        *new->total = m_matrix_add(*new->total, *new->kinetic);
    }

    return new;
}

void m_stress_output(M_stress *stress) {
    printf("stress_total\n");
    m_puts_matrix(*stress->total);
    printf("stress_electro\n");
    m_puts_matrix(*stress->electro);
    printf("stress_lj\n");
    m_puts_matrix(*stress->lj);
    printf("stress_kinetic\n");
    m_puts_matrix(*stress->kinetic);
}

void m_stress_load_config(M_system *sys, dictionary *dict) {

    sys->calc_stress = iniparser_getboolean(dict, "system:calc_stress", 0);

    if ( sys->calc_stress ) {
        int i;
        for ( i=0; i<sys->molec_specs->count; i++ ) {
            M_molec_spec *mspec = m_sys_molec_spec(sys, i);
            // we can only calculate stress tensor using the center of mass
            // if not calculating center of mass, the molecule must be a single
            // atom
            if ( mspec->center != -1 && mspec->atoms != 1 ) {
                FATAL("Calculation of the stress tensor requires the center\n");
                FATAL("of molecules to be the center of mass. See --example\n");
                exit(1);
            }
        }

        if (! sys->read_vel ) {
            DEBUG("Calculation of the full stress tensor requires read_vel\n");
            DEBUG("to be set to true, and the XYZ file must include\n");
            DEBUG("additional columns for the velocities.\n");
            DEBUG("The stress tensor here won't include the kinetic part.\n");
        }

        if (! sys->pop_mol_cell ) {
            FATAL("Calculation of the stress tensor requires pop_mol_cell\n");
            FATAL("to be set to true.\n");
            exit(1);
        }
        if ( sys->has_charges && ( ! sys->calc_ewald ) ) {
            FATAL("There are charges in the cell.\n");
            FATAL("Calculation of the stress tensor requires calc_ewald\n");
            FATAL("to be set to true.\n");
            exit(1);
        }

        sys->stress_lj_lr = iniparser_getboolean(dict, "system:stress_lj_lr",
                                                 0);
        sys->stress_toymd = iniparser_getboolean(dict, "system:stress_toymd",
                                                 0);
    }
}
