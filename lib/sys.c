#include "headers/sys.h"

M_atom_spec *m_system_add_atom_spec(M_system *sys, char *molec,
                                    char *name, char *symbol, M_uint count,
                                    prec mass, prec radius, prec charge) {
    M_molec_spec *mspec = m_molec_spec_find(sys, molec);

    return m_sys_add_atom_spec(sys, mspec,
                               name, symbol, count,
                               mass, radius, charge);
    
}

M_atom_spec *m_sys_add_atom_spec(M_system *sys, M_molec_spec *mspec,
                                    char *name, char *symbol, M_uint count,
                                    prec mass, prec radius, prec charge) {
    // we require a valid mspec
    if ( mspec != NULL ) {
        // first create a new M_atom_spec 
        // this M_atom_spec is allocated on the heap (via malloc)
        // however, it will be destroyed (via free) once it has been copied
        // to a M_array
        // in this case, its permanent address is in (M_array *)sys->atom_specs
        // while other lists will point to it, e.g. mspec->atom_specs->data[i]
        M_atom_spec *aspec = m_atom_spec_new(name,symbol,count,mass,radius,
                                             charge);
        aspec->molec_spec = mspec;
        aspec->sys_total = aspec->count*mspec->count;
        // the new aspec will be added to the end of the list in sys->atom_specs
        // and mspec->atom_specs
        aspec->id = sys->atom_specs->count;
        aspec->id_in_spec = mspec->atom_specs->count; 
        // push aspec into sys->atom_specs, its new address will be returned
        aspec = m_array_push(sys->atom_specs, aspec, 1);
        m_parray_push(mspec->atom_specs, aspec);

        mspec->atoms += aspec->count;
        if ( mspec->mass < 0 && mass > 0 ) {
            mspec->mass = mass*count;
        } else {
            mspec->mass += mass*count;
        }
        mspec->charge += charge; 

        if ( charge != 0 ) {
            sys->has_charges = 1;
            sys->net_charge += aspec->sys_total * charge;
        }

        return aspec;
    } else {
        DEBUG("FAILED %s to %s\n", name,mspec->name);
    }
    return NULL;
}

M_molec_spec *m_sys_add_molec_spec(M_system *sys, char *name, int count) { 
    M_molec_spec *spec = m_molec_spec_new(name, count);
    spec->id = sys->molec_specs->count;
    return m_array_push(sys->molec_specs, spec, 1);
}


M_molec_spec *m_molec_spec_find(M_system *sys, char *name) {
    int i;
    M_molec_spec *m;
    for ( i = 0; i < sys->molec_specs->count; i++ ) {
        m = m_sys_molec_spec(sys, i);
        if( !strcmp(m->name,name) ) {
            return m;
        }
    }
    return NULL;
}

M_atom_spec *m_atom_spec_find(M_system *sys, char *name) {
    int i;
    M_atom_spec *a;
    for ( i = 0; i < sys->atom_specs->count; i++ ) {
        a = m_sys_atom_spec(sys, i);
        if( !strcmp(a->name,name) ) {
            return a;
        }
    }
    return NULL;
}

M_atom_spec *m_atom_spec_find_symbol(M_system *sys, char *symbol) {
    int i;
    M_atom_spec *a;
    for ( i = 0; i < sys->atom_specs->count; i++ ) {
        a = m_sys_atom_spec(sys, i);
        if( !strcmp(a->symbol,symbol) ) {
            return a;
        }
    }
    return NULL;
}

void m_sys_build(M_system *sys) {
    M_molec_spec *spec, *prev;
    M_uint m;
    M_uint atom_count = 0;

    /* get the start and end index of each molec_spec */
    for ( m = 0; m < sys->molec_specs->count; m++ ) {
        spec = m_sys_molec_spec(sys, m);
        if ( m == 0 ) { 
            spec->astart = 0;
            spec->mstart = 0;
        } else{
            prev = m_array_elt_pos(sys->molec_specs, m - 1);
            spec->astart = prev->count * prev->atoms + prev->astart;
            spec->mstart = prev->count + prev->mstart;
        }
        spec->mend = spec->mstart + spec->count;
        spec->aend  = spec->astart + spec->count * spec->atoms - 1;
        atom_count = spec->astart + spec->count * spec->atoms;
    }

    /* atom_count is now equal to the total number of atoms */

    /* Populate the systems array of atoms */
    M_atom *atom;
    m_array_resize(sys->atoms, atom_count);
    M_atom_spec *aspec = m_sys_atom_spec(sys, 0);
    for ( m = 0; m < atom_count; m++ ) {
        atom = m_atom_new();
        atom->pos.dims = &sys->dims;
        atom->id = m;
        m_array_push(sys->atoms, atom, 1);
    }

    /* Populate the systems array of molecules */
    M_molec *new, *t;
    M_uint a, i, d;
    i = 0;
    for ( d = 0; d < sys->molec_specs->count; d++ ) {
        spec = m_sys_molec_spec(sys, d);
        for ( m = 0; m < spec->count; m++ ) {
            // Make a new molecule with n atoms
            new = m_molec_new();
            new->spec = spec;
            new->id = i;
            new->center.dims = &sys->dims;
            // Add the molecule to the sytem
            m_array_push(sys->molecs, new, 1);

            i++;
        }
    }

    /* Map molecules to their species. */
    for ( d = 0; d < sys->molec_specs->count; d++ ) {
        spec = m_sys_molec_spec(sys, d);
        for ( m = spec->mstart; m < spec->mstart + spec->count; m++ ) {
            t = m_sys_molec(sys, m);
            t->id_in_spec = spec->molecs->count;
            //printf("%p : %d'th %s %lf\n", t, m, mspec->name, t->dipole.z);
            m_parray_push(spec->molecs,t);
        }
    }

    /* Map atoms to their molecules */
    int sys_i = 0;
    for ( d = 0; d < sys->molec_specs->count; d++ ) {
        spec = m_sys_molec_spec(sys, d);
        int u;
        if ( spec->position == TOGETHER ) {
            for ( u = 0; u < spec->count; u++ ) {
                // For each molecule
                t = m_spec_molec(spec, u);
                for ( a = 0; a < spec->atoms; a++ ) {
                    // Get the system atom
                    //DEBUG("M: %d I: %d\n",u,i);
                    atom = m_sys_atom(sys, sys_i);
                    atom->molec = t;
                    //DEBUG("M: %d I: %d\n",atom->m_id,atom->id);
                    // Add the atom to the molecule
                    m_parray_push(t->atoms, atom);
                    // move to the next system atom
                    sys_i++;
                }
            }
        } else {
            // If the molecule is seperated
            int j = sys_i;
            for ( u = 0; u < spec->count; u++ ) {
                // For each molecule
                t = m_spec_molec(spec, u);
                for ( a = 0; a < spec->atoms; a++ ) {
                    // Get the system atom
                    i = spec->count * a + j;
                    //DEBUG("M: %d I: %d\n",u,i);
                    atom = m_sys_atom(sys, i);
                    atom->molec = t;
                    //DEBUG("M: %d I: %d\n",atom->m_id,atom->id);
                    // Add the atom to the molecule
                    m_parray_push(t->atoms, atom);
                    // move to the next system atom
                    sys_i++;
                }
                j++;
            }
        }
    }

    /* each atom: add a pointer to its spec, and add its pointer to its spec */
    int aid, mid, aspecid;
    M_molec *mol;
    for (d = 0; d < sys->molec_specs->count; d++ ) {
        spec = m_sys_molec_spec(sys, d);
        for(mid = 0; mid < spec->molecs->count; mid++ ) {
            mol = m_spec_molec(spec, mid);
            i = 0;
            for (aspecid = 0; aspecid < spec->atom_specs->count; aspecid++ ) {
                aspec = m_mspec_aspec(spec, aspecid);
                for (aid = 0; aid < aspec->count; aid++ ) {
                    //DEBUG("number of atoms: %d\t %d\n",aspec->count,i);
                    atom =  m_molec_atom(mol, i);
                    
                    // save the atom's position in mol->atoms
                    atom->id_in_molec = i;
                    
                    // save "aspec" as the atom's spec
                    atom->spec = aspec;

                    // save the atom's position in aspec->atoms
                    atom->id_in_spec = aspec->atoms->count;
                    
                    // add atom to aspec's list of atoms
                    m_parray_push(aspec->atoms,atom);

                    //DEBUG("M: %d I: %d S: %d\n",atom->m_id,atom->id,
                    //                            atom->spec_id);
                    i++;
                }
            }
        }
    }

    /* check that the correct number of atoms are allocated */
    for ( aspecid = 0; aspecid < sys->atom_specs->count; aspecid++ ) {
        aspec = m_sys_atom_spec(sys, aspecid);

        if (aspec->atoms->count != aspec->sys_total) {
            FATAL("Number mismatch in atom_spec %s!\n",aspec->name);
            FATAL("Expected: %d, Actual: %d\n",aspec->sys_total,
                                               aspec->atoms->count);
            exit(1);
        }

    }
}

void m_print_mapping( M_system *sys ) {
    M_molec *mol;
    M_molec_spec *mspec;
    M_atom *atom;
    M_atom_spec *aspec;
    int mid, mspecid, aspecid;
    int i;
    
    // print all the molec_spec
    printf("------------------\n[sys->molec_specs]\n------------------\n");
    printf("id\tname\tatoms\tcount (chk)\tmol_id range\tatom_id range\n");
    for (mspecid = 0; mspecid < sys->molec_specs->count; mspecid++ ) {
        mspec = m_sys_molec_spec(sys,mspecid);
        printf("%d\t%s\t%d\t%d (%d)\t(%d,%d)\t\t(%d,%d)\n",mspec->id,
               mspec->name, mspec->atoms, mspec->count, mspec->molecs->count,
               mspec->mstart, mspec->mend, mspec->astart, mspec->aend);
    }
    printf("\n");

    // print all the atom_spec
    printf("-----------------\n[sys->atom_specs]\n-----------------\n");
    printf("id (id_in_spec)\tname (symb)\tmspec (id)\tcount\tsys_total (chk)");
    printf("\tcharge\tmass\tradius\n");
    for (aspecid = 0; aspecid < sys->atom_specs->count; aspecid++ ) {
        aspec = m_sys_atom_spec(sys,aspecid);
        printf("%d (%d)\t\t%s (%s)\t%s (%d)\t%d\t%d (%d)\t%g\t%g\t%g\n",
               aspec->id, aspec->id_in_spec, aspec->name, aspec->symbol,
               aspec->molec_spec->name, aspec->molec_spec->id, aspec->count,
               aspec->sys_total, aspec->atoms->count, aspec->charge,
               aspec->mass, aspec->radius);
    }
    printf("\n");

    printf("---------\n[Mapping]\n---------\n");
    printf("MOLECULE (spec_id)\tm_id\t(id_in_spec)\n");
    printf(" / ATOM (spec_id)\t\t\t -> \ta_id\t(id_in_spec,\tid_in_molec)\n");
    for (mspecid = 0; mspecid < sys->molec_specs->count; mspecid++ ) {
        mspec = m_array_elt_pos(sys->molec_specs, mspecid);
        for(mid = 0; mid < mspec->molecs->count; mid++ ) {
            mol = m_spec_molec(mspec, mid);
            i = 0;
            for (i=0; i<mol->atoms->count; i++) {
                atom = mol->atoms->data[i];
                aspec = atom->spec;
                printf("%s (%d) / %s (%d)\t%d\t(%d)\t -> \t%d\t(%d,\t%d)\n",
                      atom->molec->spec->name, atom->molec->spec->id,
                      atom->spec->symbol, atom->spec->id, atom->molec->id,
                      atom->molec->id_in_spec, atom->id, atom->id_in_spec,
                      atom->id_in_molec);
            }

            printf("\n");
        }
    }
}

void m_sys_species_load_config(M_system *sys, dictionary *dict) {
    int count, i;
    int atom_spec_count;
    char key[20];
    char name[20];
    //char secname[40];
    char *p;
    char atom[20];
    char atom_sec[20];
    char *k;
    char *q;
    char atom_count_key[40];
    char atom_symbol_key[40];
    char atom_mass_key[40];
    char atom_radius_key[40];
    char atom_charge_key[40];
    char molec_together_key[40];
    char molec_center_key[40];
    double mass, radius, charge;
    char *symbol;

    // Get the number of sections in the dict
    int nsects = iniparser_getnsec(dict);
    
    // Get the number of molecules
    int nkeys = iniparser_getsecnkeys(dict, "molecules");
    //DEBUG("There are %d molecule\n", nkeys);

    // Get the names of the molecules
    char ** keys  = iniparser_getseckeys(dict, "molecules");
    // keys is an array of strings, where
    //    keys[0] = "molecules:molecule_name0"
    //    keys[1] = "molecules:molecule_name1"
    // and so on.

    M_atom_spec *a;
    // loop over each molecule (each key)
    for ( i = 0; i < nkeys; i++ ) {
        // get the number of molecules (value of the key)
        count = iniparser_getint(dict, keys[i], 0);

        strcpy(key,keys[i]);    //  key = "molecules:molecule_namei"
        p = strtok(key, ":");   //    p = "molecules"
        p = strtok(NULL, ":");  //    p = "molecule_namei"
        strcpy(name, p);        // name = "molecule_namei"
        //DEBUG("molec_spec[%d]: %s\n", i, name);

        // Add mspec!
        M_molec_spec *m = m_sys_add_molec_spec(sys, name, count);

        // Loop over the all sections finding those
        // that start with 'molecule_name#'
        int j;
        atom_spec_count = 0;
        for ( j = 0; j < nsects; j++ ) {
            // atom = "molecule_name#atom_name"
            strcpy(atom,iniparser_getsecname(dict, j));
            // atom_sec = "molecule_name#atom_name"
            strcpy(atom_sec,atom);
            p = strtok(atom, "#");   // p = "molecule_name"
            k = strtok(NULL, "#");   // k = "atom_name"
            // If the first element of the split is
            // the molecule name, it is asosciated

            // p = "molecule_name", name="molecule_name"
            if (!strcmp(p,name) ){
                // If the next element is not null it is an atom
                if (k != NULL ) {
                    // Get the atom count
                    // keys is 'species_name#atom_name:count'
                    strcpy(atom_count_key,atom_sec);
                    strcat(atom_count_key,":count");

                    strcpy(atom_symbol_key,atom_sec);
                    strcat(atom_symbol_key,":symbol");

                    strcpy(atom_mass_key,atom_sec);
                    strcat(atom_mass_key,":mass");

                    strcpy(atom_radius_key,atom_sec);
                    strcat(atom_radius_key,":radius");

                    strcpy(atom_charge_key,atom_sec);
                    strcat(atom_charge_key,":charge");

                    atom_spec_count++;
                    //strcpy(atom,k);

                    count = iniparser_getint(dict,atom_count_key, 0);
                    mass = iniparser_getdouble(dict,atom_mass_key, 0);
                    radius = iniparser_getdouble(dict,atom_radius_key, 0);
                    charge = iniparser_getdouble(dict,atom_charge_key, 0);
                    symbol = iniparser_getstring(dict,atom_symbol_key, "");
                    //DEBUG("     %s %s %d %s %lf\n", name, k, count,symbol,
                    //                                mass);
                    a = m_sys_add_atom_spec(sys, m, k, symbol, count, 
                                               mass, radius, charge);
                }
                // It must be properties of the molecule
                else {
                    strcpy(molec_together_key,p);
                    strcat(molec_together_key,":together");
                    // default behaviour is TOGETHER
                    //    TOGETHER:   OHHOHHOHH...
                    //    SEPARATED:  OOO...HHH...HHH...
                    if(iniparser_getboolean(dict,molec_together_key, 1)) {
                        m->position = TOGETHER;
                    } else {
                        m->position = SEPARATED;
                    }

                    strcpy(molec_center_key,p);
                    strcat(molec_center_key,":center");
                    q = iniparser_getstring(dict,molec_center_key, NULL);

                    if ( q != NULL ) {
                      if ( !strcmp(q,"geom") || !strcmp(q,"geometric") 
                           || !strcmp(q,"geometric_centre")
                           || !strcmp(q,"geometric_center")
                           || !strcmp(q,"centre_of_mass")
                           || !strcmp(q,"center_of_mass")
                           || !strcmp(q,"com") ) {
                          m->center = geom_center; // geom_center = -1
                      } else {
                          m->center = iniparser_getint(dict,molec_center_key,0);
                      }
                    }
                    
                }
            }
        }
        if (atom_spec_count == 0 ) {
            FATAL("Molecular Species %s had no atomic_species!\n\
                   Please add at least one.\n", name);
            exit(1);
        }
    }
}

void m_sys_properties_load_config(M_system *sys, dictionary *dict) {
    // get system constants
    sys->setT         = iniparser_getdouble(dict,"system:setT", 0);
}

void m_sys_calc(M_system *sys) {
    // calculate the density of each mol spec
    calc_densities(sys);                  // molecules.c

    // update the rcut (trimmed to min(rcut,min(dims)/2) )
    calc_rcut(sys);                       // forcefield.c

    if ( sys->calc_centers ) {
        calc_centers(sys);                // molecules.c
    }
    if ( sys->has_charges && sys->calc_dipoles ) {
        calc_dipoles(sys);                // molecules.c
    }
    if ( sys->calc_centers && sys->read_vel ) {
        calc_center_vels(sys);
    }
    if ( sys->pop_atom_cell ) {
        // cell.c
        m_cells_free(sys->cell_atoms);
        sys->cell_atoms = m_cells_populate_atom_occupancy(sys);
        int tally = m_cells_tally(sys->cell_atoms);
        if ( tally == -1 ) { 
            FATAL("Cell occupancy table is incorrectly initialised.\n");
            exit(1);
        }   
        if ( tally != sys->atoms->count ) { 
            FATAL("There are %d atoms in the system,\n",sys->atoms->count);
            FATAL("but there are %d atom objects in the occupancy table\n",
                   tally);
            exit(1);
        }
    }
    if ( sys->pop_mol_cell ) {
        // cell.c
        m_cells_free(sys->cell_molecs);
        sys->cell_molecs = m_cells_populate_molec_occupancy(sys);
        int tally = m_cells_tally(sys->cell_molecs);
        if ( tally == -1 ) { 
            FATAL("Cell occupancy table is incorrectly initialised.\n");
            exit(1);
        }   
        if ( tally != sys->molecs->count ) { 
            FATAL("There are %d molecules in the system,\n",sys->molecs->count);
            FATAL("but there are %d molecule objects in the occupancy table\n",
                   tally);
            exit(1);
        }
    }
    if ( sys->has_charges && sys->calc_ewald ) {
        // coulomb.c
        m_ewald_free(sys->ewald);
        sys->ewald = m_ewald_calc_frame(sys);
    }
    if ( sys->calc_stress ) {
        // stress.c
        m_stress_free(sys->stress);
        sys->stress = m_stress_calc_frame(sys);
    }
}

void m_sys_load_config(M_system *sys, dictionary *dict) {
    // The ini file has the following format:
    // [section1]
    // key0 = s1k0_val
    // key1 = s1k1_val
    // ...
    // [section2]
    // key0 = s2k0_val
    // key1 = s2k1_val
    // ...

    // we can access the value of a key by:
    //      char * iniparser_getstring  (dictionary *d, const char *key,
    //                                   char *def)
    //         int iniparser_getint     (dictionary *d, const char *key,
    //                                   int notfound)
    //      double iniparser_getdouble  (dictionary *d, const char *key,
    //                                   double notfound)
    //         int iniparser_getboolean (dictionary *d, const char *key,
    //                                   int notfound)
    // where key is a string of the format "section1:key0" and so on.

    // first load all the molecules & atoms
    m_sys_species_load_config(sys, dict);        // sys.c
    m_sys_properties_load_config(sys, dict);     // sys.c
    m_frames_load_config(sys, dict);             // frames.c
    m_molecules_load_config(sys, dict);          // molecules.c
    m_cells_load_config(sys, dict);              // cell.c
    m_coulomb_load_config(sys, dict);            // coulomb.c
    m_forcefield_load_config(sys, dict);         // forcefield.c
    m_stress_load_config(sys, dict);             // stress.c
}

void print_sys_example() {
    char *text \
= "[system]\n\
rcut          = 20.0    ; float, bohrs, default = dims/2, optional\n\
                        ;   cutoff for distance calculations\n\
                        ;   updated to half the box length at every frame\n\
no_cutoff     = false   ; boolean {true, false}, default = false, optional\n\
                        ;   whether or not to ignore rcut\n\
wrap_coords   = true    ; boolean {true, false}, default = true, optional\n\
                        ;   if an atom's position is outside of the box, whether\n\
                        ;   or not to wrap the coordinate back into the box\n\
start_frame   = 1       ; int, id (1-index), default = 1, optional\n\
                        ;   the id of the first frame to be processed\n\
end_frame     = 2       ; int, id (1-index), no default, optional\n\
                        ;   the id of the final frame to be processed\n\
use_end       = false   ; boolean {true, false}, default = false, optional\n\
                        ;   whether or not to stop processing at end_frame\n\
skip          = 0       ; int, default = 0, optional\n\
                        ;   the number of frames to skip between each frame processed\n\
calc_dipoles  = false   ; boolean {true, false}, default = false, optional\n\
                        ;   if true, the dipole for each molecule object is calculated\n\
                        ;   immediately after reading the frame\n\
calc_centers  = false   ; boolean {true, false}, default = false, optional\n\
                        ;   if true, the centers for each molecule object is calculated\n\
                        ;   immediately after reading the frame\n\
cell_ngrid    = 8,8,8   ; comma-separated integers, three values, default = {1}\n\
                        ;   optimisation setting only\n\
                        ;   divide the box into smaller cells, for fewer pairwise\n\
                        ;   computations outside of rcut\n\
pop_atom_cell = false ; boolean {true, false}, default = false, optional\n\
                        ;   if true, the cell occupancy array is populated using atomic\n\
                        ;   coordinates immediately after reading the frame\n\
pop_mol_cell  = false ; boolean {true, false}, default = false, optional\n\
                        ;   if true, the cell occupancy array is populated using centers\n\
                        ;   of molecules immediately after reading the frame\n\
                        ;   requires calc_centers = true\n\
\n\
[molecules]\n\
water         = 100     ; left:   string, use-specified name for molecule\n\
                        ; right:  int, number of molecules of that species\n\
benzene       = 2       ;\n\
                        ;   at least one molecular species need to be specified\n\
                        ;   for single-atom species, declare molecules that consist\n\
                        ;   of only one atom\n\
                        ;\n\
                        ;   the names of each molecular species must be unique\n\
                        ;   each molecule must have at least one corresponding sections\n\
                        ;   that specifies its atomic constituents (see below)\n\
\n\
[water#oxygen]          ; format: [molecule_name#atom_name]\n\
                        ;   molecule_name must match a declaration under [molecules]\n\
                        ;   otherwise, the section is ignored\n\
                        ;   name of atomic species do not need to be unique\n\
count         = 1       ; int, required\n\
                        ;   the number of atoms of this kind in each molecule\n\
symbol        = O       ; string, required, max length = 4\n\
                        ;   atomic symboles do not need to be unique\n\
mass          = 18      ; float, arbitrary units, default = 0, optional\n\
radius        = 1.0     ; float, bohrs, default = 0, optional\n\
                        ;   size of an atom, applicable to hard-spheres, for e.g.\n\
charge        = -0.8    ; float, multiples of electron charge, default = 0, optional\n\
[water#hydrogen]\n\
count         = 2\n\
symbol        = H\n\
mass          = 1\n\
charge        = 0.4\n\
[water]                 ; format: [molecule_name]\n\
together      = false   ; boolean {true, false}, default = true, optional\n\
                        ;   if together, then the coordinates for the molecule\n\
                        ;   are contiguous, e.g. OHHOHHOHH... in the coordinates file\n\
                        ;   if not together, then the coordinates for each atomic\n\
                        ;   species are contiguous, e.g. OOOO...HHHH...\n\
                        ;\n\
                        ;   the order of atoms are the order they appear in this input file\n\
                        ;\n\
center        = 0       ; int (id, 0-index) or keywords as below, default = 0, optional}\n\
                        ; keyword values:\n\
                        ; {com, center_of_mass, centre_of_mass, geom, geometric,\n\
                        ;  geometric_center, geometric_centre}\n\
                        ;   define the center of the molecule either as an atomic site (give\n\
                        ;   the id of the atom in the molecule), or as the center of mass.\n\
                        ;   geometric center has the same meaning as center of mass in this\n\
                        ;   context. all keyword values are equivalent.\n\
; for the \"water\" molecule, its atoms are:\n\
;   oxygen (id = 0)\n\
;   hydrogen (id = 1)\n\
;   hydrogen (id = 2)\n\
\n\
[benzene#carbon]\n\
count         = 6\n\
symbol        = C\n\
[benzene#hydrogen]\n\
count         = 6\n\
symbol        = H\n\
[benzene]\n\
center        = com\n\
together      = true\n\
; for the \"benzene\" molecule, its atoms are:\n\
;   carbon (id = 0, 1, 2, 3, 4, 5)\n\
;   hydrogen (id = 6, 7, 8, 9, 10, 11)\n\
\n\
; for this config file, the coordinates file has the format:\n\
;     O x 100 lines \n\
;     H x 200 lines \n\
;     C x 6 lines \n\
;     H x 6 lines \n\
;     C x 6 lines \n\
;     H x 6 lines \n\
";
    printf("%s\n",text);
}
