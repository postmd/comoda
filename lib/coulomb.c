#include "headers/coulomb.h"
#include "headers/db.h"

M_ewald* m_ewald_new(prec eta, M_vector *dims, M_uint *nk) {
    M_ewald *new = malloc(sizeof(M_ewald));

    new->U_coulomb = 0;
    new->U_real = 0;
    new->U_recip = 0;
    new->U_self = 0;
    new->U_mol_self = 0;
    new->eta = eta;
    new->dims = dims;
    new->kmax[0] = nk[0]/2;
    new->kmax[1] = nk[1]/2;
    new->kmax[2] = nk[2]/2;
    new->npts[0] = new->kmax[0]*2+1;
    new->npts[1] = new->kmax[1]*2+1;
    new->npts[2] = new->kmax[2]*2+1;
    new->n_k = new->npts[0]*new->npts[1]*new->npts[2];
    new->greenfn = malloc(sizeof(prec)*new->n_k);
    clear_farray(new->greenfn, new->n_k);
    new->rhohat = malloc(sizeof(M_complex)*new->n_k);
    clear_carray(new->rhohat, new->n_k);

    return new;
}

void m_ewald_free(M_ewald *ewald) {
    if ( ewald != NULL ) {
        free(ewald->greenfn);
        free(ewald->rhohat);
        free(ewald);
        ewald=NULL;
    }
}

void m_ewald_reset(M_ewald *ewald) {
    free(ewald->greenfn);
    ewald->greenfn = malloc(sizeof(prec)*ewald->n_k);
    clear_farray(ewald->greenfn, ewald->n_k);
    free(ewald->rhohat);
    ewald->rhohat = malloc(sizeof(M_complex)*ewald->n_k);
    clear_carray(ewald->rhohat, ewald->n_k);
}

M_ewald* m_ewald_copy(M_ewald *in) {
    M_ewald *out = m_ewald_new(in->eta, in->dims, in->npts);

    out->U_coulomb  = in->U_coulomb;
    out->U_real     = in->U_real;
    out->U_recip    = in->U_recip;
    out->U_self     = in->U_self;
    out->U_mol_self = in->U_mol_self;
    out->eta        = in->eta;
    out->dims       = in->dims;
    out->kmax[0]    = in->kmax[0];
    out->kmax[1]    = in->kmax[1];
    out->kmax[2]    = in->kmax[2];
    out->npts[0]    = in->npts[0];
    out->npts[1]    = in->npts[1];
    out->npts[2]    = in->npts[2];
    out->n_k        = in->n_k;

    int i;
    for(i=0; i<in->n_k; i++) {
        out->greenfn[i] = in->greenfn[i];
        out->rhohat[i] = in->rhohat[i];
    }


    return out;
}

M_vector m_ewald_get_k(M_uint k_id, M_ewald *ewald) {

    M_uint *k_id_l = get_id_list(k_id, ewald->npts, 3);

    M_vector k;
    k.x = ((int) k_id_l[0] - ewald->kmax[0])*2*pi/ewald->dims->x;
    k.y = ((int) k_id_l[1] - ewald->kmax[1])*2*pi/ewald->dims->y;
    k.z = ((int) k_id_l[2] - ewald->kmax[2])*2*pi/ewald->dims->z;

    free(k_id_l);
    return k;
}


M_ewald* m_ewald_calc_frame(M_system *sys) {

    M_ewald *ewald = m_ewald_new(sys->eta, &(sys->dims), sys->nk);

    // compute Green's function
    // g(kx,ky,kz) = 4pi/|k|^2 * exp( -|k|^2/(4 eta^2))

    M_uint k_id;

    // k_id_l incrementally cycles from (0,0,0) to
    //                                  (2*k_max[0]+1,2*k_max[1]+1,2*k_max[2]+1)
    for ( k_id = 0; k_id < ewald->n_k; k_id++ ) {

        M_vector k = m_ewald_get_k(k_id, ewald);

        prec k2 = m_vector_dot(k,k);

        if ( k2 > 0 ) {
            ewald->greenfn[k_id] = 4 * pi/k2 *
                                   exp(-1.0*k2/(4*ewald->eta*ewald->eta));
        }
    }

    int i,j;
    for (i=0; i<sys->atoms->count; i++) {
        M_atom *atom_i = m_sys_atom(sys,i);

        // no need to do anything if the atom is neutral
        if ( atom_i->spec->charge == 0 ) continue;

        // do the short-range real-space part
        //
        // U_real = Sum_{pairs} q_i * q_j * erfc(eta * r_ij)/r_ij
        //
        M_parray *pairs = m_cells_get_objs_pos( sys->cell_atoms, 
                                                &(atom_i->pos) );
        for ( j=0; j<pairs->count; j++ ) {
            M_atom *atom_j = pairs->data[j];

            if ( atom_j->spec->charge == 0 ) continue;
            if ( atom_i->id >= atom_j->id ) continue;
            if ( atom_i->molec->id == atom_j->molec->id ) continue;

            // only if they are in different molecules

            M_vector dr = m_radius(atom_i->pos, atom_j->pos);
            prec r2 = m_vector_mag2(dr);

            if ( r2 < sys->rcut2 || sys->no_cutoff ) {
                prec r = sqrt(r2);
                ewald->U_real += atom_i->spec->charge * atom_j->spec->charge *
                                 erfc(ewald->eta*r) / r;
            }
        }
        m_parray_free(pairs);

        // compute the reciprocal-space part of the charge density
        //
        // rhohat(kx,ky,kz) = Sum_{atoms} q(i)*exp(ikr)

        for ( k_id = 0; k_id < ewald->n_k; k_id++ ) {
            M_vector k = m_ewald_get_k(k_id, ewald);

            M_complex ikr = I * (k.x*atom_i->pos.x + k.y*atom_i->pos.y +
                                 k.z*atom_i->pos.z);

            ewald->rhohat[k_id] += cexp(ikr)*atom_i->spec->charge;

        }

        // U_self = - eta/sqrt(pi) Sum_{atoms} q_i^2
        ewald->U_self += atom_i->spec->charge*atom_i->spec->charge;

    }
    ewald->U_self *= -1.0 * ewald->eta / sqrt_pi;

    // U_mol_self = - \sum_{molecules} \sum_{j<k} q_j*q_k/(r_jk) * erf(eta*r_jk)
    for (i=0; i<sys->molecs->count; i++) {
        M_molec *mol = m_sys_molec(sys,i);
        for (j=0; j<mol->atoms->count-1; j++) {
            M_atom *atom_j = m_molec_atom(mol,j);
            if ( atom_j->spec->charge == 0 ) continue;

            int k;
            for (k=j+1; k<mol->atoms->count; k++) {
                M_atom *atom_k = m_molec_atom(mol,k);
                if ( atom_k->spec->charge == 0 ) continue;

                M_vector dr = m_radius(atom_j->pos, atom_k->pos);
                prec r = m_vector_mag(dr);

                ewald->U_mol_self += atom_j->spec->charge * atom_k->spec->charge
                                     * erf( ewald->eta * r ) / r;
            }
        }
    }
    ewald->U_mol_self *= -1.0;

    // U_recip = 1/(2*V) Sum_{k} greenfn(k) * |rhohat(k)|^2
    for (i=0; i<ewald->n_k; i++) {
        ewald->U_recip += ewald->greenfn[i] * 
                          creal( ewald->rhohat[i] * conj(ewald->rhohat[i]) );
    }
    ewald->U_recip *= 0.5/sys->volume;
    
    ewald->U_coulomb = ewald->U_real + ewald->U_recip + ewald->U_self +
                       ewald->U_mol_self;

    return ewald;
}

M_ewald* m_ewald_calc_add(struct _system *sys, M_point pos, prec q) {

    M_ewald *ewald = m_ewald_copy(sys->ewald);

    if ( q == 0 ) {
        return ewald;
    }

    int i,j;
    M_uint k_id;

    // do the short-range real-space part
    //  
    // U_real = Sum_{pairs} q * q_j * erfc(eta * r_ij)/r_ij
    //  
    M_parray *pairs = m_cells_get_objs_pos( sys->cell_atoms, &(pos) );
    for ( j=0; j<pairs->count; j++ ) { 
        M_atom *atom_j = pairs->data[j];

        if ( atom_j->spec->charge == 0 ) continue;

        M_vector dr = m_radius(pos, atom_j->pos);
        prec r2 = m_vector_mag2(dr);

        if ( r2 < sys->rcut2 || sys->no_cutoff ) { 
            prec r = sqrt(r2);
            ewald->U_real += q * atom_j->spec->charge * erfc(ewald->eta*r) / r;
        }   
    }   
    m_parray_free(pairs);

    // compute the reciprocal-space part of the charge density
    //  
    // rhohat(kx,ky,kz) = Sum_{atoms} q*exp(ikr)

    for ( k_id = 0; k_id < ewald->n_k; k_id++ ) { 
        M_vector k = m_ewald_get_k(k_id, ewald);

        M_complex ikr = I * (k.x*pos.x + k.y*pos.y +
                             k.z*pos.z);

        ewald->rhohat[k_id] += cexp(ikr) * q;

    }

    // U_self = - eta/sqrt(pi) Sum_{atoms} q_i^2
    ewald->U_self += ( -1.0 * q * q * ewald->eta / sqrt_pi );

    // U_mol_self = - \sum_{molecules} \sum_{j<k} q_j*q_k/(r_jk) * erf(eta*r_jk)
    // this is zero, as the inserted atom is not part of a molecule

    // U_recip = 1/(2*V) Sum_{k} greenfn(k) * |rhohat(k)|^2
    // U_recip must be re-calculated from greenfn(k) and the updated rhohat(k)
    ewald->U_recip = 0;
    for (i=0; i<ewald->n_k; i++) {
        ewald->U_recip += ewald->greenfn[i] *
                          creal( ewald->rhohat[i] * conj(ewald->rhohat[i]) );
    }
    ewald->U_recip *= 0.5/sys->volume;

    ewald->U_coulomb = ewald->U_real + ewald->U_recip + ewald->U_self +
                       ewald->U_mol_self;

    return ewald;
}

M_ewald* m_ewald_calc_del(struct _system *sys, M_atom *atom) {
    M_ewald *ewald = m_ewald_copy(sys->ewald);
    if ( atom->spec->charge == 0 ) {
        return ewald;
    }

    int i,j;
    M_uint k_id;

    // do the short-range real-space part
    //  
    // U_real = Sum_{pairs} q_i * q_j * erfc(eta * r_ij)/r_ij
    //  
    M_parray *pairs = m_cells_get_objs_pos( sys->cell_atoms, &(atom->pos) );
    for ( j=0; j<pairs->count; j++ ) { 
        M_atom *atom_j = pairs->data[j];

        if ( atom_j->spec->charge == 0 ) continue;
        if ( atom_j == atom ) continue;

        M_vector dr = m_radius(atom->pos, atom_j->pos);
        prec r2 = m_vector_mag2(dr);

        if ( r2 < sys->rcut2 || sys->no_cutoff ) { 
            prec r = sqrt(r2);
            ewald->U_real -= atom->spec->charge * atom_j->spec->charge
                             * erfc(ewald->eta*r) / r;
        }   
    }   
    m_parray_free(pairs);

    // compute the reciprocal-space part of the charge density
    //  
    // rhohat(kx,ky,kz) = Sum_{atoms} q*exp(ikr)

    for ( k_id = 0; k_id < ewald->n_k; k_id++ ) { 
        M_vector k = m_ewald_get_k(k_id, ewald);

        M_complex ikr = I * (k.x*atom->pos.x + k.y*atom->pos.y +
                             k.z*atom->pos.z);

        ewald->rhohat[k_id] -= cexp(ikr) * atom->spec->charge;

    }

    // U_self = - eta/sqrt(pi) Sum_{atoms} q_i^2
    ewald->U_self -= ( -1.0 * atom->spec->charge * atom->spec->charge
                       * ewald->eta / sqrt_pi );

    // U_mol_self = - \sum_{molecules} \sum_{j<k} q_j*q_k/(r_jk) * erf(eta*r_jk)
    M_molec *mol = atom->molec;
    M_atom *atom_j = atom;
    int k;
    for (k=0; k<mol->atoms->count; k++) {
        M_atom *atom_k = m_molec_atom(mol,k);
        if ( atom_k->spec->charge == 0 ) continue;
        if ( atom_k == atom_j ) continue;

        M_vector dr = m_radius(atom_j->pos, atom_k->pos);
        prec r = m_vector_mag(dr);

        ewald->U_mol_self -= ( -1.0 * atom_j->spec->charge * atom_k->spec->charge
                             * erf( ewald->eta * r ) / r );
    } 


    // U_recip = 1/(2*V) Sum_{k} greenfn(k) * |rhohat(k)|^2
    // U_recip must be re-calculated from greenfn(k) and the updated rhohat(k)
    ewald->U_recip = 0;
    for (i=0; i<ewald->n_k; i++) {
        ewald->U_recip += ewald->greenfn[i] *
                          creal( ewald->rhohat[i] * conj(ewald->rhohat[i]) );
    }
    ewald->U_recip *= 0.5/sys->volume;

    ewald->U_coulomb = ewald->U_real + ewald->U_recip + ewald->U_self +
                       ewald->U_mol_self;

    return ewald;
}


prec m_ewald_isolated(M_system *sys, prec Q) {
    // Calculates the energy of an isolated ion with the system's Ewald settings
    // This is the energy of an isolated ion embedded in a continuum of counter-charge
    // with periodic images at the same volume as sys

    if ( Q == 0 ) {
        return 0.0;
    }

    // U_self
    prec U_iso = -1.0 * sys->ewald->eta * Q * Q / sqrt_pi;

    // U_recip, note that |\rho(k)|^2 = Q^2 since \rho(k) is a vector of length Q in the
    // complex space. Therefore,:
    //           Q * Q
    // U_recip = -----  Sum_{k}  greenfn(k)
    //            2V

    prec U_recip = 0;
    int i;
    for (i=0; i<sys->ewald->n_k; i++) {
        U_recip += sys->ewald->greenfn[i];
    }
    U_recip *= Q * Q * 0.5 / sys->volume; 

    return U_iso + U_recip;
}


void m_coulomb_load_config(M_system *sys, dictionary *dict) {

    sys->calc_ewald = iniparser_getboolean(dict, "system:calc_ewald", 0);

    if ( sys->calc_ewald ) {
        if ( ! sys->pop_atom_cell ) {
            FATAL("calc_ewald under [system] is set to true.\n");
            FATAL("This requires pop_atom_cell = true.\n");
            exit(1);
        }

        sys->eta = iniparser_getdouble(dict, "system:eta", -1.0);
        if ( ! sys->eta > 0 ) {
            FATAL("calc_ewald under [system] is set to true.\n");
            FATAL("This requires eta > 0.\n");
            exit(1);
        }

        char *in;
        in = iniparser_getstring(dict, "system:nk", NULL);
        if ( in == NULL ) {
            FATAL("calc_ewald under [system] is set to true.\n");
            FATAL("This requires nk as three comma-separated values.\n");
            exit(1);
        } else {
            parse_csv_uiarray( sys->nk, in, 3 );
        }
    }
}
