#include "headers/molecules.h"

void calc_densities(M_system *sys) {

    // get the volume of the box
    sys->volume = sys->dims.x * sys->dims.y * sys->dims.z;

    // get the density of each molec_spec
    M_uint i;
    M_molec_spec *spec;
    for ( i=0; i < sys->molec_specs->count; i++ ) { 
      spec = m_array_elt_pos(sys->molec_specs,i);
      spec->density = ((float)spec->molecs->count)/(sys->volume);
    }   

}

void calc_dipoles(M_system *sys) {

  calc_centers(sys);
  
  int i;
  for (i=0; i<sys->molecs->count; i++) {
    M_molec *mol = m_sys_molec(sys,i);
    
    calc_dipole(mol);
  
  }

}


void calc_dipole(M_molec *mol) {

  M_atom *atom;

  mol->dipole.x=0;
  mol->dipole.y=0;
  mol->dipole.z=0;

  int i;
  prec q;
  M_vector dr;
  for (i=0; i<mol->atoms->count; i++) {
    atom=m_molec_atom(mol,i);
    q = atom->spec->charge;

    dr = m_radius(atom->pos, mol->center);
    // the dipole vector points from -ve to +ve
    // \vec{\mu} = \sum_i q_i * (r_i - r_center)
    //      dr points in the direction of center-->i
    //     +ve charge: dr*q points towards +ve charge
    //     -ve charge: dr*q points away from -ve charge
    mol->dipole = m_vector_add(mol->dipole, m_vector_scale(dr,q) );

  }
  
  mol->dip_mag = m_vector_mag(mol->dipole);

}

void calc_centers(M_system *sys) {

  // first check that we can calculate centers of mass
  int aspeci,mspeci;
  for ( mspeci=0; mspeci<sys->molec_specs->count; mspeci++ ) {
    M_molec_spec *mspec = m_sys_molec_spec(sys,mspeci);
    if ( mspec->center < 0 && mspec->mass <= 0 ) {
      // the molecule's mass is zero, or its default negative value
      // cannot calculate center of mass
      // calculate geometric center rather than center of mass
      // set all atom_spec's mass = 1
      for (aspeci=0; aspeci<mspec->atom_specs->count; aspeci++ ){
        M_atom_spec *aspec = m_mspec_aspec(mspec, aspeci);
        aspec->mass = 1.0;
      }
    }
  }

  // calculate centers of mass / geometric centers
  int i;
  for (i=0; i<sys->molecs->count; i++) {
    M_molec *mol = m_sys_molec(sys,i);
    
    calc_center(mol);
  
  }
}

void calc_center(M_molec *mol) {

  if ( mol->spec->center >= 0 ) {
  
    M_atom *atom;
    atom = m_molec_atom(mol,mol->spec->center);
    
    mol->center = m_point_copy( &atom->pos );

  } else {
  
    M_molec *flat_mol = m_molec_flatten_new( mol );

    mol->center = m_molec_com(flat_mol);

    m_molec_free(flat_mol);
    
  }
}

void calc_center_vels(M_system *sys) {
    int i;
    for (i=0; i<sys->molecs->count; i++) {
        M_molec *mol = m_sys_molec(sys, i);
        calc_center_vel(mol);
    }
}

void calc_center_vel(M_molec *mol) {
    int i;
    for (i=0; i<mol->atoms->count; i++ ) {
        M_atom *atom = m_molec_atom(mol, i);
        mol->vel = m_vector_add(mol->vel,
                                m_vector_scale(atom->vel,atom->spec->mass));
    } 
    mol->vel = m_vector_scale(mol->vel, (prec) 1.0/mol->spec->mass);
}

M_molec *m_molec_flatten_new(M_molec *molec) {
    M_molec *new = m_molec_copy(molec);
    
    m_molec_flatten(new);
    
    return new;
}

void m_molec_flatten(M_molec *molec) {
    M_point prev = m_molec_atom(molec, 0)->pos;
    M_point d;
    M_vector v;
    int i;
    for (i = 0; i < molec->atoms->count; i++ ) {
        d = m_molec_atom(molec, i)->pos;
        v = m_radius(d, prev);
        m_point_trans(&prev,v);
        //prev.dims = NULL;
        m_molec_atom(molec, i)->pos = prev;
    }
}

// Note: Use a flattened molecule
M_point m_molec_com (M_molec* molec) {
    int i;
    
    // x_com = (\sum_i m_i x_i) / (\sum_i m_i)
    //       = x_0  + ( \sum_i m_i (x_i - x_0 ) ) / (\sum_i m_i)
    //       = x_0  + ( \sum_i m_i dx_i ) / (\sum_i m_i)

    prec masses = 0;

    M_atom *a;
    a = m_molec_atom(molec,0);

    masses += a->spec->mass; // start accumulating the total mass
    M_point com = m_point_copy( &a->pos ); // set x_com = x_0
    
    M_vector c = {.x = 0, .y = 0, .z = 0}; // running sum
    
    M_vector dr;
    M_atom_spec *aspec_i;
    M_atom      *atom_i;
    for ( i=1; i<molec->atoms->count; i++ ) {
      atom_i = m_molec_atom(molec,i);
      aspec_i = atom_i->spec;
      
      dr = m_radius(atom_i->pos,a->pos);

      c = m_vector_add(c, m_vector_scale(dr, aspec_i->mass));
      
      masses += aspec_i->mass;
    }
    
    c = m_vector_scale(c, (prec) 1.0/masses );
    
    m_point_trans( &com, c );
    
    return com;
    
}

// Note: Use a flattened molecule
prec rg(M_molec *m) {
    prec rg, u;
    M_atom *a, *b;
    prec sum = 0.0;
    int i, j;
    for ( i = 0; i < m->atoms->count; i++ ) {
        for ( j = 0; j < m->atoms->count; j++ ) {
            if ( i != j ) {
                a = m->atoms->data[i];
                b = m->atoms->data[j];
                //M_vector *ad = a->pos.dims; 
                //M_vector *bd = b->pos.dims; 
                u = m_vector_mag( m_radius(a->pos, b->pos));
                sum += u*u;
            }
        }
    }
    rg = (sum / ( 2.0 * ( (prec)m->atoms->count * (prec)m->atoms->count) ) );
    return rg;
}


M_neighbours *m_nearest_neighbours_new( int n_max, prec default_rank) {
  M_neighbours *new = malloc(sizeof(M_neighbours));
  new->n_max = n_max;
  new->min_rank = default_rank;
  new->ranks = malloc(sizeof(prec) * n_max);
  new->molecs = m_parray_new( 0 );
  new->dummy = malloc(sizeof(prec) * n_max);
  clear_farray(new->ranks, n_max);
  clear_farray(new->dummy, n_max);

  return new;
}

void m_nearest_neighbours_add( M_neighbours *neighbours, M_molec *new_neighbour,
                               prec rank) {
  if ( rank < neighbours->min_rank
       || neighbours->molecs->count < neighbours->n_max ) {
    int i=0;
    while (i<neighbours->molecs->count) {
      if ( rank < neighbours->ranks[i] ) {
        break;
      } 
      i++;
    } 
    
    m_parray_insert(neighbours->molecs, new_neighbour, i);
    while ( neighbours->molecs->count > neighbours->n_max ) {
      m_parray_delete(neighbours->molecs, neighbours->n_max);
    } 
    
    
    prec *new_rank = malloc(sizeof(prec) * neighbours->n_max);
    new_rank[i] = rank;
    prec *new_dummy = malloc(sizeof(prec) * neighbours->n_max);
    new_dummy[i] = 0.0;
    int j=0;
    
    while (j<neighbours->molecs->count) {
      if (j<i) {
        new_rank[j]=neighbours->ranks[j];
        new_dummy[j]=neighbours->dummy[j];
      } else if (j>i) {
        new_rank[j]=neighbours->ranks[j-1];
        new_dummy[j]=neighbours->dummy[j-1];
      } 
      j++;
    } 

    free(neighbours->ranks);
    free(neighbours->dummy);
    neighbours->ranks = new_rank;
    neighbours->dummy = new_dummy;

    neighbours->min_rank = new_rank[neighbours->molecs->count-1];
  }
}

void m_nearest_neighbours_append( M_neighbours *neighbours,
                                  M_molec *new_neighbour ) {

    m_parray_push(neighbours->molecs, new_neighbour);

    neighbours->n_max=neighbours->molecs->count;

    prec *new_rank = realloc(neighbours->ranks,
                             sizeof(prec) * neighbours->n_max);
    if ( !new_rank ) {
      FATAL("Reallocation failed.");
      exit(1);
    }
    new_rank[neighbours->n_max-1] = 0;
    neighbours->ranks = new_rank;
    
    prec *new_dummy = realloc(neighbours->dummy,
                              sizeof(prec) * neighbours->n_max);
    if ( !new_dummy ) {
      FATAL("Reallocation failed.");
      exit(1);
    }
    new_dummy[neighbours->n_max-1] = 0;
    neighbours->dummy = new_dummy;
    
}

void m_nearest_neighbours_free( M_neighbours *neighbours ) {
    if ( neighbours != NULL ) {
        m_parray_free(neighbours->molecs);
        free(neighbours->ranks);
        free(neighbours->dummy);
        free(neighbours);
    }
}

void m_nearest_neighbours_print( M_neighbours *neighbours ) {
  int k;
  printf("n: %d/%d\t min_rank: %f\tlist: ",neighbours->molecs->count,
                                           neighbours->n_max,
                                           neighbours->min_rank);
  for( k=0; k<neighbours->molecs->count; k++ ) {
      M_molec *test;
      test=neighbours->molecs->data[k];
      printf("(m_id: %d, rank: %f, dummy: %f), ",test->id,neighbours->ranks[k],
                                                 neighbours->dummy[k]);
  }   
  printf("\n");
}

void m_molecules_load_config(M_system *sys, dictionary *dict) {
    sys->calc_dipoles = iniparser_getboolean(dict,"system:calc_dipoles",0);
    sys->calc_centers = iniparser_getboolean(dict,"system:calc_centers",0);
}


/*
 * Returns the moments of inertia and the eigen values of the 
 * moment of inertia tensor of the molecule
 *
 * `moments` must be large enough to hold 3 vectors
 * `eigen` must be large enough to hold 3 doubles
 *
 * Eg:
 *      double eigen_values[3];
 *      M_vector moments[3];
 *      m_molec_mit(molecule, &moments, &eigen_values);
 *
 *      // print principle moment
 *      m_puts_vector(moments[0]);
 */
//void m_molec_mit(M_molec *molec, M_vector *moments, double *eigen) {
//    double aa, bb, cc, ab, ac, bc, x, y, z;
//    M_vector v1, v2, v3;
//    M_point com;
//    aa = 0; bb = 0; cc = 0;
//    ab = 0; ac = 0; bc = 0;
//    m_molec_flatten(molec);
//    com = m_molec_com(molec);
//    M_atom *atom;
//    int i;
//    for(i = 0; i < molec->atoms->count; i++){
//        atom = m_molec_atom(molec, i);
//        x = -atom->pos.x  + com.x; 
//        y = -atom->pos.y  + com.y; 
//        z = -atom->pos.z + com.z;
//        aa += y*y + z*z;
//        bb += x*x + z*z;
//        cc += x*x + y*y;
//        ab += -x*y;
//        ac += -x*z;
//        bc += -y*z;
//    }
//
//    MKL_INT n = 3, lda = 3, info;
//
//    double matrix[9] = {
//        aa, ab, ac,
//        ab, bb, bc,
//        ac, bc, cc
//    };
//
//    double w[3];
//    info = LAPACKE_dsyevd(LAPACK_COL_MAJOR, 'V', 'U', n, matrix, lda, eigen);
//    if(info >0) {
//        exit(1);
//    }
//
//    // Each column of the matrix is now an eigen vector
//    moments[0] = (M_vector){ 
//            .x = matrix[0],
//            .y = matrix[3],
//            .z = matrix[6]
//    };
//
//    moments[1] = (M_vector){ 
//            .x = matrix[1],
//            .y = matrix[4],
//            .z = matrix[7]
//    };
//
//    moments[2] = (M_vector){ 
//            .x = matrix[2],
//            .y = matrix[5],
//            .z = matrix[8]
//    };
//}
