#include "headers/cell.h"
#include "headers/db.h"

M_cells *m_cells_new(M_uint *ngrid, M_vector *dims, prec rcut) {
    M_cells *new = (M_cells *) malloc( sizeof(M_cells) );

    new->ngrid[0] = ngrid[0];
    new->ngrid[1] = ngrid[1];
    new->ngrid[2] = ngrid[2];
    new->ngridpts = ngrid[0]*ngrid[1]*ngrid[2];

    new->occupancy = malloc( sizeof(M_parray * ) * new->ngridpts );

    int i;
    for (i=0; i<new->ngridpts; i++) {
        new->occupancy[i] = m_parray_new( 0 );
    }
    
    new->dgrid.x = (prec) dims->x/ngrid[0];
    new->dgrid.y = (prec) dims->y/ngrid[1];
    new->dgrid.z = (prec) dims->z/ngrid[2];

    new->rcut_grid_width[0] = (M_uint) ceil( rcut / new->dgrid.x );
    new->rcut_grid_width[1] = (M_uint) ceil( rcut / new->dgrid.y );
    new->rcut_grid_width[2] = (M_uint) ceil( rcut / new->dgrid.z );

    return new;
}

M_uint *m_cells_get_cell_coords(M_cells *cell, M_point *pos) {
    M_uint *cell_coord = malloc(sizeof(M_uint) * 3);

    cell_coord[0] = (M_uint) pmod( (int) floor(pos->x/cell->dgrid.x),
                                         cell->ngrid[1]);
    cell_coord[1] = (M_uint) pmod( (int) floor(pos->y/cell->dgrid.y),
                                         cell->ngrid[2]);
    cell_coord[2] = (M_uint) pmod( (int) floor(pos->z/cell->dgrid.z),
                                         cell->ngrid[2]);
    return cell_coord;
}

M_cells *m_cells_populate_atom_occupancy(M_system *sys) {
    M_cells *cell = m_cells_new(sys->cell_ngrid, &(sys->dims), sys->rcut);

    int i;
    for (i=0; i<sys->atoms->count; i++) {
        M_atom *atom = m_sys_atom(sys, i);
        M_uint *grid = m_cells_get_cell_coords(cell, &(atom->pos));
        M_uint  grid_id = get_id( grid, cell->ngrid, 3);
        free(grid);

        m_parray_push( cell->occupancy[grid_id], atom );
    }

    return cell;
}

M_cells *m_cells_populate_molec_occupancy(M_system *sys) {
    M_cells *cell = m_cells_new(sys->cell_ngrid, &(sys->dims), sys->rcut);

    int i;
    for (i=0; i<sys->molecs->count; i++) {
        M_molec *molec = m_sys_molec(sys, i);
        M_uint  *grid = m_cells_get_cell_coords(cell, &(molec->center));
        M_uint   grid_id = get_id( grid, cell->ngrid, 3);
        free(grid);

        m_parray_push( cell->occupancy[grid_id], molec );
    }

    return cell;
}

M_parray *m_cells_get_objs_cell(M_cells *cell, M_uint *cell_coord) {
    int ii, jj, kk;
    int l;
    M_uint grid[3];
    M_uint grid_id;

    M_parray *out = m_parray_new( 0 );

    ii=0;
    // loop over atoms within the r_cut cube
    //
    // in arithmetic.c:
    // pspan ensures the looping index never counts the same cell twice by
    //    limiting the maximum span of the index to the number of grid points
    // pmod applies periodic boundary conditions to the looping indices
    //
    // for i in [ -rcut_grid_width[0], rcut_grid_width[0] ]
    while (ii<pspan(cell->rcut_grid_width[0]*2+1,cell->ngrid[0])) {
        grid[0] = pmod( (ii + cell_coord[0] - cell->rcut_grid_width[0]),
                        cell->ngrid[0]);
        jj=0;
        // for j in [ -rcut_grid_width[1], rcut_grid_width[1] ]
        while (jj<pspan(cell->rcut_grid_width[1]*2+1,cell->ngrid[1])) {
            grid[1] = pmod( (jj + cell_coord[1] - cell->rcut_grid_width[1]),
                            cell->ngrid[1]);
            kk=0;
            // for k in [ -rcut_grid_width[2], rcut_grid_width[2] ]
            while (kk<pspan(cell->rcut_grid_width[2]*2+1,cell->ngrid[2])) {
                grid[2] = pmod( (kk + cell_coord[2] - cell->rcut_grid_width[2]),
                                cell->ngrid[2]);

                // the array index of this cell is:
                grid_id = get_id( grid, cell->ngrid, 3 );
              
                for (l=0; l<cell->occupancy[grid_id]->count; l++) {
                    m_parray_push(out, cell->occupancy[grid_id]->data[l]);
                }
                kk++;
            }
            jj++;
        }
        ii++;
    }

    return out;
}

M_parray *m_cells_get_objs_pos(M_cells *cell, M_point *pos) {

      M_uint *cell_coord = m_cells_get_cell_coords(cell, pos);

      M_parray *out = m_cells_get_objs_cell(cell, cell_coord);

      free(cell_coord);

      return out;
}

void m_cells_reset(M_cells *cell) {
    int i;
    for (i=0; i<cell->ngridpts; i++) {
        if ( cell->occupancy[i] != NULL ) {
            m_parray_free(cell->occupancy[i]);
        }
        cell->occupancy[i] = m_parray_new( 0 );
    }
}

void m_cells_free(M_cells *cell) {
    if ( cell != NULL ) {
        int i;
        for (i=0; i<cell->ngridpts; i++) {
            if ( cell->occupancy[i] != NULL ) {
                m_parray_free(cell->occupancy[i]);
            }
        }
        free(cell->occupancy);
        cell->occupancy = NULL;
        free(cell);
        cell = NULL;
    }
}

M_cells *m_cells_copy(M_cells *cell) {
    M_cells *new = m_cells_new(cell->ngrid,&(M_ZERO_VEC),0);

    new->dgrid.x = cell->dgrid.x;
    new->dgrid.y = cell->dgrid.y;
    new->dgrid.z = cell->dgrid.z;

    new->rcut_grid_width[0] = cell->rcut_grid_width[0];
    new->rcut_grid_width[1] = cell->rcut_grid_width[1];
    new->rcut_grid_width[2] = cell->rcut_grid_width[2];

    int i,j;
    for (i=0; i<new->ngridpts; i++) {
        for (j=0; j<cell->occupancy[i]->count; j++) {
            m_parray_push(new->occupancy[i], cell->occupancy[i]->data[j]);
        }
    }
    
    return new;
}

int m_cells_tally(M_cells *cell) {
    int i;
    int counter = 0;

    for (i=0; i<cell->ngridpts; i++) {
        if ( cell->occupancy[i] == NULL ) {
            return -1; // occupancy array uninitialised
        }
        counter += cell->occupancy[i]->count;
    }
    return counter;
}

void m_cells_load_config(M_system *sys, dictionary *dict) {

    char *in;

    sys->pop_atom_cell = iniparser_getboolean(dict,"system:pop_atom_cell",0);
    sys->pop_mol_cell  = iniparser_getboolean(dict,"system:pop_mol_cell",0);

    in = iniparser_getstring(dict, "system:cell_ngrid",NULL);
    if ( in == NULL && ( sys->pop_atom_cell || sys->pop_mol_cell ) ) {
        FATAL("cell_ngrid under [system] must be specified\n");
        FATAL("as three comma separated integers.\n");
        exit(1);
    } else if ( sys->pop_mol_cell && !(sys->calc_centers) ) {
        FATAL("pop_mol_cell under [system] requires calc_centers = true.\n");
        exit(1);
    } else if ( in != NULL ) {
        parse_csv_uiarray(sys->cell_ngrid, in, 3);
    }
}
