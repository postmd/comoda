#include "headers/array.h"

const char trimdelims[3] = " \t\n";
const int n_trimdelims = 3;

void clear_barray( bool *dest, int nfield ) {
  set_barray(dest, 0, nfield);
}

void clear_uiarray( M_uint *dest, int nfield ) {
  set_uiarray(dest, 0, nfield);
}

void clear_iarray( int *dest, int nfield ) {
  set_iarray(dest, 0, nfield);
}

void clear_farray( prec *dest, int nfield ) {
  set_farray(dest, 0, nfield);
}

void clear_carray( M_complex *dest, int nfield) {
  M_complex zero = 0.0 + 0.0 * I;
  set_carray(dest, zero, nfield);
}

void set_barray( bool *dest, bool val, int nfield ) {
  int i;
  for (i=0; i<nfield; i++ ) {
    dest[i] = val;
  }
}

void set_uiarray( M_uint *dest, M_uint val, int nfield ) {
  int i;
  for (i=0; i<nfield; i++ ) {
    dest[i] = val;
  }
}

void set_iarray( int *dest, int val, int nfield ) {
  int i;
  for (i=0; i<nfield; i++ ) {
    dest[i] = val;
  }
}

void set_farray( prec *dest, prec val, int nfield ) {
  int i;
  for (i=0; i<nfield; i++ ) {
    dest[i] = val;
  }
}

void set_carray( M_complex *dest, M_complex val, int nfield ) {
  int i;
  for (i=0; i<nfield; i++ ) {
    dest[i] = val;
  }
}

void parse_iarray( int *dest, char *in, int nfield ) {
  int i;
  char *temp;

  char *trimmed = trim_delims(in, trimdelims, n_trimdelims);;

  temp = strtok(trimmed, ",");
  dest[0] = atoi(temp);
  for (i=1; i<nfield; i++) {
    temp = strtok(NULL, ",");
    if ( temp == NULL ) {
      FATAL("Input string '%s' has fewer fields than expected.\n",in);
      FATAL("Expecting %i fields, found %i fields.\n",nfield,i);
      exit(1);
    }
    dest[i] = atoi(temp);
  }

  free(trimmed);
}

void parse_uiarray( M_uint *dest, char *in, int nfield ) {
  int i;
  char *temp;

  char *trimmed = trim_delims(in, trimdelims, n_trimdelims);;

  temp = strtok(trimmed, ",");
  dest[0] = (M_uint) atoi(temp);
  for (i=1; i<nfield; i++) {
    temp = strtok(NULL, ",");
    if ( temp == NULL ) {
      FATAL("Input string '%s' has fewer fields than expected.\n",in);
      FATAL("Expecting %i fields, found %i fields.\n",nfield,i);
      exit(1);
    }
    dest[i] = (M_uint) atoi(temp);
  }

  free(trimmed);
}

void parse_farray( prec *dest, char *in, int nfield) {
  int i;
  char *temp;

  char *trimmed = trim_delims(in, trimdelims, n_trimdelims);;

  temp = strtok(trimmed, ",");
  dest[0] = atof(temp);
  for (i=1; i<nfield; i++) {
    temp = strtok(NULL, ",");
    if ( temp == NULL ) {
      FATAL("Input string '%s' has fewer fields than expected.\n",in);
      FATAL("Expecting %i fields, found %i fields.\n",nfield,i);
      exit(1);
    }
    dest[i] = atof(temp);
  }

  free(trimmed);
}

void parse_strarray( char **dest, char *in, int nfield ) {
  int i;
  char *temp;

  char *trimmed = trim_delims(in, trimdelims, n_trimdelims);;

  temp = strtok(trimmed, ",");
  dest[0] = temp;
  for (i=1; i<nfield; i++) {
    temp = strtok(NULL, ",");
    if ( temp == NULL ) {
      FATAL("Input string '%s' has fewer fields than expected.\n",in);
      FATAL("Expecting %i fields, found %i fields.\n",nfield,i);
      exit(1);
    }
    dest[i] = temp;
  }
}

int increment_id_list( M_uint *id_list, M_uint *length, int nfield) {

  int i;
  for ( i=nfield-1; i>=0; i-- ) {
    id_list[i]++;

    if ( id_list[i] < length[i] ) {
      return 1;
    } else {
      id_list[i] = 0;
    }
  }
  return 0;

}

M_uint get_id( M_uint *id_list, M_uint *length, int nfield) {
  int i;
  M_uint id = 0;
  for ( i=1; i<nfield; i++ ) {
    id += id_list[i-1];
    id *= length[i];
  }
  id += id_list[nfield-1];

  return id;
}

M_uint *get_id_list( M_uint id, M_uint *length, int nfield) {
  int i;

  M_uint *id_list = malloc(sizeof(M_uint)*nfield);
  clear_uiarray(id_list,nfield);

  for (i=nfield-1; i>=0; i--) {
    id_list[i] = id % length[i];
    id -= id_list[i];
    id /= length[i];
  }

  return id_list;

}

bool *barray_copy( bool *a, int n ) {
  bool *b = malloc(sizeof(bool) * n);

  int i;
  for ( i=0; i<n; i++ ) {
    b[i] = a[i];
  }

  return b;
}

M_uint *uiarray_copy( M_uint *a, int n ) {
  M_uint *b = malloc(sizeof(M_uint) * n);

  int i;
  for ( i=0; i<n; i++ ) {
    b[i] = a[i];
  }

  return b;
}

int *iarray_copy( int *a, int n ) {
  int *b = malloc(sizeof(int) * n);

  int i;
  for ( i=0; i<n; i++ ) {
    b[i] = a[i];
  }

  return b;
}

prec *farray_copy( prec *a, int n ) {
  prec *b = malloc(sizeof(prec) * n);

  int i;
  for ( i=0; i<n; i++ ) {
    b[i] = a[i];
  }

  return b;
}

M_complex *carray_copy( M_complex *a, int n ) {
  M_complex *b = malloc(sizeof(M_complex) * n);

  int i;
  for ( i=0; i<n; i++ ) {
    b[i] = a[i];
  }

  return b;
}

bool sum_barray( bool *a, int n ) {
  int i;
  for ( i=0; i<n; i++ ) {
    if ( ! a[i] ) { 
      return 0;
    }
  }

  return 1;
}

int sum_iarray( int *a, int n ) {
  int b = 0;
  int i;
  for ( i=0; i<n; i++ ) {
    b += a[i];
  }

  return b;
}

M_uint sum_uiarray( M_uint *a, int n ) {
  M_uint b = 0;
  int i;
  for ( i=0; i<n; i++ ) {
    b += a[i];
  }

  return b;
}

prec sum_farray( prec *a, int n ) {
  prec b = 0;
  int i;
  for ( i=0; i<n; i++ ) {
    b += a[i];
  }

  return b;
}

M_complex sum_carray( M_complex *a, int n ) {
  M_complex b = 0;
  int i;
  for ( i=0; i<n; i++ ) {
    b += a[i];
  }

  return b;
}
