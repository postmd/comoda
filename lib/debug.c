#include "headers/debug.h"

void DEBUG_barray(bool *in, int n) {
  int i;
  fprintf(stderr, WHERESTR, WHEREARG);
  for (i=0; i<n; i++) {
    fprintf(stderr, "%d\t",in[i]);
  }
  fprintf(stderr, "\n");
}

void DEBUG_iarray(int *in, int n) {
  int i;
  fprintf(stderr, WHERESTR, WHEREARG);
  for (i=0; i<n; i++) {
    fprintf(stderr, "%d\t",in[i]);
  }
  fprintf(stderr, "\n");
}

void DEBUG_farray(prec *in, int n) {
  int i;
  fprintf(stderr, WHERESTR, WHEREARG);
  for (i=0; i<n; i++) {
    fprintf(stderr, "%f\t",in[i]);
  }
  fprintf(stderr, "\n");
}

void DEBUG_carray(M_complex *in, int n) {
  int i;
  fprintf(stderr, WHERESTR, WHEREARG);
  for (i=0; i<n; i++) {
    fprintf(stderr, "%f+%fi\t",creal(in[i]),cimag(in[i]));
  }
  fprintf(stderr, "\n");
}

void DEBUG_barray_2D(bool *in, int dim1, int dim2) {
  int i,j;
  for (i=0; i<dim1; i++) {
    fprintf(stderr, WHERESTR, WHEREARG);
    fprintf(stderr, "row %d:\t", i);
    for (j=0; j<dim2; j++) {
      fprintf(stderr, "%d\t", in[j+dim1*i]);
    }
    fprintf(stderr, "\n");
  }
}

void DEBUG_iarray_2D(int *in, int dim1, int dim2) {
  int i,j;
  for (i=0; i<dim1; i++) {
    fprintf(stderr, WHERESTR, WHEREARG);
    fprintf(stderr, "row %d:\t", i);
    for (j=0; j<dim2; j++) {
      fprintf(stderr, "%d\t", in[j+dim1*i]);
    }
    fprintf(stderr, "\n");
  }
}

void DEBUG_farray_2D(prec *in, int dim1, int dim2) {
  int i,j;
  for (i=0; i<dim1; i++) {
    fprintf(stderr, WHERESTR, WHEREARG);
    fprintf(stderr, "row %d:\t", i);
    for (j=0; j<dim2; j++) {
      fprintf(stderr, "%f\t", in[j+dim2*i]);
    }
    fprintf(stderr, "\n");
  }
}

void DEBUG_barray_3D(bool *in, int dim1, int dim2, int dim3) {
  int i,j,k;
  for (i=0; i<dim1; i++) {
    for (j=0; j<dim2; j++) {
      fprintf(stderr, WHERESTR, WHEREARG);
      fprintf(stderr, "slice %d, row %d:\t", i, j);
      for (k=0; k<dim3; k++) {
        fprintf(stderr, "%d\t", in[k+dim3*j+dim3*dim2*i]);
      }
      fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
  }
}

void DEBUG_iarray_3D(int *in, int dim1, int dim2, int dim3) {
  int i,j,k;
  for (i=0; i<dim1; i++) {
    for (j=0; j<dim2; j++) {
      fprintf(stderr, WHERESTR, WHEREARG);
      fprintf(stderr, "slice %d, row %d:\t", i, j);
      for (k=0; k<dim3; k++) {
        fprintf(stderr, "%d\t", in[k+dim3*j+dim3*dim2*i]);
      }
      fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
  }
}

void DEBUG_farray_3D(prec *in, int dim1, int dim2, int dim3) {
  int i,j,k;
  for (i=0; i<dim1; i++) {
    for (j=0; j<dim2; j++) {
      fprintf(stderr, WHERESTR, WHEREARG);
      fprintf(stderr, "slice %d, row %d:\t", i, j);
      for (k=0; k<dim3; k++) {
        fprintf(stderr, "%f\t", in[k+dim3*j+dim3*dim2*i]);
      }
      fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
  }
}
