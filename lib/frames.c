#include "headers/frames.h"

int m_next_frame(M_system *sys) {
    sys->current_frame_id += 1;

    if ( sys->current_frame_id < sys->start_frame ) { 
        return 1;  // keep looping
    } else if ( sys->use_end && sys->current_frame_id > sys->end_frame ) {
        return -1; // finished all the frames required
    } else if ( (sys->current_frame_id - sys->start_frame) % (sys->skip + 1) 
                != 0 ) {
        return 1;  // keep looping to skip
    }
    return 0; // perform calculations on this frame
}

int m_read_frame_stdin_xyz(M_system *sys) {

    char *line = NULL;
    size_t line_s = 0;
    ssize_t read;

    const char delims[3] = "\n\t ";
    int n_delims = 3;

    bool chk_numsites = false;
    bool chk_box = false;
    int i = 0;

    prec ang_x, ang_y, ang_z;

    M_atom *new;
    int nsites;
    char s[8];

    while ( (read = getline(&line, &line_s, stdin )) != -1 ) {

        char *line_m = trim_consec_delims(line,delims,3,"\t");

        // do we need to check the number of atoms?
        if (! chk_numsites ) {
            // if so, check that the number of atoms specified in the XYZ file
            // is the same as the number of atoms in the system
            if ( sscanf(line_m,"%d", &nsites) == 1 
                 && nsites == sys->atoms->count ) { 
                chk_numsites = true;
            } else {
            // otherwise, something wentwrong
                free(line);
                return 0;
            }
        } 
        // do we need to get the size of the system?
        else if (! chk_box ) {
            if ( sys->constant_dims ) {
                // no need to do anything
                chk_box = true;
            } else if (sscanf(line_m,"BOX\t%lf\t%lf\t%lf", &ang_x, &ang_y, &ang_z)
                 == 3 ) {
                chk_box = true;
                sys->dims.x = ang_x/ang2au;
                sys->dims.y = ang_y/ang2au;
                sys->dims.z = ang_z/ang2au;
            } else {
                free(line);
                return 0;
            }
        }
        // do we need to read in an atomic position?
        else if (i < sys->atoms->count) {
            new = m_sys_atom(sys, i);

            char *p = line_m;

            if ( sscanf(p, "%s\t%lf\t%lf\t%lf", s, &ang_x, &ang_y, &ang_z)
                 == 4 ) {
                new->pos.x = ang_x/ang2au;
                new->pos.y = ang_y/ang2au;
                new->pos.z = ang_z/ang2au;
            } else {
                // something went wrong
                free(line);
                free(line_m);
                return 0;
            }
            if (sys->wrap_coords) {
              m_point_wrap( &new->pos );
            }

            // keep reading if we have other coordinates 
            if ( sys->read_dip || sys->read_vel || sys->read_force ) {
                p = next_n_fields(p, delims, n_delims, 4); 

                if ( sys->read_dip ) {
                    prec dip_x, dip_y, dip_z;
                    if ( p != NULL && sscanf(p, "%lf\t%lf\t%lf", &dip_x, &dip_y,
                                             &dip_z) == 3 ) {
                        new->dip.x = dip_x;
                        new->dip.y = dip_y;
                        new->dip.z = dip_z;
                        p = next_n_fields(p, delims, n_delims, 3);
                    } else {
                        free(line);
                        free(line_m);
                        return 0;
                    }
                }
                if ( sys->read_vel ) {
                    prec vel_x, vel_y, vel_z;
                    if ( p != NULL && sscanf(p, "%lf\t%lf\t%lf", &vel_x, &vel_y,
                                             &vel_z) == 3 ) {
                        new->vel.x = vel_x;
                        new->vel.y = vel_y;
                        new->vel.z = vel_z;
                        p = next_n_fields(p, delims, n_delims, 3);
                    } else {
                        free(line);
                        free(line_m);
                        return 0;
                    }
                }
                if ( sys->read_force ) {
                    prec force_x, force_y, force_z;
                    if ( p != NULL && sscanf(p, "%lf\t%lf\t%lf", &force_x,
                                             &force_y, &force_z) == 3 ) {
                        new->force.x = force_x;
                        new->force.y = force_y;
                        new->force.z = force_z;
                        p = next_n_fields(p, delims, n_delims, 3);
                    } else {
                        free(line);
                        free(line_m);
                        return 0;
                    }
                }
            }

            i++;
            free(line_m);

            if ( i == sys->atoms->count ) {
                int stat = m_next_frame(sys);
                if ( stat == 0 ) { 
                    free(line);
                    return 1;
                } else if ( stat < 0 ) {
                    free(line);
                    return 0;
                } else if (stat > 0 ) {
                    i = 0;
                    chk_numsites = 0;
                    chk_box = 0;
                }
            }
        }
    }
    free(line);
    return 0;
}

M_atom *m_atom_new_stdin (M_vector *dims) {
    M_atom *new = malloc(sizeof(M_atom));
    new->pos.dims = dims;
    if (   scanf("%lf,%lf,%lf",&new->pos.x,  &new->pos.y,  &new->pos.z) == 3
        && scanf("%lf,%lf,%lf",&new->vel.x,  &new->vel.y,  &new->vel.z) == 3
        && scanf("%lf,%lf,%lf",&new->force.x,&new->force.y,&new->force.z) == 3
       ) {
        return new;
    } else {
        return NULL;
    }
}

void m_frames_load_config(M_system *sys, dictionary *dict) {
    char *in;

    sys->wrap_coords  = iniparser_getboolean(dict,"system:wrap_coords",1);
    sys->read_dip     = iniparser_getboolean(dict,"system:read_dip",0);
    sys->read_vel     = iniparser_getboolean(dict,"system:read_vel",0);
    sys->read_force   = iniparser_getboolean(dict,"system:read_force",0);
    sys->constant_dims= iniparser_getboolean(dict,"system:constant_dims",0);

    if ( sys->constant_dims ) {
        in = iniparser_getstring(dict,"system:dims",NULL);
        if ( in == NULL ) {
            FATAL("Must supply system dimensions if constant_dims=true.\n");  
            exit(1);
        } else {
            prec dims[3];
            parse_csv_farray(dims, in, 3);
            sys->dims.x = dims[0];
            sys->dims.y = dims[1];
            sys->dims.z = dims[2];
        }
    }

    sys->start_frame  = iniparser_getint(dict,"system:start_frame",1) - 1;
    sys->end_frame    = iniparser_getint(dict,"system:end_frame",0) - 1;
    sys->skip         = iniparser_getint(dict,"system:skip",0);
    
    in = iniparser_getstring(dict,"system:use_end",NULL);
    if ( in == NULL ) {
        if ( sys->end_frame > 0 && sys->end_frame > sys->start_frame ) {
            sys->use_end = 1; 
        } else {
            sys->use_end = 0;
        }   
    } else {
        sys->use_end = iniparser_getboolean(dict,"system:use_end",0);
    }
}
