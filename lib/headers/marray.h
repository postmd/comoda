#ifndef MARRAY_H
#define MARRAY_H

#include "mtypes.h"
#include "debug.h"

#define THREADS 4
#define M_RESERVE_METHOD nearest_power
#define M_RESERVE_MINIMUM 32
#define m_array_elt_len(array,i) ((array)->e_size * (i))
#define m_array_elt_pos(array,i) ((array)->data + m_array_elt_len((array),(i)))
#define m_len(array) ((array)->count)
#define m_length(array) ((array)->count)

typedef void (*M_callback)(void*);

typedef void (*M_map_fn)(void*, void*);

typedef struct _array {
    M_uint  length;
    size_t  e_size;
    void    *data;
    M_uint  count;
    M_callback free_func;
} M_array;

typedef struct _parray {
    M_uint  length;
    void    **data;
    M_uint  count;
} M_parray;


M_array *m_array_new(M_uint length, size_t size, void (*myfunc)(void*));
M_array *m_array_copy(M_array *array);
void m_array_free(M_array *array);

int m_array_resize( M_array *array, M_uint length );
void  *m_array_push(M_array *array, void* element, M_uint length);
void  *m_array_push_nofree(M_array *array, void* element, M_uint length);

M_parray *m_parray_new(M_uint length);
M_parray *m_parray_copy(M_parray *array);
void m_parray_free(M_parray *array);
void m_parray_free_children(M_parray *array, void (*myfunc)(void*));

int m_parray_resize( M_parray *array, M_uint length);
int m_parray_push(M_parray *array, void* element);

void m_parray_swap( M_parray *array, M_uint i, M_uint j );
void m_parray_reverse( M_parray *array);

void m_parray_insert( M_parray *array, void* element, M_uint i);
void m_parray_delete( M_parray *array, M_uint i);

M_array *m_array_map(M_array *array, size_t element, M_map_fn fn);
M_array *m_parray_map(M_parray *array, size_t element, M_map_fn fn);
M_array *m_thparray_map(M_parray *array, size_t element, M_map_fn fn);
void m_parray_qufree(M_parray* p);
#endif
