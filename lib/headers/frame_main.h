#ifndef FRAME_MAIN_H
#define FRAME_MAIN_H

#include "frames.h"
#include "commander.h"

// frame/main.c contains no functions for now

// frame/main contains a main() program, which performs the following:
//    m_load_config                  // loads the config ini file
//    m_sys_build                    // builds the system according to config
//    frame_init                     // external routine executed before read
//    while(m_read_from_stdin_xyz) { // loop over frames from stdin
//        frame_routine              // external routine executed every frame
//    }
//    frame_finalise                 // external routine executed after read

// the three external routines are defined here
// use these routine names in your application and build with
//   $(CC) src/frame/main.c your_application.c $(LIB)... -o bin/your_application

// routine executed after m_load_config and  m_sys_build
void frame_init(M_system* sys, dictionary* dict);

// routine executed each frame after m_read_from_stdin_xyz
void frame_routine(M_system* sys);

// routine executed at the end of the run (m_read_from_stdin_xyz returns 0)
void frame_finalise(M_system* sys);

// variable to fill the description with
char *description;
char *description2;

// function to print config file example
void print_routine_example();

#endif
