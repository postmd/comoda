#ifndef DB_H
#define DB_H

#include "mtypes.h"
#include "vector.h"
#include "marray.h"
#include "cell.h"
#include "coulomb.h"
#include "stress.h"

// methods to obtain M_molec_spec or M_atom_spec from the list of specs in sys
#define m_sys_molec_spec(sys, i) \
        (M_molec_spec*) m_array_elt_pos(sys->molec_specs, i)
#define m_sys_atom_spec(sys, i) \
        (M_atom_spec*) m_array_elt_pos(sys->atom_specs, i)
#define m_mspec_aspec(mspec, i) (M_atom_spec*) mspec->atom_specs->data[i]

// methods to obtain M_molec or M_atom from the list of molecs and atoms in sys
#define m_sys_molec(sys, i) (M_molec*) m_array_elt_pos(sys->molecs, i)
#define m_sys_atom(sys, i) (M_atom*) m_array_elt_pos(sys->atoms, i)

// method to obtain M_molec or M_atom from a list in M_molec/atom_spec
#define m_spec_molec(spec, i) ((M_molec*)(spec->molecs->data[i]))
#define m_spec_atom(spec, i) ((M_atom*)(spec->atoms->data[i]))

// method to obtain M_atom from list of atoms in M_molec
#define m_molec_atom(molec, i) ((M_atom*)(molec->atoms->data[i]))

typedef struct _atom M_atom;
typedef struct _atom_spec M_atom_spec;
typedef struct _molec M_molec;
typedef struct _molec_spec M_molec_spec;

typedef struct _system {
    // atoms and molecules that make up the system
    M_array  *atoms;
    M_array  *molecs;
    M_array  *atom_specs;
    M_array  *molec_specs;
    bool      has_charges;  // dynamic update, =1 if any atom_spec has a charge
    prec      net_charge;   // dynamic update

    // constant properties of the system
    prec      setT;         // load from ini, sys.c

    // rcut, cutoff for classical forcefields
    bool      no_cutoff;    // load from ini, forcefield.c
    prec      rcut;         // load from ini, forcefield.c
    prec      rcut2;        // dynamic update
    // rcut updated to min(rcut, min(sys->dims)/2) every frame

    // dimensions of the box
    M_vector  dims;         // read from xyz
    prec      volume;       // dynamic update
    bool      constant_dims;// load from ini, frames.c
    bool      wrap_coords;  // load from ini, frames.c
    bool      read_dip;     // load from ini, frames.c
    bool      read_vel;     // load from ini, frames.c
    bool      read_force;   // load from ini, frames.c

    // molecular properties
    bool      calc_dipoles; // load from ini, molecules.c
    bool      calc_centers; // load from ini, molecules.c

    // cell method, include the atom and molecule cell as standard
    M_uint    cell_ngrid[3];// load from ini, cell.c
    bool      pop_atom_cell;// load from ini, cell.c
    bool      pop_mol_cell; // load from ini, cell.c
    M_cells  *cell_atoms;
    M_cells  *cell_molecs;

    // Ewald sum, remember the eta parameter and nk
    M_uint    nk[3];        // load from ini, coulomb.c
    prec      eta;          // load from ini, coulomb.c
    bool      calc_ewald;   // load from ini, coulomb.c
    M_ewald  *ewald;

    // stress tensor
    bool      calc_stress;  // load from ini, stress.c
    bool      stress_lj_lr; // load from ini, stress.c 
    bool      stress_toymd; // load from ini, stress.c
    M_stress *stress;

    // frame id and loop processing
    // current_frame_id initialise at -1
    // the first frame has current_frame_id = 0
    M_uint    current_frame_id;  // dynamic update
    M_uint    frames_processed;  // dynamic update
    M_uint    start_frame;       // load from ini, frames.c
    M_uint    end_frame;         // load from ini, frames.c
    bool      use_end;           // load from ini, frames.c
    M_uint    skip;              // load from ini, frames.c

} M_system;

struct _atom {
    M_uint        id;         // m_sys_atom(sys,i)
    M_uint        id_in_spec; // m_spec_atom(spec,i)
    M_uint        id_in_molec;// m_molec_atom(molec,i)
    M_atom_spec  *spec;
    M_molec      *molec;
    M_point       pos;
    M_vector      vel;
    M_vector      force;
    M_vector      dip;
    // a couple of variables to store arbitrary numbers
    prec          dummy;
    M_vector      dummy_v;
};

struct _atom_spec {
    M_uint        id;         // m_sys_atom_specs(sys,i)
    M_uint        id_in_spec; // m_mspec_aspec(spec,i)
    M_uint        count;      // the number of atoms *per molecule*
    M_parray     *atoms;      // array of pointers to atoms of this atom_spec
    M_uint        sys_total;  // the total number of atoms of this atom_spec
                              //   *in the system*.
    M_molec_spec *molec_spec; // the molec_spec which this atom_spec belongs to
                              //   an atom_spec can only belong to 1 molec_spec
    char          name[20];
    char          symbol[4];
    prec          charge;
    prec          mass;
    prec          radius;
    prec         *smallsig;
    prec         *ss12;
    prec         *C6;
};

struct _molec {
    M_uint        id;         // m_sys_molec(sys,i)
    M_uint        id_in_spec; // m_spec_molec(spec,i)
    M_parray     *atoms;
    M_vector      dipole;
    prec          dip_mag;
    M_point       center;     // position of the center 
    M_vector      vel;        // velocity of the center of mass
    M_molec_spec *spec;
    // If a molecule is made with atoms outside of the system
    // we need a pointer to this array, so we can free these atoms
    // when we are done with the molecule.
    M_array      *atoms_array;
    // a couple of variables to store arbitrary numbers
    prec          dummy;
    M_vector      dummy_v;
};

enum placement { TOGETHER, SEPARATED };
struct _molec_spec {
    M_uint        id;         // m_sys_molec_specs(sys,i)
    M_uint        count;      // number of molecules
    M_uint        astart;     // start and end of contiguous atom indices
    M_uint        aend;       //    id of (M_array*)sys->atoms
    M_uint        mstart;     // start and end of contiguous molec indices
    M_uint        mend;       //    id of (M_array*)sys->molecs
    M_uint        atoms;      // number of atoms per molecule
    enum placement  position; // boolean, [T] OHHOHH... vs [F] OO...HH...HH...
    char          name[20];
    M_parray     *atom_specs;
    M_parray     *molecs;
    prec          density;
    prec          mass;
    prec          charge;
    M_int         center;     // the id_in_molec of the atom
                              // that is the "center" of the molecule
                              // if center=geom_center=-1, then we calculate
                              // the center according to the geometric center
};

M_system *m_system_new();
void m_system_free(M_system *sys);

M_atom *m_atom_new();
void m_atom_free(M_atom *atom);
void m_atom_free_children(M_atom *atom);
M_atom *m_atom_copy(M_atom *atom);

M_atom_spec *m_atom_spec_new ( char *name, char *symbol, M_uint count, 
                               prec mass, prec radius, prec charge );
void m_atom_spec_free ( M_atom_spec *spec );
void m_atom_spec_free_children ( M_atom_spec *spec );

M_molec *m_molec_new();
void m_molec_free(M_molec *molec);
void m_molec_free_children(M_molec *molec);
M_molec *m_molec_copy(M_molec *molec);

M_molec_spec *m_molec_spec_new ( char *name, int count ); 
void m_molec_spec_free (M_molec_spec *spec); 
void m_molec_spec_free_children (M_molec_spec *spec); 

/* M_frame : not used

typedef struct _frame M_frame;

struct _frame {
    char file[64];
    M_uint id;
};

M_frame *m_frame_new();
void m_frame_free(M_frame *frame);
void m_frame_free_children(M_frame *frame);
*/

#endif
