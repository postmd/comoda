#ifndef FFT_H
#define FFT_H

#include "mtypes.h"
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr


// ndim: number of dimensions in FFT data
// nn[i]: length of the i^th dimension
// data: prod(nn)*2 length.
// data[2*i] is the real component
// data[2*i+1] is the imag component
void fourn(prec data[], M_uint nn[], int ndim, int isign);

M_complex *fft(M_complex data_c[], M_uint nn[], int ndim, prec dx);
M_complex *ifft(M_complex hat_c[], M_uint nn[], int ndim, prec dq);


#endif
