#ifndef CELL_H
#define CELL_H

#include "marray.h"
#include "arithmetic.h"
#include "array.h"
#include "vector.h"
#include "iniparser.h"

typedef struct _cells {
    M_uint      ngrid[3];
    M_uint      ngridpts;
    M_vector    dgrid;
    M_parray ** occupancy;
    M_uint      rcut_grid_width[3];
} M_cells;


// allocate memory for the M_cell struct
M_cells  *m_cells_new(M_uint *ngrid, M_vector *dims, prec rcut);

// get the cell coordinates (i,j,k) for a point in space
M_uint   *m_cells_get_cell_coords(M_cells *cell, M_point *pos);

// the following items depend on the M_system struct declared in db.c
// due to interdependence, we must define a placeholder here
struct   _system;
// populate the cell occupancy array with atoms in the system
M_cells  *m_cells_populate_atom_occupancy(struct _system *sys);
// populate the cell occupancy array with molecules (by the position of
// their centers)
M_cells  *m_cells_populate_molec_occupancy(struct _system *sys);

// get a list of objects within rcut_grid_width of a cell coordinate or
// a point in space
M_parray *m_cells_get_objs_cell(M_cells *cell, M_uint *cell_coord);
M_parray *m_cells_get_objs_pos(M_cells *cell, M_point *pos);

// reset the occupancy arrays with fresh m_parray_new( 0 ) 
void      m_cells_reset(M_cells *cell);
// completely free M_cell struct
void      m_cells_free(M_cells *cell);
// copy the cell object to a new object
M_cells  *m_cells_copy(M_cells *cell);

// tally up the total number of objects in the occupancy table
// if any of the M_parray objects (occupancy[i]) is NULL, the initialisation
// of the M_cells struct has failed. -1 is returned as the tally.
int       m_cells_tally(M_cells *cell);

void      m_cells_load_config(struct _system *sys, dictionary *dict);

#endif
