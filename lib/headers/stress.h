#ifndef STRESS_H
#define STRESS_H

#include "coulomb.h"
#include "vector.h"
#include "iniparser.h"
#include "cell.h"

typedef struct _stress_tensor {
    M_matrix  *electro;        // the electrostatic part of the stress tensor
    M_matrix  *lj;             // the Lennard-Jones part of the stress tensor
    M_matrix  *kinetic;        // the kinetic part of the stress tensor
    M_matrix  *total;          // the total stress tensor
} M_stress;

M_stress* m_stress_new();
void      m_stress_free(M_stress *stress);
void      m_stress_reset(M_stress *stress);
M_stress* m_stress_copy(M_stress *stress);
void      m_stress_output(M_stress *stress);

struct _system;

// electrostatics contribution as per toyMD
M_matrix* m_stress_calc_electro_toy(struct _system *sys);

// electrostatics contribution to the molecular virial, as given by
// Alejandre, Tildesley & Chapela, doi: 10.1063/1.469505
M_matrix* m_stress_calc_electro_mol(struct _system *sys);

// the Lennard-Jones contribution to the stress tensor, short-range only
M_matrix* m_stress_calc_lj(struct _system *sys);
// long-range correction for the LJ contribution to the stress tensor
// use only for a bulk simulation!
M_matrix m_stress_calc_lj_lr(struct _system *sys);

// kinetic/thermal part of the stress tensor
M_matrix* m_stress_calc_kinetic(struct _system *sys);

// this calls other subroutines to calculate the stress tensor
M_stress* m_stress_calc_frame(struct _system *sys);

void m_stress_load_config(struct _system *sys, dictionary *dict);
#endif
