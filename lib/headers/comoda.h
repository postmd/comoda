// basic functions and types
#include "mtypes.h"         // basic types
#include "debug.h"          // debugging printf & trace
#include "vector.h"         // vector types
#include "array.h"          // simple array operations
#include "marray.h"         // pointer array types
#include "arithmetic.h"     // some additional arithmetic functions
#include "mstring.h"        // extend string handling

// comoda arrays and systems
#include "maggregate.h"     // aggregate tables  
#include "db.h"             // data structures for atoms and molecules
#include "sys.h"            // functions for systems of atoms and molecules
#include "cell.h"           // cell method: track occupancy of atoms in evenly-
                            // spaced cells, optimises pairwise calculations
#include "coulomb.h"        // ewald sum method
#include "stress.h"         // calculates the stress tensor from the molecular
                            // virial, see Alejandre, Tildesley & Chapela

// application-specific
#include "frames.h"         // functions for reading/handling frames
#include "molecules.h"      // functions for molecular properties
//#include "gsd.h"            // some functions that deal with drawing surfaces


