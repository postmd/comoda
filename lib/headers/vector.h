#ifndef VEC_H
#define VEC_H

#include "mtypes.h"
#include "arithmetic.h"
#include "debug.h"

#define m_vector_length m_vector_mag
#define m_displacement m_disp
#define m_radius m_disp

struct _3d {
    prec x, y, z;
};

typedef struct _3d M_vector;
#define M_ZERO_VEC (M_vector) {.x=0, .y=0, .z=0}

struct _point {
    prec x, y, z;
    // This points to the dimensions which of the box
    // which the point exists in
    M_vector *dims;
};

typedef struct _point M_point;
#define M_ZERO_POINT (M_point) {.x=0, .y=0, .z=0, .dims=NULL}

struct _3d_2 {
    prec xx, xy, xz;
    prec yx, yy, yz;
    prec zx, zy, zz;
};

typedef struct _3d_2 M_matrix;
#define M_ZERO_MAT (M_matrix) {.xx=0, .xy=0, .xz=0,\
                               .yx=0, .yy=0, .yz=0,\
                               .zx=0, .zy=0, .zz=0 }
#define M_IDENT_MAT (M_matrix) {.xx=1, .xy=0, .xz=0,\
                                .yx=0, .yy=1, .yz=0,\
                                .zx=0, .zy=0, .zz=1 }

M_vector m_read_dims_stdin();

void     m_puts_point(M_point v);
void     m_puts_vector(M_vector v);
void     m_puts_matrix(M_matrix m);
void     m_puts_matrix_oneline(M_matrix m);
void     m_puts_trace(M_matrix m);
void     m_puts_diag(M_matrix m);

//displacement
M_vector m_disp(M_point a, M_point b);
//M_vector m_radius(M_point a, M_point b);

//basic vector functions
M_vector m_vector_add(M_vector v1, M_vector v2);
M_vector m_vector_subtract(M_vector v1, M_vector v2);
M_vector m_vector_scale(M_vector v1, prec k);
M_vector m_vector_unit(M_vector v1);
M_vector m_vector_invert(M_vector v1);

prec     m_vector_dot( M_vector v1, M_vector v2);
prec     m_vector_mag(M_vector v);
prec     m_vector_mag2(M_vector v);

//working with points
void     m_point_trans( M_point *p, M_vector v ); 
void     m_point_wrap( M_point *p );

//type conversion
M_vector m_point_to_vector(M_point *p);
M_point  m_vector_to_point(M_vector *v, M_vector *dims); 
void     m_point_bound(M_point *point, M_vector *dims); 

//duplicate
M_vector m_vector_copy(M_vector *v);
M_point  m_point_copy(M_point *p);
M_matrix m_matrix_copy(M_matrix *m);

// basic matrix functions
M_matrix m_matrix_add(M_matrix m1, M_matrix m2);
M_matrix m_matrix_subtract(M_matrix m1, M_matrix m2);
M_matrix m_matrix_scale(M_matrix m, prec k);
prec     m_matrix_trace(M_matrix m);
M_matrix m_matrix_multiply(M_matrix m1, M_matrix m2);
M_matrix m_matrix_square(M_matrix m);
M_vector m_matrix_vector_multiply(M_matrix m, M_vector v);
void     m_matrix_zero(M_matrix *m);
M_matrix m_vector_outer(M_vector a, M_vector b);

// special functions
M_vector rand_sphere1();

#endif
