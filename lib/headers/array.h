#ifndef ARRAY_H
#define ARRAY_H

#include "mtypes.h"
#include "mstring.h"
#include "debug.h"

#define parse_csv_iarray parse_iarray
#define parse_csv_farray parse_farray
#define parse_csv_uiarray parse_uiarray

// sets the array to zero, from index=0 to index=nfield-1
void clear_barray( bool *dest, int nfield );
void clear_iarray( int *dest, int nfield );
void clear_uiarray( M_uint *dest, int nfield );
void clear_farray( prec *dest, int nfield );
void clear_carray( M_complex *dest, int nfield );

// sets the array to the value supplied, from index=0 to index=nfield-1
void set_barray( bool *dest, bool val, int nfield );
void set_iarray( int *dest, int val, int nfield );
void set_uiarray( M_uint *dest, M_uint val, int nfield );
void set_farray( prec *dest, prec val, int nfield );
void set_carray( M_complex *dest, M_complex val, int nfield );

// parses a string of nfield elements into the array dest
const char trimdelims[3];
const int n_trimdelims;
void parse_iarray( int *dest, char *in, int nfield );
void parse_uiarray( M_uint *dest, char *in, int nfield );
void parse_farray( prec *dest, char *in, int nfield );
// a copy of (char *in) is made via malloc, then dest[i] point to addresse in the new copy
// to free the copy, use free(*dest) or free(dest[0])
void parse_strarray( char **dest, char *in, int nfield );

// increment a list of id by 1 in the last position
//  ticks up according to a list of lengths
//  returns 1 if successfully incremented
//  returns 0 if the list is already at the maximum, the list is reset to all 0
int increment_id_list( M_uint *id_list, M_uint *length, int nfield);

// For an N-dimensional array stored as an 1D array, the storage order is:
// (0,0,0,0,...0)
// (0,0,0,0,...1)
// (0,0,0,0,...2) and so on
// Then, for an N-length list of indices, we can calculate the 1-D id
M_uint get_id( M_uint *id_list, M_uint *length, int nfield);
M_uint *get_id_list ( M_uint id, M_uint *length, int nfield);

bool *barray_copy( bool *a, int n );
int *iarray_copy( int *a, int n );
M_uint *uiarray_copy( M_uint *a, int n );
prec *farray_copy( prec *a, int n );
M_complex *carray_copy( M_complex *a, int n );

bool sum_barray( bool *a, int n);
int sum_iarray( int *a, int n);
M_uint sum_uiarray( M_uint *a, int n);
prec sum_farray( prec *a, int n);
M_complex sum_carray( M_complex *a, int n);

#endif
