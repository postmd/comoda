#ifndef MOLECULES_H
#define MOLECULES_H

//#include <mkl_lapacke.h>

#include "db.h"
#include "array.h"
#include "iniparser.h"

void calc_densities(M_system *);

// calculates the dipole vector
// note the dipole vector points from -ve to +ve
// see comments in source for more details
void calc_dipoles(M_system *);
void calc_dipole(M_molec *);

void calc_centers(M_system *);
void calc_center(M_molec *);

void calc_center_vels(M_system *);
void calc_center_vel(M_molec *);

void m_molec_flatten(M_molec *);
M_molec *m_molec_flatten_new(M_molec *);

M_point m_molec_com(M_molec *);

double rg(M_molec *m);

typedef struct _neighbours {
  M_parray *molecs;
  prec min_rank;
  prec *ranks;
  prec *dummy;
  int n_max;
} M_neighbours;

M_neighbours *m_nearest_neighbours_new(int n_max, prec default_rank);
void m_nearest_neighbours_add(M_neighbours *neighbours, M_molec *new_neighbour,
                              prec rank);
void m_nearest_neighbours_append(M_neighbours *neighbours,
                                 M_molec *new_neighbour);
void m_nearest_neighbours_free(M_neighbours *neighbours);
void m_nearest_neighbours_print(M_neighbours *neighbours);

void m_molecules_load_config(M_system *sys, dictionary *dict);

#endif
