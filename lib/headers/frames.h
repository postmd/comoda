#ifndef FRAMES_H
#define FRAMES_H

#include "sys.h"
#include "mstring.h"

// returns 1 if another frame need to be read
// returns 0 if no more frames need to be read
// returns -1 if we are past the end_frame limit
int m_next_frame(M_system *);

// returns 1 if frame read successfully
// returns 0 if frame was not read successfully
int m_read_frame_stdin_xyz(M_system *sys);

M_atom *m_atom_new_stdin();

void m_frames_load_config(M_system *sys, dictionary *dict);

#endif
