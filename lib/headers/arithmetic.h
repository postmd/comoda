#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include "mtypes.h"
#include "vector.h"
#include "debug.h"

// a modulo operation, a % b, that always returns a positive integer
int pmod( int a, int b );

//              { 0,    if a<0
// pspan(a,b) = { a,    if 0<a<|b|
//              { |b|,  if a>=|b|
int pspan( int a, int b );

M_uint bin_id( prec val, prec min, prec width );

prec rand_1();

#endif
