#ifndef SYS_H
#define SYS_H

#include "db.h"
#include "molecules.h"
#include "cell.h"
#include "array.h"
#include "iniparser.h"
#include "frames.h"
#include "forcefield.h"

#define m_system_add_molec_spec m_sys_add_molec_spec

M_atom_spec *m_sys_add_atom_spec(M_system *sys, M_molec_spec *mspec, 
                                 char *name, char *symbol, M_uint count, 
                                 prec mass, prec radius, prec charge );
M_molec_spec *m_sys_add_molec_spec(M_system *sys, char *name, int count ); 

M_molec_spec *m_molec_spec_find(M_system *sys, char *name);
M_atom_spec *m_atom_spec_find(M_system *sys, char *name);
M_atom_spec *m_atom_spec_find_symbol(M_system *sys, char *symbol);
// old version that uses a string, and looks up M_molec_spec via string
M_atom_spec *m_system_add_atom_spec(M_system *sys, char *molec, 
                                    char *name, char *symbol, M_uint count, 
                                    prec mass, prec radius, prec charge );

void m_sys_build(M_system *sys);
void m_print_mapping(M_system *sys);

void m_sys_species_load_config(M_system *sys, dictionary *dict);
void m_sys_properties_load_config(M_system *sys, dictionary *dict);
void m_sys_load_config(M_system *sys, dictionary *dict);

void m_sys_calc(M_system *sys);

void print_sys_example();

#endif
