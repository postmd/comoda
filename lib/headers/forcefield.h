#ifndef FORCEFIELD_H
#define FORCEFIELD_H

#include "db.h"
#include "iniparser.h"
#include "array.h"
#include "vector.h"

void calc_rcut(M_system *sys);

M_vector m_lj_force( M_atom *a, M_atom *b );

void m_forcefield_load_config(M_system *sys, dictionary *dict);

#endif
