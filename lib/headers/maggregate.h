#ifndef M_AGG_H
#define M_AGG_H

#include "mtypes.h"
#include "arithmetic.h"
#include "marray.h"
#include "array.h"

#define THREADS 4

typedef struct _aggregate {
    M_uint  dims;
    M_uint  *count;
    prec    **values;
    prec    *row;
    prec    min, max, width;
    M_uint  length;
    M_uint  print_empty; // boolean, are empty rows are printed?
    M_uint  print_count; // boolean, are the count column is printed?
    M_uint  *print_col;  // boolean, print_col[i]: is column i printed?
    // whether the aggregate table should auto-tend
    M_uint  auto_extend; // boolean, default = true
} M_aggregate;

typedef struct _n_aggregate {
    M_uint    n;                // the number of keys
    M_uint    dims;             // number of data values per key combination
                                //   can be 0, then only the count is stored

    prec     *min,*max,*width;  // length = n, the number of size of each key
    M_uint   *length;           // length = n, the number of bins in each key
    M_uint    total_length;     // total_length = length[0]*length[1]*....

    prec    **keys;             // length = n, keys[i] = array of length[i]
                                // contains key values, spans min[i]->max[i]
    M_uint  **keys_count;       // dimensions same as **keys,
                                // the count per key (summed over other keys)

    M_uint   *count;            // length = total_length,
                                // the count per key combination
    // data stored as:
    // id = 0,              keys_id = { 0, 0, 0, ...., 0, 0 }
    // id = 1,              keys_id = { 0, 0, 0, ...., 0, 1 }
    // ...
    // id = length[n]-1,    keys_id = { 0, 0, 0, ...., 0, length[n]-1 }
    // id = length[n],      keys_id = { 0, 0, 0, ...., 1, 0 }
    // ...
    // id = total_length-1, keys_id = { length[0]-1, length[1]-1, ....,
    //                                  length[n]-1 }
    // use m_n_aggregate_get_count_id to get the id corresponding to key
    
    prec     *values;           // length = total_length*dims, can be 0
    // data stored as:
    // 
    // dim = 0, id = 0,               keys_id = { 0, 0, 0, ..., 0, 0 }
    // dim = 0, id = 1,               keys_id = { 0, 0, 0, ..., 0, 1 }
    // ...
    // dim = 0, id = length[n]-1,     keys_id = { 0, 0, 0, ..., 0, length[n]-1 }
    // dim = 0, id = length[n],       keys_id = { 0, 0, 0, ..., 1, 0 }
    // ...
    // dim = 0, id = total_length-1,  keys_id = { length[0]-1, length[1]-1, ...,
    //                                            length[n]-1 }
    // ...
    // dim = 1, id = total_length,    keys_id = { 0, 0, 0, ..., 0, 0}
    // dim = 1, id = total_length+1,  keys_id = { 0, 0, 0, ..., 0, 1}
    // ...
    // dim = 1, id =2*total_length-1, keys_id = { length[0]-1, length[1]-1, ...,
    //                                            length[n]-1 }
    // ...
    // dim=d-1, id =d*total_length-1, keys_id = { length[0]-1, length[1]-1, ...,
    //                                            length[n]-1 }
    // use m_n_aggregate_get_values_id to get the id corresponding to key & dim

    // these are the data to be aggregated
    prec     *key;              // length = n, the key of the data
    prec     *data;             // length = dims, the data values

    // whether the aggregate table should auto-tend
    M_uint    auto_extend;      // boolean, default = true

    // printing flags
    M_uint   *print_col;        // whether or not to print dims columns
    M_uint    print_count;      // whether or not to print count
    M_uint    print_empty;      // whether or not to print empty rows
    // some specific flags for 2d printing, requires n=2
    M_uint    print_2d_values;  // should the values be printed as a 2d table?
    M_uint    print_2d_count;   // should the count be printed as a 2d table?
    M_uint    print_0_count;    // should keys_count[0] be printed?
} M_n_aggregate;

/* A function for a threaded aggregater which takes one value*/
typedef void (*M_aggregate_fn)(M_aggregate*, void*);

/* A function for a threaded aggregater which takes a pair of values*/
typedef void (*M_aggregate_pair_fn)(M_aggregate*, void*, void*);

/*
 *
 * primitives
 *
 */

/* Initialize an aggregate structure
 * Note that the M_aggregate's max property may not equal the specified max.
 * (agg->max - agg->min) is an integer multiple of agg->width
 * agg->max = ceil((max-min)/width)*width+min >= max.
 * The keys (agg->values[i][0]) are the midpoints of bins.
 * The first key is always (min + 0.5*width)
 */
M_aggregate *m_aggregate_new( prec min, prec max, prec width, M_uint dims);
M_n_aggregate *m_n_aggregate_new( M_uint n, prec *min, prec *max, prec *width,
                                  M_uint dims);

/* Reset Aggregate, All values become 0.0 */
void m_aggregate_reset(M_aggregate*);
void m_n_aggregate_reset(M_n_aggregate*);

/* Free an aggregate */
void m_aggregate_free(M_aggregate*);
void m_n_aggregate_free(M_n_aggregate*);

/*
 *
 * aggregate function
 *
 */

/* Aggregate a row, Set the key [0] and values [1..dims] of agg->row */
void m_aggregate(M_aggregate**);
void m_n_aggregate(M_n_aggregate**);
void _m_aggregate(M_aggregate*);
void _m_n_aggregate(M_n_aggregate*);

M_uint m_n_aggregate_get_count_id(M_n_aggregate *agg);
M_uint m_n_aggregate_get_values_id(M_n_aggregate *agg, M_uint dim);
M_uint *m_n_aggregate_get_id_list(M_n_aggregate *agg, M_uint id);

/* Clear all values of the temporary agg->row to 0 */
void m_aggregate_clear_row(M_aggregate*);
void m_n_aggregate_clear_data(M_n_aggregate*);

/* Extend the keyspace of an aggregate table
 * The pivots & key spacing will be kept the same.
 * The keyspace of agg will be extended by an integer multiple of 
 * the existing agg->width to enclose min and max */
M_aggregate *m_aggregate_extend(M_aggregate *agg, prec min, prec max);
M_n_aggregate *m_n_aggregate_extend(M_n_aggregate *agg);

/* Overwrite the count column for the bin corresponding to the supplied key */
void m_aggregate_overwrite_count(M_aggregate *aggregate, prec key,
                                 M_uint count);
void m_n_aggregate_overwrite_count(M_n_aggregate *aggregate, M_uint count);
void m_n_aggregate_overwrite_keys_count(M_n_aggregate *agg, M_uint n,
                                        M_uint count);

/*
 *
 * arithmetics
 *
 */

// retrieve the list of values associated with agg->key
// the list of values is stored in agg->data
// agg->count[id] is returned
M_uint m_n_aggregate_lookup(M_n_aggregate *agg);

/* Scale a column by a factor */
void m_aggregate_col_scale(M_aggregate *aggregate, int col_index, prec k );
void m_n_aggregate_col_scale(M_n_aggregate *aggregate, int col_index, prec k );
// scales a certain range of data, matching to aggregate->key, by k
// the match criteria is:
//    - must match the bin as aggregate->key
//    - if aggregate->key[i] is NaN, it is treated as a wildcard
//    - if aggregate->key[i] is out of range, an error is produced
// the columns will be scaled according to aggregate->data:
//    - if aggregate->data[i] is NaN, it will not be scaled 
//    - otherwise, aggregate->values[...] *= data[i]
void m_n_aggregate_key_scale(M_n_aggregate *aggregate);


/* Scale a column by the volume of a shell
 *
 * Note: multiplies the column with 4/3*pi*( (r+dr/2)^3 - (r-dr/2)^3 )
 */
void m_aggregate_col_scale_dV(M_aggregate *aggregate, int col_index);
// col_index is the column to be scaled
// key_id is the id of the key which corresponds to r
void m_n_aggregate_col_scale_dV(M_n_aggregate *aggregate, int col_index,
                                int key_id );

/* Sum two aggregates into the first M_aggregate object */
void m_aggregate_add(M_aggregate *aggregate1, M_aggregate *aggregate2);

/* check if two aggregates have the same dimensions */
int m_aggregate_chkdims(M_aggregate *aggregate1, M_aggregate *aggregate2);

/*
 *
 * Printing
 *
 */

/* Set flags to print all columns
 *    agg->print_empty = 0    - do not print empty rows
 *    agg->print_count = 1    - print count
 *    agg->print_col[i] = 1   - print all columns
 */
void m_aggregate_set_print_all(M_aggregate*);
void m_n_aggregate_set_print_all(M_n_aggregate*);

/* Set flags to print nothing
 *    agg->print_empty = 0    - do not print empty rows
 *    agg->print_count = 0    - do not print count
 *    agg->print_col[i] = 0   - do not print any columns
 */
void m_aggregate_set_print_none(M_aggregate*);
void m_n_aggregate_set_print_none(M_n_aggregate*);

/* Print aggregate table
 *
 * Note: columns are switched on or off according to aggregate->print_col[i]
 *       and aggregate->print_count
 */
void m_aggregate_print(M_aggregate *aggregate );
void m_aggregate_fprint(FILE *f, M_aggregate *aggregate );
void m_n_aggregate_print(M_n_aggregate *aggregate );
void m_n_aggregate_fprint(FILE *f, M_n_aggregate *aggregate );

/* Print the entire aggregate table
 *
 * Note: the entire aggregate table, including count, is printe
 *       this subroutine disregards the boolean flags aggregate->print_col[i]
 *       and aggregate->print_count
 */
void m_aggregate_print_all(M_aggregate *aggregate );

/* Apply a function to each element of the array to generate an aggregate
 * 
 * The function operates in a multithreaded manner.
 */
int m_thaggregate(M_aggregate *agg, M_array* array, M_aggregate_fn fn);

/* Generate all pairs of elements from the array and pass them to
 * the threaded pair function
 *
 * This function uses threading
 */
int m_thaggregate_pair(M_aggregate *agg, M_array* array,
                       M_aggregate_pair_fn fn);

#endif
