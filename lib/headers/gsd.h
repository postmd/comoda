#ifndef GSD_H
#define GSD_H

#include "db.h"

#define NODES_MAX 20000
#define SPHERES_MAX 500

/* The gsd sphere is divided into 8 equal segments
 *
 * The following diagram shows the naming convention
 * for the quadrants
 *                   Yp
 *                   ^
 *                   |
 *                   |     Zn
 *                   |    /
 *                   |   /
 *                   |  /
 *                   | /
 *                   |/
 * Xn<---------------0---------------->Xp 
 *                  /|
 *                 / |
 *                /  |
 *               /   |
 *              /    |
 *             Zp    |
 *                   |
 *                   Yn
 * So each eigth is simply named X?Y?Z?
 *
 * XpYpZp
 * XpYpZn
 * XpYnZp
 * XnYpZp
 * XpYnZn
 * XnYnZp
 * XnYpZn
 * XnYnZn
 *
 */

typedef struct {
    M_point pos;
    int valid;
} M_node;
typedef struct {
    M_node *nodes;
    M_uint size;
    prec scale;
    M_point center;
    M_parray *XpYpZp;
    M_parray *XpYpZn;
    M_parray *XpYnZp;
    M_parray *XnYpZp;
    M_parray *XpYnZn;
    M_parray *XnYnZp;
    M_parray *XnYpZn;
    M_parray *XnYnZn;
} M_gsd;

prec m_gsd_sasa( M_molec *m, M_uint n, prec sol); 
prec m_gsd_sasa2( M_molec *m, M_uint n, prec sol); 

M_gsd m_gsd_new(M_uint, prec, M_point); 
#endif
