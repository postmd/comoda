#ifndef MTYPES_H
#define MTYPES_H

// provides basic classes

#define M_int int
#define M_uint unsigned int
#define prec double
#define M_complex prec complex
#define pi 3.14159265359
#define sqrt_pi 1.77245385090552
#define ang2au 0.529177249
#define geom_center -1

#define _GNU_SOURCE

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <complex.h>
#include <unistd.h>
#include <pthread.h>

#endif
