#ifndef ENV_H
#define ENV_H

#include "mtypes.h"
#include "debug.h"

#include "vector.h"

typedef struct species_t {
    char name[16];
    int start;
    int end;
} species_t;

typedef struct {
  int a;
  int b;
  double scale;
  int size;
} dipole_vector;

int get_num_mols(int *);
int get_num_atoms(int *);
int get_num_objects(int *);
int get_bin_size(double *);
int get_r_cut(double *);
int get_box_dim(vector *);
int get_dipole_matrix(dipole_vector * );

#endif
