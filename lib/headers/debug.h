#ifndef DEBUG_H
#define DEBUG_H

#include "mtypes.h"

#define WHERESTR  "[DEBUG] %s():\t"
#define FATALSTR  "[FATAL] %s():\t"
#define WHEREARG  __func__
#define TIMESTR   "%f"
#define TIMEEND   "\n"
#define DEBUGPRINT2(...)       fprintf(stderr, ##__VA_ARGS__)
#define DEBUG(_fmt,...)  DEBUGPRINT2(WHERESTR _fmt, WHEREARG, ##__VA_ARGS__)
#define FATAL(_fmt,...)  DEBUGPRINT2(FATALSTR _fmt, WHEREARG, ##__VA_ARGS__)
#define TIME() DEBUGPRINT2(WHERESTR TIMESTR TIMEEND, WHEREARG, \
                           (double) clock()/CLOCKS_PER_SEC) 

void DEBUG_barray(bool *in, int n);
void DEBUG_iarray(int *in, int n);
void DEBUG_farray(prec *in, int n);
void DEBUG_carray(M_complex *in, int n);

void DEBUG_barray_2D(bool *in, int dim1, int dim2);
void DEBUG_iarray_2D(int *in, int dim1, int dim2);
void DEBUG_farray_2D(prec *in, int dim1, int dim2);

void DEBUG_barray_3D(bool *in, int dim1, int dim2, int dim3);
void DEBUG_iarray_3D(int *in, int dim1, int dim2, int dim3);
void DEBUG_farray_3D(prec *in, int dim1, int dim2, int dim3);

#endif
