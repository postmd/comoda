#ifndef MSTRING_H
#define MSTRING_H

#include "mtypes.h"
#include "debug.h"
#include <string.h>

int chk_delim(char *in, const char *delims, int n_delims);

char *next_field(char *str, const char *delims, int n_delims);
char *next_n_fields(char *str, const char *delims, int n_delims, int n);

int count_n_fields(char *str, const char *delims, int n_delims);

char *trim_delims(char *str, const char *delims, int n_delims);
char *trim_consec_delims(char *str, const char *delims, int n_delims, const char *new_delim);

#endif
