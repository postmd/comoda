#ifndef COULOMB_H
#define COULOMB_H

#include "complex.h"
#include "vector.h"
#include "iniparser.h"

typedef struct _ewald {
    prec        U_coulomb;
    prec        U_real;
    prec        U_recip;
    prec        U_self;
    prec        U_mol_self;
    prec        eta;
    M_vector   *dims;
    int         kmax[3];
    M_uint      npts[3];
    M_uint      n_k; // = npts[0]*npts[1]*npts[2]
    prec       *greenfn;
    M_complex  *rhohat;
} M_ewald;

M_ewald* m_ewald_new(prec eta, M_vector *dims, M_uint *nk);
void m_ewald_free(M_ewald *ewald);
void m_ewald_reset(M_ewald *ewald);
M_ewald* m_ewald_copy(M_ewald *in);
M_vector m_ewald_get_k(M_uint k_id, M_ewald *ewald);

// the following items depend on the M_system struct declared in db.c
// due to interdependence, we must define a placeholder here
struct _system;
M_ewald* m_ewald_calc_frame(struct _system *sys);
M_ewald* m_ewald_calc_add(struct _system *sys, M_point pos, prec q);
struct _atom;
M_ewald* m_ewald_calc_del(struct _system *sys, struct _atom *atom); 
prec m_ewald_isolated(struct _system *sys, prec Q);

void m_coulomb_load_config(struct _system *sys, dictionary *dict);

#endif
