#include "headers/frame_main.h"

typedef struct {
    int status;
    char config[256];
    int mapping;
    int dry;
    int example;
}option_t;

static void status (command_t *cmd) {
    if ( ! cmd->arg ) {
        ((option_t*)cmd->data)->status = 1;
    } else {
        ((option_t*)cmd->data)->status = atoi(cmd->arg);
    }
}

static void config (command_t *cmd) {
    strcpy(((option_t*)cmd->data)->config,cmd->arg);
}

static void mapping (command_t *cmd) {
    ((option_t*)cmd->data)->mapping = 1;
}

static void dry (command_t *cmd) {
    ((option_t*)cmd->data)->dry = 1;
}

static void example (command_t *cmd) {
    ((option_t*)cmd->data)->example = 1;
}

void print_example() {
    print_sys_example();
    print_routine_example();
}

int main (int argc, char **argv) {
    M_system *s = m_system_new();
    command_t cmd;
    option_t options = (option_t){.dry = 0, .status = 0, .mapping = 0,
                                  .example = 0, .config=""};
    cmd.data = &options;

    int print_size = 1024;
    char *print = malloc(sizeof(char)*print_size);
    int print_used = 0;
    
    if ( description ) {
      while ( strlen(description) > print_size - print_used ) {
        print_size*=2;
        print = realloc(print, sizeof(char)*print_size);
      }
      strcat(print, description);
      print_used = strlen(print);
    }

    if ( description2 ) {
      while ( strlen(description2) > print_size - print_used ) {
        print_size*=2;
        print = realloc(print, sizeof(char)*print_size);
      }
      strcat(print, description2);
      print_used = strlen(print);
    }
    command_init(&cmd, argv[0], print);
    command_option(&cmd, "-c", "--config <arg>",
                   "path to config file", config);
    command_option(&cmd, "-e", "--example",
                   "print example config file", example);
    command_option(&cmd, "-s", "--status [arg]", 
                   "show status, arg = #frames per status update", status);
    command_option(&cmd, "-m", "--mapping",
                   "print database mapping and exit", mapping);
    command_option(&cmd, "-d", "--dry",
                   "read data but don't call the routine", dry);
    command_parse(&cmd, argc, argv);

    if(options.example) {
      print_example();
      exit(0);
    }

    if(!strcmp(options.config,"")) {
        FATAL("Must supply a config file! Use --help for more info\n");
        exit(1);
    }

    clock_t start_routine, end_routine, start_program;
    double init_routine, delta, elapsed, avg;
    int fid;

    if(options.dry) {
      options.status=options.dry;
    }

    if(options.status) {
        start_program = clock();
    }

    // Perform initialization

    // Load the system
    dictionary *dict = iniparser_load(options.config);
    m_sys_load_config(s, dict);
    m_sys_build(s);
    
    // Do Mapping if required
    if(options.mapping) {
        m_print_mapping(s);
        return 0;
    }

    //---
    // call the frame_initialise routine
    //---

    if(! options.dry) {
        frame_init(s, dict);
    }

    if (options.status) {
        end_routine = clock();
        init_routine = (double)(end_routine - start_program)/CLOCKS_PER_SEC;
        fprintf(stderr,"[TIME] FID\tTotal     Routine   Average\n");
        fprintf(stderr,"[TIME] INIT\t%0.2e  %0.2e\n",init_routine,init_routine);
        start_routine = end_routine;
    }

    //---
    // loop over the frame_initialize routine
    //---

    while(m_read_frame_stdin_xyz(s)) {
        if(! options.dry) {
            m_sys_calc(s);
            frame_routine(s);
            fflush(stdout);
        }
        s->frames_processed += 1;

        if (options.status) {
            fid = s->current_frame_id+1;
            if (fid % options.status == 0) {
                end_routine = clock();
                delta = (double)(end_routine - start_routine)/CLOCKS_PER_SEC;
                elapsed = (double)(end_routine - start_program)/CLOCKS_PER_SEC;
                avg = (elapsed-init_routine)/s->frames_processed;
                fprintf(stderr,"[TIME] %d\t%0.2e  %0.2e  %0.2e\n",fid,elapsed,delta,avg);
                start_routine = end_routine;
            }
        }
    }

    //---
    // call the frame_finalise routine
    //---

    if(! options.dry) {
        frame_finalise(s);
    }

    if (options.status) {
        end_routine = clock();
        delta = (double)(end_routine - start_routine)/CLOCKS_PER_SEC;
        elapsed = (double)(end_routine - start_program)/CLOCKS_PER_SEC;
        fprintf(stderr,"[TIME] FIN\t%0.2e  %0.2e\n",elapsed,delta);
    }

    return(0);

}
