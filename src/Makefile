#---
# Makefile for comoda programs
# written by myliu, 2012
#---

############################################################
# front matters
# 	Some important things are defined here, mostly because
# 	they have to be defined at the start.
# 	You can probably skip over this part!
############################################################

# set the compiler
cc = gcc
cflags = -pthread -Wall -std=c99

#---
#	define the library objects used by (almost) all programs 
#---
BUILD_DIR := ../build
BIN_DIR := ../bin
LIB_DIR := ../lib
LIB_SRC := debug.c vector.c marray.c array.c arithmetic.c db.c maggregate.c \
           molecules.c commander.c sys.c frames.c cell.c coulomb.c mstring.c \
           forcefield.c stress.c
LIB_OBJ := $(addprefix $(BUILD_DIR)/lib.,$(LIB_SRC:.c=.o))
FRAME_APP := $(BUILD_DIR)/lib.frame_main.o
BUILD_LIB := $(LIB_OBJ) $(FRAME_APP)

# set flags for includes and linked libraries
INC := -I../lib/headers -I../lib/iniparser/src
LIB := -L../lib/iniparser -liniparser -lm

############################################################
# building applications
# 	this is where your src application is build
############################################################
#---
# numeric applications
#---
# folder that contains the *.c sources
NUMERIC_DIR := numeric

# binaries to be made. the prefix will be added automatically
NUMERIC_SRC := agg.c bin.c mean.c

# define the binaries and objects to be made
NUMERIC_OBJ := $(addprefix $(BUILD_DIR)/$(NUMERIC_DIR).,$(NUMERIC_SRC:.c=.o))
NUMERIC_BIN := $(addprefix $(BIN_DIR)/$(NUMERIC_DIR).,$(NUMERIC_SRC:.c=))

# targets to compile
numeric: .prereq $(NUMERIC_BIN)
	@$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"

# generic rules to compile the objects
$(NUMERIC_OBJ): $(BUILD_DIR)/$(NUMERIC_DIR).%.o: $(NUMERIC_DIR)/%.c
	$(CC) -c $< $(OBJ_CFLAGS) -o $@

# generic rules to compile the binaries
$(NUMERIC_BIN): $(BIN_DIR)/%: $(BUILD_DIR)/%.o $(LIB_OBJ)
	$(CC) $^ $(CFLAGS) -o $@


#---
# structure programs
#---
# folder that contains the *.c sources
STRUCTURE_DIR := structure_test
# binaries to be made. the prefix will be added automatically
STRUCTURE_SRC := centerofmass.c n0n_hydrophobe.c phiMD_plane.c translation.c rotationmatrix.c electricfield_ion.c bond_displacement.c hbond_correlation.c hbond_pairs.c r6r12_ion.c phi112.c phi220.c phi123.c \
                 phi110.c dipole_dist_solute.c phi121.c n0n_waterwater.c n0n_ww_ion.c \
                 ion_water_angle.c n0n_ion.c rdf.c displacement.c \
                 r_6_12.c dielectric.c LJ_FreeEnergy.c phiMD.c r6_Binary.c \
                 phiMD_solvation.c neighbour.c dipole.c sf.c phiMD_bisDipole.c dielectric_multi.c dielectric_multi_separated.c sf_test.c

# define the binaries and objects to be made
STRUCTURE_OBJ := $(addprefix $(BUILD_DIR)/$(STRUCTURE_DIR).,\
                             $(STRUCTURE_SRC:.c=.o))
STRUCTURE_BIN := $(addprefix $(BIN_DIR)/$(STRUCTURE_DIR).,$(STRUCTURE_SRC:.c=))

# targets to compile
structure: .prereq $(STRUCTURE_BIN)
	@$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"

# generic rules to compile the objects
$(STRUCTURE_OBJ): $(BUILD_DIR)/$(STRUCTURE_DIR).%.o: $(STRUCTURE_DIR)/%.c
	$(CC) -c $< $(OBJ_CFLAGS) -o $@

# generic rules to compile the binaries using lib/frame_main.c
$(STRUCTURE_BIN): $(BIN_DIR)/%: $(FRAME_APP) $(BUILD_DIR)/%.o $(LIB_OBJ)
	$(CC) $^ $(CFLAGS) -o $@

#---
# slag programs
#---
# folder that contains the *.c sources
SLAG_DIR := slag

# binaries to be made. the prefix will be added automatically
SLAG_SRC := tetrahedral.c ions_M2.c shell_distort.c delete.c coordination.c chain.c \
            coordination_pop.c nQ.c nO.c M-O-M.c shell_distort_2.c O-M-O.c \
            spherical_angles.c

# define the binaries and objects to be made
SLAG_OBJ := $(addprefix $(BUILD_DIR)/$(SLAG_DIR).,\
                        $(SLAG_SRC:.c=.o))
SLAG_BIN := $(addprefix $(BIN_DIR)/$(SLAG_DIR).,$(SLAG_SRC:.c=))

# targets to compile
slag: .prereq $(SLAG_BIN)
	@$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"

# generic rules to compile the objects
$(SLAG_OBJ): $(BUILD_DIR)/$(SLAG_DIR).%.o: $(SLAG_DIR)/%.c
	$(CC) -c $< $(OBJ_CFLAGS) -o $@

# generic rules to compile the binaries using lib/frame_main.c
$(SLAG_BIN): $(BIN_DIR)/%: $(FRAME_APP) $(BUILD_DIR)/%.o $(LIB_OBJ)
	$(CC) $^ $(CFLAGS) -o $@

#---
# interface programs
#---
# folder that contains the *.c sources
INTERFACE_DIR := interface_test

# binaries to be made. the prefix will be added automatically
INTERFACE_SRC := tetrahedral_dist.c dipole_dist.c n0n_waters.c phi110.c phi112.c surfacetension.c n0n.c \
				 angle_dist.c dangling_OH.c charge_dist.c pairs.c rdf.c \
				 phi_slices.c correlations.c dielectric_z.c

# define the binaries and objects to be made
INTERFACE_BIN := $(addprefix $(BIN_DIR)/$(INTERFACE_DIR).,$(INTERFACE_SRC:.c=))
INTERFACE_OBJ := $(addprefix $(BUILD_DIR)/$(INTERFACE_DIR).,\
                             $(INTERFACE_SRC:.c=.o))

# targets to compile
interface: .prereq $(INTERFACE_BIN)
	@$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"

$(INTERFACE_OBJ): $(BUILD_DIR)/$(INTERFACE_DIR).%.o: $(INTERFACE_DIR)/%.c
	$(CC) -c $< $(OBJ_CFLAGS) -o $@

# generic rules to compile the binaries
#   note that the frame binaries must be compiled with lib/frame_main.c
#   as that is where the main() routine resides.
$(INTERFACE_BIN): $(BIN_DIR)/%: $(BUILD_DIR)/lib.frame_main.o $(BUILD_DIR)/%.o \
                  $(LIB_OBJ)
	$(CC) $^ $(CFLAGS) -o $@


#---
# interfacial test programs - Quinn
#---
# folder that contains the *.c sources
#INTERFACIAL_DIR := interfacial_test
#
# binaries to be made. the prefix will be added automatically
#INTERFACIAL_SRC := correlations.c
#
# define the binaries and objects to be made
#INTERFACIAL_BIN := $(addprefix $(BIN_DIR)/$(INTERFACIAL_DIR).,$(INTERFACIAL_SRC:.c=))
#INTERFACIAL_OBJ := $(addprefix $(BUILD_DIR)/$(INTERFACIAL_DIR).,\
#                               $(INTERFACIAL_SRC:.c=.o))
#
# targets to compile
#interfacial: .prereq $(INTERFACIAL_BIN)
#  @$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"
#
#$(INTERFACIAL_OBJ): $(BUILD_DIR)/$(INTERFACIAL_DIR).%.o: $(INTERFACIAL_DIR)/%.c
#	$(CC) -c $< $(OBJ_CFLAGS) -o $@
#
#$(INTERFACIAL_BIN): $(BIN_DIR)/%: $(BUILD_DIR)/lib.frame_main.o $(BUILD_DIR)/%.o \
#										$(LIB_OBJ)
#	$(CC) $^ $(CFLAGS) -o $@


#---
# widom programs
#---
# folder that contains the *.c sources
WIDOM_DIR := widom

# binaries to be made. the prefix will be added automatically
WIDOM_SRC := insert.c tetrahedral.c e_field.c rdf.c dip_dist.c h_bond.c \
             h_bond_cluster.c pair_pmf.c

# define the binaries and objects to be made
WIDOM_APP := $(BUILD_DIR)/$(WIDOM_DIR).widom.o
WIDOM_OBJ := $(addprefix $(BUILD_DIR)/$(WIDOM_DIR).,$(WIDOM_SRC:.c=.o)) \
             $(WIDOM_APP)
WIDOM_BIN := $(addprefix $(BIN_DIR)/$(WIDOM_DIR).,$(WIDOM_SRC:.c=))

# targets to compile
widom: .prereq $(WIDOM_BIN) 
	@$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"

$(WIDOM_OBJ): $(BUILD_DIR)/$(WIDOM_DIR).%.o : $(WIDOM_DIR)/%.c
	$(CC) -c $< $(OBJ_CFLAGS) -o $@

# generic rules to compile the binaries using frame_main.c and widom.c
$(WIDOM_BIN): $(BIN_DIR)/% : $(FRAME_APP) $(WIDOM_APP) $(BUILD_DIR)/%.o \
              $(LIB_OBJ)
	$(CC) $^ $(CFLAGS) -o $@

############################################################
# backstage
# 	this is where lib objects are built, and other targets
# 	are defined
############################################################
#---
# some housekeeping routines
#---

lib: .prereq
	@exit

# the prereq checks that submodules are compiled, create directories,
# then builds lib objects
.prereq: | ../lib/iniparser.a .chkdirs $(BUILD_LIB)
	@$(ECHO) "-----\n[make $@]: Successfully built $@.\n-----"

# this checks for build and bin directories
.chkdirs: $(BUILD_DIR) $(BIN_DIR)
	@exit

veryclean: clean
	@cd ../lib/iniparser; make clean

clean:
	rm -f $(BUILD_DIR)/*.o $(BIN_DIR)/*

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

#---
# build the library objects
#---
../lib/iniparser.a:
	@cd ../lib/iniparser; make libiniparser.a

$(BUILD_LIB): $(BUILD_DIR)/lib.%.o: $(LIB_DIR)/%.c
	$(CC) -c $< $(OBJ_CFLAGS) -o $@

#---
# print all available targets
#---
# `make -pn` prints all the rules and targets in the makefile
# `grep -e '^[a-z]\+:'` selects lines that are "target:" (lowercase)
# `sed 's/:.*//g'` discards any string following ":"
# `sed 's/\(.*\)/  \1/g'` indents the target
help: 
	@bash -c "echo Available targets:; \
            make -pn | grep -e '^[a-z]\+:' | sed 's/:.*//g' | \
            sed 's/\(.*\)/	\1/g'"

#---
# determine which echo to use for multiline output
#---
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
  ECHO := echo -e
else
  ECHO := echo
endif

#---
# some compiler options
#---
# set the default compiler to be gcc
defaultc = gcc
ifndef $(cc)
	cc = $(defaultc)
endif

CC := $(cc)
CFLAGS := $(INC) $(LIB) $(cflags)
OBJ_CFLAGS := $(INC) $(cflags)

#---
# catch all 
#---
# run "make help" if no target is supplied
.DEFAULT_GOAL := help

# a catch-all message for any targets that do not exist
%::
	@$(ECHO) Target \`$@\' not found. Use \"make help\" to list all targets.
