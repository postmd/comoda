#include "widom.h"

char *description \
 = " performs simple Widom particle insertion to give a chemical potential.\n\
";

void widom_print_example() {
}

void widom_init(M_system* sys, dictionary* dict) {
}

void widom_frame_init(M_system* sys) {
}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {
}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {
}

void widom_frame_end(M_system* sys, prec sum_pr) {
}

void widom_finalise(M_system* sys) {
}
