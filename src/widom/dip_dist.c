#include "widom.h"

char *description \
 = "widom.dip_dist\n\
    \n\
      calculates the distribution of dipole moment orientations, given by:\n\
    \n\
                cos(theta_j) = \\hat{r}_ij.\\hat{mu}_j\n\
      where r_ij is a displacement of j from i, and mu_j is the dipole moment of j.\n\
      we calculate the distribution for solvent-solvent, solvent-solvent in presence of\n\
      solute, and solvent-solute.\n\
    \n\
    Configuration file fortmat:\n\
    ===========================\n\
      [system]\n\
      rcut            = 20.0      ; the maximum separation allowed for pairs (bohr)\n\
      setT            = 0.000944  ; temperature in atomic units\n\
      solvent_name    = water     ; the name of the solvent molecule species\n\
      [dip_dist]\n\
      dcostheta       = 0.01      ; the distribution of cos(theta)\n\
      sv_bulk_out     = bulk.cos      ; the output file for the bulk solvent\n\
      sv_sol_out      = solution.cos  ; the output file for the solvent in the solution\n\
      solute_out      = Ar.cos        ; the output file for the solute\n\
      [insert]\n\
      ; flags for what kind of calculation to perform\n\
      calc_dMu        = false     ; whether or not to calculate the free energy of\n\
                                    ; particle insertion \n\
      print_coords    = true      ; whether or not to print coordinates of insertion\n\
      print_sort      = true      ; whether or not to sort the insertion coordinates\n\
                                    ; before printing. Default = true.\n\
      print_excluded  = true      ; whether or not to print the insertion coordinates\n\
                                    ; which are within exclusion_r. Default = false.\n\
      print_range     = 1,100     ; the range of insertion coordinates to print\n\
                                    ; note that the printed coordinates are ranked\n\
                                    ; from the lowest DeltaE (most probable) to\n\
                                    ; the greatest DeltaE (least probable)\n\
      coords_file     = insert_coords\n\
                                  ; print the coordinates into this file\n\
                                    ; if unspecified, prints to stdout\n\
                                    ; see note 1 for column format\n\
      dMu_file        = insert_dMu\n\
                                  ; print the insertion energies into this file\n\
                                    ; if unspecified, prints to stdout\n\
      only_nearest    = true      ; only sort the insertion coordinates by their\n\
                                    ; nearest neighbour separation\n\
      ngrid           = 10,10,10  ; number of grid points in the x,y,z direction\n\
      ; interaction potential parameters\n\
      ; comma-separated list, the length of the list must equal the number\n\
      ; of atomic species, the order of potentials must be given in the same\n\
      ; order as the declaration above\n\
      ;\n\
      ; U_j(r) = Q/r + (smallsig(j)/r)^12 + B(j)*exp(-abm(j)*r) - C6(j)/r^6 - C8(j)/r^8\n\
      ;\n\
      ; all interaction parameters are in atomic units\n\
      Q               = 1.0,-1.0\n\
      smallsig        = 1.0,1.0\n\
      B               = 0.0,0.0\n\
      abm             = 0.0,0.0\n\
      C6              = 0.0,0.0\n\
      C8              = 0.0,0.0\n\
      ; optimization settings \n\
      exclusion_r     = 3.0,4.0   ; radius to exclude from computing U \n\
      cell_ngrid      = 10,10,10  ; divide the box into small cells, for fewer\n\
                                    ; wasted pairs\n\
    ===========================\n\
    \n\
    notes:\n\
      1. column format for coords_file:\n\
          frame_id-grid_id    x/bohr    y/bohr    z/bohr    value (calc_dMu=exp(-beta dU), nearest_only=r_min^2)\n\
";

M_n_aggregate *total_sv_bulk, *frame_sv_bulk;
M_n_aggregate *total_sv_sol;
M_n_aggregate *total_solute, *insert_solute;

M_molec_spec *solvent_mspec;

FILE *sv_bulk_out, *sv_sol_out, *solute_out;

void widom_print_example() {
};

void chk_extend( prec r, prec q ) {

    if ( r > total_sv_bulk->max[0] || q < total_sv_bulk->min[1] || q > total_sv_bulk->max[1] ) {
      total_sv_bulk->key[0] = r;
      total_sv_bulk->key[1] = q;

      total_sv_sol->key[0] = r;
      total_sv_sol->key[1] = q;

      total_solute->key[0] = r;
      total_solute->key[1] = q;

      frame_sv_bulk->key[0] = r;
      frame_sv_bulk->key[1] = q;

      insert_solute->key[0] = r;
      insert_solute->key[1] = q;

      total_sv_bulk = m_n_aggregate_extend(total_sv_bulk);
      total_sv_sol  = m_n_aggregate_extend(total_sv_sol);
      total_solute  = m_n_aggregate_extend(total_solute);
      frame_sv_bulk = m_n_aggregate_extend(frame_sv_bulk);
      insert_solute = m_n_aggregate_extend(insert_solute);
    }

}

void widom_init(M_system* sys, dictionary* dict) {

  prec *width = malloc(sizeof(prec) * 2);
  prec *min = malloc(sizeof(prec) * 2);
  prec *max = malloc(sizeof(prec) * 2);

  min[0] = 0.0;
  max[0] = sys->rcut;
  width[0] = iniparser_getdouble(dict, "rdf:dr", 0.01);

  // cos(theta) has a range of [-1,1]
  min[1] = -1.0;
  max[1] = 1.0;
  width[1] = iniparser_getdouble(dict, "dip_dist:dcostheta", 0.01);

  total_sv_bulk = m_n_aggregate_new(2, min, max, width, 1);
  total_sv_sol  = m_n_aggregate_new(2, min, max, width, 1);
  total_solute  = m_n_aggregate_new(2, min, max, width, 1);
  frame_sv_bulk = m_n_aggregate_new(2, min, max, width, 0);
  insert_solute = m_n_aggregate_new(2, min, max, width, 0);

  free(min);
  free(max);
  free(width);

  total_sv_bulk->print_2d_values = 1;
  total_sv_sol->print_2d_values  = 1;
  total_solute->print_2d_values  = 1;

  char *solvent_name;

  solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
  if ( solvent_name != NULL ) {
    solvent_mspec = m_molec_spec_find( sys, solvent_name );
    if ( solvent_mspec == NULL ) {
      FATAL("No molecules in the system matches %s.\n",solvent_name);
      exit(1);
    }
    if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
      FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
      exit(1);
    }
  } else {
    FATAL("Must supply solvent_name in the configuration file.\n");
    exit(1);
  }

  char *in;

  in = iniparser_getstring(dict, "dip_dist:sv_bulk_out", NULL );
  if ( in != NULL ) {
    sv_bulk_out = fopen(in, "w");
  } else {
    sv_bulk_out = stdout;
  }

  in = iniparser_getstring(dict, "dip_dist:sv_sol_out", NULL );
  if ( in != NULL ) {
    sv_sol_out = fopen(in, "w");
  } else {
    sv_sol_out = stdout;
  }

  in = iniparser_getstring(dict, "dip_dist:solute_out", NULL );
  if ( in != NULL ) {
    solute_out = fopen(in, "w");
  } else {
    solute_out = stdout;
  }

}

void widom_frame_init(M_system* sys) {

  calc_dipoles(sys);

  M_molec *molec_i, *molec_j;
  int i,j;

  m_n_aggregate_reset(frame_sv_bulk);
  prec cos_i, cos_j;
  for( i=0; i<solvent_mspec->count-1; i++) {
    molec_i = solvent_mspec->molecs->data[i];
    for( j=i+1; j<solvent_mspec->count; j++) {
      molec_j = solvent_mspec->molecs->data[j];

      // rhat: from i to j
      M_vector dr = m_radius(molec_j->center, molec_i->center);
      prec r = m_vector_mag(dr);

      M_vector rhat = m_vector_scale(dr,1.0/r);
      cos_j = m_vector_dot( rhat, m_vector_unit(molec_j->dipole) );
      cos_i = -m_vector_dot( rhat, m_vector_unit(molec_i->dipole) );

      chk_extend(r,cos_j);
      chk_extend(r,cos_i);

      frame_sv_bulk->key[0]=r;
      frame_sv_bulk->key[1]=cos_j;

      _m_n_aggregate(frame_sv_bulk);

      frame_sv_bulk->key[0]=r;
      frame_sv_bulk->key[1]=cos_i;

      _m_n_aggregate(frame_sv_bulk);
    }   
  }
}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
  m_n_aggregate_reset(insert_solute);
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {

  if ( atom->molec->spec == solvent_mspec && atom->id_in_molec == solvent_mspec->center ) {
    prec r = sqrt(r2);

    M_vector rhat = m_vector_scale(dr,1.0/r);
    prec cos_j = m_vector_dot( rhat, m_vector_unit(atom->molec->dipole) );

    chk_extend(r,cos_j);

    insert_solute->key[0] = r;
    insert_solute->key[1] = cos_j;
    _m_n_aggregate(insert_solute);
  }

}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {

  int i;
  for ( i=0; i<insert_solute->total_length; i++ ) {
    total_solute->values[i] += insert_solute->count[i]*pr*dV;
  }

}

void widom_frame_end(M_system* sys, prec sum_pr) {

  int i;
  for ( i=0; i<frame_sv_bulk->total_length; i++ ) { 
    total_sv_bulk->values[i] += frame_sv_bulk->count[i];
    total_sv_sol->values[i] += frame_sv_bulk->count[i] * sum_pr;
  }

}

void widom_finalise(M_system* sys) {

  int i,j;
  prec norm_sv_bulk, norm_sv_sol, norm_solute;
  int l0 = frame_sv_bulk->length[0];
  int l1 = frame_sv_bulk->length[1];
  prec dE = frame_sv_bulk->width[1];
  for ( i=0; i<l0; i++ ) { 
    norm_sv_bulk = sum_farray( total_sv_bulk->values+i*l1, l1 );
    norm_sv_sol = sum_farray( total_sv_sol->values+i*l1, l1 );
    norm_solute = sum_farray( total_solute->values+i*l1, l1 );
    if ( norm_sv_bulk == 0 ) { 
      norm_sv_bulk = 1;
    }   
    if ( norm_sv_sol == 0 ) { 
      norm_sv_sol = 1;
    }   
    if ( norm_solute == 0 ) { 
      norm_solute = 1;
    }   
    
    for ( j=0; j<l1; j++ ) { 
      total_sv_bulk->values[i*l1+j] /= norm_sv_bulk*dE;
      total_sv_sol->values[i*l1+j] /= norm_sv_sol*dE;
      total_solute->values[i*l1+j] /= norm_solute*dE;
    }   
  }   
  
  m_n_aggregate_fprint(sv_bulk_out,total_sv_bulk);
  m_n_aggregate_fprint(sv_sol_out,total_sv_sol);
  m_n_aggregate_fprint(solute_out,total_solute);

  m_n_aggregate_free(total_sv_bulk);
  m_n_aggregate_free(frame_sv_bulk);
  m_n_aggregate_free(total_solute);
  m_n_aggregate_free(total_sv_sol);
  m_n_aggregate_free(insert_solute);

  fclose(sv_bulk_out);
  fclose(sv_sol_out);
  fclose(solute_out);

}
