#include "widom.h"

char *description \
 = "frame.h_bond_cluster\n\
    \n\
      Calculates the number of H-bond clusters according to Godec & Merzel.\n\
    \n\
    Configuration file fortmat:\n\
    ===========================\n\
      [system]\n\
      rcut            = 20.0      ; the maximum separation allowed for pairs (bohr)\n\
      setT            = 0.000944  ; temperature in atomic units\n\
      solvent_name    = water     ; the name of the solvent molecule species to compute\n\
                                    ; the tetrahedral-order calculation.\n\
      [h_bond]\n\
      acceptor_atom_spec = 0      ; the atom_spec of the H-bond acceptor atom (e.g. O, N, F)\n\
                                    ; the atom_spec's id in the solvent molec_spec (atom_spec->id_in_spec)\n\
      donor_atom_spec    = 1      ; the atom_spec of the H-bond donor atom (e.g. H)\n\
                                    ; the atom_spec's id in the solvent molec_spec (atom_spec->id_in_spec)\n\
      R_sv1           = 7         ; the cutoff r for the first solvation shell\n\
      R_sv2           = 10        ; the cutoff r for the second solvation shell\n\
      min_OHO         = 150       ; minimum angle for a O-H...O angle in degrees\n\
      max_OHO         = 180       ; maximum angle for a O-H...O angle in degrees\n\
      min_HOO         = 0         ; minimum angle for a H-O...O angle in degrees\n\
      max_HOO         = 30        ; maximum angle for a H-O...O angle in degrees\n\
      min_R_OO        = 4         ; minimum O...O distance for a hydrogen bond in bohr\n\
      max_R_OO        = 6         ; maximum O...O distance for a hydrogen bond in bohr\n\
      min_R_HO        = 4         ; minimum H...O distance for a hydrogen bond in bohr\n\
      max_R_HO        = 6         ; maximum H...O distance for a hydrogen bond in bohr\n\
      average_out     = total.h_bond    ; the output file for the total H-bond and nearest-neighbours stats\n\
      clusters_out    = clusters.h_bond ; the output file for the number of H-bond clusters (Godec & Merzel)\n\
      [insert]\n\
      ; flags for what kind of calculation to perform\n\
      calc_dMu        = false     ; whether or not to calculate the free energy of\n\
                                    ; particle insertion \n\
      print_coords    = true      ; whether or not to print coordinates of insertion\n\
      print_sort      = true      ; whether or not to sort the insertion coordinates\n\
                                    ; before printing. Default = true.\n\
      print_excluded  = true      ; whether or not to print the insertion coordinates\n\
                                    ; which are within exclusion_r. Default = false.\n\
      print_range     = 1,100     ; the range of insertion coordinates to print\n\
                                    ; note that the printed coordinates are ranked\n\
                                    ; from the lowest DeltaE (most probable) to\n\
                                    ; the greatest DeltaE (least probable)\n\
      coords_file     = insert_coords\n\
                                  ; print the coordinates into this file\n\
                                    ; if unspecified, prints to stdout\n\
                                    ; see note 1 for column format\n\
      dMu_file        = insert_dMu\n\
                                  ; print the insertion energies into this file\n\
                                    ; if unspecified, prints to stdout\n\
      only_nearest    = true      ; only sort the insertion coordinates by their\n\
                                    ; nearest neighbour separation\n\
      ngrid           = 10,10,10  ; number of grid points in the x,y,z direction\n\
      ; interaction potential parameters\n\
      ; comma-separated list, the length of the list must equal the number\n\
      ; of atomic species, the order of potentials must be given in the same\n\
      ; order as the declaration above\n\
      ;\n\
      ; U_j(r) = Q/r + (smallsig(j)/r)^12 + B(j)*exp(-abm(j)*r) - C6(j)/r^6 - C8(j)/r^8\n\
      ;\n\
      ; all interaction parameters are in atomic units\n\
      Q               = 1.0,-1.0\n\
      smallsig        = 1.0,1.0\n\
      B               = 0.0,0.0\n\
      abm             = 0.0,0.0\n\
      C6              = 0.0,0.0\n\
      C8              = 0.0,0.0\n\
      ; optimization settings \n\
      exclusion_r     = 3.0,4.0   ; radius to exclude from computing U \n\
      cell_ngrid      = 10,10,10  ; divide the box into small cells, for fewer\n\
                                    ; wasted pairs\n\
    ===========================\n\
    \n\
    notes:\n\
      1. column format for coords_file:\n\
          frame_id-grid_id    x/bohr    y/bohr    z/bohr    value (calc_dMu=exp(-beta dU), nearest_only=r_min^2)\n\
";

M_molec_spec *solvent_mspec;
M_atom_spec *donor_aspec, *acceptor_aspec;

M_neighbours ** nearest_neighbours;

FILE *average_out, *clusters_out;

prec min_OHO, min_HOO, max_OHO, max_HOO;
prec min_cos_OHO, max_cos_OHO, min_cos_HOO, max_cos_HOO;
prec min_R_OO, min_R_HO, max_R_OO, max_R_HO;

prec R_sv1, R_sv2;

// notation: <> is a HB, .. is a non-HB nearest neighbour
//            * denotes the central molecule
// [0].: first solvation shell
// [1].: second solvation shell
// .[0]: n(i*<>j)                         x1
// .[1]: n(i*..j)                         x1    2-body: x1
//
// .[2]: n(j<>i*<>k)                      x1
// .[3]: n(j<>i*..k or j..i*<>k)          x2
//         j..i*..k (missing?)            x1    3-body: x4
//
// .[4]: n(j<>i*..k<>l or k<>i*..j<>l)    x1
// .[5]: n(j<>i*<>k<>l or k<>i*<>j<>l)    x1
// .[6]: n(j..i*..k..l or k..i*..j..l)    x1
// .[7]: n(j..i*<>k..l or k..i*<>j..l)    x1
// .[8]: n(j<>i*..k..l or k<>i*..j..l)    x2 (equiv j..i*..k<>l or k..i*..j<>l)
// .[9]: n(j<>i*<>k..l or k<>i*<>j..l)    x2 (equiv j..i*<>k<>l or k..i*<>j<>l)
//                                              4-body: x8
prec frame_clusters[2][10];
prec total_clusters[2][10];

prec total_bulk_nn, total_sol_nn, frame_nn;
prec total_bulk_hb, total_sol_hb, frame_hb;

void widom_print_example() {
}

void widom_init(M_system* sys, dictionary* dict) {

  int i_sv,j;
  for( i_sv=0; i_sv<2; i_sv++ ) {
    for( j=0; j<10; j++ ) {
      total_clusters[i_sv][j] = 0;
    }
  }

  char *solvent_name;

  solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
  if ( solvent_name != NULL ) {
    solvent_mspec = m_molec_spec_find( sys, solvent_name );
    if ( solvent_mspec == NULL ) {
      FATAL("No molecules in the system matches %s.\n",solvent_name);
      exit(1);
    }
    if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
      FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
      exit(1);
    }
  } else {
    FATAL("Must supply solvent_name in the configuration file.\n");
    exit(1);
  }

  int i;

  i = iniparser_getint(dict, "h_bond:donor_atom_spec", -1 );
  if ( i < 0 ) {
    FATAL("Must supply the donor_atom_spec in the configuration file.\n");
    exit(1);
  } else {
    if ( i >= 0 && i < solvent_mspec->atom_specs->count ) { 
      donor_aspec = m_mspec_aspec(solvent_mspec, i);
    } else {
      FATAL("Solvent molecule only has %d atom_specs, the specified atom_spec (id=%d) does not exist.\n",solvent_mspec->atom_specs->count,i);
      exit(1);
    }
  }

  i = iniparser_getint(dict, "h_bond:acceptor_atom_spec", -1 );
  if ( i < 0 ) {
    FATAL("Must supply the acceptor_atom_spec in the configuration file.\n");
    exit(1);
  } else {
    if ( i >= 0 && i < solvent_mspec->atom_specs->count ) { 
      acceptor_aspec = m_mspec_aspec(solvent_mspec, i);
    } else {
      FATAL("Solvent molecule only has %d atom_specs, the specified atom_spec (id=%d) does not exist.\n",solvent_mspec->atom_specs->count,i);
      exit(1);
    }
  }

  min_OHO = iniparser_getdouble(dict, "h_bond:min_OHO", -1 );
  if ( min_OHO < 0 || min_OHO > 180 ) {
    FATAL("Must supply a valid min_OHO for h_bond.\n");
    exit(1);
  }
  max_cos_OHO = cos( min_OHO * pi / 180.0 );

  max_OHO = iniparser_getdouble(dict, "h_bond:max_OHO", 180.0 );
  if ( max_OHO <= min_OHO || max_OHO > 180 ) {
    FATAL("Must supply a valid max_OHO (%f) greater than min_OHO (%f).\n",max_OHO,min_OHO);
    exit(1);
  }
  min_cos_OHO = cos( max_OHO * pi / 180.0 );

  min_HOO = iniparser_getdouble(dict, "h_bond:min_HOO", 0.0 );
  if ( min_HOO > 180 || min_HOO < 0 ) {
    FATAL("Must supply a valid min_HOO for h_bond.\n");
    exit(1);
  }
  max_cos_HOO = cos( min_HOO * pi / 180.0 );

  max_HOO = iniparser_getdouble(dict, "h_bond:max_HOO", -1 );
  if ( max_HOO <= min_HOO || max_HOO > 180 ) {
    FATAL("Must supply a max_HOO (%f) greater than min_HOO (%f).\n",max_HOO,min_HOO);
    exit(1);
  }
  min_cos_HOO = cos( max_HOO * pi / 180.0 );
  
  min_R_OO = iniparser_getdouble(dict, "h_bond:min_R_OO", 0.0 );
  max_R_OO = iniparser_getdouble(dict, "h_bond:max_R_OO", -1 );
  if ( max_R_OO < 0 || max_R_OO<min_R_OO ) {
    FATAL("Must supply a valid max_R_OO for h_bond.\n");
    exit(1);
  }
  min_R_HO = iniparser_getdouble(dict, "h_bond:min_R_HO", 0.0 );
  max_R_HO = iniparser_getdouble(dict, "h_bond:max_R_HO", -1 );
  if ( max_R_HO < 0 || max_R_HO<min_R_HO ) {
    FATAL("Must supply a valid max_R_HO for h_bond.\n");
    exit(1);
  }

  R_sv1 = iniparser_getdouble(dict, "h_bond:R_sv1", -1 );
  R_sv2 = iniparser_getdouble(dict, "h_bond:R_sv2", -1 );
  if ( R_sv1 < 0 || R_sv2 < 0 || R_sv2 < R_sv1 ) {
    FATAL("Must supply valid R for the first and second solvation shell (%lf,%lf)\n",R_sv1,R_sv2);
    exit(1);
  }

  char *in;

  in = iniparser_getstring(dict, "h_bond:average_out", NULL );
  if ( in != NULL ) {
    average_out = fopen(in, "w");
  } else {
    average_out = stdout;
  }

  in = iniparser_getstring(dict, "h_bond:clusters_out", NULL );
  if ( in != NULL ) {
    clusters_out = fopen(in, "w");
  } else {
    clusters_out = stdout;
  }

  nearest_neighbours = malloc(sizeof(M_neighbours *) * solvent_mspec->count);

  total_bulk_nn = 0;
  total_bulk_hb = 0;
}

void widom_frame_init(M_system* sys) {

  calc_centers(sys);

  int i,j;
  M_molec *molec_i, *molec_j;

  // clear nearest neighbour list
  for (i=0; i<solvent_mspec->count; i++) {
    nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
  }

  // populate nearest neighbour
  for( i=0; i<solvent_mspec->count-1; i++) {
    molec_i = solvent_mspec->molecs->data[i];

    for( j=i+1; j<solvent_mspec->count; j++) {
      molec_j = solvent_mspec->molecs->data[j];

      // r_OO = r_i --> r_j (unit vector)
      M_vector r_OO = m_radius(molec_j->center, molec_i->center);
      prec r2 = m_vector_mag2(r_OO);
      prec r = pow(r2,0.5);
      r_OO = m_vector_scale(r_OO,1.0/r);


      if ( r<max_R_OO && r>min_R_OO ) {

        m_nearest_neighbours_append( nearest_neighbours[i], molec_j);
        m_nearest_neighbours_append( nearest_neighbours[j], molec_i); 

        int ii, jj, kk;
        M_atom *acceptor_i, *donor_i, *acceptor_j, *donor_j;
        prec cosOHO, cosHOO;

        // molec_i donating to molec_j
        // select O from molecule i
        for ( ii=0; ii<molec_i->atoms->count; ii++ ) {
          acceptor_i = m_molec_atom(molec_i, ii);
          if ( acceptor_i->spec == acceptor_aspec ) {

            // select H from molecule i
            for ( jj=0; jj<molec_i->atoms->count; jj++ ) {
              donor_i = m_molec_atom(molec_i, jj);
              if ( donor_i->spec == donor_aspec && acceptor_i != donor_i ) {
                
                // HO_i = H_i --> O_i
                M_vector HO_i = m_vector_unit(m_radius(acceptor_i->pos, donor_i->pos));

                for ( kk=0; kk<molec_j->atoms->count; kk++ ) {
                  acceptor_j = m_molec_atom(molec_j, kk);
                  if ( acceptor_j->spec == acceptor_aspec ) {

                    // HO_j = H_i --> O_j
                    M_vector HO_j = m_radius(acceptor_j->pos, donor_i->pos);
                    prec HO_r = m_vector_mag(HO_j);
                    HO_j = m_vector_scale(HO_j, 1.0/HO_r);
                    //M_vector HO_j = m_vector_unit(m_radius(acceptor_j->pos, donor_i->pos));
                  
                    // (H_i --> O_i) . (H_i --> O_j)
                    cosOHO = m_vector_dot(HO_i, HO_j);

                    // (O_i --> H_i) . (O_i --> O_j)
                    cosHOO = -1.0*m_vector_dot(HO_i, r_OO);

                    if ( cosHOO < max_cos_HOO && cosHOO > min_cos_HOO && cosOHO < max_cos_OHO && cosOHO > min_cos_OHO && HO_r < max_R_HO && HO_r > min_R_HO ) {
                      nearest_neighbours[i]->min_rank += 1;
                      nearest_neighbours[i]->dummy[nearest_neighbours[i]->n_max-1] += 1;
                      nearest_neighbours[j]->min_rank += 1;
                      nearest_neighbours[j]->dummy[nearest_neighbours[j]->n_max-1] += 1;
                    }
                  }
                }
              }
            }
          }
        }
          
        // molec_j donating to molec_i
        // select O from molecule j
        for ( ii=0; ii<molec_j->atoms->count; ii++ ) {
          acceptor_j = m_molec_atom(molec_j, ii);
          if ( acceptor_j->spec == acceptor_aspec ) {

            // select H from molecule j
            for ( jj=0; jj<molec_j->atoms->count; jj++ ) {
              donor_j = m_molec_atom(molec_j, jj);
              if ( donor_j->spec == donor_aspec ) {
                
                // HO_j = H_j --> O_j
                M_vector HO_j = m_vector_unit(m_radius(acceptor_j->pos, donor_j->pos));

                for ( kk=0; kk<molec_i->atoms->count; kk++ ) {
                  acceptor_i = m_molec_atom(molec_i, kk);
                  if ( acceptor_i->spec == acceptor_aspec ) {

                    // HO_i = H_j --> O_i
                    M_vector HO_i = m_radius(acceptor_i->pos, donor_j->pos);
                    prec HO_r = m_vector_mag(HO_i);
                    HO_i = m_vector_scale(HO_i, 1.0/HO_r);
                  
                    // (H_j --> O_j) . (H_j --> O_i)
                    cosOHO = m_vector_dot(HO_j, HO_i);

                    // (O_j --> H_j) . (O_j --> O_i)
                    cosHOO = m_vector_dot(HO_j, r_OO);

                    if ( cosHOO < max_cos_HOO && cosHOO > min_cos_HOO && cosOHO < max_cos_OHO && cosOHO > min_cos_OHO && HO_r < max_R_HO && HO_r > min_R_HO ) {
                      nearest_neighbours[i]->min_rank += 1;
                      nearest_neighbours[i]->dummy[nearest_neighbours[i]->n_max-1] += 1;
                      nearest_neighbours[j]->min_rank += 1;
                      nearest_neighbours[j]->dummy[nearest_neighbours[j]->n_max-1] += 1;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  frame_nn = 0;
  frame_hb = 0;
  for( i=0; i<solvent_mspec->count; i++) {
    //m_nearest_neighbours_print(nearest_neighbours[i]);
    frame_nn += nearest_neighbours[i]->n_max;
    frame_hb += nearest_neighbours[i]->min_rank;
  }
  frame_nn /= solvent_mspec->count;
  frame_hb /= solvent_mspec->count;
  total_bulk_nn += frame_nn;
  total_bulk_hb += frame_hb;

}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {
}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {

  total_sol_nn += frame_nn*pr*dV;
  total_sol_hb += frame_hb*pr*dV;

  M_molec *molec_i, *molec_j, *molec_k, *molec_l;
  int i,j,k,l; // molecule ids (id_in_spec, m_spec_molec(mspec, id))
  M_neighbours *nn_i, *nn_j, *nn_k, *nn_l;
  int jj,kk,ll;// position in NN lists nn_i->(jj,kk), nn_j->ll or nn_k->ll

  int i_sv;
  for( i_sv=0; i_sv<2; i_sv++ ) {
    for( j=0; j<10; j++ ) {
      frame_clusters[i_sv][j] = 0;
    }
  }

  for( i=0; i<solvent_mspec->count; i++ ) {
    molec_i = m_spec_molec(solvent_mspec, i);
    nn_i = nearest_neighbours[i];

    prec r = m_vector_mag(m_radius(molec_i->center,probe->position));

    if ( r < R_sv1 ) {
      i_sv=0;
    } else if ( r < R_sv2 ) {
      i_sv=1;
    } else {
      i_sv=-1;
    }

    if ( i_sv >= 0 ) {
      for( jj=0; jj<nn_i->n_max; jj++) {
        molec_j = nn_i->molecs->data[jj];
        j = molec_j->id_in_spec;
        nn_j = nearest_neighbours[j];

        if ( nn_i->dummy[jj] > 0 ) {
          // i* <> j
          frame_clusters[i_sv][0] ++;
        } else {
          // i .. j
          frame_clusters[i_sv][1] ++;
        } 

        // unique pairs of i,j,k
        for ( kk=jj+1; kk<nn_i->n_max; kk++) {
          molec_k = nn_i->molecs->data[kk];
          k = molec_k->id_in_spec;
          nn_k = nearest_neighbours[k];

          if ( nn_i->dummy[jj] > 0 && nn_i->dummy[kk] > 0 ) {
            // j <> i* <> k
            frame_clusters[i_sv][2] ++;
          } else if ( nn_i->dummy[jj] > 0 || nn_i->dummy[kk] > 0 ) {
            // j <> i* .. k OR j .. i* <> k
            frame_clusters[i_sv][3] ++;
          }


          // l is joined to j
          for ( ll=0; ll<nn_j->n_max; ll++) {
            molec_l = nn_j->molecs->data[ll];
            l = molec_l->id_in_spec;
            nn_l = nearest_neighbours[l];

            if ( nn_i->dummy[jj] > 0 ) {
              if ( nn_i->dummy[kk] > 0 ) {
                if ( nn_j->dummy[ll] > 0 ) {
                  // k<>i*<>j<>l
                  frame_clusters[i_sv][5] ++;
                } else {
                  // k<>i*<>j..l
                  frame_clusters[i_sv][9] ++;
                }
              } else {
                if ( nn_j->dummy[ll] <= 0 ) {
                  // k..i*<>j..l
                  frame_clusters[i_sv][7] ++;
                }
              }
            } else {
              if ( nn_i->dummy[kk] > 0 ) {
                if ( nn_j->dummy[ll] > 0 ) {
                  // k<>i*..j<>l
                  frame_clusters[i_sv][4] ++;
                } else {
                  // k<>i*..j..l
                  frame_clusters[i_sv][8] ++;
                }
              } else {
                if ( nn_j->dummy[ll] <= 0 ) {
                  // k..i*..j..l
                  frame_clusters[i_sv][6] ++;
                }
              }
            }
          } // end ll

          // l is joined to k
          for ( ll=0; ll<nn_k->n_max; ll++) {
            molec_l = nn_k->molecs->data[ll];
            l = molec_l->id_in_spec;
            nn_l = nearest_neighbours[l];

            if ( nn_i->dummy[jj] > 0 ) {
              if ( nn_i->dummy[kk] > 0 ) {
                if ( nn_k->dummy[ll] > 0 ) {
                  // j<>i*<>k<>l
                  frame_clusters[i_sv][5] ++;
                } else {
                  // j<>i*<>k..l
                  frame_clusters[i_sv][9] ++;
                }
              } else {
                if ( nn_k->dummy[ll] > 0 ) {
                  // j<>i*..k<>l
                  frame_clusters[i_sv][4] ++;
                } else {
                  // j<>i*..k..l
                  frame_clusters[i_sv][8] ++;
                }
              }
            } else {
              if ( nn_i->dummy[kk] > 0 ) {
                if ( nn_k->dummy[ll] <= 0 ) {
                  // j..i*<>k..l
                  frame_clusters[i_sv][7] ++;
                }
              } else {
                if ( nn_k->dummy[ll] <= 0 ) {
                  // j..i*..k..l
                  frame_clusters[i_sv][6] ++;
                }
              }
            }
          } // end ll
        } // end kk
      } // end jj
    } // end if i_sv>=0

  } // end i

  for ( i_sv = 0; i_sv < 2; i_sv ++ ) {
    for ( j = 0; j < 10; j ++ ) {
      total_clusters[i_sv][j] += frame_clusters[i_sv][j]*pr*dV;
    }
  }

}

void widom_frame_end(M_system* sys, prec sum_pr ) {


  int i;
  // free the nearest neighbour list
  for (i=0; i<solvent_mspec->count; i++) {
    m_nearest_neighbours_free(nearest_neighbours[i]);
  }
}

void widom_finalise(M_system* sys) {

  total_bulk_nn /= sys->frames_processed;
  total_bulk_hb /= sys->frames_processed;
  total_sol_nn /= total_sum_pr;
  total_sol_hb /= total_sum_pr;

  int i_sv, j;
  for ( i_sv = 0; i_sv < 2; i_sv ++ ) {
    for ( j = 0; j < 10; j ++ ) {
      total_clusters[i_sv][j] /= total_sum_pr;
    }
  }
  total_clusters[0][3] *= 2;
  total_clusters[1][3] *= 2;
  total_clusters[0][8] *= 2;
  total_clusters[1][8] *= 2;
  total_clusters[0][9] *= 2;
  total_clusters[1][9] *= 2;

  fprintf(average_out,"%lf\t%lf\n",total_bulk_nn, total_sol_nn);
  fprintf(average_out,"%lf\t%lf\n",total_bulk_hb, total_sol_hb);

  for ( j = 0; j < 10; j ++ ) {
    fprintf(clusters_out,"%d\t%lf\t%lf\n",j,total_clusters[0][j],total_clusters[1][j]);
  }

  fclose(average_out);
  fclose(clusters_out);
  free( nearest_neighbours );

}
