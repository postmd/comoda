#include "coulomb.h"

M_ewald* m_ewald_new(prec eta, prec L, M_uint n_x, M_uint n_y, M_uint n_z) {
    M_ewald *new = malloc(sizeof(M_ewald));

    new->U_coulomb = 0;
    new->U_real = 0;
    new->U_recip = 0;
    new->U_self = 0;
    new->eta = eta;
    new->k_nyquist = (prec) 2*pi/L; 
    new->kmax[0] = n_x/2;
    new->kmax[1] = n_y/2;
    new->kmax[2] = n_z/2;
    new->npts[0] = new->kmax[0]*2+1;
    new->npts[1] = new->kmax[1]*2+1;
    new->npts[2] = new->kmax[2]*2+1;
    new->n_k = new->npts[0]*new->npts[1]*new->npts[2];
    new->greenfn = malloc(sizeof(prec)*new->n_k);
    clear_farray(new->greenfn, new->n_k);
    new->rhohat = malloc(sizeof(M_complex)*new->n_k);
    clear_carray(new->rhohat, new->n_k);

    return new;
}

void m_ewald_free(M_ewald *ewald) {
    free(ewald->greenfn);
    free(ewald->rhohat);
    free(ewald);
    ewald=NULL;
}

M_ewald* m_ewald_copy(M_ewald *in) {
    M_ewald *out = m_ewald_new(in->eta, (prec) 2*pi/(in->k_nyquist), in->npts[0], in->npts[1], in->npts[2]);

    int i;
    for(i=0; i<in->n_k; i++) {
        out->greenfn[i] = in->greenfn[i];
        out->rhohat[i] = in->rhohat[i];
    }

    return out;
}

M_ewald* calc_ewald_frame(M_system *sys, prec eta, M_uint n_x, M_uint n_y, M_uint n_z) {
    M_ewald *ewald = m_ewald_new(eta, sys->dims.x, n_x, n_y, n_z);

    // compute Green's function
    // g(kx,ky,kz) = 4pi/|k|^2 * exp( -|k|^2/(4 eta^2))

    int k[3];
    M_uint k_id;
    M_uint k_id_l[3];

    k_id = 0;
    clear_uiarray(k_id_l,3);

    while ( k_id < ewald->n_k ) {
        k[0] = k_id_l[0] - ewald->kmax[0];
        k[1] = k_id_l[1] - ewald->kmax[1];
        k[2] = k_id_l[2] - ewald->kmax[2];

        prec k2 = pow(ewald->k_nyquist,2.0)*(k[0]*k[0]+k[1]*k[1]+k[2]*k[2]);

        if ( k2 > 0 ) {
            ewald->greenfn[k_id] = 4*M_PI/k2 * exp(-1.0*k2/(4*ewald->eta*ewald->eta));
        }

        increment_id_list(k_id_l, ewald->npts, 3);
        k_id++;
    }

    int i,j;
    for (i=0; i<sys->atoms->count; i++) {
        M_atom *atom_i = m_sys_atom(sys,i);

        if ( atom_i->spec->charge == 0 ) continue;
      
        int ii, jj, kk;
        M_uint cell_i[3], cell_j[3], cell_array_j;

        cell_i[0] = floor(atom_i->pos.x/cell_dgrid.x);
        cell_i[1] = floor(atom_i->pos.y/cell_dgrid.y);
        cell_i[2] = floor(atom_i->pos.z/cell_dgrid.z);

        // compute the real-space part of the Ewald sum
        //
        // U_real = Sum_{pairs} q_i * q_j * erfc(eta * r_ij)/r_ij
        //
        // Use the cell method. This makes things slightly faster
        ii=0;
        while (ii<pspan(rcut_cell_grid_width[0]*2+1,cell_ngrid[0])) {
            cell_j[0] = pmod( (ii + cell_i[0] - rcut_cell_grid_width[0]), cell_ngrid[0]);
            jj=0;
            //for (jj=-rcut_cell_grid_width[1]; jj <= rcut_cell_grid_width[1]; jj++ ) { 
            while (jj<pspan(rcut_cell_grid_width[1]*2+1,cell_ngrid[1])) {
                cell_j[1] = pmod( (jj + cell_i[1] - rcut_cell_grid_width[1]), cell_ngrid[1]);
                kk=0;
                //for (kk=-rcut_cell_grid_width[2]; kk <= rcut_cell_grid_width[2]; kk++ ) { 
                while (kk<pspan(rcut_cell_grid_width[2]*2+1,cell_ngrid[2])) {
                    cell_j[2] = pmod( (kk + cell_i[2] - rcut_cell_grid_width[2]), cell_ngrid[2]);

                    // the array index of this cell is:
                    cell_array_j = get_id( cell_j, cell_ngrid, 3 );
                    // the atoms in this cell can be retrived by:
                    //    cell_occupancy[i]->count
                    //    cell_occupancy[i]->data = { M_atom *a1, *a2, *a3, ... }
                    for (j=0; j < cell_occupancy[cell_array_j]->count; j++) {
                        M_atom *atom_j = cell_occupancy[cell_array_j]->data[j];

                        if ( atom_j->spec->charge == 0 ) continue;
                        if ( atom_i->id >= atom_j->id ) continue;
                        if ( atom_i->molec->id == atom_j->molec->id ) continue;

                        // only if they are in different molecules

                        M_vector dr = m_radius(atom_i->pos, atom_j->pos);
                        prec r2 = m_vector_mag2(dr);

                        if ( r2 < rcut2 || sys->no_cutoff ) {
                            prec r = sqrt(r2);
                            ewald->U_real += atom_i->spec->charge*atom_j->spec->charge*erfc(ewald->eta*r)/r;
                        }
                    }
                    kk++;
                }
                jj++;
            }
            ii++;
        }

        // compute the reciprocal-space part of the charge density
        //
        // rhohat(kx,ky,kz) = Sum_{atoms} q(i)*exp(ikr)
        k_id = 0;
        clear_uiarray(k_id_l,3);

        while ( k_id < ewald->n_k ) {
            k[0] = k_id_l[0] - ewald->kmax[0];
            k[1] = k_id_l[1] - ewald->kmax[1];
            k[2] = k_id_l[2] - ewald->kmax[2];

            M_complex ikr = ewald->k_nyquist * I * (k[0]*atom_i->pos.x + k[1]*atom_i->pos.y + k[2]*atom_i->pos.z);

            ewald->rhohat[k_id] += cexp(ikr)*atom_i->spec->charge;

            increment_id_list(k_id_l, ewald->npts, 3);
            k_id++;
        }

        // U_self = - eta/sqrt(pi) Sum_{atoms} q_i^2
        ewald->U_self += atom_i->spec->charge*atom_i->spec->charge;
    }
    ewald->U_self *= -0.5 * ewald->eta * M_2_SQRTPI;

    // U_recip = 1/(2*L^3) Sum_{k} greenfn(k) * |rhohat(k)|^2
    for (i=0; i<ewald->n_k; i++) {
        ewald->U_recip += ewald->greenfn[i]*creal(ewald->rhohat[i]*conj(ewald->rhohat[i]));
    }
    // 1/(2 L^3) = 1/2 * 1/V
    ewald->U_recip *= 0.5/sys->volume;
    
    ewald->U_coulomb = ewald->U_real + ewald->U_recip + ewald->U_self;

    return ewald;
}



