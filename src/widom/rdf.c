#include "widom.h"

char *description \
 = "widom.rdf\n\
    \n\
      Calculates the RDFs after a solute has been inserted.\n\
    \n\
    Configuration file fortmat:\n\
    ===========================\n\
      [system]\n\
      rcut            = 20.0      ; the maximum separation allowed for pairs (bohr)\n\
      setT            = 0.000944  ; temperature in atomic units\n\
      [rdf]\n\
      dr              = 0.01      ; the bin size for the rdf histogram\n\
      no_scale        = false     ; do not scale/normalise the rdf, i.e. counts per bin\n\
      sv_bulk_out     = bulk.rdf  ; the output file for the bulk solvent rdf\n\
      sv_sol_out      = solution.rdf\n\
                                  ; the output file for the solvent in the presence of\n\
                                    ; solute\n\
      solute_out      = Ar.rdf    ; the output file for the solute rdf\n\
      [insert]\n\
      ; flags for what kind of calculation to perform\n\
      calc_dMu        = false     ; whether or not to calculate the free energy of\n\
                                    ; particle insertion \n\
      print_coords    = true      ; whether or not to print coordinates of insertion\n\
      print_sort      = true      ; whether or not to sort the insertion coordinates\n\
                                    ; before printing. Default = true.\n\
      print_excluded  = true      ; whether or not to print the insertion coordinates\n\
                                    ; which are within exclusion_r. Default = false.\n\
      print_range     = 1,100     ; the range of insertion coordinates to print\n\
                                    ; note that the printed coordinates are ranked\n\
                                    ; from the lowest DeltaE (most probable) to\n\
                                    ; the greatest DeltaE (least probable)\n\
      coords_file     = insert_coords\n\
                                  ; print the coordinates into this file\n\
                                    ; if unspecified, prints to stdout\n\
                                    ; see note 1 for column format\n\
      dMu_file        = insert_dMu\n\
                                  ; print the insertion energies into this file\n\
                                    ; if unspecified, prints to stdout\n\
      only_nearest    = true      ; only sort the insertion coordinates by their\n\
                                    ; nearest neighbour separation\n\
      ngrid           = 10,10,10  ; number of grid points in the x,y,z direction\n\
      ; interaction potential parameters\n\
      ; comma-separated list, the length of the list must equal the number\n\
      ; of atomic species, the order of potentials must be given in the same\n\
      ; order as the declaration above\n\
      ;\n\
      ; U_j(r) = Q/r + (smallsig(j)/r)^12 + B(j)*exp(-abm(j)*r) - C6(j)/r^6 - C8(j)/r^8\n\
      ;\n\
      ; all interaction parameters are in atomic units\n\
      Q               = 1.0,-1.0\n\
      smallsig        = 1.0,1.0\n\
      B               = 0.0,0.0\n\
      abm             = 0.0,0.0\n\
      C6              = 0.0,0.0\n\
      C8              = 0.0,0.0\n\
      ; optimization settings \n\
      exclusion_r     = 3.0,4.0   ; radius to exclude from computing U \n\
      cell_ngrid      = 10,10,10  ; divide the box into small cells, for fewer\n\
                                    ; wasted pairs\n\
    ===========================\n\
    \n\
    notes:\n\
      1. column format for coords_file:\n\
          frame_id-grid_id    x/bohr    y/bohr    z/bohr    value (calc_dMu=exp(-beta dU), nearest_only=r_min^2)\n\
";

M_aggregate *total_sv_bulk, *frame_sv_bulk;
M_aggregate *total_sv_sol;
M_aggregate *total_solute, *insert_solute;

FILE *sv_bulk_out, *sv_sol_out, *solute_out;

prec bulk_sum_V, solution_sum_V;

bool no_scale;

void widom_print_example() {
}

void widom_init(M_system* sys, dictionary* dict) {

  prec dr;

  dr = iniparser_getdouble(dict, "rdf:dr", 0.01);
  no_scale = iniparser_getboolean(dict, "rdf:no_scale", 0);

  // columns:
  // solvent (sv)
  //    { r, g(r) (1-1, 1-2, 1-3, ...., 2-1, 2-2, 2-3, ... n-1, n-2, .... } + count
  // solute
  //    { r, g(r) (s-1, s-2, s-3, ..., s-n) } + count

  total_sv_bulk = m_aggregate_new(0.0, dr, dr, nspecies*nspecies + 1);
  total_sv_sol = m_aggregate_new(0.0, dr, dr, nspecies*nspecies + 1);
  frame_sv_bulk = m_aggregate_new(0.0, dr, dr, nspecies*nspecies + 1);

  total_solute = m_aggregate_new(0.0, dr, dr, nspecies + 1);
  insert_solute = m_aggregate_new(0.0, dr, dr, nspecies + 1);
  
  // set output styles for the final aggregators
  m_aggregate_set_print_none(total_sv_bulk);
  total_sv_bulk->print_col[0] = 1;
  total_sv_bulk->print_empty = 1;

  m_aggregate_set_print_none(total_sv_sol);
  total_sv_sol->print_col[0] = 1;
  total_sv_sol->print_empty = 1;

  m_aggregate_set_print_none(total_solute);
  total_solute->print_col[0] = 1;
  total_solute->print_empty = 1;

  bulk_sum_V = 0;
  solution_sum_V = 0;

  char *in;

  in = iniparser_getstring(dict, "rdf:sv_bulk_out", NULL );
  if ( in != NULL ) {
    sv_bulk_out = fopen(in, "w");
  } else {
    sv_bulk_out = stdout;
  }

  in = iniparser_getstring(dict, "rdf:sv_sol_out", NULL );
  if ( in != NULL ) {
    sv_sol_out = fopen(in, "w");
  } else {
    sv_sol_out = stdout;
  }

  in = iniparser_getstring(dict, "rdf:solute_out", NULL );
  if ( in != NULL ) {
    solute_out = fopen(in, "w");
  } else {
    solute_out = stdout;
  }
}

void widom_frame_init(M_system* sys) {

  m_aggregate_reset(frame_sv_bulk);

  int i,j;
  M_atom *atom_i, *atom_j;

  prec r;
  int col_id;

  for (i=0; i<sys->atoms->count; i++) {
    atom_i = m_sys_atom(sys,i);
    for (j=i+1; j<sys->atoms->count; j++) {
      atom_j = m_sys_atom(sys,j);

      if ( atom_i->molec->id != atom_j->molec->id ) {

        r = m_vector_length(m_radius(atom_i->pos, atom_j->pos));

        if ( r < sys->rcut || sys->no_cutoff ) {

          if ( r > frame_sv_bulk->max || r < frame_sv_bulk->min ) { 
            frame_sv_bulk = m_aggregate_extend(frame_sv_bulk, fmin(frame_sv_bulk->min, r), fmax(frame_sv_bulk->max, r));
            total_sv_bulk = m_aggregate_extend(total_sv_bulk, fmin(total_sv_bulk->min, r), fmax(total_sv_bulk->max, r));
            total_sv_sol = m_aggregate_extend(total_sv_sol, fmin(total_sv_sol->min, r), fmax(total_sv_sol->max, r));
          }    

          frame_sv_bulk->row[0] = r;

          //col_id=get_col_num(atom_i->spec->id,atom_j->spec->id,nspecies);
          col_id=(atom_i->spec->id)*nspecies+(atom_j->spec->id+1);
          frame_sv_bulk->row[col_id] = 1.0;

          col_id=(atom_j->spec->id)*nspecies+(atom_i->spec->id+1);
          frame_sv_bulk->row[col_id] = 1.0;

          _m_aggregate(frame_sv_bulk);
        }
      }
    }
  }


}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
  m_aggregate_reset(insert_solute);
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {
  
  if ( r2 < rcut2 || sys->no_cutoff ) {
    prec r = sqrt(r2);
    int col_id=atom->spec->id+1;

    if ( r > insert_solute->max || r < insert_solute->min ) {
      insert_solute = m_aggregate_extend(insert_solute, fmin(insert_solute->min, r), fmax(insert_solute->max, r));
      total_solute = m_aggregate_extend(total_solute, fmin(total_solute->min, r), fmax(total_solute->max, r));
    }

    insert_solute->row[0] = r;
    insert_solute->row[col_id] = 1.0;
    _m_aggregate(insert_solute);
  }

}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {

  int i;
  int col_id;

  for (i=0; i<nspecies; i++ ) {

    col_id = i+1;

    int j;
    for ( j=0; j<insert_solute->length; j++ ) {

      total_solute->row[0] = insert_solute->values[j][0];
      total_solute->row[col_id] = insert_solute->values[j][col_id] * pr * dV;
      _m_aggregate(total_solute);

    }
  }
}

void widom_frame_end(M_system* sys, prec sum_pr) {
  
  bulk_sum_V += sys->volume;
  solution_sum_V += sys->volume * sum_pr;

  int i,j;
  M_atom_spec *aspec_i, *aspec_j;
  int col_id;
  for ( i=0; i<nspecies; i++ ) {
    aspec_i = m_sys_atom_spec(sys,i);

    for ( j=i; j<nspecies; j++ ) {
      aspec_j = m_sys_atom_spec(sys,j);

      col_id = i*nspecies + j + 1;

      int k;
      for ( k=0; k<frame_sv_bulk->length; k++ ) {

        total_sv_bulk->row[0] = frame_sv_bulk->values[k][0];
        total_sv_bulk->row[col_id] = frame_sv_bulk->values[k][col_id];
        _m_aggregate(total_sv_bulk);

        total_sv_sol->row[0] = frame_sv_bulk->values[k][0];
        total_sv_sol->row[col_id] = frame_sv_bulk->values[k][col_id]*sum_pr;
        _m_aggregate(total_sv_sol);
      }
    }
  }
}

void widom_finalise(M_system* sys) {

  int i,j;
  M_atom_spec *aspec_i, *aspec_j;
  int col_id;

  int nframes;
  nframes = sys->frames_processed;

  solution_sum_V = solution_sum_V / total_sum_pr;

  prec bulk_prefactor;
  prec solution_prefactor;
  prec solute_prefactor;
  for (i=0;i<nspecies;i++) {
    aspec_i = m_sys_atom_spec(sys,i);

    for (j=i;j<nspecies;j++) {
      aspec_j = m_sys_atom_spec(sys,j);

      col_id = i*nspecies + (j+1); 

      // turn on printing for this column
      total_sv_bulk->print_col[col_id] = 1;
      total_sv_sol->print_col[col_id] = 1;

      bulk_prefactor = 1.0;
      solution_prefactor = 1.0/total_sum_pr;
      if ( ! no_scale ) {

        bulk_prefactor = bulk_sum_V/((prec) aspec_i->sys_total * aspec_j->sys_total * nframes * nframes);
        solution_prefactor = solution_sum_V/((prec) aspec_i->sys_total * aspec_j->sys_total * total_sum_pr);
      
        if ( i == j ) { 
          bulk_prefactor = bulk_prefactor * 2.0;
          solution_prefactor = solution_prefactor * 2.0;
        }   

        m_aggregate_col_scale_dV(total_sv_bulk,col_id);
        m_aggregate_col_scale_dV(total_sv_sol,col_id);
      }

      m_aggregate_col_scale(total_sv_bulk,col_id,bulk_prefactor);
      m_aggregate_col_scale(total_sv_sol,col_id,solution_prefactor);

    }   

    col_id = i + 1;
    total_solute->print_col[col_id] = 1;

    solute_prefactor = 1.0/total_sum_pr;
    if ( ! no_scale ) {
      m_aggregate_col_scale_dV(total_solute, col_id);
      solute_prefactor = solution_sum_V/((prec) aspec_i->sys_total * total_sum_pr);
    }
    m_aggregate_col_scale(total_solute,col_id,solute_prefactor);

  }  

  m_aggregate_fprint(sv_bulk_out,total_sv_bulk);
  m_aggregate_fprint(sv_sol_out, total_sv_sol);
  m_aggregate_fprint(solute_out,total_solute);

  m_aggregate_free(total_sv_bulk);
  m_aggregate_free(total_sv_sol);
  m_aggregate_free(total_solute);

  m_aggregate_free(frame_sv_bulk);
  m_aggregate_free(insert_solute);

  if ( sv_bulk_out != stdout ) {
    fclose(sv_bulk_out);
  }
  if ( sv_sol_out != stdout ) {
    fclose(sv_sol_out);
  }
  if ( solute_out != stdout ) {
    fclose(solute_out);
  }

}
