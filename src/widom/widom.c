#include "widom.h"

bool print_coords;
bool print_sort;
bool print_excluded;
M_uint print_range[2];
char coords_file[256];
FILE* coords_output = NULL;
char dMu_file[256];
FILE* dMu_output = NULL;

char *description2 \
 = "\n\
    ==================\n\
    This program uses the widom.h library. Widom.h calculates the bare test particle\n\
    insertion probabilities, which may be called as a library to calculate structures\n\
    and correlations from insertion probabilities. See --example for widom.h options.\n\
    \n\
    widom.h output format:\n\
        frame\tsum(pr)\t<sum(pr)>\tn_inserts\n\
    where sum(pr) is given by:\n\
                sumpr = < V * int ds exp(-beta DU ) > \n\
        exp(-beta DF) = sumpr / < V > \n\
                   DF = - kT ln ( sumpr / < V > )\n\
    with < V > the average volume of the simulation cell, DF the free energy of the test\n\
    particle, and kT is the temperature of the system.\n\
    \n\
    This output can be suppresed by setting insert:dMu_file in the config file to be /dev/null.\n\
    "; 

void print_routine_example() {
    char *text \
= "[system]\n\
setT            = 0.000944  ; float, atomic units (kT), required calc_dMu = true\n\
                              ; temperature of the system\n\
out_mode        = append    ; keywords, default = write, optional\n\
                              ; {append, a}\n\
                              ; append instead of overwrite to dMu_file and\n\
                              ; coords_file. If keyword is unrecognised, the usual\n\
                              ; write mode (overwrite) is used.\n\
[insert]\n\
calc_dMu        = false     ; boolean {true, false}, default = true, optional\n\
                              ; whether or not to calculate the free energy of\n\
                              ; particle insertion \n\
dMu_file        = insert_dMu; string, default = stdout, optional\n\
                              ; print the insertion energies into this file\n\
                              ; overwrites existing file (not append)\n\
integral_type   = grid      ; keywords, default = grid, optional\n\
                              ; {grid, monte_carlo, monte, MC, random}\n\
                              ; select how to compute the integral over a volume\n\
                              ; default is to use grids\n\
                              ; alternatively, set to monte_carlo or the other\n\
                              ; variations to perform Monte Carlo integration\n\
ngrid           = 10,10,10  ; comma-separated integers, three values\n\
                              ; required if integral_type=grid\n\
                              ; number of grid points in the x,y,z direction\n\
n_mc            = 10000     ; integer, required if integral_type = monte_carlo\n\
                              ; number of Monte Carlo insertions to perform\n\
cell_ngrid      = 10,10,10  ; comma-separated integers, three values, default = {1}\n\
                              ; optimisation setting only\n\
                              ; divide the box into small cells, for fewer pairwise\n\
                              ; computations outside of rcut\n\
exclusion_res   = 1         ; integer, default = 1, optional\n\
                              ; only used when integral_type = monte_carlo\n\
                              ; optimisation setting only\n\
                              ; A grid is used to evaluate exclusion_r even when using\n\
                              ; Monte Carlo integration. exclusion_res is the resolution\n\
                              ; of the grid, given as the number of grid points in the\n\
                              ; smallest exclusion_r (0 is ignored, rcut if all 0)\n\
; interaction potential parameters\n\
; comma-separated lists of floats, atomic units, default = {0}\n\
; the length of the list must equal the number of atomic species in the solvent\n\
; the order of lists must be given in the same order as the declaration of atoms\n\
;          { infinity                                                   r < exclusion_r(j)\n\
; U_j(r) = { (smallsig(j)/r)^12 - C6(j)/r^6 - C8(j)/r^8 + B(j)*exp(-abm(j)*r)\n\
;          {                                                     exclusion_r(j) < r < rcut\n\
;               /   r=infinity                        \\\n\
; U_tail = sum | int U_j(r) * density(j) * 4 pi r^2 dr |\n\
;           j   \\   r=rcut                            /\n\
; U_coulomb is calculated using an Ewald sum method.\n\
exclusion_r     = 3.0,4.0\n\
smallsig        = 1.0,1.0\n\
C6              = 4.0,8.0\n\
C8              = 0.0,0.0\n\
B               = 0.0,0.0\n\
abm             = 0.0,0.0\n\
Q               = -0.8\n\
; additional options\n\
only_nearest    = false     ; boolean {true, false}, default = false, optional\n\
                              ; if not calculating dMu, then generate a ranked list of\n\
                              ; coordinates sorted by their nearest-neighbour distance\n\
                              ; (further is better)\n\
                              ; if true, requires print_coords = true and calc_dMu = false\n\
                              ; if false, requires calc_dMu = true\n\
two_particle    = false     ; boolean {true, false}, default = false, optional\n\
                              ; insert two test particles at a specified separation\n\
                              ; if true, requires calc_dMu = true,\n\
                              ;                   integral_type = monte_carlo,\n\
                              ;                   only_nearest = false\n\
                              ;                   r_tt is positive\n\
r_tt            = 18.0      ; float, bohrs, required if two_particle = true\n\
                              ; distance between the two particles\n\
U_tt            = 1e-6      ; float, Ha, required if two_particle = true\n\
                              ; interaction energy between the two test particles\n\
                              ; at the specified separation of r_tt\n\
; printing options\n\
print_coords    = true      ; boolean {true, false}, default = false, optional\n\
                              ; print coordinates of insertion\n\
print_range     = 1,100     ; comma separated integers, two values, default = 1,max, optional\n\
                              ; the range of insertion coordinates to print\n\
print_sort      = true      ; boolean {true, false}, default = true, optional\n\
                              ; sort coordinates of insertion before printing\n\
                              ; sort by dMu (lower is better) or\n\
                              ; nearest neighbour distance (higher is better)\n\
print_excluded  = true      ; boolean {true, false}, default = false, optional\n\
                              ; print coordinates of insertion that are within exclusion_r\n\
coords_file     = crds.txt  ; string, default = stdout, optional\n\
                              ; print the coordinates into this file\n\
                              ; column format:\n\
                              ; frame_id-grid_id    x/bohr    y/bohr    z/bohr    value\n\
                              ; value: calc_dMu=exp(-beta dU), nearest_only=r_min^2\n\
                              ; overwrites existing file (not append)\n\
";

    printf("%s\n",text);
    widom_print_example();
}

void frame_init(M_system* sys, dictionary* dict) {

    //if ( ! sys->rcut ) {
    //    FATAL("rcut must be defined under [system] in the configuration file!");
    //    exit(1);
    //}

    // we'll be using this variable a lot
    nspecies = sys->atom_specs->count;

    total_inserts = 0;
    total_sum_pr = 0;

    get_mem();
    read_insert_input(dict);
    get_more_mem(sys);

    if ( (Q != 0) && (! sys->calc_ewald) ) {
        FATAL("The insertion probe has a non-zero charge, but calc_ewald is not configured.\n");
        FATAL("Aborting...\n");
        exit(1);
    }

    if ( rank_type == dMu ) {
        fprintf(dMu_output,"frame\tsum(pr)\t<sum(pr)>\tn_inserts\n");
    }
    
    widom_init(sys, dict);
}

void frame_routine(M_system* sys) {

    prec dU_i, pr, sum_pr, dU_tail;
    prec sum_pr2;
    int n_inserts_frame;
    int frames;

    // initialize some variables
    n_inserts_frame = 0;
    sum_pr = 0;
    if ( integral_type == monte_carlo ) {
        sum_pr2 = 0;
    }

    // counters for loops
    int i;

    rcut2 = pow(sys->rcut,2.0);

    // initilise the grids, populate the cell occupancy arrays
    init_grid(sys);

    // perform calculations on the solvent frame
    widom_frame_init(sys);

    // get the energy of the LJ tail
    dU_tail = U_tail(sys);
    
    // get the energy of the isolated charged particle
    U_isolated = m_ewald_isolated(sys, Q);

    // -------------------
    // perform insertions
    // -------------------
    // for each grid point:
    //    if the point is included:
    //        make insertion, calculate LJ dU, sum
    print_rank = m_parray_new( 0 );
    
    int exclusion = 0;
    M_point_probe *probe, *probe2;
    if ( integral_type == monte_carlo ) {
        probe = malloc(sizeof(M_point_probe));
        probe->position.dims = &sys->dims;
        if (insertion_type == two_particles) {
            probe2 = malloc(sizeof(M_point_probe));
            probe2->position.dims = &sys->dims;
        }
    }
    for (i=0; i<npts; i++) {
        
        if ( integral_type == grid ) {
            probe = probe_pts->data[i];
            exclusion = probe->exclusion;
        } else {
            probe->position.x = rand_1()*sys->dims.x;
            probe->position.y = rand_1()*sys->dims.y;
            probe->position.z = rand_1()*sys->dims.z;
            
            probe->cell_coord[0] = (M_uint) floor(probe->position.x/cell_dgrid.x);
            probe->cell_coord[1] = (M_uint) floor(probe->position.y/cell_dgrid.y);
            probe->cell_coord[2] = (M_uint) floor(probe->position.z/cell_dgrid.z);
            probe->cell_array_i = get_id( probe->cell_coord, cell_ngrid, 3 );
            
            exclusion = is_excluded(sys,probe);
            
            if ( (! exclusion) && insertion_type == two_particles ) {
                probe2->position.x = probe->position.x;
                probe2->position.y = probe->position.y;
                probe2->position.z = probe->position.z;
                
                m_point_trans(&(probe2->position), m_vector_scale(rand_sphere1(),r_tt));
                probe2->cell_coord[0] = (int) floor(probe2->position.x/cell_dgrid.x);
                probe2->cell_coord[1] = (int) floor(probe2->position.y/cell_dgrid.y);
                probe2->cell_coord[2] = (int) floor(probe2->position.z/cell_dgrid.z);
                probe2->cell_array_i = get_id( probe2->cell_coord, cell_ngrid, 3 );
                
                exclusion = is_excluded(sys,probe2);
            }
        }
        

        // zero some variables 
        dU_i=0;
        pr=0;

        // exclusion == 1: this grid point is excluded (too close to atoms)
        // exclusion == 0: use this grid point
        if ( ! exclusion ) { 

            // perform additional calculations on the inserted probe
            widom_insert_init(sys,probe);

            // calculate dU for the insertion of the probe
            dU_i = calc_dU(sys, probe) + dU_tail;
            
            if ( insertion_type == two_particles ) {
                // calculate dU for the insertion of the probe
                dU_i += calc_dU(sys, probe2);
            }
            
            if ( rank_type == dMu ) {
                // tally up the energy
                pr = exp(-1.0*beta*dU_i);
                //DEBUG("%d\t%g\t%g\n",probe->grid_array_i,dU,pr);

                // add the pr to the pr of the frame
                sum_pr += pr;
                if ( integral_type == monte_carlo ) {
                    sum_pr2 += pr*pr;
                }
            } // end if rank_type == dMu

            if ( print_coords ) {
                // assign the rank to the probe to dMu
                if ( rank_type == dMu ) {
                    probe->rank = pr;
                } else if ( rank_type == dU ) {
                    probe->rank = dU_i;
                }
                // otherwise, the rank is already the nearest neighbour distance

                // save into array
                m_parray_push(print_rank, probe);
                if ( integral_type == monte_carlo ) {
                    // probe is the only copy of the M_point_probe during MC integration
                    // allocate a new one, so that we don't overwrite the coordinates
                    probe = malloc(sizeof(M_point_probe));
                    probe->position.dims = &sys->dims;
                }
            } // end if print_coords

            n_inserts_frame += 1;

            // perform additional calculations on the inserted probe
            widom_insert_end(sys,probe,pr);

        } else { // else exclusion==1, point has been excluded
            if ( print_excluded ) { // but we want to print excluded points anyways
                if ( rank_type == dU ) {
                    probe->rank = INFINITY;
                } else if ( rank_type == dMu ) {
                    probe->rank = 0;
                } else if ( rank_type == nearest ) {
                    probe->rank = 0;
                }
                m_parray_push(print_rank, probe);
                if ( integral_type == monte_carlo ) {
                    // probe is the only copy of the M_point_probe during MC integration
                    // allocate a new one, so that we don't overwrite the coordinates
                    probe = malloc(sizeof(M_point_probe));
                    probe->position.dims = &sys->dims;
                }
            }
        }
        
    } // end for i

    frames = sys->frames_processed + 1;
    total_inserts += n_inserts_frame;
    
    if ( print_coords ) {
        if ( print_sort ) {
            print_rank_bubble_sort(print_range[1]);
        } else {
            m_parray_reverse( print_rank );
        } 
        //print_rank_table();
        print_rank_print_coords(frames-1, print_range[0], print_range[1]);
    } // end if print_coords

    if ( rank_type == dMu ) {
        //   V \int d\vec{s}_{N+1} \exp(-\beta U)
        // = \int d\vec{r}_{N+1} \exp(-\beta U)
        // = \sum_{r_grid} dV * \exp(-beta U)
        // = dV * \sum_{r_grid} \exp(-beta U)
        // = dV * sum_pr
        sum_pr = sum_pr * dV;
        
        total_sum_pr += sum_pr;

        if ( integral_type == grid ) {
            fprintf(dMu_output,"%d\t%e\t%e\t%d\n",sys->current_frame_id+1,sum_pr,total_sum_pr/frames,n_inserts_frame);
        } else if ( integral_type == monte_carlo ) {
            //   dV   = V / npts
            // I = V \int ds pr
            //   = V < pr >
            //   = < pr.V >
            // the error in I is therefore
            // var(I) = var(pr.V)
            //        = E( pr^2 * V^2 ) - I^2
            //        = sum_pr2 * V^2 / N - I^2
            //        = sum_pr2 * dV * dV * N - I^2
            // stderr = sqrt(var(I)/N)
            sum_pr2 = sum_pr2 * dV * dV * npts;
            sum_pr2 -= sum_pr*sum_pr;
            sum_pr2 = pow(sum_pr2/npts, 0.5);
            fprintf(dMu_output,"%d\t%e +/- %e\t%e\t%d\n",sys->current_frame_id+1,sum_pr,sum_pr2,total_sum_pr/frames,n_inserts_frame);
        }
    } // end if calc_Mu

    // perform outputs and final calculations
    widom_frame_end(sys, sum_pr);

    fflush(stdout);
    fflush(dMu_output);
    
    // free some memory
    for( i=0; i< cell_ngridpts; i++) {
        m_parray_free(cell_occupancy[i]);
    }
    
    if ( integral_type == monte_carlo ) {
        for (i=0; i<print_rank->count; i++) {
            free(print_rank->data[i]);
        }
    }
    m_parray_free(print_rank);

} // end frame_routine

void frame_finalise(M_system* sys) {
    free(cell_occupancy);

    int i;
    for( i=0; i<probe_pts->count; i++) {
        free(probe_pts->data[i]);
    }
    m_parray_free(probe_pts);

    if ( coords_output != stdout && coords_output != NULL ) {
      fclose(coords_output);
    }

    if ( dMu_output != stdout && dMu_output != NULL ) {
      fclose(dMu_output);
    }
    
    widom_finalise(sys);
}

void get_mem() {
    exclusion_r = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(exclusion_r, nspecies);

    Q = 0;

    smallsig = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(smallsig, nspecies);

    ss12 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(ss12, nspecies);

    B = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(B, nspecies);

    abm = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(abm, nspecies);

    C6 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(C6, nspecies);

    C8 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(C8, nspecies);
}

void read_insert_input(dictionary *dict) {

    char *in;
    bool chk;

    bool calc_dMu;
    bool only_nearest;


    in = iniparser_getstring(dict, "system:out_mode", "w");
    if ( !strcmp(in,"append") || !strcmp(in,"a") ) {
      out_mode = "a";
    } else {
      out_mode = "w";
    }

    // possible options:
    // print_coords=1, calc_dMu=1, only_nearest=0
    // print_coords=1, calc_dMu=0, only_nearest=1
    // print_coords=0, calc_dMu=1, only_nearest=0
    calc_dMu     = iniparser_getboolean(dict, "insert:calc_dMu", 1);
    print_coords = iniparser_getboolean(dict, "insert:print_coords", 0);
    only_nearest = iniparser_getboolean(dict, "insert:only_nearest", 0);
    // check what kind of calculation we are doing
    if (calc_dMu && (!only_nearest) ) {
        // we are calculating a chemical potential
        rank_type = dMu;

        // do we save the chemical potentials to a file?
        in = iniparser_getstring(dict, "insert:dMu_file", NULL );
        if ( in != NULL ) {
            strcpy(dMu_file, in);
            dMu_output = fopen(dMu_file,out_mode);
        } else {
            dMu_output = stdout;
        }

        setT = iniparser_getdouble(dict, "system:setT", 0.0);
        if ( !setT ) {
            FATAL("Conflicting options:\n");
            FATAL("\tI cannot calculate dMu without setT specified.\n");
            FATAL("Aborting...\n");
            exit(1);
        }
        beta = 1.0/setT;

    } else {
        // we are not calculating a chemical potential
        if (print_coords) {
            // sorting coordinates according to nearest neighbour distances?
            if (only_nearest) {
                rank_type = nearest;
            // otherwise sort according to dU
            } else {
                rank_type = dU;
            }
        } else {
            FATAL("Conflicting options:\n");
            if ( calc_dMu && only_nearest ) {
                FATAL("\tcalc_dMu and only_nearest are both switched on.\n"); 
                FATAL("Aborting...\n");
            } else {
                FATAL("\tNot calculating dMu nor printing coordinates.\n");
                FATAL("\tNothing to do. Aborting...\n");
            }
            exit(1);
        }
    }

    in = iniparser_getstring(dict, "insert:integral_type", "grid" );
    if ( !strcmp(in,"grid") ) {
        // we are using a grid
        integral_type = grid;

        // get the number of grid points
        in = iniparser_getstring(dict, "insert:ngrid", NULL );
        if ( in != NULL ) {
            parse_csv_uiarray( ngrid, in, 3);  //note that ngrid is an array, we must pass the pointer
        } else {
            FATAL("ngrid must be specified in [insert] in the configuraiton file!\n");
            exit(1);
        }
        ngridpts = ngrid[0]*ngrid[1]*ngrid[2];
        npts = ngridpts;
    } else if ( !strcmp(in,"monte_carlo") || !strcmp(in,"monte") || !strcmp(in,"montecarlo") || !strcmp(in,"MC") || !strcmp(in,"random") ) {
        integral_type = monte_carlo;

        // get the number of insertion points
        npts = iniparser_getint(dict, "insert:n_mc", 0 );
        ngrid[0] = 0;
        ngrid[1] = 0;
        ngrid[2] = 0;
        ngridpts = 0;
        exclusion_res = iniparser_getint(dict, "insert:exclusion_res", 1);
        if ( npts <= 0 ) {
            FATAL("n_mc must be a positive number!\n");
            exit(1);
        }
        /* initialize random seed: */
        srand ( time(NULL) );
    } else {
        FATAL("Invalid keyword for integral_type. See --example for a list of keywords for this option.\n");
        exit(1);
    }


    chk = iniparser_getboolean(dict, "insert:two_particle", 0);
    if ( chk ) {
        insertion_type = two_particles;

        if ( integral_type != monte_carlo ) {
            FATAL("Two-particle insertion only works with Monte-Carlo integration.\n");
            exit(1);
        }
        
        r_tt = iniparser_getdouble(dict, "insert:r_tt", 0);
        if ( ! (r_tt > 0) ) {
            FATAL("Requires r_tt to be a positive number!\n");
            exit(1);
        }

        in = iniparser_getstring(dict, "insert:U_tt", NULL);
        if ( in != NULL ) {
            U_tt = iniparser_getdouble(dict, "insert:U_tt", NAN);
        } else {
            FATAL("U_tt is required!\n");
            exit(1);
        }

    } else {
        insertion_type = one_particle;
    }


    if ( print_coords ) {
        print_sort = iniparser_getboolean(dict, "insert:print_sort", 1);
        print_excluded = iniparser_getboolean(dict, "insert:print_excluded", 0);

        in = iniparser_getstring(dict, "insert:print_range", NULL );
        if ( in != NULL ) {
            parse_csv_uiarray( print_range, in, 2);
            if ( !print_range[0]>0 || !print_range[1]>print_range[0]) {
                FATAL("Invalid print_range. Please enter positive integers 'i,j' where i<j\n");
                exit(1);
            } else if ( print_range[1] > npts ) {
                FATAL("print_range must not exceed the total number of insertions per frame.\n");
                exit(1);
            }
        } else {
            print_range[0]=1;
            print_range[1]=npts;
        }

        in = iniparser_getstring(dict, "insert:coords_file", NULL );
        if ( in != NULL ) {
            strcpy(coords_file, in);
            coords_output = fopen(coords_file,out_mode);
        } else {
            coords_output = stdout;
        }
    }

    // exclusion_r may be used even if only using only_nearest
    // but it is not a required option (defaults to zero)
    in = iniparser_getstring(dict, "insert:exclusion_r", NULL );
    if ( in != NULL ) {
        parse_csv_farray( exclusion_r, in, nspecies );
    }

    // cell_ngrid is not a required option (defaults to 1 )
    in = iniparser_getstring(dict, "insert:cell_ngrid", NULL );
    if ( in != NULL ) {
      parse_csv_uiarray( cell_ngrid, in, 3);  //note that cell_ngrid is an array, we must pass the pointer
    } else {
      set_uiarray( cell_ngrid, 1, 3); // set the number of gridpts to 1 in each dimension
    }

    // interaction potentials are only parsed if calculating chemical potentials
    if ( rank_type == dMu || rank_type == dU ) {
        Q = iniparser_getdouble(dict, "insert:Q", 0);

        in = iniparser_getstring(dict, "insert:smallsig", NULL);
        if ( in != NULL ) {
            parse_csv_farray( smallsig, in, nspecies );
        }

        int i;
        for( i=0; i<nspecies; i++ ) {
            ss12[i] = pow(smallsig[i],12);
        }

        in = iniparser_getstring(dict, "insert:B", NULL);
        if ( in != NULL ) {
            parse_csv_farray( B, in, nspecies );
        }

        in = iniparser_getstring(dict, "insert:abm", NULL);
        if ( in != NULL ) {
            parse_csv_farray( abm, in, nspecies );
        }

        in = iniparser_getstring(dict, "insert:C6", NULL);
        if ( in != NULL ) {
            parse_csv_farray( C6, in, nspecies );
        }

        in = iniparser_getstring(dict, "insert:C8", NULL);
        if ( in != NULL ) {
            parse_csv_farray( C8, in, nspecies );
        }
    }
}

void get_more_mem(M_system *sys) {
    int i;

    // -------------------
    // initialize variables for excluded volume
    // -------------------
    // these variables tell us the size of the exclusion volume parameters
    exclusion_r2  = (prec *) malloc(sizeof(prec) * nspecies);
    for (i=0; i<nspecies; i++) {
        exclusion_r2[i] = pow(exclusion_r[i],2.0);
    }
    exclusion_grid_width = (int *) malloc(sizeof(int) * 3 * nspecies);

    // -------------------
    // use the cell method for loop optimisation
    // -------------------
    // an array of integer arrays, each integer array containing
    // the indices of atoms located inside a grid-box
    //  
    // note that the size of 'occupancy' is ngrid^3
    // it is the 'flattened' array of a 3D matrix
    //  
    // also note that the number of grids in occpancy is different to number of grids for in
    // it is much coarser
    cell_ngridpts = cell_ngrid[0] * cell_ngrid[1] * cell_ngrid[2];
    cell_occupancy = malloc(sizeof(M_parray *) * cell_ngridpts);

    // -------------------
    // populate the probe points
    // -------------------
    probe_pts = NULL;
    if ( integral_type == grid ) {
        init_probe_pts(sys);
    }
}


prec U_ij( prec r2, int spc_id ) {
  prec deltaU = 0;
  prec r_r2, r_r6, r_r8, r_r12;
  prec r = 0;

  if ( B[spc_id] ) {
    if ( abm[spc_id] ) {
      r = sqrt(r2);
      deltaU += B[spc_id] * exp(-1.0 * abm[spc_id] * r);
    } else {
      deltaU += B[spc_id];
    }
  }

  r_r2 = 1.0/r2;
  r_r6 = pow(r_r2, 3);
  r_r8 = r_r2 * r_r6;
  r_r12 = r_r6 * r_r6; 

  deltaU += ss12[spc_id] * r_r12 - C6[spc_id] * r_r6 - C8[spc_id] * r_r8;

  if ( deltaU < -100 ) {
      DEBUG("%lf\t%lf\n",sqrt(r2),deltaU);
  }

  return deltaU;
}

prec U_tail( M_system *sys ) {

  prec deltaU = 0;

  prec rcut3=pow(sys->rcut,3.0);
  prec rcut5=rcut3*sys->rcut*sys->rcut;
  prec rcut9=pow(rcut3,3.0);

  int i;
  for ( i=0; i<sys->atom_specs->count; i++ ) {

    M_atom_spec *aspec = m_sys_atom_spec(sys,i);
    prec rho = aspec->sys_total/sys->volume;

    //   U_lj = ss12/r^12 - C6/r^6 - C8/r^8
    // U_tail = int_rcut^infty  U_lj(r) 4 pi r^2 rho dr
    //        = 4 pi rho int_rcut^infty U_lj(r) r^2 dr     

    deltaU += ( ss12[i]/(9.0*rcut9) - C6[i]/(3.0*rcut3) - C8[i]/(5.0*rcut5)) * 4 * pi * rho;

  }

  if ( insertion_type == two_particles ) {
    deltaU *= 2.0;
    deltaU += U_tt;
  }

  return deltaU;
}

// debug routine to print the occupancy of each cell grid
void print_occupancy_atom_ids() {
    int i,n;
    M_atom *this_atom;

    for (n=0; n<cell_ngridpts; n++) {
      int len = cell_occupancy[n]->count;
      printf("cell_occupancy[%d]: ",n);
      for (i=0; i<len; i++) {
        this_atom = cell_occupancy[n]->data[i];
        printf("%d ",this_atom->id);
      }
      printf("\n");
    }

}

/* We use bubble sort to sort the print_rank array

   The quantities we use to sort are:
     if calc_dMu, sort by pr:
        poor:    dU>>0, pr ~ 0, low rank
        good:    dU<0, pr > 1, high rank
     if only_nearest, sort by r2 of nearest neighbour:
        poor:    r2 ~ 0, low rank
        good:    r2 >> 0, high rank
     in both cases, sort rank by high to low.

   Bubble sort, unlike other methods such as merge-sort,
   creates a sorted partial list at the high end, i.e.
   we can leave the smaller elements unsorted. The next
   best sorting algorithm, quick-sort, can create a partially
   sorted list at the low end, but the speed can be variable
   depending on how lucky the location of the pivots are */
void print_rank_bubble_sort( int n_required ) {
    M_point_probe *probe_i, *probe_j;
    int n = print_rank->count;
    int i;
    int n_sorted = 0;
    int last_swap = 0;
   
    // if we require all to be sorted, this is the same as requiring
    // all but one to be sorted 
    if ( n_required == n ) {
      n_required = n - 1;
    }

    // bubble sort!
    while ( n_sorted < n_required ) {
      last_swap = 0;
      for ( i=1; i<n-n_sorted; i++ ) {
        probe_i = print_rank->data[i-1];
        probe_j = print_rank->data[i];
        if ( probe_i->rank > probe_j->rank ) {
          m_parray_swap( print_rank, i-1, i );
          last_swap = i;
        }
      }
      n_sorted = n - last_swap;
    }
}

// for debugging. print the entire table
void print_rank_table() {
    int n;
    M_point_probe *probe;
    //DEBUG("%d, %d, %d\n",print_rank->count,print_rank->length,print_rank->e_size);
    //exit(0);

    for (n=0; n<print_rank->count; n++ ) {
      probe = print_rank->data[n];
      printf("%d:\t %f\n",probe->grid_array_i,probe->rank);
    }

}

// for output of coordinates
void print_rank_print_coords( int frame_id, int start, int end ) {
    M_point_probe *temp;
    int n = print_rank->count;
    
    if ( start > n ) {
      DEBUG("Print range starts at %d, list is only %d long. Nothing to print.\n",start,n);
      return;
    }
    if ( end > n ) {
      DEBUG("Print range ends at %d, list is only %d long. Printing only until end of list.\n",end,n);
      end = n;
    }

    DEBUG("writing grid coordinates, output range [%d--%d] (%d out of %d).\n",n-end+1,n-start+1,end-start+1,n);

    int i;
    for ( i=n-start; i>=n-end; i-- ) { 
        temp = print_rank->data[i];
        fprintf(coords_output,"%d-%d\t%f\t%f\t%f\t%.10g\n",frame_id,temp->grid_array_i,temp->position.x,temp->position.y,temp->position.z,temp->rank);
    }

}

void init_probe_pts(M_system* sys) {
    int i,j,k;
    
    probe_pts = m_parray_new( 0 );
    
    ngridpts = ngrid[0]*ngrid[1]*ngrid[2];
    
    for ( i=0; i<ngridpts; i++) {
        M_point_probe *probe = malloc(sizeof(M_point_probe));
        probe->position.dims = &sys->dims;
        m_parray_push( probe_pts, probe );
    }
    
    for (i=0; i<ngrid[0]; i++) {
        for (j=0; j<ngrid[1]; j++) {
            for (k=0; k<ngrid[2]; k++) {

                int id = i*ngrid[2]*ngrid[1]+j*ngrid[2]+k;

                M_point_probe *probe = probe_pts->data[id];

                probe->grid_coord[0] = i;
                probe->grid_coord[1] = j;
                probe->grid_coord[2] = k;
                probe->grid_array_i = get_id(probe->grid_coord, ngrid, 3);

                probe->cell_coord[0] = floor((prec) (i+0.5)/ngrid[0]*cell_ngrid[0]);
                probe->cell_coord[1] = floor((prec) (j+0.5)/ngrid[1]*cell_ngrid[1]);
                probe->cell_coord[2] = floor((prec) (k+0.5)/ngrid[2]*cell_ngrid[2]);
                probe->cell_array_i = get_id(probe->cell_coord, cell_ngrid, 3);

            }
        }
    }
}

void free_probe_pts() {
    if ( probe_pts != NULL ) {
        m_parray_free_children(probe_pts, (M_callback) free);
    }
}

void init_grid(M_system* sys) {

    M_atom *this_atom;
    M_uint aspec_id;
    M_point_probe *probe;
    
    int i,j,k,n;
    int ii,jj,kk;

    M_uint this_cell_array_i;
    M_uint this_grid_coord[3], this_cell_coord[3];

    M_vector dr;
    prec r2;
    
    // -------------------
    // initialise the grid
    // -------------------
    // - calculate the insertion grid spacing
    // - calculate the cell grid spacing
    // - convert exclusion radii and rcut in terms of grid spacings
    // - clear the gridpoints
    
    // get the size of a grid
    
      
    dV = (prec) sys->volume/npts;
    
    if ( integral_type == monte_carlo ) {
      
        prec min_r = sys->rcut;
        for (i=0; i<nspecies; i++) {
            if (exclusion_r[i]>0) {
                min_r = fmin(min_r, exclusion_r[i]);
            }
        }
        
        prec dx = min_r/exclusion_res;
        
        M_uint old_ngrid[3] = {ngrid[0], ngrid[1], ngrid[2]};
        
        ngrid[0] = (M_uint) ceil(sys->dims.x/dx);
        ngrid[1] = (M_uint) ceil(sys->dims.y/dx);
        ngrid[2] = (M_uint) ceil(sys->dims.z/dx);
        
        if ( ngrid[0] != old_ngrid[0] || ngrid[1] != old_ngrid[1] || ngrid[2] != old_ngrid[2] ) {
            free_probe_pts();
            init_probe_pts(sys);
        }
        
    }
    dgrid.x=sys->dims.x/ngrid[0];
    dgrid.y=sys->dims.y/ngrid[1];
    dgrid.z=sys->dims.z/ngrid[2];
    cell_dgrid.x=sys->dims.x/cell_ngrid[0];
    cell_dgrid.y=sys->dims.y/cell_ngrid[1];
    cell_dgrid.z=sys->dims.z/cell_ngrid[2];

    // get the size of excluded volume of each atom_spec, in number of grids
    for (i=0;i<nspecies;i++) {
        exclusion_grid_width[3*i+0]=ceil(exclusion_r[i]/dgrid.x);
        exclusion_grid_width[3*i+1]=ceil(exclusion_r[i]/dgrid.y);
        exclusion_grid_width[3*i+2]=ceil(exclusion_r[i]/dgrid.z);
    }

    // get the size of rcut, in number of grids
    rcut_cell_grid_width[0]=ceil(sys->rcut/cell_dgrid.x);
    rcut_cell_grid_width[1]=ceil(sys->rcut/cell_dgrid.y);
    rcut_cell_grid_width[2]=ceil(sys->rcut/cell_dgrid.z);
    if ( sys->no_cutoff ) {
      rcut_cell_grid_width[0] = ceil(cell_ngrid[0]/2.0);
      rcut_cell_grid_width[1] = ceil(cell_ngrid[1]/2.0);
      rcut_cell_grid_width[2] = ceil(cell_ngrid[2]/2.0);
    }

    // update the position of the probe points, the box size may have changed
    for (i=0; i<ngridpts; i++) {
        probe = probe_pts->data[i];
      
        probe->position.x = dgrid.x*((prec) probe->grid_coord[0]+0.5);
        probe->position.y = dgrid.y*((prec) probe->grid_coord[1]+0.5);
        probe->position.z = dgrid.z*((prec) probe->grid_coord[2]+0.5);

        // also set the default rank to rcut
        if ( rank_type == nearest ) {
          probe->rank = rcut2;
        }

        // by default, all points are included
        probe->exclusion = 0;
    }

    // -------------------
    // build excluded volume and occupancy
    // -------------------
    // for each atom:
    //    save its cell_occupancy
    //    subtract its excluded volume
    //
    // allocate cell_occupancy 
    for( i=0; i< cell_ngridpts; i++) {
        cell_occupancy[i] = m_parray_new( 0 );
    }
    for (n=0; n<sys->atoms->count; n++) {
        this_atom = m_sys_atom(sys,n);
        aspec_id = this_atom->spec->id;
        
        this_grid_coord[0] = floor(this_atom->pos.x/dgrid.x);
        this_grid_coord[1] = floor(this_atom->pos.y/dgrid.y);
        this_grid_coord[2] = floor(this_atom->pos.z/dgrid.z);

        this_cell_coord[0] = floor(this_atom->pos.x/cell_dgrid.x);
        this_cell_coord[1] = floor(this_atom->pos.y/cell_dgrid.y);
        this_cell_coord[2] = floor(this_atom->pos.z/cell_dgrid.z);

        this_cell_array_i = get_id(this_cell_coord, cell_ngrid, 3);

        m_parray_push( cell_occupancy[this_cell_array_i], this_atom );

        for (i=-exclusion_grid_width[aspec_id*3+0]; i <= exclusion_grid_width[aspec_id*3+0]; i++) {
            ii = pmod((i + this_grid_coord[0]) , ngrid[0]);
            for (j=-exclusion_grid_width[aspec_id*3+1]; j <= exclusion_grid_width[aspec_id*3+1]; j++) {
                jj = pmod((j + this_grid_coord[1]) , ngrid[1]);
                for (k=-exclusion_grid_width[aspec_id*3+2]; k <= exclusion_grid_width[aspec_id*3+2]; k++) {
                    kk = pmod((k + this_grid_coord[2]) , ngrid[2]);
                    
                    M_uint new_grid_coord[3] = {ii,jj,kk};

                    probe = probe_pts->data[get_id(new_grid_coord,ngrid,3)];

                    dr = m_radius(this_atom->pos,probe->position);
                    r2 = m_vector_mag2(dr);

                    if ( r2 < exclusion_r2[aspec_id]) {
                        probe->exclusion = 1;
                    }   
                }   
            }   
        }
    }
    //print_occupancy_atom_ids();
}

int is_excluded(M_system *sys, M_point_probe *probe) {
    int i = floor(probe->position.x/dgrid.x);
    int j = floor(probe->position.y/dgrid.y);
    int k = floor(probe->position.z/dgrid.z);
    
    M_uint this_grid_coord[3];
    
    int ii,jj,kk;
    for (ii=i-1; ii<=i+1; ii++) {
        this_grid_coord[0] = pmod(ii,ngrid[0]);
        for (jj=j-1; jj<=j+1; jj++) {
            this_grid_coord[1] = pmod(jj,ngrid[1]);
            for (kk=k-1; kk<=k+1; kk++) {
                this_grid_coord[2] = pmod(kk,ngrid[2]);
                
                int id = get_id(this_grid_coord, ngrid, 3);
                
                M_point_probe *grid_probe = probe_pts->data[id];
                
                if ( grid_probe->exclusion == 0 ) {
                    return 0;
                }
              
            }
        }
    }
    return 1;
}

prec calc_dU(M_system *sys, M_point_probe *probe) {

    M_atom *this_atom;
    int aspec_id;
    
    int ii,jj,kk;   
    int l;
    M_uint this_cell_array_i;
    M_uint this_cell_coord[3];

    M_vector dr;
    prec r2;
    
    prec dU_i;
    
    dU_i = 0;

    ii=0;
    // loop over atoms within the r_cut cube
    //for (ii=-rcut_cell_grid_width[0]; ii <= rcut_cell_grid_width[0]; ii++ ) { 
    while (ii<pspan(rcut_cell_grid_width[0]*2+1,cell_ngrid[0])) {
        this_cell_coord[0] = pmod( (ii + probe->cell_coord[0] - rcut_cell_grid_width[0]), cell_ngrid[0]);
        jj=0;
        //for (jj=-rcut_cell_grid_width[1]; jj <= rcut_cell_grid_width[1]; jj++ ) { 
        while (jj<pspan(rcut_cell_grid_width[1]*2+1,cell_ngrid[1])) {
            this_cell_coord[1] = pmod( (jj + probe->cell_coord[1] - rcut_cell_grid_width[1]), cell_ngrid[1]);
            kk=0;
            //for (kk=-rcut_cell_grid_width[2]; kk <= rcut_cell_grid_width[2]; kk++ ) { 
            while (kk<pspan(rcut_cell_grid_width[2]*2+1,cell_ngrid[2])) {
                this_cell_coord[2] = pmod( (kk + probe->cell_coord[2] - rcut_cell_grid_width[2]), cell_ngrid[2]);

                // the array index of this cell is:
                this_cell_array_i = get_id( this_cell_coord, cell_ngrid, 3 );
                // the atoms in this cell can be retrived by:
                //    cell_occupancy[i]->count
                //    cell_occupancy[i]->data = { M_atom *a1, *a2, *a3, ... }
                for (l=0; l < cell_occupancy[this_cell_array_i]->count; l++) {
                    this_atom = cell_occupancy[this_cell_array_i]->data[l];
                    aspec_id = this_atom->spec->id;

                    dr = m_radius(this_atom->pos,probe->position);
                    r2 = m_vector_mag2(dr);
                   
                    // first make sure we are within rcut 
                    if ( r2 < rcut2 || sys->no_cutoff ) {
                        // if we are calculating the energy, then
                        if ( (rank_type == dMu || rank_type == dU) && r2 < rcut2 ) {
                            //DEBUG("%d\t%d\t%g\t%g\t%g\n",probe->grid_array_i,aspec_id,r2,U_ij(r2, aspec_id), dU);
                            dU_i += U_ij(r2, aspec_id);
                        // otherwise, store the nearest neighbour distance
                        } else if ( rank_type == nearest ) { 
                            if ( r2 < probe->rank ) {
                                probe->rank = r2;
                            }
                        }

                        widom_insert_pair(sys, probe, this_atom, dr, r2);
                    } // end if ( r2 < rcut2 )
                } // end for l in cell_occupancy

            kk+=1;
            } // end for kk
        jj+=1;
        } // end for jj
    ii+=1;
    } // end for ii

    // now get the Ewald sum if there is a charge
    if ( Q != 0 ) {
        M_ewald *new = m_ewald_calc_add(sys, probe->position, Q);
        dU_i += (new->U_coulomb - (sys->ewald->U_coulomb + U_isolated));
        m_ewald_free(new);
    }
//    DEBUG("%.12E\t%.12E\t%.12E\t%.12E\n",probe->position.x,probe->position.y,probe->position.z,dU_i);
    
    return dU_i;

}
