#include "widom.h"

char *description \
 = "widom.pair_pmf\n\
    \n\
      Calculates the excess PMF between a pair of solutes via Widom insertion.\n\
    \n\
      Output format:\n\
        r/bohr \t S \t count (#pairs in each bin)\n\
    \n\
      Note that for the output of the second column, S\n\
             exp(-beta DF) = S = < V * sum( exp(-beta DU) ) > / < V >\n\
      where\n\
             DF = DF_excess(i,j) + DF(i) + DF(j)\n\
      and so the single-particle free energy must be subtracted from the pair pmf.\n\
      Note that the solute-solute potential U_ij(r) is not included, as it is a constant\n\
      and trivial factor on the statistical average.\n\
      \n\
      In the convergence limit, exp(-beta DF) = S\n\
      In practical calculations, S may be averaged over many calculations to yield a\n\
      converged estimate of exp(-beta DF), with uncertainty estimates.\n\
      \n\
      See source code for more description of S.\n\
";

M_n_aggregate *excess_pmf;
prec  pr_cut;
prec  max_r2;
M_parray *saved_probes;
bool print_debug;
FILE* pmf_output = NULL;
char pmf_file[256];

void widom_print_example() {
  char *text \
= "[pmf]\n\
; requires:\n\
; insert:calc_dMu = true\n\
; insert:integral_type = monte_carlo\n\
; insert:two_particle = false\n\
dr      = 5     ; float, atomic units, default = 0.1, optional\n\
                  ; bin size for the PMF. Recommend a much larger value than\n\
                  ; default (e.g. ~ 1 bohr)\n\
max_r   = 90    ; float, atomic units, required\n\
                  ; max range of PMF to be calculated.\n\
pr_cut  = 0.0   ; float, dimensionless, default = 0.0, optional\n\
                  ; the minimum probability exp(-dU/kT) accepted for considering\n\
                  ; a successful insertion. Also see en_cut below.\n\
en_cut  = 0.002 ; float, atomic units (Ha), optional, ignored if pr_cut is present\n\
                  ; This calculation calculates the PMF by taking 'successful'\n\
                  ; single particle insertions. en_cut or pr_cut sets a energy ceiling\n\
                  ; above which single particle insertions are discarded as they\n\
                  ; contribute insignificantly to the overall PMF.\n\
                  ; A conservative estimate will be (mu + 5kT), where mu is the\n\
                  ; chemical potential of a single particle insertion.\n\
pmf_file = excess_pmf.out\n\
                ; string, default = stdout, optional\n\
                  ; print the pmf into this file, note system:out_mode sets the\n\
                  ; append property.\n\
print_debug = true\n\
                ; boolean, default = false, optional\n\
                  ; print the number of single particle insertions that are above the\n\
                  ; pr_cut limit. This is the number of positions used in each frame\n\
                  ; to generate the PMF\n\
";
  printf("%s\n",text);
}

void widom_init(M_system* sys, dictionary* dict) {

  if ( rank_type != dMu ) {
    FATAL("'calc_dMu' must be set to 'true' in the configuration file!\n");
    exit(1);
  }
  if ( integral_type != monte_carlo ) {
    FATAL("'integral_type' must be set to 'monte_carlo' in the configuration file!\n");
    exit(1);
  }
  if ( insertion_type != one_particle ) {
    FATAL("'two_particle' must be set to 'false' in the configuration file!\n");
    exit(1);
  }

  prec min = 0.0;
  prec max = iniparser_getdouble(dict, "pmf:max_r", 0.0);
  if ( max <= 0.0 ) {
    FATAL("'max_r' must be a positive real number!\n");
    exit(1);
  }
  max_r2 = max*max;
  prec width = iniparser_getdouble(dict, "pmf:dr", 0.1);

  excess_pmf = m_n_aggregate_new(1, &min, &max, &width, 2); 
  // key: r between Xe-Xe
  // column 1: exp(-dF/kT)
  // column 2: count (#pairs in the bin)

  prec en_cut_ini, pr_cut_ini;
  pr_cut_ini = iniparser_getdouble(dict, "pmf:pr_cut", 0.0);

  if ( pr_cut_ini > 0 ) {
    pr_cut = pr_cut_ini;
  } else if ( iniparser_getstring(dict, "pmf:en_cut", NULL) != NULL ) {
    en_cut_ini = iniparser_getdouble(dict, "pmf:en_cut", 0.0);
    pr_cut = exp( - en_cut_ini / setT );
  } else {
    pr_cut = 0;
  }

  char *in;
  in = iniparser_getstring(dict, "pmf:pmf_file", NULL );
  if ( in != NULL ) {
    strcpy(pmf_file, in);
    pmf_output = fopen(pmf_file, out_mode);
  } else {
    pmf_output = stdout;
  }

  print_debug = iniparser_getstring(dict, "pmf:print_debug", 0);
}

void widom_frame_init(M_system* sys) {
  saved_probes = m_parray_new( 0 );
}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {
}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {

  if ( pr > pr_cut ) {
    M_point_probe *save = malloc(sizeof(M_point_probe));

    save->position.x    = probe->position.x;
    save->position.y    = probe->position.y;
    save->position.z    = probe->position.z;
    save->position.dims = &sys->dims;
    save->rank          = pr;

    m_parray_push(saved_probes, save);
  }

}

void widom_frame_end(M_system* sys, prec sum_pr) {

  int l = saved_probes->count;
  //int n = npts;
  //int n_trials = n*(n-1)/2;

  if ( print_debug ) {
    DEBUG("saved: %d\n",l);
  } else if ( l < 2 ) {
    DEBUG("Only %d insertions satisfied the en_cut / pr_cut limit. No pair PMF calculated.\n",l);
  }

  int i,j;
  for ( i=0; i<l-1; i++ ) {
    M_point_probe *probe_i = saved_probes->data[i];
    for ( j=i+1; j<l; j++ ) {
      M_point_probe *probe_j = saved_probes->data[j];

      prec r2 = m_vector_mag2(m_disp(probe_i->position, probe_j->position));
      if ( r2 < max_r2 ) {
        excess_pmf->key[0] = sqrt(r2);
        excess_pmf->data[0] = probe_i->rank * probe_j->rank * dV;
        excess_pmf->data[1] = 1.0;

        _m_n_aggregate(excess_pmf);
      }
    }
  }

  for ( i=0; i<l; i++ ) {
    free(saved_probes->data[i]);
  }
  free(saved_probes);
}

void widom_finalise(M_system* sys) {

  /* Note on normalisation

     In an NpT Widom insertion calculation, the free energy is given by Paschek:
        - \beta DF = ln [ < V \int ds exp(-\beta DU) > / < V > ]
     where the integral ds runs over [0,1]^3, i.e. the entire cell. This is computed
     by a grid integration.

     In a Monte-Carlo insertion calculation, the integral is instead given by:
        \int ds exp(-\beta DU) = lim_{N->inf} [ \sum_i^N exp(-\beta dU) / N ]

     If we were considering pairwise PMF, the free energy from an integral approach
     would be:
    I(|r|) = \int ds [ exp(-\beta DU(\vec{s})) \int dr exp(-\beta DU(\vec{s}+\vec{r}) ] 
     where |r| is fixed, and so the inner integral is only over angular coordinates

     In a Monte-Carlo approach, we insert particles randomly, then seek pairs such that
     their separation is within |r|+/-dr. We are therefore sampling both the inner and
     outer integrals at the same time. The weighting function is therefore:

        I(|r|) =        \sum          exp( - \beta (DU(x_i) + DU(x_j) ) / E(N(r))
                i<j \in |r_ij-r|<dr/2
     where E(N(r)) is the expected number of pairs at that distance.

        E(N(r)) = 1/2 * N_mc * N_mc,slice(r) * n_frames
                = 1/2 * n_frames * N_mc * N_mc * V_slice(r) / < V >

     therefore
        - \beta DF = ln [ < V I(|r|) > / < V > ]
                   = ln [ < V \sum exp(-\beta DU(x_i)+DU(x_j)) > / ( < V > * E(N(r)) ) ]
                         /  < V \sum exp(-\beta DU(x_i)+DU(x_j)) >  \
                   = ln | ------------------------------------------ |
                         \   1/2 * n_frames * N_mc^2 * V_slice(r)   /
                         /  < \sum V/N exp(-\beta DU(x_i)+DU(x_j)) >  \
                   = ln | -------------------------------------------- |
                         \     1/2 * n_frames * N_mc * V_slice(r)     /
                   = ln < S >

     Previously, S = \sum dV exp(-\beta DU(x_i)+DU(x_j))
     At final output, we scale S by ( 1/2 * n_frames * N_mc * V_slice(r) )^-1 to obtain the 
     inside term of the logarithm.
     When converged, S = exp( -\beta DF ). This term can be averaged over many insertion 
     calculations to produce a converged average.
  */
  int nframes = sys->frames_processed + 1;
  prec prefactor = ( 2.0 ) / ( (prec) npts * nframes );


  m_n_aggregate_col_scale_dV(excess_pmf, 0, 0);
  m_n_aggregate_col_scale(excess_pmf, 0, prefactor);

  m_n_aggregate_set_print_all(excess_pmf);
  excess_pmf->print_count = 0;

  m_n_aggregate_fprint(pmf_output,excess_pmf);

  free(excess_pmf);

  if ( pmf_output != stdout && pmf_output != NULL ) {
    fclose(pmf_output);
  }

}
