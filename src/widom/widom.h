#ifndef WIDOM_LIB_H
#define WIDOM_LIB_H
#include "comoda.h"
#include "frame_main.h"

typedef struct _point_probe {
    M_uint grid_coord[3];
    M_uint grid_array_i;
    bool exclusion;
    prec rank;
    M_point position;
    M_uint cell_coord[3];
    M_uint cell_array_i;
} M_point_probe;

void widom_print_example();
void widom_init(M_system* sys, dictionary* dict);
void widom_frame_init(M_system* sys);
void widom_insert_init(M_system* sys, M_point_probe* probe);
void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2); // dr: from probe -> atom
void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr);
void widom_frame_end(M_system* sys, prec sum_pr);
void widom_finalise(M_system* sys);

void get_mem();
void read_insert_input(dictionary* dict);
void get_more_mem(M_system *sys);
prec U_ij(prec r2, int spc_id);
void print_occupancy_atom_ids();
void print_rank_bubble_sort( int n_required );
void print_rank_table();
void print_rank_print_coords( int frame_id, int start, int end );
void init_probe_pts(M_system *sys);
void free_probe_pts();
void init_grid(M_system *sys);
int  is_excluded(M_system *sys, M_point_probe *probe);
prec calc_dU(M_system *sys, M_point_probe *probe);
prec U_tail(M_system *sys);

char *out_mode;

int nspecies;
prec setT, beta;

typedef enum { grid, monte_carlo } integral_t;
integral_t integral_type;

typedef enum { dMu, dU, nearest } rank_t;
rank_t rank_type;

typedef enum { one_particle, two_particles } insertion_t;
insertion_t insertion_type;
prec r_tt; // separation between the two particles
prec U_tt; // interaction energy between the two test particles at r_tt

// -------------------
// We divide the box into smaller boxes of ngrid x ngrid x ngrid
// -------------------
M_uint ngrid[3];
M_uint npts, ngridpts;
M_vector dgrid;
prec dV;
int rcut_cell_grid_width[3];
prec rcut2;

// -------------------
// interaction potentials
// -------------------
prec Q, U_isolated;
prec *smallsig;
prec *ss12;
prec *B;
prec *abm;
prec *C6;
prec *C8;

// -------------------
// how large are the exclusion volumes? 
// -------------------
prec *exclusion_r;
prec *exclusion_r2;
prec  exclusion_res;
int  *exclusion_grid_width; // nspecies*3 elements, using cell_grid

// -------------------
// each insertion point is a M_point_probe struct
// -------------------
M_parray *probe_pts; //  a list of all grid points (M_point_probe *)
M_parray *print_rank; // a list of grid points that will be sorted/printed

// -------------------
// a coarser grid that holds the indices of atoms that occupy that cell
// -------------------
M_uint cell_ngrid[3];
M_uint cell_ngridpts;
M_vector cell_dgrid;
M_parray ** cell_occupancy;
int cell_grid_width[3];

// -------------------
// some global sums
// -------------------
prec total_sum_pr;
M_uint total_inserts;

#endif
