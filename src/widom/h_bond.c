#include "widom.h"

char *description \
 = "frame.h_bond\n\
    \n\
      Calculates the average number of hydrogen bonds per solvent molecule.\n\
    \n\
      Parameters:                     Default:\n\
          OHO angles                      [0-180] (OFF)\n\
          HOO angles                      [0-180] (OFF)\n\
          OO distance (donor-donor)       [0-?] Must specify max\n\
          HO distance (donor-acceptor)    [0-?] Must specify max\n\
    \n\
    Configuration file fortmat:\n\
    ===========================\n\
      [system]\n\
      rcut            = 20.0      ; the maximum separation allowed for pairs (bohr)\n\
      setT            = 0.000944  ; temperature in atomic units\n\
      solvent_name    = water     ; the name of the solvent molecule species to compute\n\
                                    ; the tetrahedral-order calculation.\n\
      [h_bond]\n\
      acceptor_atom_spec = 0      ; the atom_spec of the H-bond acceptor atom (e.g. O, N, F)\n\
                                    ; the atom_spec's id in the solvent molec_spec (atom_spec->id_in_spec)\n\
      donor_atom_spec    = 1      ; the atom_spec of the H-bond donor atom (e.g. H)\n\
                                    ; the atom_spec's id in the solvent molec_spec (atom_spec->id_in_spec)\n\
      dr              = 0.01      ; bin size for r from solute (or solvent)\n\
      min_OHO         = 150       ; minimum angle for a O-H...O angle in degrees\n\
      max_OHO         = 180       ; maximum angle for a O-H...O angle in degrees\n\
      min_HOO         = 0         ; minimum angle for a H-O...O angle in degrees\n\
      max_HOO         = 30        ; maximum angle for a H-O...O angle in degrees\n\
      min_R_OO        = 4         ; minimum O...O distance for a hydrogen bond in bohr\n\
      max_R_OO        = 6         ; maximum O...O distance for a hydrogen bond in bohr\n\
      min_R_HO        = 4         ; minimum H...O distance for a hydrogen bond in bohr\n\
      max_R_HO        = 6         ; maximum H...O distance for a hydrogen bond in bohr\n\
      sv_bulk_out     = bulk.h_bond      ; the output file for the bulk solvent\n\
      sv_sol_out      = solution.h_bond  ; the output file for the solvent in the solution\n\
      solute_out      = Ar.h_bond        ; the output file for the solute\n\
      average_out     = total.h_bond     ; the output file for the total H-bond statistic\n\
      [insert]\n\
      ; flags for what kind of calculation to perform\n\
      calc_dMu        = false     ; whether or not to calculate the free energy of\n\
                                    ; particle insertion \n\
      print_coords    = true      ; whether or not to print coordinates of insertion\n\
      print_sort      = true      ; whether or not to sort the insertion coordinates\n\
                                    ; before printing. Default = true.\n\
      print_excluded  = true      ; whether or not to print the insertion coordinates\n\
                                    ; which are within exclusion_r. Default = false.\n\
      print_range     = 1,100     ; the range of insertion coordinates to print\n\
                                    ; note that the printed coordinates are ranked\n\
                                    ; from the lowest DeltaE (most probable) to\n\
                                    ; the greatest DeltaE (least probable)\n\
      coords_file     = insert_coords\n\
                                  ; print the coordinates into this file\n\
                                    ; if unspecified, prints to stdout\n\
                                    ; see note 1 for column format\n\
      dMu_file        = insert_dMu\n\
                                  ; print the insertion energies into this file\n\
                                    ; if unspecified, prints to stdout\n\
      only_nearest    = true      ; only sort the insertion coordinates by their\n\
                                    ; nearest neighbour separation\n\
      ngrid           = 10,10,10  ; number of grid points in the x,y,z direction\n\
      ; interaction potential parameters\n\
      ; comma-separated list, the length of the list must equal the number\n\
      ; of atomic species, the order of potentials must be given in the same\n\
      ; order as the declaration above\n\
      ;\n\
      ; U_j(r) = Q/r + (smallsig(j)/r)^12 + B(j)*exp(-abm(j)*r) - C6(j)/r^6 - C8(j)/r^8\n\
      ;\n\
      ; all interaction parameters are in atomic units\n\
      Q               = 1.0,-1.0\n\
      smallsig        = 1.0,1.0\n\
      B               = 0.0,0.0\n\
      abm             = 0.0,0.0\n\
      C6              = 0.0,0.0\n\
      C8              = 0.0,0.0\n\
      ; optimization settings \n\
      exclusion_r     = 3.0,4.0   ; radius to exclude from computing U \n\
      cell_ngrid      = 10,10,10  ; divide the box into small cells, for fewer\n\
                                    ; wasted pairs\n\
    ===========================\n\
    \n\
    notes:\n\
      1. column format for coords_file:\n\
          frame_id-grid_id    x/bohr    y/bohr    z/bohr    value (calc_dMu=exp(-beta dU), nearest_only=r_min^2)\n\
";

M_n_aggregate *total_sv_bulk, *frame_sv_bulk;
M_n_aggregate *total_sv_sol;
M_n_aggregate *total_solute, *insert_solute;

M_molec_spec *solvent_mspec;
M_atom_spec *donor_aspec, *acceptor_aspec;

FILE *sv_bulk_out, *sv_sol_out, *solute_out, *average_out;

M_neighbours ** h_bonds;

prec min_OHO, min_HOO, max_OHO, max_HOO;
prec min_cos_OHO, max_cos_OHO, min_cos_HOO, max_cos_HOO;
prec min_R_OO, min_R_HO, max_R_OO, max_R_HO;

prec total_bulk_n, total_bulk_n2, total_sol_n, frame_n, frame_n2;

void widom_print_example() {
}

void chk_extend( prec r ) {

    if ( r > total_sv_bulk->max[0] ) {
      total_sv_bulk->key[0] = r;

      total_sv_sol->key[0] = r;

      total_solute->key[0] = r;

      frame_sv_bulk->key[0] = r;

      insert_solute->key[0] = r;

      total_sv_bulk = m_n_aggregate_extend(total_sv_bulk);
      total_sv_sol  = m_n_aggregate_extend(total_sv_sol);
      total_solute  = m_n_aggregate_extend(total_solute);
      frame_sv_bulk = m_n_aggregate_extend(frame_sv_bulk);
      insert_solute = m_n_aggregate_extend(insert_solute);
    }

}

void widom_init(M_system* sys, dictionary* dict) {

  total_bulk_n = 0;
  total_bulk_n2 = 0;
  total_sol_n = 0;

  prec *width = malloc(sizeof(prec) * 1);
  prec *min = malloc(sizeof(prec) * 1);
  prec *max = malloc(sizeof(prec) * 1);

  min[0] = 0.0;
  max[0] = sys->rcut;
  width[0] = iniparser_getdouble(dict, "h_bond:dr", 0.01);

  total_sv_bulk = m_n_aggregate_new(1, min, max, width, 1);
  total_sv_sol  = m_n_aggregate_new(1, min, max, width, 1);
  total_solute  = m_n_aggregate_new(1, min, max, width, 2);
  frame_sv_bulk = m_n_aggregate_new(1, min, max, width, 1);
  insert_solute = m_n_aggregate_new(1, min, max, width, 1);

  total_sv_bulk->print_count = 0;
  total_sv_sol->print_count = 0;
  total_solute->print_count = 0;
  total_sv_bulk->print_empty = 1;
  total_sv_sol->print_empty = 1;
  total_solute->print_empty = 1;

  free(min);
  free(max);
  free(width);

  char *solvent_name;

  solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
  if ( solvent_name != NULL ) {
    solvent_mspec = m_molec_spec_find( sys, solvent_name );
    if ( solvent_mspec == NULL ) {
      FATAL("No molecules in the system matches %s.\n",solvent_name);
      exit(1);
    }
    if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
      FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
      exit(1);
    }
  } else {
    FATAL("Must supply solvent_name in the configuration file.\n");
    exit(1);
  }

  int i;

  i = iniparser_getint(dict, "h_bond:donor_atom_spec", -1 );
  if ( i < 0 ) {
    FATAL("Must supply the donor_atom_spec in the configuration file.\n");
    exit(1);
  } else {
    if ( i >= 0 && i < solvent_mspec->atom_specs->count ) { 
      donor_aspec = m_mspec_aspec(solvent_mspec, i);
    } else {
      FATAL("Solvent molecule only has %d atom_specs, the specified atom_spec (id=%d) does not exist.\n",solvent_mspec->atom_specs->count,i);
      exit(1);
    }
  }

  i = iniparser_getint(dict, "h_bond:acceptor_atom_spec", -1 );
  if ( i < 0 ) {
    FATAL("Must supply the acceptor_atom_spec in the configuration file.\n");
    exit(1);
  } else {
    if ( i >= 0 && i < solvent_mspec->atom_specs->count ) { 
      acceptor_aspec = m_mspec_aspec(solvent_mspec, i);
    } else {
      FATAL("Solvent molecule only has %d atom_specs, the specified atom_spec (id=%d) does not exist.\n",solvent_mspec->atom_specs->count,i);
      exit(1);
    }
  }

  min_OHO = iniparser_getdouble(dict, "h_bond:min_OHO", 0.0 );
  if ( min_OHO < 0 || min_OHO > 180 ) {
    FATAL("Must supply a valid min_OHO for h_bond.\n");
    exit(1);
  }
  max_cos_OHO = cos( min_OHO * pi / 180.0 );

  max_OHO = iniparser_getdouble(dict, "h_bond:max_OHO", 180.0 );
  if ( max_OHO <= min_OHO || max_OHO > 180 ) {
    FATAL("Must supply a valid max_OHO (%f) greater than min_OHO (%f).\n",max_OHO,min_OHO);
    exit(1);
  }
  min_cos_OHO = cos( max_OHO * pi / 180.0 );

  min_HOO = iniparser_getdouble(dict, "h_bond:min_HOO", 0.0 );
  if ( min_HOO > 180 || min_HOO < 0 ) {
    FATAL("Must supply a valid min_HOO for h_bond.\n");
    exit(1);
  }
  max_cos_HOO = cos( min_HOO * pi / 180.0 );

  max_HOO = iniparser_getdouble(dict, "h_bond:max_HOO", 180.0 );
  if ( max_HOO <= min_HOO || max_HOO > 180 ) {
    FATAL("Must supply a max_HOO (%f) greater than min_HOO (%f).\n",max_HOO,min_HOO);
    exit(1);
  }
  min_cos_HOO = cos( max_HOO * pi / 180.0 );
  
  min_R_OO = iniparser_getdouble(dict, "h_bond:min_R_OO", 0.0 );
  max_R_OO = iniparser_getdouble(dict, "h_bond:max_R_OO", -1 );
  if ( max_R_OO < 0 || max_R_OO<min_R_OO ) {
    FATAL("Must supply a valid max_R_OO for h_bond.\n");
    exit(1);
  }
  min_R_HO = iniparser_getdouble(dict, "h_bond:min_R_HO", 0.0 );
  max_R_HO = iniparser_getdouble(dict, "h_bond:max_R_HO", -1 );
  if ( max_R_HO < 0 || max_R_HO<min_R_HO ) {
    FATAL("Must supply a valid max_R_HO for h_bond.\n");
    exit(1);
  }

  char *in;

  in = iniparser_getstring(dict, "h_bond:sv_bulk_out", NULL );
  if ( in != NULL ) {
    sv_bulk_out = fopen(in, "w");
  } else {
    sv_bulk_out = stdout;
  }

  in = iniparser_getstring(dict, "h_bond:sv_sol_out", NULL );
  if ( in != NULL ) {
    sv_sol_out = fopen(in, "w");
  } else {
    sv_sol_out = stdout;
  }

  in = iniparser_getstring(dict, "h_bond:solute_out", NULL );
  if ( in != NULL ) {
    solute_out = fopen(in, "w");
  } else {
    solute_out = stdout;
  }

  in = iniparser_getstring(dict, "h_bond:average_out", NULL );
  if ( in != NULL ) {
    average_out = fopen(in, "w");
  } else {
    average_out = stdout;
  }


  h_bonds = malloc(sizeof(M_neighbours *) * solvent_mspec->count);
}

void widom_frame_init(M_system* sys) {

  calc_centers(sys);

  int i,j;
  M_molec *molec_i, *molec_j;

  // clear nearest neighbour list
  for (i=0; i<solvent_mspec->count; i++) {
    h_bonds[i] = m_nearest_neighbours_new( 0, 0.0 );
  }

  // populate nearest neighbour
  for( i=0; i<solvent_mspec->count-1; i++) {
    molec_i = solvent_mspec->molecs->data[i];

    for( j=i+1; j<solvent_mspec->count; j++) {
      molec_j = solvent_mspec->molecs->data[j];

      // r_OO = r_i --> r_j (unit vector)
      M_vector r_OO = m_radius(molec_j->center, molec_i->center);
      prec r = m_vector_mag(r_OO);
      r_OO = m_vector_scale(r_OO,1.0/r);

      if ( r<max_R_OO && r>min_R_OO ) {

        int ii, jj, kk;
        M_atom *acceptor_i, *donor_i, *acceptor_j, *donor_j;
        prec cosOHO, cosHOO;

        // molec_i donating to molec_j
        // select O from molecule i
        for ( ii=0; ii<molec_i->atoms->count; ii++ ) {
          acceptor_i = m_molec_atom(molec_i, ii);
          if ( acceptor_i->spec == acceptor_aspec ) {

            // select H from molecule i
            for ( jj=0; jj<molec_i->atoms->count; jj++ ) {
              donor_i = m_molec_atom(molec_i, jj);
              if ( donor_i->spec == donor_aspec && acceptor_i != donor_i ) {
                
                // HO_i = H_i --> O_i
                M_vector HO_i = m_vector_unit(m_radius(acceptor_i->pos, donor_i->pos));

                for ( kk=0; kk<molec_j->atoms->count; kk++ ) {
                  acceptor_j = m_molec_atom(molec_j, kk);
                  if ( acceptor_j->spec == acceptor_aspec ) {

                    // HO_j = H_i --> O_j
                    M_vector HO_j = m_radius(acceptor_j->pos, donor_i->pos);
                    prec HO_r = m_vector_mag(HO_j);
                    HO_j = m_vector_scale(HO_j, 1.0/HO_r);
                    //M_vector HO_j = m_vector_unit(m_radius(acceptor_j->pos, donor_i->pos));
                  
                    // (H_i --> O_i) . (H_i --> O_j)
                    cosOHO = m_vector_dot(HO_i, HO_j);

                    // (O_i --> H_i) . (O_i --> O_j)
                    cosHOO = -1.0*m_vector_dot(HO_i, r_OO);

                    if ( cosHOO < max_cos_HOO && cosHOO > min_cos_HOO && cosOHO < max_cos_OHO && cosOHO > min_cos_OHO && HO_r < max_R_HO && HO_r > min_R_HO ) {
                      m_nearest_neighbours_append( h_bonds[i], molec_j);
                      m_nearest_neighbours_append( h_bonds[j], molec_i);
                    }
                  }
                }
              }
            }
          }
        }
          
        // molec_j donating to molec_i
        // select O from molecule j
        for ( ii=0; ii<molec_j->atoms->count; ii++ ) {
          acceptor_j = m_molec_atom(molec_j, ii);
          if ( acceptor_j->spec == acceptor_aspec ) {

            // select H from molecule j
            for ( jj=0; jj<molec_j->atoms->count; jj++ ) {
              donor_j = m_molec_atom(molec_j, jj);
              if ( donor_j->spec == donor_aspec ) {
                
                // HO_j = H_j --> O_j
                M_vector HO_j = m_vector_unit(m_radius(acceptor_j->pos, donor_j->pos));

                for ( kk=0; kk<molec_i->atoms->count; kk++ ) {
                  acceptor_i = m_molec_atom(molec_i, kk);
                  if ( acceptor_i->spec == acceptor_aspec ) {

                    // HO_i = H_j --> O_i
                    M_vector HO_i = m_radius(acceptor_i->pos, donor_j->pos);
                    prec HO_r = m_vector_mag(HO_i);
                    HO_i = m_vector_scale(HO_i, 1.0/HO_r);
                  
                    // (H_j --> O_j) . (H_j --> O_i)
                    cosOHO = m_vector_dot(HO_j, HO_i);

                    // (O_j --> H_j) . (O_j --> O_i)
                    cosHOO = m_vector_dot(HO_j, r_OO);

                    if ( cosHOO < max_cos_HOO && cosHOO > min_cos_HOO && cosOHO < max_cos_OHO && cosOHO > min_cos_OHO && HO_r < max_R_HO && HO_r > min_R_HO ) {
                      m_nearest_neighbours_append( h_bonds[j], molec_i);
                      m_nearest_neighbours_append( h_bonds[i], molec_j);
                    }
                  }
                }
              }
            }
          }
        }

        
      }
    }
  }

  frame_n = 0;
  frame_n2 = 0;
  // save the number of hydrogen bonds into molec->dummy
  for( j=0; j<solvent_mspec->count; j++) {
    molec_j = solvent_mspec->molecs->data[j];
    molec_j->dummy = h_bonds[j]->molecs->count;
    frame_n += molec_j->dummy;
    frame_n2 += pow(molec_j->dummy,2.0);
  }
  frame_n /= solvent_mspec->count;
  frame_n2 /= solvent_mspec->count;
  total_bulk_n += frame_n;
  total_bulk_n2 += frame_n2;

  // free the nearest neighbour list
  for (i=0; i<solvent_mspec->count; i++) {
    m_nearest_neighbours_free(h_bonds[i]);
  }

  m_n_aggregate_reset(frame_sv_bulk);
  prec r;
  for ( i=0; i<solvent_mspec->count-1; i++) {
    molec_i = solvent_mspec->molecs->data[i];
    for ( j=i+1; j<solvent_mspec->count; j++) {
      molec_j = solvent_mspec->molecs->data[j];

        r = m_vector_mag(m_radius(molec_i->center,molec_j->center));
        chk_extend(r);

        frame_sv_bulk->key[0]=r;
        frame_sv_bulk->data[0]=molec_i->dummy;

        _m_n_aggregate(frame_sv_bulk);

        frame_sv_bulk->key[0]=r;
        frame_sv_bulk->data[0]=molec_j->dummy;

        _m_n_aggregate(frame_sv_bulk);
    }
  }
}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
  m_n_aggregate_reset(insert_solute);
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {

  if ( atom->molec->spec == solvent_mspec && atom->id_in_molec == solvent_mspec->center ) {
    prec r = sqrt(r2);

    chk_extend(r);

    insert_solute->key[0] = r;
    insert_solute->data[0] = atom->molec->dummy;
    _m_n_aggregate(insert_solute);
  }

}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {

  int i;
  int tl = insert_solute->total_length;
  for ( i=0; i<insert_solute->total_length; i++ ) {
    if ( insert_solute->count[i] > 0 ) {
      total_solute->values[i] += insert_solute->values[i]*dV*pr/insert_solute->count[i];
      total_solute->values[i+tl] += dV*pr;
    }
  }

}

void widom_frame_end(M_system* sys, prec sum_pr ) {

  int i;
  for ( i=0; i<frame_sv_bulk->total_length; i++ ) {
    if ( frame_sv_bulk->count[i] > 0 ) { 
      total_sv_bulk->values[i] += frame_sv_bulk->values[i]/frame_sv_bulk->count[i];
      total_sv_bulk->count[i] += 1;
      total_sv_sol->values[i] += frame_sv_bulk->values[i]*sum_pr/frame_sv_bulk->count[i];
    }
  }

  total_sol_n += frame_n * sum_pr;

}

void widom_finalise(M_system* sys) {

  int i;
  int tl = total_solute->total_length;
  for ( i=0; i<total_sv_bulk->total_length; i++ ) {
    if ( total_sv_bulk->count[i] > 0 ) {
      total_sv_bulk->values[i] /= total_sv_bulk->count[i];
    }
    total_sv_sol->values[i] /= total_sum_pr;
    total_solute->values[i] /= total_solute->values[i+tl];
  }

  total_bulk_n /= sys->frames_processed;
  total_bulk_n2 /= sys->frames_processed;
  total_sol_n /= total_sum_pr;

  fprintf(average_out,"%lf\t%lf\t%lf\n",total_bulk_n,sqrt(total_bulk_n2-pow(total_bulk_n,2.0)),total_sol_n);
  
  m_n_aggregate_fprint(sv_bulk_out,total_sv_bulk);
  m_n_aggregate_fprint(sv_sol_out,total_sv_sol);
  m_n_aggregate_fprint(solute_out,total_solute);
  
  m_n_aggregate_free(total_sv_bulk);
  m_n_aggregate_free(frame_sv_bulk);
  m_n_aggregate_free(total_solute);
  m_n_aggregate_free(total_sv_sol);
  m_n_aggregate_free(insert_solute);
  


  fclose(average_out);
  fclose(sv_bulk_out);
  fclose(sv_sol_out);
  fclose(solute_out);

  free( h_bonds );

}
