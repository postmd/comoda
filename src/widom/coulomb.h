#ifndef COULOMB_H
#define COULOMB_H

#include "comoda.h"
#include "widom.h"
#include "complex.h"

typedef struct _ewald {
    prec    U_coulomb;
    prec    U_real;
    prec    U_recip;
    prec    U_self;
    prec    eta;
    prec    k_nyquist;
    int     kmax[3];
    M_uint  npts[3];
    M_uint  n_k; // = npts[0]*npts[1]*npts[2]
    prec       *greenfn;
    M_complex  *rhohat;
} M_ewald;

M_ewald* m_ewald_new(prec eta, prec L, M_uint n_x, M_uint n_y, M_uint n_z);
void m_ewald_free(M_ewald *ewald);
M_ewald* m_ewald_copy(M_ewald *in);

M_ewald* calc_ewald_frame(M_system *sys, prec eta, M_uint n_x, M_uint n_y, M_uint n_z);
prec calc_ewald_dU(M_system *sys, M_point_probe *probe, M_ewald *ewald_in);

prec eta;
M_uint n_x, n_y, n_z;

void read_ewald(dictionary *dict);

#endif
