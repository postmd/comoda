#include "widom.h"

char *description \
 = "frame.e_field_pair\n\
    \n\
      Calculates the e_field distribution as a function R from solute using pairwise\n\
      addition of Coulombic forces.\n\
      This method is inaccurate due to long-range correlation of Coulombic forces.\n\
      The Ewald/Particle-Mesh method needs to be implemented to properly handle charge\n\
      interactions.\n\
    \n\
    Configuration file fortmat:\n\
    ===========================\n\
      [system]\n\
      setT            = 0.000944  ; temperature in atomic units\n\
      [e_field]\n\
      dE              = 0.01      ; the bin size for aggregating |E|\n\
      mirrors         = 1         ; the number of mirror images to calculate E over\n\
      [insert]\n\
      ; flags for what kind of calculation to perform\n\
      calc_dMu        = false     ; whether or not to calculate the free energy of\n\
                                    ; particle insertion \n\
      print_coords    = true      ; whether or not to print coordinates of insertion\n\
      print_sort      = true      ; whether or not to sort the insertion coordinates\n\
                                    ; before printing. Default = true.\n\
      print_excluded  = true      ; whether or not to print the insertion coordinates\n\
                                    ; which are within exclusion_r. Default = false.\n\
      print_range     = 1,100     ; the range of insertion coordinates to print\n\
                                    ; note that the printed coordinates are ranked\n\
                                    ; from the lowest DeltaE (most probable) to\n\
                                    ; the greatest DeltaE (least probable)\n\
      coords_file     = insert_coords\n\
                                  ; print the coordinates into this file\n\
                                    ; if unspecified, prints to stdout\n\
                                    ; see note 1 for column format\n\
      dMu_file        = insert_dMu\n\
                                  ; print the insertion energies into this file\n\
                                    ; if unspecified, prints to stdout\n\
      only_nearest    = true      ; only sort the insertion coordinates by their\n\
                                    ; nearest neighbour separation\n\
      ngrid           = 10,10,10  ; number of grid points in the x,y,z direction\n\
      ; interaction potential parameters\n\
      ; comma-separated list, the length of the list must equal the number\n\
      ; of atomic species, the order of potentials must be given in the same\n\
      ; order as the declaration above\n\
      ;\n\
      ; U_j(r) = Q/r + (smallsig(j)/r)^12 + B(j)*exp(-abm(j)*r) - C6(j)/r^6 - C8(j)/r^8\n\
      ;\n\
      ; all interaction parameters are in atomic units\n\
      Q               = 1.0,-1.0\n\
      smallsig        = 1.0,1.0\n\
      B               = 0.0,0.0\n\
      abm             = 0.0,0.0\n\
      C6              = 0.0,0.0\n\
      C8              = 0.0,0.0\n\
      ; optimization settings \n\
      exclusion_r     = 3.0,4.0   ; radius to exclude from computing U \n\
      cell_ngrid      = 10,10,10  ; divide the box into small cells, for fewer\n\
                                    ; wasted pairs\n\
    ===========================\n\
    \n\
    notes:\n\
      1. column format for coords_file:\n\
          frame_id-grid_id    x/bohr    y/bohr    z/bohr    value (calc_dMu=exp(-beta dU), nearest_only=r_min^2)\n\
";


M_aggregate *total_E;
M_aggregate *frame_E;

int mirrors;
prec mirror_rcut2;

void widom_print_example() {
}

// add the E_field exerted by j onto i
void add_E_on_sites( M_atom *atom_ii, M_atom *atom_jj) {

  M_atom_spec *aspec_j;
  aspec_j = atom_jj -> spec;

  // if charge of j is non-zero
  if ( aspec_j->charge != 0 ) {
    // dr = r_ij = r_j - r_i (from r_i to r_j)
    M_vector dr = m_disp(atom_jj->pos, atom_ii->pos);
    M_vector m_dr;
    M_vector dr_mirror;
    prec dr2;
    M_vector E_i_hat;

    int m_i, m_j, m_k;
    for (m_i = -mirrors; m_i<=mirrors; m_i++ ) {
      dr_mirror.x = m_i*atom_ii->pos.dims->x;
    for (m_j = -mirrors; m_j<=mirrors; m_j++ ) {
      dr_mirror.y = m_j*atom_ii->pos.dims->y;
    for (m_k = -mirrors; m_k<=mirrors; m_k++ ) {
      dr_mirror.z = m_k*atom_ii->pos.dims->z;

      if (m_i == 0 && m_j == 0 && m_k == 0 && atom_ii->molec->id == atom_jj->molec->id) {
        // atoms in the same molecule don't exert E field on each other
        // doesn't apply when we are working with mirror images
      } else {

        m_dr = m_vector_add(dr, dr_mirror);
        dr2 = m_vector_mag2(m_dr);

        if ( dr2 < mirror_rcut2 ) {
          // E_i_hat = \hat{r}_ij / |r_ij|^2
          E_i_hat = m_vector_scale( m_dr, pow(dr2, -1.5) );

          // field exerted by j onto i: q_j E_i_hat
          atom_ii->force = m_vector_add(atom_ii->force, m_vector_scale( E_i_hat, aspec_j->charge ));
        }
      }
    }
    }
    }
  }
}

void widom_init(M_system* sys, dictionary* dict) {

  prec dE;

  dE = iniparser_getdouble(dict, "e_field:dE", 0.01);
  mirrors = iniparser_getint(dict, "e_field:mirrors", 0);

  // we keep 3 columns:
  //    { |E|, pr(|E|,bulk) (species 1, species 2...),
  //           pr(|E|,solute) (species 1, species 2..., solute) } + count
  total_E = m_aggregate_new(-dE,dE,dE, 1 + 2*nspecies + 1);
  frame_E = m_aggregate_new(-dE,dE,dE, 1 + nspecies + 1);

}

void widom_frame_init(M_system* sys) {

  // we will store the E-field in the "force" member
  //  clear the "force" member of each atom
  M_atom *this_atom;
  int i;
  for ( i=0; i<sys->atoms->count; i++ ) {
    this_atom = m_sys_atom(sys,i);

    this_atom->force.x = 0;
    this_atom->force.y = 0;
    this_atom->force.z = 0;

  }
  m_aggregate_reset(frame_E);

  mirror_rcut2 = pow(sys->rcut*(2*mirrors + 1),2.0);

  M_atom *atom_ii, *atom_jj;
  int ii,jj;
  for ( ii=0; ii<sys->atoms->count; ii++ ) {
    atom_ii = m_sys_atom(sys, ii);

    for ( jj=0; jj<sys->atoms->count; jj++ ) {
      atom_jj = m_sys_atom(sys, jj);
      
      add_E_on_sites(atom_ii, atom_jj);

    }
  }

  // now, every atom's ``force'' member is the electric field on that site

  prec E_mag;
  M_atom_spec *aspec_i;
  // loop over each species of atom
  for ( i=0; i<nspecies; i++ ) {
    aspec_i = m_sys_atom_spec(sys, i);

    // loop over each atom
    for ( ii=0; ii<aspec_i->sys_total; ii++ ) {
      // bin up |E| experienced by each atom
      atom_ii = m_spec_atom(aspec_i, ii);

      E_mag = m_vector_mag(atom_ii->force);
      if ( i==0 ) { 
        fprintf(stderr,"%d:\t %e\t%e\t%e\t%e\n",ii,atom_ii->force.x,atom_ii->force.y,atom_ii->force.z,E_mag);
      }


      if ( E_mag < frame_E->min || E_mag > frame_E->max ) {
        frame_E = m_aggregate_extend( frame_E, fmin(frame_E->min, E_mag), fmax(frame_E->max, E_mag) );
        total_E = m_aggregate_extend( total_E, fmin(frame_E->min, E_mag), fmax(frame_E->max, E_mag) );
      }

      frame_E->row[0] = E_mag;
      frame_E->row[i+1] = 1.0;

      _m_aggregate(frame_E);
    }

    // divide by the number of atoms in this species. this is now a probability distribution
    m_aggregate_col_scale(frame_E, i+1, (prec) 1.0/aspec_i->sys_total);

  }

}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {
}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {
}

void widom_frame_end(M_system* sys, prec sum_pr) {
  
  int i;
  prec E_mag;
  for ( i=0; i<frame_E->length; i++ ) {
    if ( frame_E->count[i] > 0 ) {
      E_mag = frame_E->values[i][0];
      
      total_E->row[0] = E_mag;

      int j;
      // pr(E) of solvent frame
      for ( j=0; j<nspecies; j++) {
        total_E->row[j+1] = frame_E->values[i][j+1];
      }

      // pr(E) of solvent species in presence of solute
      for ( j=0; j<nspecies; j++) {
        total_E->row[j+1+nspecies] = frame_E->values[i][j+1] * sum_pr;
      }

      // pr(E) of the solute
      total_E->row[2*nspecies+1] = frame_E->values[i][nspecies+1];

      _m_aggregate(total_E);
    }

  }

  //m_aggregate_print(frame_E);

}

void widom_finalise(M_system* sys) {

  int i;
  // scale the solvent pr(E) by the number of frames
  for ( i=0; i<nspecies; i++ ) {
    m_aggregate_col_scale(total_E, i+1, (prec) 1.0/sys->frames_processed);
  }

  // scale the solvent pr(E) in presence of solute by total_sum_pr
  for ( i=0; i<nspecies; i++ ) {
    m_aggregate_col_scale(total_E, i+1+nspecies, (prec) 1.0/total_sum_pr );
  }
  // scale the solute pr(E) by total_sum_pr
  m_aggregate_col_scale(total_E, 2*nspecies+1, (prec) 1.0/total_sum_pr );

  m_aggregate_print(total_E);
}
