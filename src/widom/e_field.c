#include "widom.h"

char *description \
 = "frame.e_field\n\
    \n\
      Calculates the distribution of E-field as a function of R from solute using the force\n\
      output from toyMD.\n\
    \n\
    Configuration file format:\n\
    ===========================\n\
      [system]\n\
      setT            = 0.000944  ; temperature in atomic units\n\
      solvent_name    = water     ; the name of the solvent molecule species\n\
      [e_field]\n\
      dE              = 0.01      ; the bin size for aggregating |E|\n\
      forces          = spce512-300.00.20.f ; the file containing toyMD force outputs\n\
      sv_bulk_out     = bulk.e_field      ; the output file for the bulk solvent\n\
      sv_sol_out      = solution.e_field  ; the output file for the solvent in the solution\n\
      solute_out      = Ar.e_field        ; the output file for the solute\n\
      [insert]\n\
      ; flags for what kind of calculation to perform\n\
      calc_dMu        = false     ; whether or not to calculate the free energy of\n\
                                    ; particle insertion \n\
      print_coords    = true      ; whether or not to print coordinates of insertion\n\
      print_sort      = true      ; whether or not to sort the insertion coordinates\n\
                                    ; before printing. Default = true.\n\
      print_excluded  = true      ; whether or not to print the insertion coordinates\n\
                                    ; which are within exclusion_r. Default = false.\n\
      print_range     = 1,100     ; the range of insertion coordinates to print\n\
                                    ; note that the printed coordinates are ranked\n\
                                    ; from the lowest DeltaE (most probable) to\n\
                                    ; the greatest DeltaE (least probable)\n\
      coords_file     = insert_coords\n\
                                  ; print the coordinates into this file\n\
                                    ; if unspecified, prints to stdout\n\
                                    ; see note 1 for column format\n\
      dMu_file        = insert_dMu\n\
                                  ; print the insertion energies into this file\n\
                                    ; if unspecified, prints to stdout\n\
      only_nearest    = true      ; only sort the insertion coordinates by their\n\
                                    ; nearest neighbour separation\n\
      ngrid           = 10,10,10  ; number of grid points in the x,y,z direction\n\
      ; interaction potential parameters\n\
      ; comma-separated list, the length of the list must equal the number\n\
      ; of atomic species, the order of potentials must be given in the same\n\
      ; order as the declaration above\n\
      ;\n\
      ; U_j(r) = Q/r + (smallsig(j)/r)^12 + B(j)*exp(-abm(j)*r) - C6(j)/r^6 - C8(j)/r^8\n\
      ;\n\
      ; all interaction parameters are in atomic units\n\
      Q               = 1.0,-1.0\n\
      smallsig        = 1.0,1.0\n\
      B               = 0.0,0.0\n\
      abm             = 0.0,0.0\n\
      C6              = 0.0,0.0\n\
      C8              = 0.0,0.0\n\
      ; optimization settings \n\
      exclusion_r     = 3.0,4.0   ; radius to exclude from computing U \n\
      cell_ngrid      = 10,10,10  ; divide the box into small cells, for fewer\n\
                                    ; wasted pairs\n\
    ===========================\n\
    \n\
    notes:\n\
      1. column format for coords_file:\n\
          frame_id-grid_id    x/bohr    y/bohr    z/bohr    value (calc_dMu=exp(-beta dU), nearest_only=r_min^2)\n\
      2. must specify LJ interaction potentials (smallsig, C6) in each atomic species specification, using a csv\n\
          [molecule#atom]\n\
            ...\n\
            smallsig = 1.0,2.0,3.0,....\n\
            C6 = 1.0,2.0,3.0,....\n\
      3. charges of atomic species must be specified\n\
";

M_n_aggregate *total_sv_bulk, *frame_sv_bulk;
M_n_aggregate *total_sv_sol;
M_n_aggregate *total_solute, *insert_solute;

M_molec_spec *solvent_mspec;

FILE *sv_bulk_out, *sv_sol_out, *solute_out;

FILE* forces = NULL;
int lines_read;
int forces_frame;

void widom_print_example() {
}

void subtract_LJ_f( M_atom *atom_i, M_atom *atom_j ) {
  M_atom_spec *aspec_i = atom_i->spec;
  M_atom_spec *aspec_j = atom_j->spec;

  // dr = r_ij = r_j - r_i  ( from i to j )
  M_vector dr = m_disp( atom_j->pos, atom_i->pos );

  prec r2 = m_vector_mag2(dr);

  M_vector f;
  prec r_r2, r_r8, r_r14;
  prec forcefactor;

  if ( r2 < rcut2 ) {
    
    // U(r_ij) = smallsig^12/r^12 - C6/r^6
    //
    // the force exerted by i on j is
    // f(r_ij) = - grad U
    //         = - dU/dr (grad r_ij)
    //         = [ ( 12 smallsig^12 / r^13 ) - ( 6 C6 / r^7 ) ] \hat{r}_ij
    //         = [ ( 12 smallsig^12 / r^14 ) - ( 6 C6 / r^8 ) ] r_ij

    r_r2 = 1.0/r2;
    r_r8 = pow(r_r2,4.0);
    r_r14 = r_r8*r_r8*r2;

    forcefactor = 0;
    forcefactor += 12.0*pow(aspec_i->smallsig[aspec_j->id],12.0)*r_r14;
    forcefactor += -6.0*aspec_i->C6[aspec_j->id]*r_r8;

    f = m_vector_scale( dr, forcefactor ); 

    atom_j->force = m_vector_subtract( atom_j->force, f );
    atom_i->force = m_vector_add( atom_i->force, f );

  }
}

void chk_extend( prec r, prec E ) {

    if ( r > total_sv_bulk->max[0] || E < total_sv_bulk->min[1] || E > total_sv_bulk->max[1] ) {
      total_sv_bulk->key[0] = r;
      total_sv_bulk->key[1] = E;

      total_sv_sol->key[0] = r;
      total_sv_sol->key[1] = E;

      total_solute->key[0] = r;
      total_solute->key[1] = E;

      frame_sv_bulk->key[0] = r;
      frame_sv_bulk->key[1] = E;

      insert_solute->key[0] = r;
      insert_solute->key[1] = E;

      total_sv_bulk = m_n_aggregate_extend(total_sv_bulk);
      total_sv_sol  = m_n_aggregate_extend(total_sv_sol);
      total_solute  = m_n_aggregate_extend(total_solute);
      frame_sv_bulk = m_n_aggregate_extend(frame_sv_bulk);
      insert_solute = m_n_aggregate_extend(insert_solute);
    }

}

void widom_init(M_system* sys, dictionary* dict) {

  prec *width = malloc(sizeof(prec) * 2);
  prec *min = malloc(sizeof(prec) * 2);
  prec *max = malloc(sizeof(prec) * 2);

  min[0] = 0.0;
  max[0] = sys->rcut;
  width[0] = iniparser_getdouble(dict, "rdf:dr", 0.01);

  min[1] = 0.0;
  max[1] = 0.0;
  width[1] = iniparser_getdouble(dict, "e_field:dE", 0.01);

  total_sv_bulk = m_n_aggregate_new(2, min, max, width, 1);
  total_sv_sol  = m_n_aggregate_new(2, min, max, width, 1);
  total_solute  = m_n_aggregate_new(2, min, max, width, 1);
  frame_sv_bulk = m_n_aggregate_new(2, min, max, width, 0);
  insert_solute = m_n_aggregate_new(2, min, max, width, 0);

  free(min);
  free(max);
  free(width);

  total_sv_bulk->print_2d_values = 1;
  total_sv_sol->print_2d_values  = 1;
  total_solute->print_2d_values  = 1;

  char force_file[256];

  char *in;
  in = iniparser_getstring(dict, "e_field:forces", NULL );
  if ( in != NULL ) {
    strcpy(force_file, in);
    forces = fopen(force_file, "r");
    lines_read = 0;
    forces_frame = 0;
  } else {
    FATAL("must supply a filename for the forces!\n");
    exit(1);
  }

  char *solvent_name;

  solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
  if ( solvent_name != NULL ) { 
    solvent_mspec = m_molec_spec_find( sys, solvent_name );
    if ( solvent_mspec == NULL ) { 
      FATAL("No molecules in the system matches %s.\n",solvent_name);
      exit(1);
    }   
    if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
      FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
      exit(1);
    }
  } else {
    FATAL("Must supply solvent_name in the configuration file.\n");
    exit(1);
  }

  in = iniparser_getstring(dict, "e_field:sv_bulk_out", NULL );
  if ( in != NULL ) {
    sv_bulk_out = fopen(in, "w");
  } else {
    sv_bulk_out = stdout;
  }

  in = iniparser_getstring(dict, "e_field:sv_sol_out", NULL );
  if ( in != NULL ) {
    sv_sol_out = fopen(in, "w");
  } else {
    sv_sol_out = stdout;
  }

  in = iniparser_getstring(dict, "e_field:solute_out", NULL );
  if ( in != NULL ) {
    solute_out = fopen(in, "w");
  } else {
    solute_out = stdout;
  }

}

void widom_frame_init(M_system* sys) {

  calc_centers(sys);

  int i;
  prec f_x, f_y, f_z;
  int id;

  // skip to the current frame
  while ( forces_frame < sys->current_frame_id ) {
    for ( i=0; i<sys->atoms->count; i++ ) {
      if ( fscanf(forces,"f\t%d\t%lf\t%lf\t%lf\n",&id,&f_x,&f_y,&f_z)==4 ) {
        lines_read ++;
      } else {
        FATAL("Reading the forces file failed.\n");
        exit(1);
      }
    }
    forces_frame ++;
  }

  // we will store the E-field in the "force" member
  //  clear the "force" member of each atom
  M_atom *this_atom;
  for ( i=0; i<sys->atoms->count; i++ ) {
    
    if ( fscanf(forces,"f\t%d\t%lf\t%lf\t%lf\n",&id,&f_x,&f_y,&f_z)==4 ) {
      lines_read ++;
    } else {
      FATAL("Reading the forces file failed.\n");
      exit(1);
    }

    this_atom = m_sys_atom(sys,i);

    if ( i+1 == id ) {
      this_atom->force.x = f_x;
      this_atom->force.y = f_y;
      this_atom->force.z = f_z;
    } else {
      FATAL("ID mismatch when reading forces. Expected: %d, read: %d.\n",i+1,id);
      exit(1);
    }

  }
  forces_frame++;

  // first, subtract the LJ forces
  M_atom *atom_ii, *atom_jj;
  int ii,jj;
  for ( ii=0; ii<sys->atoms->count; ii++ ) {
    atom_ii = m_sys_atom(sys, ii);

    for ( jj=ii+1; jj<sys->atoms->count; jj++ ) {
      atom_jj = m_sys_atom(sys, jj);

      subtract_LJ_f( atom_ii, atom_jj );
      
    }
  }

  M_atom_spec *aspec_i;
  // then, obtain the electric field by \vec{E} = \vec{F_coulomb}/q
  // note that this only works for species with a charge
  for ( ii=0; ii<sys->atoms->count; ii++ ) {
    atom_ii = m_sys_atom(sys, ii);
    aspec_i = atom_ii->spec;

    if ( aspec_i->charge == 0 ) {
      // then this method fails, as \vec{F_coulomb} = \vec{E} q
      //  so if q=0, then the information about \vec{E} is lost
      atom_ii->force.x = 0;
      atom_ii->force.y = 0;
      atom_ii->force.z = 0;
    } else {
      atom_ii->force.x /= aspec_i->charge;
      atom_ii->force.y /= aspec_i->charge;
      atom_ii->force.z /= aspec_i->charge;
    }
  }

  // now, every atom's ``force'' member is the electric field on that site

  prec E_mag;
  M_molec *molec_i, *molec_j;
  M_atom *atom_i, *atom_j;
  // loop over each solvent molecule to save the E_mag into the dummy field
  for ( i=0; i<solvent_mspec->count; i++ ) {
    molec_i = solvent_mspec->molecs->data[i];

    atom_i = m_molec_atom(molec_i,solvent_mspec->center);

    E_mag = m_vector_mag(atom_i->force);

    molec_i->dummy = E_mag;
  }


  m_n_aggregate_reset(frame_sv_bulk);

  // loop over each pair of solvent molecules to accumulate the distribution of p(r,E)
  int j;
  prec r;
  for ( i=0; i<solvent_mspec->count-1; i++) {
    molec_i = solvent_mspec->molecs->data[i];
    for ( j=i+1; j<solvent_mspec->count; j++) {
      molec_j = solvent_mspec->molecs->data[j];

        r = m_vector_mag(m_radius(molec_i->center,molec_j->center));
        chk_extend(r,molec_i->dummy);
        chk_extend(r,molec_j->dummy);

        frame_sv_bulk->key[0]=r;
        frame_sv_bulk->key[1]=molec_i->dummy;

        _m_n_aggregate(frame_sv_bulk);
        
        frame_sv_bulk->key[0]=r;
        frame_sv_bulk->key[1]=molec_j->dummy;

        _m_n_aggregate(frame_sv_bulk);
    }
  }
}

void widom_insert_init(M_system* sys, M_point_probe* probe) {
  m_n_aggregate_reset(insert_solute);
}

void widom_insert_pair(M_system* sys, M_point_probe* probe, M_atom* atom, M_vector dr, prec r2) {

  if ( atom->molec->spec == solvent_mspec && atom->id_in_molec == solvent_mspec->center ) {
    prec r = sqrt(r2);

    chk_extend(r,atom->molec->dummy);

    insert_solute->key[0] = r;
    insert_solute->key[1] = atom->molec->dummy;
    _m_n_aggregate(insert_solute);
  }

}

void widom_insert_end(M_system* sys, M_point_probe* probe, prec pr) {

  int i;
  for ( i=0; i<insert_solute->total_length; i++ ) {
    total_solute->values[i] += insert_solute->count[i]*pr*dV;
  }

}

void widom_frame_end(M_system* sys, prec sum_pr) {
  
  int i;
  for ( i=0; i<frame_sv_bulk->total_length; i++ ) {
    total_sv_bulk->values[i] += frame_sv_bulk->count[i];
    total_sv_sol->values[i] += frame_sv_bulk->count[i] * sum_pr;
  }

}

void widom_finalise(M_system* sys) {

  int i,j;
  prec norm_sv_bulk, norm_sv_sol, norm_solute;
  int l0 = frame_sv_bulk->length[0];
  int l1 = frame_sv_bulk->length[1];
  prec dE = frame_sv_bulk->width[1];
  for ( i=0; i<l0; i++ ) {
    norm_sv_bulk = sum_farray( total_sv_bulk->values+i*l1, l1 );
    norm_sv_sol = sum_farray( total_sv_sol->values+i*l1, l1 );
    norm_solute = sum_farray( total_solute->values+i*l1, l1 );
    if ( norm_sv_bulk == 0 ) {
      norm_sv_bulk = 1;
    }
    if ( norm_sv_sol == 0 ) {
      norm_sv_sol = 1;
    }
    if ( norm_solute == 0 ) {
      norm_solute = 1;
    }
    
    for ( j=0; j<l1; j++ ) {
      total_sv_bulk->values[i*l1+j] /= norm_sv_bulk*dE;
      total_sv_sol->values[i*l1+j] /= norm_sv_sol*dE;
      total_solute->values[i*l1+j] /= norm_solute*dE;
    }
  }

  m_n_aggregate_fprint(sv_bulk_out,total_sv_bulk);
  m_n_aggregate_fprint(sv_sol_out,total_sv_sol);
  m_n_aggregate_fprint(solute_out,total_solute);

  m_n_aggregate_free(total_sv_bulk);
  m_n_aggregate_free(frame_sv_bulk);
  m_n_aggregate_free(total_solute);
  m_n_aggregate_free(total_sv_sol);
  m_n_aggregate_free(insert_solute);

  fclose(sv_bulk_out);
  fclose(sv_sol_out);
  fclose(solute_out);

  fclose(forces);
}
