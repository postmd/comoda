#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;

char *description \
 = ".\n\
";
prec c101;

void print_routine_example() {

  char *example \
= "[n0n]\n\
";
  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"n0n:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;

    agg = m_n_aggregate_new(1, &min, &max, &width, 3);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows

    prec temp_vec[3];
    char *in;


    c101 = pow(3/2,0.5);

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void update_n0n( prec *data, prec cos_ij ) {

    prec phi112;
    prec alpha;

    alpha = cos_ij;

    phi112=alpha;
    data[0]=1.0;
    data[1]=phi112;
}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i, j;
    M_molec *molec_i, *molec_j;
    prec cos_i, cos_j, cos_ij, cos_full;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
		for( j=i+1; j<solvent_mspec->count; j++) {
	        	molec_j = solvent_mspec->molecs->data[j];
				
			M_vector dr_ij = m_radius(molec_j->center, molec_i->center);
			prec r_ij = m_vector_mag(dr_ij);		
	
			M_vector rhat = m_vector_unit(dr_ij);
			cos_i = -m_vector_dot( rhat, m_vector_unit(molec_i->dipole));
			cos_j = m_vector_dot( rhat, m_vector_unit(molec_j->dipole));
			cos_ij = m_vector_dot( m_vector_unit(molec_i->dipole), m_vector_unit(molec_j->dipole));		
			
			cos_full = (3*cos_i*cos_j)-cos_ij;			

			agg->key[0] = r_ij;
			if ( r_ij < agg->min[0] || r_ij > agg->max[0] ){ 
				agg = m_n_aggregate_extend(agg);
	  	      	}


	        	update_n0n(agg->data, cos_full);

	        	_m_n_aggregate(agg);


		}
	    }

}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    m_n_aggregate_col_scale(agg,0,f);
    m_n_aggregate_col_scale(agg,1,-c101*f);

    m_n_aggregate_print(agg);
}
