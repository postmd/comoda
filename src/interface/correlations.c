#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;

char *description \
 = "Calculates the correlation of orientation of dipoles near an interface.\n\
\n\
\tOutput format:\n\
\tr\t<n>\tp101\tp202\tp303\t....p909\n\
\twhere:\n\
\t  <n> is the average number of molecules found within the given slice\n\
\t  pn0n is calculated as ( Sum_{slice} Phi_n0n )/n_frames\n\
\t    Phi_n0n is a rotational invariant, e.g. Phi_101 = Sqrt[3]*cos(theta)\n\
\t    Divide pn0n by (V_{slice} * rho_{NpT}) to normalise to a correlation fn.";

prec c101, c202, c303, c404, c505, c606, c707, c808, c909;

void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
[interface]\n\
normal = 1,0,0  ; vector normal to the interface\n\
origin = 0,0,0  ; coordinatess of a point of reference";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"n0n:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;

    agg = m_n_aggregate_new(1, &min, &max, &width, 10);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows

    prec temp_vec[3];
    char *in;

    in = iniparser_getstring(dict, "interface:normal", "0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    normal.x=temp_vec[0];
    normal.y=temp_vec[1];
    normal.z=temp_vec[2];

    normal = m_vector_unit(normal);

    in = iniparser_getstring(dict, "interface:origin","0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    origin.dims = &(sys->dims);
    origin.x=temp_vec[0];
    origin.y=temp_vec[1];
    origin.z=temp_vec[2];

    c101 = pow(3,0.5);
    c202 = pow(5,0.5)/2.0;
    c303 = pow(7,0.5)/2.0;
    c404 = 3.0/8.0;
    c505 = pow(11,0.5)/8.0;
    c606 = pow(13,0.5)/16.0;
    c707 = pow(15,0.5)/16.0;
    c808 = pow(17,0.5)/128.0;
    c909 = pow(19,0.5)/128.0;

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void update_n0n( prec *data, prec cos_i ) {

    prec phi101, phi202, phi303, phi404, phi505, phi606, phi707, phi808, phi909;
    prec alpha, alpha2, alpha3, alpha4, alpha5, alpha6, alpha7, alpha8, alpha9;

    alpha = cos_i;
    alpha2=alpha*alpha;
    alpha3=alpha2*alpha;
    alpha4=alpha3*alpha;
    alpha5=alpha4*alpha;
    alpha6=alpha5*alpha;
    alpha7=alpha6*alpha;
    alpha8=alpha7*alpha;
    alpha9=alpha8*alpha;

    phi101=alpha;
    phi202=-1.0+3.0*alpha2;
    phi303=-3.0*alpha+5.0*alpha3;
    phi404=3.0-30.0*alpha2+35.0*alpha4;
    phi505=15.0*alpha-70.0*alpha3+63.0*alpha5;
    phi606=-5.0+105*alpha2-315.0*alpha4+231.0*alpha6;
    phi707=-35.0*alpha+315.0*alpha3-693.0*alpha5+429.0*alpha7;
    phi808=35.0-1260.0*alpha2+6930.0*alpha4-12012.0*alpha6+6435.0*alpha8;
    phi909=315.0*alpha-4620.0*alpha3+18018.0*alpha5-25740.0*alpha7+12155.0*alpha9;

    data[0]=1.0;
    data[1]=phi101;
    data[2]=phi202;
    data[3]=phi303;
    data[4]=phi404;
    data[5]=phi505;
    data[6]=phi606;
    data[7]=phi707;
    data[8]=phi808;
    data[9]=phi909;
}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i, j;
    M_molec *molec_i, *molec_j;
    prec cos_i, x_i;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
        for( j=i+1; i<solvent_mspec->count; i++){
            molec_j = solvent_mspeec->data[j];
            

            cos_mu1mu2 = -1.0*m_vector_dot(m_vector_unit(molec_i->dipole), m_vector_unit(molec_j->dipole));        
            M_vector r_ij = m_disp(molec_i->center, molec_j->center);
            
            M_vector r_i = m_disp(molec_i->center, origin);
            x_i = m_vector_dot(r_i, normal);
            
            
 
            agg->key[0] = x_i;
            if ( x_i < agg->min[0] || x_i > agg->max[0] ) {
                agg = m_n_aggregate_extend(agg);
             }

            update_n0n(agg->data, cos_i);

            m_n_aggregate(agg);
        }
    }

}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    m_n_aggregate_col_scale(agg,0,f);
    m_n_aggregate_col_scale(agg,1,c101*f);
    m_n_aggregate_col_scale(agg,2,c202*f);
    m_n_aggregate_col_scale(agg,3,c303*f);
    m_n_aggregate_col_scale(agg,4,c404*f);
    m_n_aggregate_col_scale(agg,5,c505*f);
    m_n_aggregate_col_scale(agg,6,c606*f);
    m_n_aggregate_col_scale(agg,7,c707*f);
    m_n_aggregate_col_scale(agg,8,c808*f);
    m_n_aggregate_col_scale(agg,9,c909*f);

    m_n_aggregate_print(agg);
}
