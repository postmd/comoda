#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;

char *description \
 = "Columns:\n\
r\t#atoms\t#anions\t#cations\tavg_charge";


void print_routine_example() {

  char *example \
= "[dist]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
[interface]\n\
normal = 1,0,0  ; vector normal to the interface\n\
origin = 0,0,0  ; coordinatess of a point of reference";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"dist:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;

    agg = m_n_aggregate_new(1, &min, &max, &width, 4);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows

    prec temp_vec[3];
    char *in;

    in = iniparser_getstring(dict, "interface:normal", "0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    normal.x=temp_vec[0];
    normal.y=temp_vec[1];
    normal.z=temp_vec[2];

    normal = m_vector_unit(normal);

    in = iniparser_getstring(dict, "interface:origin","0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    origin.dims = &(sys->dims);
    origin.x=temp_vec[0];
    origin.y=temp_vec[1];
    origin.z=temp_vec[2];

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    int i;
    prec cos_i, x_i;
    M_atom *atom_i;
    for( i=0; i<sys->atoms->count; i++) {
        atom_i = m_sys_atom(sys,i);

        M_vector r_i = m_disp(atom_i->pos,origin);
        
        x_i = m_vector_dot(r_i, normal);

        agg->key[0] = x_i;
        if ( x_i < agg->min[0] || x_i > agg->max[0] ) {
            agg = m_n_aggregate_extend(agg);
        }

        agg->data[0] = 1.0;
        if ( atom_i->spec->charge < 0 ) {
            agg->data[1] = 1.0;
        } else if ( atom_i->spec->charge > 0 ) {
            agg->data[2] = 1.0; 
        }
        agg->data[3] = atom_i->spec->charge;

        _m_n_aggregate(agg);
    }

}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    m_n_aggregate_col_scale(agg,0,f);
    m_n_aggregate_col_scale(agg,1,f);
    m_n_aggregate_col_scale(agg,2,f);
    m_n_aggregate_col_scale(agg,3,f);

    m_n_aggregate_print(agg);
}
