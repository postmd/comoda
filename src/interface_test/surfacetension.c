#include "comoda.h"
#include "frame_main.h"

prec Gamma, axis;

char *description \
 = "Calculates the surface tension across an interface";

FILE *surfacetension_out;

void print_routine_example() {

	char *example \
= " ";

	printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {
	axis = iniparser_getdouble(dict,"interface:axis",2);

}

void frame_routine(M_system* sys) {

	if ( axis == 0 ) {
		Gamma = ((sys->stress->total->zz+sys->stress->total->yy)*-0.5 + sys->stress->total->xx)*0.5*sys->dims.x;
		prec p_norm = sys->stress->total->xx;
		prec p_total = m_matrix_trace(*sys->stress->total)/3;
		prec p_electro = m_matrix_trace(*sys->stress->electro)/3;
		prec p_lj = m_matrix_trace(*sys->stress->lj)/3;
		prec p_kinetic = m_matrix_trace(*sys->stress->kinetic)/3;
		printf("%.12E\t%.12E\t%.12E\t%.12E\t%.12E\t%.12E\n",p_norm, p_total, p_electro, p_lj, p_kinetic, Gamma);
	}
	else if ( axis == 1 ) {
		Gamma = ((sys->stress->total->zz+sys->stress->total->xx)*-0.5 + sys->stress->total->yy)*0.5*sys->dims.y;
                prec p_norm = sys->stress->total->yy;
		prec p_total = m_matrix_trace(*sys->stress->total)/3;
                prec p_electro = m_matrix_trace(*sys->stress->electro)/3;
                prec p_lj = m_matrix_trace(*sys->stress->lj)/3;
                prec p_kinetic = m_matrix_trace(*sys->stress->kinetic)/3;
                printf("%.12E\t%.12E\t%.12E\t%.12E\t%.12E\t%.12E\n", p_norm, p_total, p_electro, p_lj, p_kinetic, Gamma);
	}
	else if ( axis == 2 ) {
		Gamma = ((sys->stress->total->xx+sys->stress->total->yy)*-0.5 + sys->stress->total->zz)*0.5*sys->dims.z;
                prec p_norm = sys->stress->total->zz;
		prec p_total = m_matrix_trace(*sys->stress->total)/3;
                prec p_electro = m_matrix_trace(*sys->stress->electro)/3;
                prec p_lj = m_matrix_trace(*sys->stress->lj)/3;
                prec p_kinetic = m_matrix_trace(*sys->stress->kinetic)/3;
                printf("%.12E\t%.12E\t%.12E\t%.12E\t%.12E\t%.12E\n", p_norm, p_total, p_electro, p_lj, p_kinetic, Gamma);
	}	
	else {
		FATAL("The choice of axis perpendicular to interface is not one of X, Y, or Z\n");
		exit(1);
	}	 

}

void frame_finalise(M_system* sys) {

}
