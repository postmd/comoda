#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;

char *description \
 = "Calculates the distribution of orientation of dipoles near an interface.\n\
\n\
\tOutput format:\n\
";


void print_routine_example() {

  char *example \
= "[dist]\n\
dr = 0.1;\n\
dcos = 0.1;\n\
[interface]\n\
normal = 1,0,0  ; vector normal to the interface\n\
origin = 0,0,0  ; coordinatess of a point of reference";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"dist:dr",0.1); 
    prec dcos = iniparser_getdouble(dict,"dist:dcos",0.1); 

    prec min[2];
    min[0] = 0.0;
    min[1] = -1.0;
    prec max[2];
    max[0] = sys->rcut;
    max[1] = 1.0;
    prec width[2];
    width[0] = dr;
    width[1] = dcos;

    agg = m_n_aggregate_new(2, min, max, width, 1);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows
    agg->print_2d_values = 1;
    agg->print_0_count = 1;

    prec temp_vec[3];
    char *in;

    in = iniparser_getstring(dict, "interface:normal", "0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    normal.x=temp_vec[0];
    normal.y=temp_vec[1];
    normal.z=temp_vec[2];

    normal = m_vector_unit(normal);

    in = iniparser_getstring(dict, "interface:origin","0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    origin.dims = &(sys->dims);
    origin.x=temp_vec[0];
    origin.y=temp_vec[1];
    origin.z=temp_vec[2];

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i;
    M_molec *molec_i;
    prec cos_i, x_i;
    for( i=0; i<solvent_mspec->count; i++) {
        molec_i = solvent_mspec->molecs->data[i];

        M_vector r_i = m_disp(molec_i->center, origin);
        x_i = m_vector_dot(r_i, normal);
	agg->key[0] = x_i;
        if ( x_i < agg->min[1] || x_i > agg->max[1] ) {
            agg = m_n_aggregate_extend(agg);
        }

	M_atom *oxygen = m_molec_atom(molec_i,0);
	M_atom *hydrogen_1 = m_molec_atom(molec_i,1);
	M_atom *hydrogen_2 = m_molec_atom(molec_i,2);

	M_vector dh1 = m_disp(oxygen->pos,hydrogen_1->pos);
	M_vector dh2 = m_disp(oxygen->pos,hydrogen_2->pos);
        
        cos_i = -1.0*m_vector_dot(m_vector_unit(dh1), normal);

        agg->key[0] = x_i;
	agg->key[1] = cos_i;
	agg->data[0] = 1.0;
        _m_n_aggregate(agg);

        cos_i = -1.0*m_vector_dot(m_vector_unit(dh2), normal);

        agg->key[0] = x_i;
	agg->key[1] = cos_i;
	agg->data[0] = 1.0;
        _m_n_aggregate(agg);

    }

}

void frame_finalise(M_system* sys) {

    //prec f = 1.0/sys->frames_processed;

    //m_n_aggregate_col_scale(agg,0,f);

    //int i,j;
    //for (i=0; i<agg->length[1]; i++) {
    //    int n = agg->keys_count[1][i];
    //    if ( n > 0 ) {
    //        prec n_r = 1.0/n;
    //        for (j=0; j<agg->length[0]; j++) {
    //            agg->values[j*agg->length[0]+i] *= n_r;
    //        }
    //    }
    //}

    m_n_aggregate_print(agg);
}
