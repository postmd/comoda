#include "comoda.h"
#include "frame_main.h"
// IN PROGRESS - NOT AT FUNCTIONAL POINT YET! Quinn
M_n_aggregate *agg;
M_vector M; 
M_vector normal;
M_point  origin;
prec alphaMax, dipolesum, M_sum, volume, kT;

M_molec_spec *solvent_mspec;

char *description \
 = "Columns:\n\
r\t#atoms\t#anions\t#cations\tavg_charge";


void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
[interface]\n\
normal = 1,0,0  ; vector normal to the interface\n\
origin = 0,0,0  ; coordinatess of a point of reference";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"n0n:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;

    agg = m_n_aggregate_new(1, &min, &max, &width, 3);
    
    m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
//    agg->print_empty = 1;               // print empty rows

    prec temp_vec[3];
    char *in;

    in = iniparser_getstring(dict, "interface:normal", "0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    normal.x=temp_vec[0];
    normal.y=temp_vec[1];
    normal.z=temp_vec[2];

    normal = m_vector_unit(normal);

    in = iniparser_getstring(dict, "interface:origin","0,0,0");
    parse_csv_farray(temp_vec, in, 3);
    origin.dims = &(sys->dims);
    origin.x=temp_vec[0];
    origin.y=temp_vec[1];
    origin.z=temp_vec[2];

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }
	volume = 6718 ; //for dr =1
	kT = 0.000950043; // for 298K
	M_sum = 0;	

	alphaMax = 3.8;
//    rho_sum = 0;
//    dipolemoment = 0;
//    dipolesum = 0;
}


void update_M( prec z_i, M_vector M){


//	prec M22 = m_vector_add(M22, dipole2);
//        prec M2 = m_vector_mag2( M );
//        dipolesum += dipole2;
//	prec epsi = ((1/(6718*0.000950043)))*(dipolesum-M2);

//        dipolesum = m_vector_mag2(M);

//	M_vector Pz=m_vector_scale(alpha, (1/volume));

//	prec PzMag = m_vector_mag(Pz);

	prec Msys = m_vector_mag(M);
	
	agg->key[0] = z_i;


	agg->data[0] =1.0;
	agg->data[1] = Msys;
//	agg->data[2] = Msys;
	m_n_aggregate(&agg);
}

void frame_routine(M_system* sys){

 //   prec dipolemoment_sum = 0;
    calc_dipoles(sys);
    dipolesum=0;
    M.x = 0;
    M.y = 0;
    M.z = 0;



    int i, j;
    M_molec *molec_i;
    prec z_i;
    M_vector dipole1;
    prec dipole2;
    M_atom *atom_j;


    M_point ref; 
	
    prec x_j, charge, disp, M2;
    M_vector chargeR, alpha;
   // prec rho = solvent_mspec->density;
    for( i=0; i<solvent_mspec->count; i++) {
        	molec_i = solvent_mspec->molecs->data[i];
//		for( j=0; j<94; j++){
//		ref.dims = &(sys->dims);	
//		ref.x = 42.0;
//		ref.y = 42.0;
//		ref.z = 0.0+j;


		M_vector r_i = m_disp(molec_i->center,origin);
//		prec R_i = sqrt(pow((molec_i->center.x - (origin.x+38)),2) + pow((molec_i->center.y - (origin.y+38)),2) + pow((molec_i->center.z - (dZ)),2)); 
		z_i = m_vector_dot(r_i, normal);
	

//		if ( m_vector_mag(r_i) < alphaMax ){
//			M = m_vector_add( M, molec_i->dipole1);
//			M2 = m_vector_mag2(M);
//			}
//
//		}	
//
		M = m_vector_add( M, molec_i->dipole);
		M2 = m_vector_mag2(M);
		update_M(z_i, M);



//	alpha.x = 0;
//	alpha.y = 0;
//	alpha.z = 0;


//	for( j=0; j<sys->atoms->count; j++){

//		atom_j = m_sys_atom(sys, j);
//		M_vector r_j = m_disp(atom_j->pos, origin);
//		x_j = m_vector_dot(r_j, normal);
//
//		charge = atom_j->spec->charge;
//
//		chargeR = m_vector_scale(r_j, charge );
//
//		alpha = m_vector_add(alpha, chargeR);
//
//		}


//        M_vector r_i = m_disp(molec_i->center,origin);
 //       z_i = m_vector_dot(r_i, normal);

   //     dipole1 = molec_i->dipole;
//	dipole2= m_vector_mag2(molec_i->dipole);

//	M = m_vector_add( M, dipole1);
//	M_sum += m_vector_mag(M);	
//	update_M(z_i, dipole1, M, alpha);
       
    		
	}


}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    agg->key[0]=NAN;
    agg->data[0]=f;
    agg->data[1]=f;
    agg->data[2]=f;
 //   agg->data[3]=f;
 //   agg->data[4]=f;   

    m_n_aggregate_key_scale(agg);
    m_n_aggregate_print(agg);
}
