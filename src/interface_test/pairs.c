#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;

char *description \
 = "Calculates all the pair to pair distances (r_ij) across all molecular centers.\n\
\n\
\tOutput format:\n\
\tr\tcount\t<count>_frame";

void print_routine_example() {

  char *example \
= "[dist]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
rmax = 100   ; max distance (in atomic units) between molecular centers";
  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"dist:dr",0.1); 
    prec rmax = iniparser_getdouble(dict,"dist:rmax",100);
		  
    prec min[1];
    min[0] = 0.0;
    prec max[1]; 
    max[0] = rmax;
    prec width[1];	
    width[0] = dr;


    agg = m_n_aggregate_new(1, min, max, width, 2);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
   // agg->print_empty = 1;               // print empty rows

 
    
    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i,j;
    M_molec *molec_i, *molec_j;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	for( j=i+1; j<solvent_mspec->count; j++) {
		molec_j = solvent_mspec->molecs->data[j];        
        	
            	M_vector r_i = m_disp(molec_i->center, molec_j->center);
		prec r_mag = m_vector_mag(r_i);
                prec r_header = m_vector_mag(r_i);
	       
	
		agg->key[0] = r_header;
//	        agg->key[1] = r_mag;	        	
					
		if ( r_mag < agg->min[0] || r_mag > agg->max[0] ) {
			agg = m_n_aggregate_extend(agg);
		}
     
		agg->data[0] = 1.0;
		agg->data[1] = 1.0;
		_m_n_aggregate(agg);
	}
    }	
}

void frame_finalise(M_system* sys) {
	
	prec f = 1.0/sys->frames_processed;
		
	
	m_n_aggregate_col_scale(agg,1,f);

	m_n_aggregate_print(agg);
}
