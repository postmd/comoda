#include "comoda.h"
#include "frame_main.h"
#include "time.h"

M_n_aggregate *agg;
M_vector normal;
M_point  origin;
M_molec_spec *solvent_mspec;
prec x_ave, y_ave, z_ave;



//Quinn A. Besford
// Take a trakectory of SPC/E water molecules and translate them into new random coordinates with same shape (e.g. as a sphere)
char *description \
 = "Translation\n\
\n\
\tOutput format:\n\
\tr\t<count>_frame";

void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
count    = 1";
  printf("%s\n",example);
};


void frame_init(M_system* sys, dictionary* dict) {
    srand(time(NULL));
    char *solvent_name;
    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {
    calc_dipoles(sys);
    int i,k,N,frame;
    float ran_x, ran_y, ran_z, x_component_ran, y_component_ran, z_component_ran;
    prec center_x, center_y, center_z,
    center_x_p1, center_x_m1, center_y_p1, center_y_m1, center_z_p1, center_z_m1,
    test_x, test_y, test_z, new_coord_x, new_coord_y, new_coord_z,
    atom_i_x, atom_i_y, atom_i_z, coord_x, coord_y, coord_z, u1, dist_x, dist_y, dist_z, delta_x, delta_xy, r, phi, theta;
    M_molec *molec_i, *molec_j;
	
	ran_x = (-200)+(2*200)*((float)rand()/RAND_MAX);
        ran_y = (-200)+(2*200)*((float)rand()/RAND_MAX);	
        ran_z = (-200)+(2*200)*((float)rand()/RAND_MAX);

	//printf("%f\t%f\t%f\n", ran_x, ran_y, ran_z);
	//exit(0);

         for(i=0;i<solvent_mspec->count;i++){
         	 molec_i = solvent_mspec->molecs->data[i];
                 for(k=0;k<3;k++){

                	M_atom *atom_i = m_molec_atom(molec_i,k);

                	coord_x = atom_i->pos.x;
                	coord_y = atom_i->pos.y;
                	coord_z = atom_i->pos.z;

			new_coord_x = coord_x+ran_x;
			new_coord_y = coord_y+ran_y;
			new_coord_z = coord_z+ran_z;

			test_x = new_coord_x-ran_x;
			test_y = new_coord_y-ran_y;
			test_z = new_coord_z-ran_z;

			printf("%f\t%f\t%f\t%f\t%f\t%f\n", test_x, test_y, test_z, new_coord_x, new_coord_y,new_coord_z);
 			}

		}
	

	}	


void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;
}
