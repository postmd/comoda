#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;
M_n_aggregate *agg_rho;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;
prec bulk_rho;

char *description \
 = "Calculates the radial distribution function for molecules in a slice of width dz from an ion.\n\
\n\
\tOutput format:\n\
";


void print_routine_example() {

  char *example \
= "[rdf]\n\
dr = 0.1;\n\
dz = 1.0;\n\
bulk_density = 0.00495; defaults to 1\n\
[interface]\n\
normal = 1,0,0  ; vector normal to the interface\n\
origin = 0,0,0  ; coordinatess of a point of reference";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    bulk_rho = iniparser_getdouble(dict, "rdf:bulk_density",1);

    prec dr = iniparser_getdouble(dict,"rdf:dr",0.1); 
    prec dz = iniparser_getdouble(dict,"rdf:dz",1.0); 

    prec min[2];
    min[0] = 0.0;   // r
    min[1] = -1.0;  // z
    prec max[2];
    max[0] = sys->rcut;
    max[1] = 1.0;
    prec width[2];
    width[0] = dr;  // r
    width[1] = dz;  // z

    agg = m_n_aggregate_new(2, min, max, width, 1);

    prec min0 = -1;
    prec max0 = 1;
    prec dz0 = dz;

    agg_rho = m_n_aggregate_new(1, &min0, &max0, &dz0, 1); 
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows
    agg->print_2d_values = 1;
    agg->print_0_count = 1;

    prec temp_vec[3];
    char *in;


    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_centers(sys);

    int i,j;
    M_molec *molec_i, *molec_j;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
        for( j=i+1; j<solvent_mspec->count; j++) {
            molec_j = solvent_mspec->molecs->data[j];


            M_vector r_ij = m_disp(molec_i->center, molec_j->center);
            prec r2 = m_vector_mag2(r_ij);

            if ( r2 > sys->rcut2 ) continue;
            prec r = sqrt(r2);

            M_vector r_i = m_disp(molec_i->center, origin);
            M_vector r_j = m_disp(molec_j->center, origin);

            prec z_i = m_vector_dot(r_i, normal);
            prec z_j = m_vector_dot(r_j, normal);

            agg->key[0] = r;
            agg->key[1] = z_i;
            agg->data[0] = 1.0;
            m_n_aggregate(&agg);
        
            agg->key[0] = r;
            agg->key[1] = z_j;
            agg->data[0] = 1.0;
            m_n_aggregate(&agg);

        }
    }

    for ( i=0; i<solvent_mspec->count; i++ ) {
        molec_i = solvent_mspec->molecs->data[i];
        M_vector r_i = m_disp(molec_i->center, origin);
        prec z_i = m_vector_dot(r_i, normal);

        agg_rho->key[0] = z_i;
        agg_rho->data[0] = 1.0;

        m_n_aggregate(&agg_rho);
    }

}

void frame_finalise(M_system* sys) {

    int nframes = sys->frames_processed;
    prec prefactor = 1.0/((prec) bulk_rho);

    //                              col_id, key_id for r
    m_n_aggregate_col_scale_dV(agg, 0, 0);

    int i;
    for (i=0; i<agg->length[1]; i++) {
        agg->key[0] = NAN;
        agg->key[1] = agg->keys[1][i];

        agg_rho->key[0] = agg->key[1];
        M_uint n_slice = m_n_aggregate_lookup(agg_rho);

        agg->data[0] = prefactor/((prec) n_slice);
        m_n_aggregate_key_scale(agg);
    }

    m_n_aggregate_print(agg);
}
