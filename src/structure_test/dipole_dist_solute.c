#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;
//Quinn A. Besford


void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
[system]; \n\
ion_name = fluorine;\n\
[fluorine#F];\n\
symbol   = F;\n\
count    = 1";
  printf("%s\n",example);
};

M_molec_spec *solute_mspec;


void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"n0n:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;

    agg = m_n_aggregate_new(1, &min, &max, &width, 3);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows


 
    char *solute_name;
    solute_name = iniparser_getstring(dict, "system:solute_name", NULL );
    if ( solute_name != NULL ) {
        solute_mspec = m_molec_spec_find( sys, solute_name );
        if ( solute_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",solute_name);
            exit(1);
        }
    } else {
        FATAL("Must supply solute_name in the configuration file.\n");
        exit(1);
    }

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i,k;
    M_molec *molec_i, *molec_j;
    prec dip_i, x_i;
    for( k=0; k<solute_mspec->molecs->count; k++){
    	for( i=0; i<solvent_mspec->count; i++) {
        	molec_i = solvent_mspec->molecs->data[i];
	// position of solute in each frame
		molec_j = solute_mspec->molecs->data[k];
	// displacement vector from each water to the solute
        	M_vector r_i = m_disp(molec_i->center, molec_j->center);
	// distance from water to solute
		x_i = m_vector_mag(r_i);
	// dipole moments
		dip_i = molec_i->dip_mag;
	// aggregate together
        	agg->key[0] = x_i;
        	if ( x_i < agg->min[0] || x_i > agg->max[0] ) {
        	    agg = m_n_aggregate_extend(agg);
        	}

		agg->data[0] =1;
		agg->data[1] = dip_i;
    	         _m_n_aggregate(agg);
    	}
	}
}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    m_n_aggregate_col_scale(agg,0,f);
    m_n_aggregate_col_scale(agg,1,f);
    m_n_aggregate_col_scale(agg,2,f);

    m_n_aggregate_print(agg);
}
