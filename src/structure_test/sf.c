#include "comoda.h"
#include "frame_main.h"

int nspecies;
M_aggregate *Sq;

prec qmax,dq;
int qbins;

char *description \
 = "frame.sf calculates the total structure factor S(q) from a set of coordinates\n\
    \n\
    notes:\n\
      1. qmax must be specified in the configuration file, under [system], this is the\n\
         maximum q that is calculated for S(q). qmax has the unit of 1/Bohr.\n\
      2. qbins must be specified in the configuration file, under [system], this is the\n\
         number of q-points at which S(q) is calculated\n\
      3. scatter_len, the neutron or x-ray scattering length, must be specified for each\n\
         atomic species. scatter_len has the unit of Bohr.\n\
";

void print_routine_example() {
}

void frame_init(M_system* sys, dictionary* dict) {

    qmax = iniparser_getdouble(dict,"system:qmax",0);    
    qbins = iniparser_getint(dict,"system:qbins",0);

    if (! qmax>0 || ! qbins>0) {
      FATAL("ERROR: you must define qmax and qbins!\n");
      exit(1);
    }

    dq=qmax/qbins;
    
    //Sq = (prec*) malloc(sizeof(prec)*qbins);

    //for (i=0;i<qbins;i++) {
    //  Sq[i] = 0;
    //  //DEBUG("S[%f] = %f\n",(prec)(i+0.5)*dq,Sq[i]);
    //}

    nspecies = sys->atom_specs->count;

    Sq = m_aggregate_new(0.0,qmax,dq,nspecies*nspecies+1);
    m_aggregate_set_print_none(Sq);
    Sq->print_col[0] = 1;
    Sq->print_empty = 1;
    Sq->print_count = 1;

    //int i;
    //for (i=0;i<nspecies;i++) {
    //  M_atom_spec *aspec = m_sys_atom_spec(sys,i);
    //  char key[100];
    //  sprintf(key,"%s#%s:scatter_len",aspec->molec_spec->name,aspec->name);

    //  aspec->scatter_len = iniparser_getdouble(dict,key,0);

    //  if ( ! aspec->scatter_len > 0 ) {
    //    FATAL("ERROR: you must define the scatter_len for each atomic species!\n");
    //    exit(1);
    //  }
    //}

}


void frame_routine(M_system* sys) {
    // this routine calculates the total structure factor from coordinates
    M_atom *atom_i, *atom_j;
    //M_atom_spec *aspec_i, *aspec_j;
    int col_id;
    prec r;
    int i,j,k;
    
    for (i=0;i<sys->atoms->count;i++) {
      atom_i = m_sys_atom(sys,i);
      //aspec_i = atom_i->spec;
      for(j=0;j<sys->atoms->count;j++) {
        atom_j = m_sys_atom(sys,j);
        //aspec_j = atom_j->spec;

        if (i==j) {
          //col_id = atom_i->spec->id*nspecies + atom_j->spec->id + 1;
          //for (k=0;k<qbins;k++) {
          //  Sq->values[k][col_id] += 1.0;
          //}
        
        } else {

          r = m_vector_length(m_radius(atom_i->pos,atom_j->pos));
          
          //prec prefactor = aspec_i->scatter_len * aspec_j->scatter_len; 

          //if (r < sys->rcut && atom_i->molec->id != atom_j->molec->id) {
          if (atom_i->molec->id != atom_j->molec->id) {

          for (k=0;k<qbins;k++) {
            prec q = (prec) (k+0.5)*dq;
            prec ans = sin(q*r)/(q*r); 

            col_id = (atom_i->spec->id)*nspecies+(atom_j->spec->id+1);
            Sq->values[k][col_id] += ans;
            //Sq->row[col_id] = ans;

            col_id = (atom_j->spec->id)*nspecies+(atom_i->spec->id+1);
            Sq->values[k][col_id] += ans;
            //Sq->row[col_id] = ans;

            //m_aggregate(Sq);
          }
          }
        }
      }
    }
}

void frame_finalise(M_system* sys) {

  int nframes = sys->frames_processed;
  int numsites = sys->atoms->count;
  int col_id;
  int i,j;

  prec prefactor;

  for (i=0;i<nspecies;i++) {
    M_atom_spec *aspec_i = m_sys_atom_spec(sys,i);

    //prefactor = (prec) aspec_i->scatter_len * aspec_i->scatter_len / numsites;
    //prefactor = (prec) numsites/(aspec_i->sys_total * aspec_i->sys_total * nframes);
    prefactor = (prec) 1.0/(numsites * nframes);

    col_id = aspec_i->id*nspecies + aspec_i->id + 1;
    m_aggregate_col_scale(Sq,col_id,prefactor);
    Sq->print_col[col_id] = 1;
    
    for (j=i+1;j<nspecies;j++) {
      M_atom_spec *aspec_j = m_sys_atom_spec(sys,j);

      //prefactor = (prec) aspec_i->scatter_len * aspec_j->scatter_len / numsites;
      //prefactor = (prec) numsites/(aspec_i->sys_total * aspec_j->sys_total * nframes);

      col_id = aspec_i->id * nspecies + aspec_j->id + 1;
      m_aggregate_col_scale(Sq,col_id,prefactor);
      Sq->print_col[col_id] = 1;

      //col_id = aspec_j->id * nspecies + aspec_i->id + 1;
      //m_aggregate_col_scale(Sq,col_id,prefactor);
    }
  }

  m_aggregate_print(Sq);

  //for (k=0;k<qbins;k++) {
  //  Sq[k] = Sq[k] / (nframes * numsites);
  //  printf("%.10e\t%.10e\t%.10e\n",dq*(0.5+k),Sq[k]+self_scattering, Sq[k]);
  //}
  
  m_aggregate_free(Sq);

}
