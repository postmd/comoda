#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;
prec r_h_max, cos_max;
prec h_bond_sum;

M_molec_spec *solvent_mspec;
// Quinn A. Besford
char *description \
 = "Calculates the total number of hydrogen bonds in a simulation averaged over frames\n\
\n\
\tOutput format:\n\
\tr\t<count>_frame";

void print_routine_example() {

  char *example \
= "[dist]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
rmax = 100   ; max distance (in atomic units) between molecular centers";
  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    r_h_max = iniparser_getdouble(dict,"hbond:r_h_max",6.614044);
    cos_max = iniparser_getdouble(dict,"hbond:cos_max",0.866025403);
    h_bond_sum = 0;
	  
 
    
    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i,j;
    prec cos_1, cos_2, r_mag;
    M_molec *molec_i, *molec_j;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	for( j=0; j<solvent_mspec->count; j++) {
		molec_j = solvent_mspec->molecs->data[j];        
        	
		if (i==j) continue;
//
		// Get molecule i's atoms
		M_atom *oxygen_i = m_molec_atom(molec_i,0);
		M_atom *hydrogen1_i = m_molec_atom(molec_i,1);
		M_atom *hydrogen2_i = m_molec_atom(molec_i,2);			
		// Get molecule j's atoms
                M_atom *oxygen_j = m_molec_atom(molec_j,0);
                M_atom *hydrogen1_j = m_molec_atom(molec_j,1);
                M_atom *hydrogen2_j = m_molec_atom(molec_j,2); 

		M_vector r = m_disp(oxygen_i->pos,oxygen_j->pos);
		r_mag = m_vector_mag(r);
		// You must choose your definition of the max distane for a h-bond
                if ( r_mag >= r_h_max ){
                        continue;
                }

		// Calculate i's HtoO bond vector
		M_vector rH1O = m_disp(hydrogen1_i->pos,oxygen_i->pos);	
		M_vector rH2O = m_disp(hydrogen2_i->pos,oxygen_i->pos);
		// Calculate i's OtoH bond vector
		M_vector rOH1 = m_disp(oxygen_i->pos,hydrogen1_i->pos);
		M_vector rOH2 = m_disp(oxygen_i->pos, hydrogen2_i->pos);
		// Calcualte O_i to O_j bond vector
		M_vector rOO = m_disp(oxygen_i->pos,oxygen_j->pos);
		// angles
		cos_1 = -1.0*m_vector_dot(m_vector_unit(rOO),m_vector_unit(rH1O));
		cos_2 = -1.0*m_vector_dot(m_vector_unit(rOO),m_vector_unit(rH2O));
	
		// You must choose the revelant angle rule
		if ( cos_1 > cos_max){
			h_bond_sum+=1;
		}
		else if ( cos_2 > cos_max){
			h_bond_sum+=1;
		}
		else{
			continue;

		}

		// Other hydrogen bonding rules

	

	}
    }	
}

void frame_finalise(M_system* sys) {
	
	prec f = 1.0/sys->frames_processed;
	prec h_bond_ave = h_bond_sum*f;		

	printf("%f\n", h_bond_ave);	
}
