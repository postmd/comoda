#include "comoda.h"
#include "frame_main.h"

M_parray *c_pool;
M_parray *n_pool;
int       n_max;

FILE     *outfile = NULL;
bool      print_coords;

enum match_type { ALL, NAME, ID };

char *description \
 = "frame.neighbour finds the nearest neighbour of the specified atom/mol(s),\n\
    and gives the distances and identities of the neighbours\n\
    \n\
    Output format:\n\
      id(specified)   name(neighbour)   id(neighbour)   r / bohr\n\
";

void print_routine_example() {
    char *text \
= "[neighbour]\n\
; first, specify the central species of atom/mol(s)\n\
c_match = name      ; keywords, default = name, optional\n\
                      ; find matches based on name or ID?\n\
                      ; Allowed keywords:\n\
                      ; {a*, A*} = match all\n\
                      ; {n*, N*} = match names\n\
                      ; {i*, I*} = match IDs\n\
c_query = oxide     ; keywords (comma separated), required if c_match!=all\n\
                      ; \"spec_A,spec_B,spec_C\" matches the name of molecules\n\
                      ; \"0,1,2,3,10\" matches the id of the molecules\n\
n_max   = 4         ; int, default = 1, optional\n\
                      ; how many neighbours to find for each specified atom/mol?\n\
; now, specify the type of neighbours\n\
; if unspecified, n_match = c_match\n\
; options are the same as above\n\
n_match = name\n\
n_query = water\n\
; additional options for outputs\n\
print_coords = true ; boolean, default = false, optional\n\
                      ; print the coordinates of each neighbour unit as XYZ\n\
                      ; each neighbour unit is printed as a separate frame\n\
coords_file = nn.xyz; path, default = stdout, optional\n\
                      ; print the coordinates to this file\n\
";
    printf("%s\n",text);
}

M_parray* build_pool(M_system* sys, enum match_type m, void *list, int list_len) {
    int i,j;

    M_parray *pool = m_parray_new(0);

    if ( m == ALL ) {

        // if we are adding all of the molecules, then push all elements
        // we ignore the list and its length (3rd & 4th argument)
        for (i=0; i<sys->molecs->count; i++) {
            m_parray_push(pool, m_sys_molec(sys, i));
        }

    } else {

        if ( list == NULL ) {
            FATAL("Invalid pointer list == NULL.\n");
            exit(1);
        }

        if ( m == ID ) {

            // if we are adding a list of IDs
            // first cast the list into a list of integers
            M_uint *ids = (M_uint *) list;

            // loop over the list
            for (i=0; i<list_len; i++) {
                // retrieve the molecule and push it to the pool
                M_molec *el = m_sys_molec(sys, ids[i]);
                m_parray_push(pool, el);
            }

        } else {

            // if it is a list of names
            // we must then be operating on a list of species
            // first declare a list of (M_molec_spec*)
            M_molec_spec **specs = malloc(sizeof(M_molec_spec *)*list_len);
            // now cast the input list into a list of strings (char*)
            char **names = (char **) list;

            // loop over the list
            for (i=0; i<list_len; i++) {

                // search for the spec by name
                specs[i] = m_molec_spec_find(sys, names[i]);

                // if a spec has not been found, a NULL pointer will be returned
                if ( specs[i] == NULL ) {
                    FATAL("Species not found : %s. Aborting.\n",names[i]);
                    exit(1);
                }

                for (j=0; j<specs[i]->molecs->count; j++) {
                    m_parray_push(pool, specs[i]->molecs->data[j]);
                }
                
            }
            free(*names);
        }

        // free the list
        free(list);

    }

    //for ( i=0; i<pool->count; i++ ) {
    //    M_molec *a = pool->data[i];
    //    DEBUG("%d : %d\n",i,a->id);
    //}

    return pool;
}

enum match_type parse_match_type(const char *in) {
    if ( in != NULL ) {
        if ( in[0]=='n' || in[0]=='N') {
            return NAME;
        } else if ( in[0]=='i' || in[0]=='I' ) {
            return ID;
        } else if ( in[0]=='a' || in[0]=='A' ) {
            return ALL;
        }
    } else {
        FATAL("Input string is NULL.\n");
        exit(1);
    }
    return 0;
}

void frame_init(M_system* sys, dictionary* dict) {

    char *in;
    enum match_type c_match;
    enum match_type n_match;
    int list_len;
    void *list = NULL;

    n_max = iniparser_getint(dict, "neighbour:n_max", 1);

    in = iniparser_getstring(dict, "neighbour:c_match", "all");
    c_match = parse_match_type(in);

    in = iniparser_getstring(dict, "neighbour:n_match", NULL);
    if ( in != NULL ) {
        n_match = parse_match_type(in);
    } else {
        n_match = c_match;
    }

    if ( c_match != ALL ) {
        in = iniparser_getstring(dict, "neighbour:c_query", NULL);
        if ( in != NULL ) {
            const char del[1] = ",";
            list_len = count_n_fields(in, del, 1);
            
            if ( c_match == ID ) {
                list = malloc(sizeof(M_uint)*list_len);
                parse_uiarray(list, in, list_len);
            } else {
                list = malloc(sizeof(char*)*list_len);
                parse_strarray(list, in, list_len);
            }
        } else {
            FATAL("A c_query must be specified for the c_match chosen. Aborting.\n");
            exit(1);
        }
    } else {
        list = NULL;
        list_len = 0;
    }
    c_pool = build_pool(sys, c_match, list, list_len);

    if ( n_match != ALL ) {
        in = iniparser_getstring(dict, "neighbour:n_query", NULL);
        if ( n_match == c_match && in == NULL ) {
            // if n_match is the same as c_match, and n_query == NULL,
            // then we'll use the same query as c_query
            in = iniparser_getstring(dict, "neighbour:c_query", NULL);
        }

        if ( in != NULL ) {
            const char del[1] = ",";
            list_len = count_n_fields(in, del, 1);
            
            if ( n_match == ID ) {
                list = malloc(sizeof(M_uint)*list_len);
                parse_uiarray(list, in, list_len);
            } else {
                list = malloc(sizeof(char*)*list_len);
                parse_strarray(list, in, list_len);
            }
        } else {
            FATAL("A n_query must be specified. Aborting.\n");
            exit(1);
        }
    } else {
        list = NULL;
        list_len = 0;
    }
    n_pool = build_pool(sys, n_match, list, list_len);


    print_coords = iniparser_getboolean(dict, "neighbour:print_coords", 0);
    if ( print_coords ) {
        in = iniparser_getstring(dict, "neighbour:coords_file", NULL);
        if ( in != NULL ) {
            outfile = fopen(in,"w");
        } else {
            outfile = stdout;
        }
    }

}


void frame_routine(M_system* sys) {

    calc_centers(sys);

    int i;
    for (i=0; i<c_pool->count; i++ ) {

        M_neighbours *nn = m_nearest_neighbours_new(n_max, sys->rcut2);

        M_molec *c_mol = (M_molec *) c_pool->data[i];

        int j;
        for ( j=0; j<n_pool->count; j++ ) {
            M_molec *n_mol = (M_molec *) n_pool->data[j];

            if ( n_mol != c_mol ) {
                prec r2 = m_vector_mag2(m_disp(c_mol->center, n_mol->center));
                m_nearest_neighbours_add(nn, n_mol, r2);
            }

        }

        if ( print_coords ) {
            int n_atoms = 0;
            n_atoms += c_mol->atoms->count;
            for ( j=0; j<nn->molecs->count; j++ ) {
                M_molec *n_mol = nn->molecs->data[j];
                n_atoms += n_mol->atoms->count;
            }
            fprintf(outfile, "%d\n", n_atoms);
            fprintf(outfile, "BOX\t%lf\t%lf\t%lf\n",
                    sys->dims.x*ang2au, sys->dims.y*ang2au, sys->dims.z*ang2au);
            int m;
            for ( m=0; m<c_mol->atoms->count; m++ ) {
                M_atom *a = c_mol->atoms->data[m];
                fprintf(outfile, "%s\t%lf\t%lf\t%lf\n", a->spec->symbol,
                        a->pos.x*ang2au, a->pos.y*ang2au, a->pos.z*ang2au);
            }
        }
        for ( j=0; j<nn->molecs->count; j++ ) {
            M_molec *n_mol = nn->molecs->data[j];
            printf("%d\t%d\t%lf\n",
                   c_mol->id, n_mol->id, sqrt(nn->ranks[j]));
            if ( print_coords ) {
                int m;
                for ( m=0; m<n_mol->atoms->count; m++ ) {
                    M_atom *a = n_mol->atoms->data[m];
                    fprintf(outfile, "%s\t%lf\t%lf\t%lf\n", a->spec->symbol,
                            a->pos.x*ang2au, a->pos.y*ang2au, a->pos.z*ang2au);
                }
                fflush(outfile);
            }
            fflush(stdout);
        }

        m_nearest_neighbours_free(nn);

    }

}

void frame_finalise(M_system* sys) {

    if ( outfile != stdout && outfile != NULL ) {
        fclose(outfile);
    }
    m_parray_free(n_pool);
    m_parray_free(c_pool);
}
