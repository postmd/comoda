#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;
M_molec_spec *ion_mspec;
//Quinn A Besford
char *description \
 = "Calculates the distribution of orientations of dipoles near an ion.\n\
\n\
\tOutput format:\n\
";


void print_routine_example() {

  char *example \
= "[dist]\n\
dr = 0.1;\n\
dcos = 0.1;\n\
[fluorine#F];\n\
symbol = F;\n\
count  = 1";
  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"dist:dr",0.1); 
    prec dcos = iniparser_getdouble(dict,"dist:dcos",0.1); 

    prec min[2];
    min[0] = 0.0;
    min[1] = -1.0;
    prec max[2];
    max[0] = sys->rcut;
    max[1] = 1.0;
    prec width[2];
    width[0] = dr;
    width[1] = dcos;

    agg = m_n_aggregate_new(2, min, max, width, 1);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows
    agg->print_2d_values = 1;
    agg->print_0_count = 1;

    prec temp_vec[3];
    char *in;
	
    char *ion_name;
    ion_name = iniparser_getstring(dict, "system:ion_name", NULL );
    if ( ion_name != NULL ) {
        ion_mspec = m_molec_spec_find( sys, ion_name );
        if ( ion_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",ion_name);
            exit(1);
        }
    } else {
        FATAL("Must supply ion_name in the configuration file.\n");
        exit(1);
    }


    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i;
    M_molec *molec_i, *molec_j;
    prec cos_i, x_i;
    for( i=0; i<solvent_mspec->count; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	// Find the ion
        molec_j = ion_mspec->molecs->data[0];
	// Find the displacement from each water
	// Note I have reversed this displacement as the vector.c library calculates BtoA, not AtoB
	M_vector r_i = m_disp(molec_j->center, molec_i->center);
	// What is the Cos\theta between them?
        cos_i = -1.0*m_vector_dot(m_vector_unit(molec_i->dipole), m_vector_unit(r_i));
        // Distance between the water and the ion?
        x_i = m_vector_mag(r_i);

        agg->key[0] = x_i;
	agg->key[1] = cos_i;
        if ( x_i < agg->min[1] || x_i > agg->max[1] ) {
            agg = m_n_aggregate_extend(agg);
        }

	agg->data[0] = 1.0;

        _m_n_aggregate(agg);
    }

}

void frame_finalise(M_system* sys) {

    //prec f = 1.0/sys->frames_processed;

    //m_n_aggregate_col_scale(agg,0,f);

    //int i,j;
    //for (i=0; i<agg->length[1]; i++) {
    //    int n = agg->keys_count[1][i];
    //    if ( n > 0 ) {
    //        prec n_r = 1.0/n;
    //        for (j=0; j<agg->length[0]; j++) {
    //            agg->values[j*agg->length[0]+i] *= n_r;
    //        }
    //    }
    //}

    m_n_aggregate_print(agg);
}
