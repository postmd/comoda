#include "comoda.h"
#include "frame_main.h"
// Quinn A. Besford
prec rho_sum, Gk_sum, dipolemoment;
M_vector M;
prec kT;
M_molec_spec *solvent_mspec;
char *description \
 = "Calculates the dielectric and physical properties of homogenous systems.\n\
\n\
\tOutput format:\n\
\t kT \t rho \t mu^2 \t Gk \t y \t epsilon \t gk \n ";

void print_routine_example() {

  char *example \
= "----->  ----->    We correlate! /:D/";
  printf("%s\n",example);
};


void frame_init(M_system* sys, dictionary* dict) {
    //prec rho = iniparser_getdouble(dict,"density:number_vol",0.0004);
    prec T = iniparser_getdouble(dict,"density:T",298);
    kT = T/(4.35974434E-18/1.3806488E-23);
    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }
    Gk_sum = 0;
    rho_sum = 0;
    dipolemoment = 0;
}

void frame_routine(M_system* sys) {

    M.x = 0;
    M.y = 0;
    M.z = 0;

    prec dipolemoment_sum = 0;
    calc_dipoles(sys);
    int i;
    M_molec *molec_i;
    prec rho = solvent_mspec->density;
    for( i=0; i<solvent_mspec->count; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	 		     
	M = m_vector_add( M, molec_i->dipole );
	dipolemoment_sum += molec_i->dip_mag;

	

    }
    prec dipolemoment_ave = dipolemoment_sum/solvent_mspec->count; 
    dipolemoment += dipolemoment_ave;
    Gk_sum += m_vector_mag2(M)/(solvent_mspec->count*(dipolemoment_ave*dipolemoment_ave));
    rho_sum += rho;
}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;
    prec dipolemoment_final = dipolemoment*f;
    prec rho_ave = rho_sum*f;
    prec Gk_ave = Gk_sum*f;
    prec y_ave = (4*3.14159265358979323846*rho_ave*dipolemoment_final)/(9*kT);
    prec epsilon = 1+(3*y_ave*Gk_ave);
    prec gk = (((epsilon-1)*(2*epsilon+1))/(9*epsilon))*(1/y_ave);
    
    printf("kT\t \trho\t \tmu\t \tGk\t \ty\t \teps\t \tgK\n");
    printf("--------------------------------------------------------------------------------------------------------\n");
    printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\n", kT, rho_ave, dipolemoment_final, Gk_ave, y_ave, epsilon, gk);
    
}
