#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

prec c101, c202, c303, c404, c505, c606, c707, c808, c909;
prec c110, c112, c121, c123, c211, c213, c220, c222, c224;
prec c011, c022, c033, c044, c055, c066, c077, c088, c099;

M_molec_spec *solvent_mspec, *solute_mspec;

char *description \
 = "structure.phiMD\n\
\n\
\tOutput format:\n\
\tr\t-phiMD(r)/kT\t<n>\n\
\twhere:\n\
\t  <n> is the average number of molecules found within the given slice\n\
";

void print_routine_example() {

  char *example \
= "[phi]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"phi:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;
    
    // 000
    // 110, 112, 121, 123, 211, 213, 220, 222, 224
    // 101, 202, 303, 404, 505, 606, 707, 808, 909
    // 011, 022, 033, 044, 055, 066, 077, 088, 099
    agg = m_n_aggregate_new(1, &min, &max, &width, 28);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows

    char *solvent_name, *solute_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }
    
 
    solute_name = iniparser_getstring(dict, "system:solute_name", NULL );
    if ( solute_name != NULL ) {
      solute_mspec = m_molec_spec_find( sys, solute_name );
      if ( solute_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solute_name);
        exit(1);
      }
      if ( solute_mspec->center < 0 || solute_mspec->center >= solute_mspec->atoms ) {
        FATAL("The center for the solute %s must be a valid atom.\n",solute_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solute_name in the configuration file.\n");
      exit(1);
    }



    c101 = pow(3,0.5);
    c202 = pow(5,0.5)/2.0;
    c303 = pow(7,0.5)/2.0;
    c404 = 3.0/8.0;
    c505 = pow(11,0.5)/8.0;
    c606 = pow(13,0.5)/16.0;
    c707 = pow(15,0.5)/16.0;
    c808 = pow(17,0.5)/128.0;
    c909 = pow(19,0.5)/128.0;

    c011 = pow(3,0.5);
    c022 = pow(5,0.5)/2.0;
    c033 = pow(7,0.5)/2.0;
    c044 = 3.0/8.0;
    c055 = pow(11,0.5)/8.0;
    c066 = pow(13,0.5)/16.0;
    c077 = pow(15,0.5)/16.0;
    c088 = pow(17,0.5)/128.0;
    c099 = pow(19,0.5)/128.0;

    c110 = pow(3,0.5);               // 110
    c112 = pow(1.5,0.5);             // 112
    c121 = pow(1.5,0.5);             // 121
    c123 = 1.5;                      // 123
    c211 = c121;                     // 211
    c213 = c123;                     // 213
    c220 = pow(5.0,0.5)/4.0;         // 220
    c222 = 5.0/(2.0*pow(14.0,0.5));  // 222
    c224 = 1.5*pow(5.0/14.0,0.5);    // 224

}

void update_mnl(M_vector mu1hat, M_vector mu2hat, M_vector rhat, prec r) {

    prec alpha = m_vector_dot(mu1hat, rhat);
    prec beta = m_vector_dot(mu2hat, rhat);

    // project the dipole moments perpendicular to the displacement
    // mu_p = mu_hat - (mu_hat . r_hat) * r_hat
    M_vector mu1p, mu2p;
    mu1p.x = mu1hat.x-rhat.x*alpha;
    mu1p.y = mu1hat.y-rhat.y*alpha;
    mu1p.z = mu1hat.z-rhat.z*alpha;
    mu2p.x = mu2hat.x-rhat.x*beta;
    mu2p.y = mu2hat.y-rhat.y*beta;
    mu2p.z = mu2hat.z-rhat.z*beta;

    M_vector mu1phat = m_vector_unit(mu1p);
    M_vector mu2phat = m_vector_unit(mu2p);

    // calculate the azimuthal cosine gamma=cos(phi1-phi2)
    prec gamma = m_vector_dot(mu1phat,mu2phat);

    // calculate a few common expressions
    prec alpha2, alpha3, alpha4, alpha5, alpha6, alpha7, alpha8, alpha9;
    prec beta2, beta3, beta4, beta5, beta6, beta7, beta8, beta9;
    prec alphac, betac, gammac;
    prec ab, a2b, ab2, a2b2, acbcg, aacbbcg, ac2bc2gc;

    // A few common expressions
    alpha2 = alpha * alpha;
    alphac = pow((1.0 - alpha2),0.5);
    beta2 = beta * beta;
    betac = pow((1.0-beta2),0.5);
    gammac = 2.0*gamma*gamma - 1.0;

    // A few more complex quantities
    ab=alpha*beta;
    ab2=ab*beta;
    a2b=ab*alpha;
    a2b2=ab2*alpha;
    acbcg=alphac*betac*gamma;
    aacbbcg=ab*acbcg;
    ac2bc2gc=(1.0-alpha2)*(1.0-beta2)*gammac;

    alpha3=alpha2*alpha;
    alpha4=alpha3*alpha;
    alpha5=alpha4*alpha;
    alpha6=alpha5*alpha;
    alpha7=alpha6*alpha;
    alpha8=alpha7*alpha;
    alpha9=alpha8*alpha;

    beta3=beta2*beta;
    beta4=beta3*beta;
    beta5=beta4*beta;
    beta6=beta5*beta;
    beta7=beta6*beta;
    beta8=beta7*beta;
    beta9=beta8*beta;

    // now calculate the rotational invariants
    prec phi101, phi202, phi303, phi404, phi505, phi606, phi707, phi808, phi909;
    prec phi011, phi022, phi033, phi044, phi055, phi066, phi077, phi088, phi099;
    prec phi110, phi112, phi121, phi123, phi211, phi213, phi220, phi222, phi224;

    // symmetrical identities
    phi110=-ab-acbcg;
    phi112=2.0*ab-acbcg;
    phi220=1.0-3.0*alpha2-3.0*beta2+9.0*a2b2+12.0*aacbbcg+3.0*ac2bc2gc;
    phi222=1.0-3.0*beta2+alpha2*(-3.0+9.0*beta2)+6.0*aacbbcg-3.0*ac2bc2gc;
    phi224=1.0-3.0*alpha2-3.0*beta2+9.0*a2b2-8.0*aacbbcg+0.5*ac2bc2gc;

    // asymmetrical identities
    phi121=alpha-3.0*ab2-3.0*beta*acbcg;
    phi123=-alpha+3.0*ab2-2.0*beta*acbcg;
    phi211=beta-3.0*a2b-3.0*alpha*acbcg;
    phi213=-beta+3.0*a2b-2.0*alpha*acbcg;

    // n0n functions
    phi101=alpha;
    phi202=-1.0+3.0*alpha2;
    phi303=-3.0*alpha+5.0*alpha3;
    phi404=3.0-30.0*alpha2+35.0*alpha4;
    phi505=15.0*alpha-70.0*alpha3+63.0*alpha5;
    phi606=-5.0+105*alpha2-315.0*alpha4+231.0*alpha6;
    phi707=-35.0*alpha+315.0*alpha3-693.0*alpha5+429.0*alpha7;
    phi808=35.0-1260.0*alpha2+6930.0*alpha4-12012.0*alpha6+6435.0*alpha8;
    phi909=315.0*alpha-4620.0*alpha3+18018.0*alpha5-25740.0*alpha7+12155.0*alpha9;

    // 0nn functions
    phi011=beta;
    phi022=-1.0+3.0*beta2;
    phi033=-3.0*beta+5.0*beta3;
    phi044=3.0-30.0*beta2+35.0*beta4;
    phi055=15.0*beta-70.0*beta3+63.0*beta5;
    phi066=-5.0+105*beta2-315.0*beta4+231.0*beta6;
    phi077=-35.0*beta+315.0*beta3-693.0*beta5+429.0*beta7;
    phi088=35.0-1260.0*beta2+6930.0*beta4-12012.0*beta6+6435.0*beta8;
    phi099=315.0*beta-4620.0*beta3+18018.0*beta5-25740.0*beta7+12155.0*beta9;

    // aggregate i-->j
    agg->key[0] = r;

    agg->data[0]=1.0;
    agg->data[1]=phi110;
    agg->data[2]=phi112;
    agg->data[3]=phi121;
    agg->data[4]=phi123;
    agg->data[5]=phi211;
    agg->data[6]=phi213;
    agg->data[7]=phi220;
    agg->data[8]=phi222;
    agg->data[9]=phi224;
    agg->data[10]=phi101;
    agg->data[11]=phi202;
    agg->data[12]=phi303;
    agg->data[13]=phi404;
    agg->data[14]=phi505;
    agg->data[15]=phi606;
    agg->data[16]=phi707;
    agg->data[17]=phi808;
    agg->data[18]=phi909;
    agg->data[19]=phi011;
    agg->data[20]=phi022;
    agg->data[21]=phi033;
    agg->data[22]=phi044;
    agg->data[23]=phi055;
    agg->data[24]=phi066;
    agg->data[25]=phi077;
    agg->data[26]=phi088;
    agg->data[27]=phi099;

    m_n_aggregate(&agg);

    // aggregate j-->i
    agg->key[0] = r;

    agg->data[0]=1.0;
    agg->data[1]=phi110;
    agg->data[2]=phi112;
    agg->data[3]=phi211;
    agg->data[4]=phi213;
    agg->data[5]=phi121;
    agg->data[6]=phi123;
    agg->data[7]=phi220;
    agg->data[8]=phi222;
    agg->data[9]=phi224;
    agg->data[10]=phi011;
    agg->data[11]=phi022;
    agg->data[12]=phi033;
    agg->data[13]=phi044;
    agg->data[14]=phi055;
    agg->data[15]=phi066;
    agg->data[16]=phi077;
    agg->data[17]=phi088;
    agg->data[18]=phi099;
    agg->data[19]=phi101;
    agg->data[20]=phi202;
    agg->data[21]=phi303;
    agg->data[22]=phi404;
    agg->data[23]=phi505;
    agg->data[24]=phi606;
    agg->data[25]=phi707;
    agg->data[26]=phi808;
    agg->data[27]=phi909;

    m_n_aggregate(&agg);

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i,j;
    for( i=0; i<solvent_mspec->count-1; i++) {
        M_molec *molec_i = solvent_mspec->molecs->data[i];
        for ( j=0; j<solute_mspec->count; j++) {

            M_molec *molec_j = solute_mspec->molecs->data[j];

            // points from i-->j
            M_vector r_ij = m_disp(molec_j->center, molec_i->center);

            // don't do anything if we are beyond rcut. This will just add noise
            prec r2 = m_vector_mag2(r_ij);
            if ( r2 > sys->rcut2 ) continue;

            // unit dipoles
            M_vector mu1hat = m_vector_scale(m_vector_unit(molec_i->dipole),-1.0);
            M_vector mu2hat = m_vector_scale(m_vector_unit(molec_j->dipole),-1.0);
            M_vector rhat = m_vector_unit(r_ij);

            update_mnl(mu1hat, mu2hat, rhat, sqrt(r2));

        }

    }

}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    agg->key[0]=NAN;

    agg->data[0]=f;
    agg->data[1]=c110*f;
    agg->data[2]=c112*f;
    agg->data[3]=c121*f;
    agg->data[4]=c123*f;
    agg->data[5]=c211*f;
    agg->data[6]=c213*f;
    agg->data[7]=c220*f;
    agg->data[8]=c222*f;
    agg->data[9]=c224*f;
    agg->data[10]=c101*f;
    agg->data[11]=c202*f;
    agg->data[12]=c303*f;
    agg->data[13]=c404*f;
    agg->data[14]=c505*f;
    agg->data[15]=c606*f;
    agg->data[16]=c707*f;
    agg->data[17]=c808*f;
    agg->data[18]=c909*f;
    agg->data[19]=c011*f;
    agg->data[20]=c022*f;
    agg->data[21]=c033*f;
    agg->data[22]=c044*f;
    agg->data[23]=c055*f;
    agg->data[24]=c066*f;
    agg->data[25]=c077*f;
    agg->data[26]=c088*f;
    agg->data[27]=c099*f;

    m_n_aggregate_key_scale(agg);

    int i,j;
    for (i=0; i<agg->total_length; i++) {
        prec r = agg->keys[0][i];
        prec N = agg->values[i];
        prec phi = 0;
        if (N>0) {
            for (j=1; j<=27; j++) {
                int id = j*agg->total_length+i;
                phi+=pow(agg->values[id],2);
            }
            phi/=(2.0*pow(N,2));
        }
        printf("%lf\t%lf\t%lf\n",r,phi,N);
    }

//    m_n_aggregate_print(agg);

}
