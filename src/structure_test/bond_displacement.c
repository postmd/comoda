#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;
M_molec_spec *solvent_mspec;
// Quinn A. Besford
// This program allows you to check that atoms in a molecule are constrained over the simulation period.
char *description \
 = "Calculates the bond displacements averaged over frames\n\
\n\
\tOutput format:\n\
\tN x frame \tdisplacement";

void print_routine_example() {

  char *example \
= "[dist]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
rmax = 100   ; max distance (in atomic units) between molecular centers";
  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {
    
    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i;
    prec O_H1_mag, O_H2_mag, H1_H2_mag;
    M_molec *molec_i;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];

		// Get molecule i's atoms
		M_atom *oxygen_i = m_molec_atom(molec_i,0);
		M_atom *hydrogen1_i = m_molec_atom(molec_i,1);
		M_atom *hydrogen2_i = m_molec_atom(molec_i,2);			

		M_vector O_H1 = m_disp(oxygen_i->pos,hydrogen1_i->pos);
		M_vector O_H2 = m_disp(oxygen_i->pos,hydrogen2_i->pos);
		M_vector H1_H2 = m_disp(hydrogen1_i->pos,hydrogen2_i->pos);

  		O_H1_mag = m_vector_mag(O_H1);
		O_H2_mag = m_vector_mag(O_H2);
		H1_H2_mag = m_vector_mag(H1_H2);
		

		printf("%f\t%f\t%f\n", O_H1_mag, O_H2_mag, H1_H2_mag);
	}
	
    }	

void frame_finalise(M_system* sys) {
	
	}
