#include "comoda.h"
#include "frame_main.h"
#include "time.h"

M_n_aggregate *agg;
M_vector normal;
M_point  origin;
M_molec_spec *solvent_mspec;
prec x_ave, y_ave, z_ave;



//Quinn A. Besford
// calculate the centre of mass for a system

char *description \
 = "Rotation matrix\n\
\n\
\tOutput format:\n\
\tr\t<count>_frame";

void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
count    = 1";
  printf("%s\n",example);
};


void frame_init(M_system* sys, dictionary* dict) {
    srand(time(NULL));
    char *solvent_name;
    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {
    calc_dipoles(sys);
    int i,k,N,frame;
    prec center_x, center_y, center_z, atom_i_x, atom_i_y, atom_i_z;
    M_molec *molec_i;
	


	
	    // Begin by calculating the total center of mass
	    // Rm = 1/M Σ m_i * r_i
	x_ave = 0;
	y_ave = 0;
	z_ave = 0;
	N = solvent_mspec->count*3; // This is for water (3 atoms). Need to adjust to an atom count
	for(i=0;i<solvent_mspec->count;i++){
		molec_i = solvent_mspec->molecs->data[i];
		for(k=0;k<3;k++){

		        M_atom *atom_i = m_molec_atom(molec_i,k);


			atom_i_x = atom_i->pos.x;
			atom_i_y = atom_i->pos.y;	
			atom_i_z = atom_i->pos.z;

			x_ave += atom_i_x;
			y_ave += atom_i_y;
			z_ave += atom_i_z;
			}
		}
    
	  center_x = x_ave/N;
	  center_y = y_ave/N; 
	  center_z = z_ave/N;

//          printf("%f\t%f\t%f\n",center_x, center_y, center_z);
//          //          exit(0);

	    printf("%f\t%f\t%f\n", center_x, center_y, center_z);
 	}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;
}
