#include "comoda.h"
#include "frame_main.h"

prec sum_r6;
prec sum_r12;
prec sum_r6_2;
prec sum_r12_2;

M_molec_spec *solvent_mspec;

char *description \
 = "Calculates the < 1/r^6 >_frames across simulation frames.\n\
\n\
\tOutput format:\n\
\t<1/r^6>\tstdev < 1/r^6>\t<1/r^12> \t st dev <1/r^12>";

void print_routine_example() {

  char *example \
= "";
  printf("%s\n",example);
};


void frame_init(M_system* sys, dictionary* dict) {

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }
    sum_r6 = 0;
    sum_r12 = 0;
    sum_r6_2 = 0;
    sum_r12_2 = 0;
}

void frame_routine(M_system* sys) {

    calc_centers(sys);

    prec f_r6 = 0;
    prec f_r12 = 0;
    int i,j;
    M_molec *molec_i, *molec_j;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	for( j=i+1; j<solvent_mspec->count; j++) {
		molec_j = solvent_mspec->molecs->data[j];        
        	
            	M_vector r_i = m_disp(molec_i->center, molec_j->center);
		prec r_mag2 = m_vector_mag2(r_i);
		prec r_r6 = pow(r_mag2, -3.0);
		prec r_r12 = pow(r_r6, 2.0);

		f_r6 += r_r6;
		f_r12 += r_r12;
	}
    }	
//    printf("%.10E\t%.10E\n", f_r6, f_r12);
    sum_r6 += f_r6;
    sum_r6_2 += f_r6*f_r6;
    sum_r12 += f_r12;
    sum_r12_2 += f_r12*f_r12;
    
}

void frame_finalise(M_system* sys) {
	
	prec f = 1.0/sys->frames_processed;

	prec ave_r6 = sum_r6*f;
	prec ave_r12 = sum_r12*f;

	prec sigma_r6 = sqrt(sum_r6_2*f - ave_r6*ave_r6)*(1.0/sqrt(sys->frames_processed));
	prec sigma_r12 = sqrt(sum_r12_2*f - ave_r12*ave_r12)*(1.0/sqrt(sys->frames_processed));
	
//	printf("ave r^6 \t stdev r^6 \t ave r^12 \t stdev r^12\n") testing;
	printf("%.10E\t%.10E\t%.10E\t%.10E\n",ave_r6,sigma_r6,ave_r12,sigma_r12);
}
