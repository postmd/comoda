#include "comoda.h"
#include "frame_main.h"

int nspecies;
M_n_aggregate *agg;

prec sum_V;
prec r;
int col_id;
int i,j;

M_atom *atom_i, *atom_j;

bool no_scale;
bool use_angstrom;

char *description \
 = "frame.rdf calculates the radial distribution function for each pair\n\
    of atomic species.\n\
    \n\
    Output format:\n\
      r/bohr  1-1  1-2  1-3  1-4 .... 2-2  2-3  2-4... 3-3  3-4 ...\n\
";

void print_routine_example() {
    char *text \
= "[rdf]\n\
dr        = 0.1     ; float, bohrs, default = 0.1, optional\n\
                      ; the size of the bin for calculating the RDF.\n\
no_scale  = false   ; boolean {true, false}, default = false, optional\n\
                      ; if no_scale = true, the raw count of #pairs\n\
                      ; in each bin will be printed instead of the RDF.\n\
                      ; i.e. N(r) instead of g(r).\n\
";
    printf("%s\n",text);
}

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"rdf:dr",0.1); 
    no_scale = iniparser_getboolean(dict, "rdf:no_scale", 0);
    use_angstrom = iniparser_getboolean(dict, "rdf:angstrom", 0);

    sum_V = 0;

    nspecies = sys->atom_specs->count;

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;
    if ( use_angstrom ) {
        width = dr/ang2au;
    }

    agg = m_n_aggregate_new(1, &min, &max, &width, nspecies*nspecies);
    
    m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    agg->print_empty = 1;               // print empty rows
}


void frame_routine(M_system* sys) {
    // this routine calculates the RDFs between different species
    
    // keep a running sum of the cell volume
    sum_V += sys->volume;

    // only consider unique pairs
    for (i=0;i<sys->atoms->count;i++) {
      atom_i = m_sys_atom(sys,i);
      for(j=i+1;j<sys->atoms->count;j++) {
        atom_j = m_sys_atom(sys,j);

        // only if the atoms did not belong to the same molecule
        if (atom_i->molec->id != atom_j->molec->id) {

          r = m_vector_length(m_radius(atom_i->pos,atom_j->pos));

          if (r<sys->rcut || sys->no_cutoff) {

            agg->key[0] = r;
            if ( r > agg->max[0] || r < agg->min[0] ) {
              agg = m_n_aggregate_extend(agg);
            }            

            //col_id=get_col_num(atom_i->spec->id,atom_j->spec->id,nspecies);
            col_id=(atom_i->spec->id)*nspecies+(atom_j->spec->id);
            agg->data[col_id] = 1.0;

            col_id=(atom_j->spec->id)*nspecies+(atom_i->spec->id);
            agg->data[col_id] = 1.0;

            m_n_aggregate(&agg);
          }
        }
      }
    }

}

void frame_finalise(M_system* sys) {
  M_atom_spec *aspec_i, *aspec_j;

  int nframes;
  nframes = sys->frames_processed;

  prec prefactor;
  for (i=0;i<nspecies;i++) {
    for (j=i;j<nspecies;j++) {
      
      aspec_i = m_sys_atom_spec(sys,i);
      aspec_j = m_sys_atom_spec(sys,j);

      
      col_id = i*nspecies + j; 

      // turn on printing for this column
      agg->print_col[col_id] = 1;

      
      prefactor = sum_V/((prec) aspec_i->sys_total * aspec_j->sys_total * nframes * nframes);
      
      if ( i == j ) {
        prefactor = prefactor * 2.0;
      }

      if ( ! no_scale ) {
        m_n_aggregate_col_scale_dV(agg,col_id,0);
        m_n_aggregate_col_scale(agg,col_id,prefactor);
      }

    }
  }

  if ( use_angstrom ) {
    for ( i=0; i<agg->length[0]; i++ ) {
      agg->keys[0][i] *= ang2au;
    }
  }

  m_n_aggregate_print(agg);

}
