#include "comoda.h"
#include "frame_main.h"
// Quinn A. Besford
// Same as dielectric.c but with multiple molecules included (aim for 3)
prec rho_sum, rho_sum2, Gk_sum, dipolemoment, dipolemoment2, Gk_sum2;
M_vector M, M2, Mtot;
prec kT, number;
int Counttest;
M_molec_spec *solvent_mspec, *solute_mspec, *solute2_mspec;
char *description \
 = "Calculates the dielectric and physical properties of homogenous systems.\n\
\n\
\tOutput format:\n\
\t kT \t rho \t mu^2 \t Gk \t y \t epsilon \t gk \n ";

void print_routine_example() {

  char *example \
= "----->  ----->    We correlate! /:D/";
  printf("%s\n",example);
};


void frame_init(M_system* sys, dictionary* dict) {
    // prec rho = iniparser_getdouble(dict,"density:number_vol",0.0004);
    prec T = iniparser_getdouble(dict,"density:T",298);
    kT = T/(4.35974434E-18/1.3806488E-23);
    char *solvent_name, *solute_name, solute2_name;

    // insert of multiple solute grabs 

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

    solute_name = iniparser_getstring(dict, "system:solute_name", NULL );
    if ( solute_name != NULL ) {
      solute_mspec = m_molec_spec_find( sys, solute_name );
      if ( solute_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solute_name);
        exit(1);
      }
      if ( solute_mspec->center < 0 || solute_mspec->center >= solute_mspec->atoms ) {
        FATAL("The center for the solute %s must be a valid atom.\n",solute_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solute_name in the configuration file.\n");
      exit(1);
    }

    solute2_name = iniparser_getstring(dict, "system:solute_name2", NULL );
    if ( solute2_name != NULL ) {
      solute2_mspec = m_molec_spec_find( sys, solute2_name );
      if ( solute2_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solute2_name);
        exit(1);
      }
      if ( solute2_mspec->center < 0 || solute2_mspec->center >= solute2_mspec->atoms ) {
        FATAL("The center for the solute %s must be a valid atom.\n",solute2_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solute_name in the configuration file.\n");
      exit(1);
    }

    Gk_sum = 0;
    Gk_sum2 = 0;
    rho_sum = 0;
    rho_sum2 = 0;
    dipolemoment = 0;
    dipolemoment2 = 0;
}
void frame_routine(M_system* sys) {

    M.x = 0;
    M.y = 0;
    M.z = 0;

    M2.x = 0;
    M2.y = 0;
    M2.z = 0;
 
    Mtot.x = 0;
    Mtot.y = 0;
    Mtot.z = 0;

    prec dipolemoment_sum = 0;
    prec dipolemoment_sum2 = 0;
    calc_dipoles(sys);
    int i, j;
    M_molec *molec_i, *molec_j;
    prec rho = (solvent_mspec->density);
    prec rho2 = (solute_mspec->density);
    for( i=0; i<solvent_mspec->count; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	 		     
	M = m_vector_add( M, molec_i->dipole );
	dipolemoment_sum += m_vector_mag2( molec_i->dipole );
     	

    }
    for( j=0; j<solute_mspec->count; j++) {
    
        molec_j = solute_mspec->molecs->data[j];

        M2 = m_vector_add( M2, molec_j->dipole );
        dipolemoment_sum += m_vector_mag2( molec_j->dipole );
    
    }

    Mtot = m_vector_add( M, M2 );  
    number = (solvent_mspec->count) + (solute_mspec->count);
    prec dipolemoment_ave = dipolemoment_sum/(solvent_mspec->count+solute_mspec->count); 
//    prec dipolemoment_ave2 = dipolemoment_sum2/solute_mspec->count;
    dipolemoment += dipolemoment_ave;
//    dipolemoment2 += dipolemoment_ave2;
  
    Gk_sum += (m_vector_mag2(Mtot))/((solvent_mspec->count+solute_mspec->count)*(dipolemoment_ave));
//    Gk_sum2 += (m_vector_mag2(M2)/(solute_mspec->count)*dipolemoment_ave2);
    rho_sum += rho;
    rho_sum += rho2;
    Counttest += number;
}

void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;
    prec dipolemoment_final = dipolemoment*f;
//    prec dipolemoment2_final = dipolemoment2*f;
    prec rho_ave = rho_sum*f;
 //   prec rho_ave2 = rho_sum2*f;
    prec Gk_ave = Gk_sum*f;
//    prec Gk_ave2 = Gk_sum2*f;
    prec count = Counttest*f;
    prec y_ave = (4*3.14159265358979323846*rho_ave*dipolemoment_final)/(9*kT);
    prec epsilon = 1+(3*y_ave*Gk_ave);
    prec gk = (((epsilon-1)*(2*epsilon+1))/(9*epsilon))*(1/y_ave);

    printf("count\t \trho1\t \tmu^2\t \tGk\t \ty\t \tepsilon\t \tgk\n");
    printf("----------------------------------------------------------------------------------------------------------\n");
    printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\n", count, rho_ave, dipolemoment_final, Gk_ave, y_ave, epsilon, gk);
    
}
