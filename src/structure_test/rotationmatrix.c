#include "comoda.h"
#include "frame_main.h"
#include "time.h"

M_n_aggregate *agg;
M_vector normal;
M_point  origin;
M_molec_spec *solvent_mspec;
prec x_ave, y_ave, z_ave;



//Quinn A. Besford
// From an input xyz coordinate generate the rotated coordinate from a rotation matrix along unit n and angle phi

char *description \
 = "Rotation matrix\n\
\n\
\tOutput format:\n\
\tr\t<count>_frame";

void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
count    = 1";
  printf("%s\n",example);
};


void frame_init(M_system* sys, dictionary* dict) {
    srand(time(NULL));
    char *solvent_name;
    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}

void frame_routine(M_system* sys) {
    calc_dipoles(sys);
    int i,k,N,frame;
    float ran_x, ran_y, ran_z, x_component_ran, y_component_ran, z_component_ran;
    prec center_x, center_y, center_z,
    center_x_p1, center_x_m1, center_y_p1, center_y_m1, center_z_p1, center_z_m1,
    Rxx, Rxy, Rxz, Ryx, Ryy, Ryz, Rzx, Rzy, Rzz,
    nx, ny, nz, alpha, atom_i_x, atom_i_y, atom_i_z,
    new_coord_x, new_coord_y, new_coord_z,
    coord_x, coord_y, coord_z, u1, dist_x, dist_y, dist_z, delta_x, delta_xy, r, phi, theta;
    M_molec *molec_i, *molec_j;
	

    frame=sys->current_frame_id;
    if(frame==0){
	
	    // Begin by calculating the total center of mass
	    // Rm = 1/M Σ m_i * r_i
	    x_ave = 0;
	    y_ave = 0;
	    z_ave = 0;
	    N = solvent_mspec->count*3; // This is for water (3 atoms). Need to adjust to an atom count
	    for(i=0;i<solvent_mspec->count;i++){
		molec_i = solvent_mspec->molecs->data[i];
		for(k=0;k<3;k++){

		        M_atom *atom_i = m_molec_atom(molec_i,k);


			atom_i_x = atom_i->pos.x;
			atom_i_y = atom_i->pos.y;	
			atom_i_z = atom_i->pos.z;

			x_ave += atom_i_x;
			y_ave += atom_i_y;
			z_ave += atom_i_z;
			}
		}
    
	    center_x = x_ave/N;
	    center_y = y_ave/N; 
	    center_z = z_ave/N;
	 // Random z
//	    ran_z = -1+2*((float)rand()/RAND_MAX);
	 // Random alpha   	    
	    alpha = ((float)rand()/RAND_MAX)*2*M_PI;
	    nx = pow((1-pow(ran_z,2)),0.5)*cos(alpha);
	    ny = pow((1-pow(ran_z,2)),0.5)*sin(alpha);
            nz = ran_z;

	    printf("%f\t%f\t%f\n",center_x, center_y, center_z);
	    exit(0);
   
	    for( i=0; i<solvent_mspec->count; i++) {
	        molec_i = solvent_mspec->molecs->data[i]; // Find the waters
		for(k=0;k<3;k++){
			
		M_atom *atom_i = m_molec_atom(molec_i,k);
	
		coord_x = atom_i->pos.x;	
		coord_y = atom_i->pos.y;
		coord_z = atom_i->pos.z;

//		printf("%f\t%f\t%f\t%f\t%f\t%f\n", coord_x, coord_y, coord_z, center_x, center_y, center_z);
//		exit(0);


//		new_coord_x = coord_x*cos(alpha) - coord_y*sin(alpha);
//		new_coord_y = coord_y*cos(alpha) + coord_x*sin(alpha);
//		new_coord_z = coord_z; // rotate around z		



	
		// Define the rotation matrix elements
		Rxx = 1-2*(pow(ny,2)+pow(nz,2))*pow(sin(0.5*alpha),2);
		Rxy = -nz*sin(alpha)+2*nx*ny*pow(sin(0.5*alpha),2);
		Rxz = ny*sin(alpha)+2*nz*nx*pow(sin(0.5*alpha),2);
		Ryx = nz*sin(alpha)+2*nx*ny*pow(sin(0.5*alpha),2);
		Ryy = 1-2*(pow(nz,2)+pow(nx,2))*pow(sin(0.5*alpha),2);
		Ryz = -nx*sin(alpha)+2*ny*nz*pow(sin(0.5*alpha),2);	
		Rzx = -ny*sin(alpha)+2*nz*nx*pow(sin(0.5*alpha),2);
		Rzy = nx*sin(alpha)+2*ny*nz*pow(sin(0.5*alpha),2);
		Rzz = 1-2*(pow(nx,2)+pow(ny,2))*pow(sin(0.5*alpha),2);


//		Rxx = cos(alpha)+pow(nx,2)*(1-cos(alpha));
//		Rxy = nx*ny*(1-cos(alpha))-nz*sin(alpha);
//		Rxz = nx*nz*(1-cos(alpha))+ny*sin(alpha);
//		Ryx = nz*nx*(1-cos(alpha))+nz*sin(alpha);
//		Ryy = cos(alpha)+pow(ny,2)*(1-cos(alpha));
//		Ryz = ny*nz*(1-cos(alpha))-nx*sin(alpha);
//		Rzx = nz*nx*(1-cos(alpha))-ny*sin(alpha);
//		Rzy = nz*ny*(1-cos(alpha))+nx*sin(alpha);
//		Rzz = cos(alpha)+pow(nz,2)*(1-cos(alpha));
		
//		dist_x = coord_x - center_x;
//		dist_y = coord_y - center_y;
//		dist_z = coord_z - center_z;



	// The matrix transformation will be of the form:
	//   |Rxx	Rxy	Rxz|  |x|
	//   |Ryx	Ryy	Ryz|  |y|
	//   |Rzx	Rzy	Rzz|  |z|
	
		new_coord_x = coord_x*Rxx + coord_y*Rxy + coord_z*Rxz;
		new_coord_y = coord_x*Ryx + coord_y*Ryy + coord_z*Ryz;
		new_coord_z = coord_x*Rzx + coord_y*Rzy + coord_z*Rzz;




		printf("%f\t%f\t%f\t%f\t%f\t%f\n", coord_x, coord_y, coord_z, new_coord_x, new_coord_y,new_coord_z);
 		}

		}

	

	}	
}
void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;
}
