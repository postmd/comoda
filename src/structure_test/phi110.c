#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;

M_vector normal;
M_point  origin;

M_molec_spec *solvent_mspec;
// Quinn A Besford
char *description \
 = "phi110.\n\
\n\
\tOutput format:\n\
\tr\t<n>\tp101\tp202\tp303\t....p909\n\
\twhere:\n\
\t  <n> is the average number of molecules found within the given slice\n\
\t  pn0n is calculated as ( Sum_{slice} Phi_n0n )/n_frames\n\
\t    Phi_n0n is a rotational invariant, e.g. Phi_101 = Sqrt[3]*cos(theta)\n\
\t    Divide pn0n by (V_{slice} * rho_{NpT}) to normalise to a correlation fn.";

prec c101, c110;
void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
[interface]\n\
normal = 1,0,0  ; vector normal to the interface\n\
origin = 0,0,0  ; coordinatess of a point of reference";

  printf("%s\n",example);
};

void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"n0n:dr",0.1); 

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;
    

    agg = m_n_aggregate_new(1, &min, &max, &width, 10);
    
    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows

    prec temp_vec[3];
    char *in;

    c101 = pow(3,0.5);
    c110 = pow(3,0.5);
    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}
void update_n0n( prec *data, prec cos_i, prec cos_j, prec cos_ij ) {

	prec phi101, phi110;
	prec alpha, alpha2, beta, beta2, alphasquare, betasquare, gamma;
	
	alpha=cos_i;
	beta=cos_j;
	alpha2=alpha*alpha;
	beta2=beta*beta;
	alphasquare=pow(1-alpha2,0.5);
	betasquare=pow(1-beta2,0.5);
	gamma = cos_ij;

	phi101=alpha;
	phi110=-alpha*beta-alphasquare*betasquare*gamma;
	
	data[0]=1.0;
	data[1]=phi101;
	data[2]=phi110;

}

void frame_routine(M_system* sys) {

    calc_dipoles(sys);

    int i, j;
    M_molec *molec_i, *molec_j;
    prec cos_i, cos_j, cos_ij;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	for( j=0; j<solvent_mspec->count; j++){
		molec_j = solvent_mspec->molecs->data[j];
		
		if ( i == j ) continue;
   
 		M_vector r_ij = m_disp(molec_i->center, molec_j->center);    
		prec r2 = m_vector_mag2(r_ij);

		if ( r2 > sys->rcut2) continue;
		
	        cos_i = -1.0*m_vector_dot(m_vector_unit(molec_i->dipole), m_vector_unit(r_ij));
		cos_j = -1.0*m_vector_dot(m_vector_unit(molec_j->dipole), m_vector_unit(r_ij));
		cos_ij = m_vector_dot(m_vector_unit(molec_i->dipole), m_vector_unit(molec_j->dipole));

		agg->key[0] = sqrt(r2);

	        update_n0n(agg->data, cos_i, cos_j, cos_ij);

	        m_n_aggregate(&agg);
	}
    }

}

void frame_finalise(M_system* sys) {
    prec f = 1.0/sys->frames_processed;

    m_n_aggregate_col_scale(agg,0,f);
    m_n_aggregate_col_scale(agg,1,c101*f);
    m_n_aggregate_col_scale(agg,2,c110*f);
    m_n_aggregate_print(agg);
}
