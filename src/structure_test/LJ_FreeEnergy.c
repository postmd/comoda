#include "comoda.h"
#include "frame_main.h"

prec sum_beta_U;
prec kT, epsilon, sigma;
M_molec_spec *solvent_mspec;

char *description \
 = "Calculates ln( < Exp(-Beta U_LJ) >_Frames ) for a simulation. \n\
\n\
\tOutput format:\n\
\t  ";

void print_routine_example() {

  char *example \
= "[denisty]\n\
T         = 298 \n\
epsilon   = 0.000247645\n\
sigma     = 5.98209\n\
";
  printf("%s\n",example);
};




void frame_init(M_system* sys, dictionary* dict) {
    prec T = iniparser_getdouble(dict,"density:T",298);
    kT = T/(4.35974434E-18/1.3806488E-23);
    epsilon = iniparser_getdouble(dict,"density:epsilon",0.000247645);
    sigma = iniparser_getdouble(dict,"density:sigma",5.98209);
    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }
    sum_beta_U = 0;
}

void frame_routine(M_system* sys) {

    calc_centers(sys);
    double exp_beta_U = 0;
    prec sum_r6 = 0;
    prec sum_r12 = 0;
    prec DU;
    int i,j;
    M_molec *molec_i, *molec_j;
    for( i=0; i<solvent_mspec->count-1; i++) {
        molec_i = solvent_mspec->molecs->data[i];
	for( j=i+1; j<solvent_mspec->count; j++) {
		molec_j = solvent_mspec->molecs->data[j];        
        	
            	M_vector r_i = m_disp(molec_i->center, molec_j->center);
		prec r_mag2 = m_vector_mag2(r_i);
		prec r_r6 = pow(r_mag2, -3.0);
		prec r_r12 = pow(r_r6, 2.0);   		

		sum_r6 += r_r6;
		sum_r12 += r_r12;
	}
    }
	
    prec U = (4*epsilon*pow(sigma, 12)*sum_r12)-(4*epsilon*pow(sigma, 6)*sum_r6);
    printf("%.10E\t%.10E\t%.10E\n",sum_r6, sum_r12, U);
//    exp_beta_U += exp(-((((U)/kT))-2000));
    
//    sum_beta_U += exp_beta_U;
    
}

void frame_finalise(M_system* sys) {
	
//	prec f = 1.0/sys->frames_processed;

//	prec ave_exp_beta_U = sum_beta_U*f;

//	printf("%.10E\n",ave_exp_beta_U);
}
