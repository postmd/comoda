#include "comoda.h"
#include "frame_main.h"

M_n_aggregate *agg;
M_vector normal;
M_point  origin;
prec ElectricField;
M_molec_spec *solvent_mspec;
//Quinn A. Besford
// Calculate the electric field as a function of distance from ions center

char *description \
 = "Calculates the total number of hydrogen bonds in a simulation averaged over frames\n\
\n\
\tOutput format:\n\
\tr\t<count>_frame";

void print_routine_example() {

  char *example \
= "[n0n]\n\
dr = 0.1     ; distance (in atomic units) of bins\n\
count    = 1";
  printf("%s\n",example);
};

M_molec_spec *ion_mspec;


void frame_init(M_system* sys, dictionary* dict) {

    prec dr = iniparser_getdouble(dict,"n0n:dr",0.1);

    prec min = 0.0;
    prec max = sys->rcut;
    prec width = dr;

    agg = m_n_aggregate_new(1, &min, &max, &width, 10);

    //m_n_aggregate_set_print_none(agg);    // turn off printing for all columns
    m_n_aggregate_set_print_all(agg);
    agg->print_count = 0;
    agg->print_empty = 1;               // print empty rows
    
              

    char *ion_name;
    ion_name = iniparser_getstring(dict, "system:ion_name", NULL );
    if ( ion_name != NULL ) {
        ion_mspec = m_molec_spec_find( sys, ion_name );
        if ( ion_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",ion_name);
            exit(1);
        }
    } else {
        FATAL("Must supply ion_name in the configuration file.\n");
        exit(1);
    }


	

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) {
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) {
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

}



void update_l(prec atom_i_ion_Mag, prec atom_i_Q, prec atom_i_mag_pow_3){

ElectricField = atom_i_Q*atom_i_ion_Mag/atom_i_mag_pow_3;

agg->key[0] = atom_i_ion_Mag;

agg->data[0]=1.0;
agg->data[1]=ElectricField;

m_n_aggregate(&agg);

}


void frame_routine(M_system* sys) {

    calc_dipoles(sys);
    prec ionQ, atom_i_Q, atom_i_ion_Mag, atom_i_mag_pow_3;
    int i,k;

    M_molec *molec_i, *molec_j;

    for( i=0; i<solvent_mspec->count; i++) {
        molec_i = solvent_mspec->molecs->data[i]; // Find the waters
	for(k=0;k<3;k++){
	
	molec_j = ion_mspec->molecs->data[0]; // Find the ion

	// THIS ASSUMES MOLECULE i CONTAINS k ATOMS!
	// Find atoms
	M_atom *atom_i = m_molec_atom(molec_i,k);
	M_atom *ion_j = m_molec_atom(molec_j,0);
	//Find charges
	ionQ = ion_j->spec->charge;
	atom_i_Q = atom_i->spec->charge;

	// Displacements
	M_vector atom_i_ion = m_disp(ion_j->pos, atom_i->pos);
	atom_i_ion_Mag = m_vector_mag(atom_i_ion);	

	// Coulomb's law

	atom_i_mag_pow_3 = pow(atom_i_ion_Mag,3);

 	agg->key[0] = atom_i_ion_Mag;
        if ( atom_i_ion_Mag < agg->min[0] || atom_i_ion_Mag > agg->max[0] ) {
            agg = m_n_aggregate_extend(agg);
        }

	update_l(atom_i_ion_Mag, atom_i_Q, atom_i_mag_pow_3);


    }

}
}
void frame_finalise(M_system* sys) {

    prec f = 1.0/sys->frames_processed;

    agg->key[0]=NAN;
    agg->data[0]=f;
    agg->data[1]=ElectricField*f;
 
   m_n_aggregate_key_scale(agg); 
  
   m_n_aggregate_print(agg);
}
