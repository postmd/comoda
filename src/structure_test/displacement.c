#include "comoda.h"
#include "frame_main.h"

int nspecies;
int natoms;

prec r2;
prec *sum_dr2_spec;
M_point *x_prev;
M_vector *x_init;
M_vector *dx, *dx_i;
M_vector dims_0;
int i,j;
int npt;

M_atom *atom_i;
M_atom_spec *aspec_j;

char *description \
 = "frame.displacement calculates the mean-squared displacement for each species of atoms.\n\
    \n\
    the mean-squared displacement for one species is calculated by:\n\
          msd = 1/N  Sum_i[ | x_i(t) - x_i(0) |^2 ]\n\
    where N is the number of atoms in a species, and i is iterated over all the atoms in N.\n\
    x_i(t) is the position vector of atom i, the displacement is calculated relative to the\n\
    first frame.\n\
    \n\
    Output format:\n\
          #frames   <msd>/bohr^2 ... \n\
";

void print_routine_example() {
    char *text \
= "[system]\n\
npt       = true      ; boolean {true, false}, default = false, optional\n\
                        ; For calculating the mean displacement, the cell dimensions is usually\n\
                        ; constant over time, since minimum-image convention is used.\n\
";
    printf("%s\n",text);

}

void frame_init(M_system* sys, dictionary* dict) {

    nspecies = sys->atom_specs->count;

    natoms = sys->atoms->count;

    npt = iniparser_getboolean(dict, "system:npt", 0);
    if (npt) {
      sys->wrap_coords = 0;
      x_init = (M_vector *) malloc(sizeof (M_vector) * natoms);
    } else {
      sys->wrap_coords = 1;
      x_prev = (M_point *) malloc(sizeof (M_point) * natoms);
      dx_i = (M_vector *) malloc(sizeof (M_vector) * natoms);
    }

    dx = (M_vector *) malloc(sizeof (M_vector) * natoms);
    for (i=0;i<sys->atoms->count;i++) {
        dx[i].x = 0;
        dx[i].y = 0;
        dx[i].z = 0;
    }

    sum_dr2_spec = (prec *) malloc(sizeof (prec) * nspecies);

}


void frame_routine(M_system* sys) {

    int nframes = sys->frames_processed;

    // clear the sum
    for (j=0;j<sys->atom_specs->count;j++) {
        sum_dr2_spec[j] = 0;
    }

    if ( npt ) {
        // if this is an NpT cell

        if ( nframes == 0 ) {
            // save the coordinates of the first cell
            for (i=0; i<sys->atoms->count; i++) {
                atom_i = m_sys_atom(sys,i);

                x_init[i].x = atom_i->pos.x;
                x_init[i].y = atom_i->pos.y;
                x_init[i].z = atom_i->pos.z;      
            }
        } else {
            // for every subsequent frame
            // calculate displacements relative to initial posiiton
            for (i=0; i<sys->atoms->count; i++) {
                atom_i = m_sys_atom(sys,i);

                M_vector pos_vec;
                pos_vec.x = atom_i->pos.x;
                pos_vec.y = atom_i->pos.y;
                pos_vec.z = atom_i->pos.z;

                dx[i] = m_vector_subtract(pos_vec,x_init[i]);
            }
        }
    } else {
        // this is not an NpT cell ==> must be constant cell dimensions

        if (nframes > 1 ) {
            // if this is not the first frame
            // then accumulate the net displacement into dx[i]
            for (i=0;i<sys->atoms->count;i++) {
                atom_i = m_sys_atom(sys,i);

                dx_i[i] = m_disp(atom_i->pos,x_prev[i]);
                dx[i] = m_vector_add(dx[i],dx_i[i]);
            }
        }

        // save the cell dimensions
        // we do this because *in case* the cell dimensions change
        // the vector routine will catch the difference
        // causing the program to stop
        // and so avoids bad numbers being spat out
        //memcpy(&dims_0,&(sys->dims),sizeof(M_vector));
        dims_0.x = sys->dims.x;
        dims_0.y = sys->dims.y;
        dims_0.z = sys->dims.z;
        
        // save the positions
        for (i=0;i<sys->atoms->count;i++) {
            atom_i = m_sys_atom(sys,i);

            x_prev[i].x = atom_i->pos.x;
            x_prev[i].y = atom_i->pos.y;
            x_prev[i].z = atom_i->pos.z;
            x_prev[i].dims = &dims_0;
        }
    }

    // calculate the per-species average displacement
    for (i=0; i<sys->atoms->count; i++) {
        r2 = m_vector_mag2(dx[i]);

        atom_i = m_sys_atom(sys,i);

        sum_dr2_spec[atom_i->spec->id] += r2;
    }

    // print the average displacement
    printf("%d\t",nframes);
    for (j=0;j<sys->atom_specs->count;j++) {
        aspec_j = m_sys_atom_spec(sys,j);
        printf("%f\t",sum_dr2_spec[j]/aspec_j->sys_total);
    }
    printf("\n");
}

void frame_finalise(M_system* sys) {
    if ( npt ) {
        free(x_init);
    } else {
        free(dx_i);
        free(x_prev);
    }
    free(dx);
    free(sum_dr2_spec);
}
