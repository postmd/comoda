#include "comoda.h"
#include "frame_main.h"

M_molec_spec *solvent_mspec;

char *description \
 = "frame.dipole calculates the dipole moment of the specified molecular species\n\
    \n\
    Output format:\n\
        id\tmu.x\tmu.y\tmu.z\t|mu|\n\
";

void print_routine_example() {
    char *text \
= "[system]\n\
solvent_name = water    ; string, must match the name for a molecular species\n\
";
    printf("%s\n",text);
}

void frame_init(M_system* sys, dictionary* dict) {

    char *solvent_name;

    solvent_name = iniparser_getstring(dict, "system:solvent_name", NULL );
    if ( solvent_name != NULL ) { 
      solvent_mspec = m_molec_spec_find( sys, solvent_name );
      if ( solvent_mspec == NULL ) { 
        FATAL("No molecules in the system matches %s.\n",solvent_name);
        exit(1);
      }   
      if ( solvent_mspec->center < 0 || solvent_mspec->center >= solvent_mspec->atoms ) {
        FATAL("The center for the solvent %s must be a valid atom.\n",solvent_name);
        exit(1);
      }   
    } else {
      FATAL("Must supply solvent_name in the configuration file.\n");
      exit(1);
    }

    sys->calc_centers = true;
    sys->calc_dipoles = true;

}


void frame_routine(M_system* sys) {

    // this routine calculates the RDFs between different species

    int i;
    for( i=0; i<solvent_mspec->count; i++ ) {
        M_molec *molec_i = solvent_mspec->molecs->data[i];

        prec mux = molec_i->dipole.x;
        prec muy = molec_i->dipole.y;
        prec muz = molec_i->dipole.z;
        prec mu_abs = molec_i->dip_mag;
	prec posit = molec_i->center.z;
        printf("%d\t%lf\t%lf\t%lf\t%lf\t%lf\n",molec_i->id,mux,muy,muz,mu_abs,posit);

    }

}

void frame_finalise(M_system* sys) {

}
