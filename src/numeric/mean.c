#include "comoda.h"

int main(int argc, char** argv) {
    if ( argc != 2 ) {
        fprintf(stderr, "%s needs exactly 1 integer arguments indicating the size of a groups.\n",argv[0]);
        fprintf(stderr, "A group size of zero will average over the entire set.\n");
    }

    int group_size = atoi(argv[1]);

    double a;

    int lines = 0;

    double va;
    va = 0;
    
    if ( group_size != 0 ) {
        while(scanf("%lf", &a) == 1) {
            va += a;
            lines++;
            if ( lines % group_size == 0 ) {
                // print the averaage for the group and rezero
                printf("%lf\n", va/group_size);
                va = 0;
                lines = 0;
            }
        }
    } else {
        while(scanf("%lf", &a) == 1) {
            va += a;
            lines++;
        } 
        // print the average for the group size is total number of lines
        printf("%lf\n", va/lines);
    }
    return 0;
}
        
                
