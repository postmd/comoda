#include "comoda.h"

double bin(double value, double width) {
  if(width <= 0) {
    return value;
  } else {
    return (floor(value/width)+0.5) * width;
  }
}

int main(int argc, char **argv) {
  double t;
  double binned;
  int i = 0;
  int num_cols = argc - 1;
  double width;
  
  while( scanf("%lf",&t) == 1) {
    width = atof(argv[i+1]); 
    binned = bin(t,width); 
    printf("%lf\t", binned);
    i++;
    if (i % num_cols == 0) {
      printf("1\n");
      i = 0;
    }
  }
  return 0;
}
