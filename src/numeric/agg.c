#include "comoda.h"

int main(int argc, char **argv) {
  int num_cols = atoi(argv[1]);
  int num_keys = atoi(argv[2]);

  int i;
  int same = 0;
  int first_line = 1;

  prec *cols;
  prec *prev;
  cols = (prec*) malloc(sizeof(prec)*num_cols);
  prev = (prec*) malloc(sizeof(prec)*num_cols);
  for (i = 0; i < num_cols; i++ ) {
    prev[i] = 0;
    cols[i] = 0;
  }

  size_t nbytes = 1024;
  char *line = malloc(nbytes + 1);
  const char *delimiters = " \t\n";

  char *p;

  while ( getline( &line, &nbytes, stdin ) > 0 ) {
    i = 0;
    p = strtok(line, delimiters);

    while( p != NULL && i < num_cols ){
      if(sscanf(p,"%lf",&cols[i])) {
        i++;
      } else {
        FATAL("supplied string at %s is not numerical.\n",p);
        exit(1);
      }
      p = strtok(NULL, delimiters);
    }

    // if the break condition was p == NULL, then i is still less than num_cols
    // fill in the rest of the columns with 0
    while( i < num_cols ) {
      cols[i] = 0;
      i++;
    }

    if ( first_line ) {
      for ( i=0; i<num_keys; i++ ) {
        prev[i] = cols[i];
      } 
      first_line = 0;
    }

    same = 0;
    for ( i=0; i<num_keys; i++ ) {
      if ( prev[i] == cols[i]) {
          same += 1;
      } 
    } 

    if ( same == num_keys ) {
      for ( i = num_keys; i < num_cols; i++ ) {
          prev[i] += cols[i];
      }
    } else {
      for ( i = 0; i < num_cols; i++) {
          printf("%lf\t",prev[i]);
          prev[i] = cols[i];
      }
      printf("\n");
    }
  }

  // print the left-overs
  for ( i = 0; i < num_cols; i++ ) {
      printf("%lf\t",prev[i]);
  }
  printf("\n");
  return 0;
}
