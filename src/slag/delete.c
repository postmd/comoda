#include "comoda.h"
#include "frame_main.h"

M_parray *c_pool;

FILE     *outfile = NULL;

enum match_type { ALL, NAME, ID };

int nspecies;
prec setT, beta;
prec *smallsig;
prec *ss12;
prec *B;
prec *abm;
prec *B2;
prec *abm2;
prec *C6;
prec *C8;
prec *exclusion_r;
prec *exclusion_r2;

char *description \
 = "slag.delete deletes atom according to its species\n\
    Output format:\n\
      id(specified)   exp(-beta dU)   (beta dU)\n\
";

void print_routine_example() {
    char *text \
= "[delete]\n\
; first, specify the central species of atom/mol(s)\n\
c_match = name      ; keywords, default = name, optional\n\
                      ; find matches based on name or ID?\n\
                      ; Allowed keywords:\n\
                      ; {a*, A*} = match all\n\
                      ; {n*, N*} = match names\n\
                      ; {i*, I*} = match IDs\n\
c_query = oxide     ; keywords (comma separated), required if c_match!=all\n\
                      ; \"spec_A,spec_B,spec_C\" matches the name of molecules\n\
                      ; \"0,1,2,3,10\" matches the id of the molecules\n\
dU_file  = ff.dU    ;\n\
";
    printf("%s\n",text);
}

M_parray* build_pool(M_system* sys, enum match_type m, void *list, int list_len) {
    int i,j;

    M_parray *pool = m_parray_new(0);

    if ( m == ALL ) {

        // if we are adding all of the molecules, then push all elements
        // we ignore the list and its length (3rd & 4th argument)
        for (i=0; i<sys->molecs->count; i++) {
            m_parray_push(pool, m_sys_molec(sys, i));
        }

    } else {

        if ( list == NULL ) {
            FATAL("Invalid pointer list == NULL.\n");
            exit(1);
        }

        if ( m == ID ) {

            // if we are adding a list of IDs
            // first cast the list into a list of integers
            M_uint *ids = (M_uint *) list;

            // loop over the list
            for (i=0; i<list_len; i++) {
                // retrieve the molecule and push it to the pool
                M_molec *el = m_sys_molec(sys, ids[i]);
                m_parray_push(pool, el);
            }

        } else {

            // if it is a list of names
            // we must then be operating on a list of species
            // first declare a list of (M_molec_spec*)
            M_molec_spec **specs = malloc(sizeof(M_molec_spec *)*list_len);
            // now cast the input list into a list of strings (char*)
            char **names = (char **) list;

            // loop over the list
            for (i=0; i<list_len; i++) {

                // search for the spec by name
                specs[i] = m_molec_spec_find(sys, names[i]);

                // if a spec has not been found, a NULL pointer will be returned
                if ( specs[i] == NULL ) {
                    FATAL("Species not found : %s. Aborting.\n",names[i]);
                    exit(1);
                }

                for (j=0; j<specs[i]->molecs->count; j++) {
                    m_parray_push(pool, specs[i]->molecs->data[j]);
                }
                
            }
            free(*names);
        }

        // free the list
        free(list);

    }

    //for ( i=0; i<pool->count; i++ ) {
    //    M_molec *a = pool->data[i];
    //    DEBUG("%d : %d\n",i,a->id);
    //}

    return pool;
}

enum match_type parse_match_type(const char *in) {
    if ( in != NULL ) {
        if ( in[0]=='n' || in[0]=='N') {
            return NAME;
        } else if ( in[0]=='i' || in[0]=='I' ) {
            return ID;
        } else if ( in[0]=='a' || in[0]=='A' ) {
            return ALL;
        }
    } else {
        FATAL("Input string is NULL.\n");
        exit(1);
    }
    return 0;
}

void frame_init(M_system* sys, dictionary* dict) {

    char *in;
    enum match_type c_match;
    int list_len;
    void *list = NULL;
    int i;

    in = iniparser_getstring(dict, "delete:c_match", "all");
    c_match = parse_match_type(in);

    if ( c_match != ALL ) {
        in = iniparser_getstring(dict, "delete:c_query", NULL);
        if ( in != NULL ) {
            const char del[1] = ",";
            list_len = count_n_fields(in, del, 1);
            
            if ( c_match == ID ) {
                list = malloc(sizeof(M_uint)*list_len);
                parse_uiarray(list, in, list_len);
            } else {
                list = malloc(sizeof(char*)*list_len);
                parse_strarray(list, in, list_len);
            }
        } else {
            FATAL("A c_query must be specified for the c_match chosen. Aborting.\n");
            exit(1);
        }
    } else {
        list = NULL;
        list_len = 0;
    }
    c_pool = build_pool(sys, c_match, list, list_len);

    in = iniparser_getstring(dict, "delete:dU_file", NULL);
    if ( in != NULL ) {
        outfile = fopen(in,"w");
    } else {
        outfile = stdout;
    }

    setT = iniparser_getdouble(dict, "system:setT", 0.0);
    if ( !setT ) {
        FATAL("system:setT is required. Aborting...\n");
        exit(1);
    }
    beta = 1.0/setT;

    nspecies = sys->atom_specs->count;

    exclusion_r = (prec *) malloc(sizeof(prec) * nspecies);
    exclusion_r2  = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(exclusion_r, nspecies);
    clear_farray(exclusion_r2, nspecies);

    smallsig = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(smallsig, nspecies);

    ss12 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(ss12, nspecies);

    B = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(B, nspecies);

    abm = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(abm, nspecies);

    B2 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(B2, nspecies);

    abm2 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(abm2, nspecies);

    C6 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(C6, nspecies);

    C8 = (prec *) malloc(sizeof(prec) * nspecies);
    clear_farray(C8, nspecies);

    in = iniparser_getstring(dict, "insert:exclusion_r", NULL );
    if ( in != NULL ) {
        parse_csv_farray( exclusion_r, in, nspecies );
    }
    for (i=0; i<nspecies; i++) {
        exclusion_r2[i] = pow(exclusion_r[i],2.0);
    }

    in = iniparser_getstring(dict, "insert:smallsig", NULL);
    if ( in != NULL ) {
        parse_csv_farray( smallsig, in, nspecies );
    }    

    for( i=0; i<nspecies; i++ ) {
        ss12[i] = pow(smallsig[i],12);
    }    

    in = iniparser_getstring(dict, "insert:B", NULL);
    if ( in != NULL ) {
        parse_csv_farray( B, in, nspecies );
    }    

    in = iniparser_getstring(dict, "insert:abm", NULL);
    if ( in != NULL ) {
        parse_csv_farray( abm, in, nspecies );
    }    

    in = iniparser_getstring(dict, "insert:B2", NULL);
    if ( in != NULL ) {
        parse_csv_farray( B2, in, nspecies );
    }    

    in = iniparser_getstring(dict, "insert:abm2", NULL);
    if ( in != NULL ) {
        parse_csv_farray( abm2, in, nspecies );
    }    

    in = iniparser_getstring(dict, "insert:C6", NULL);
    if ( in != NULL ) {
        parse_csv_farray( C6, in, nspecies );
    }    

    in = iniparser_getstring(dict, "insert:C8", NULL);
    if ( in != NULL ) {
        parse_csv_farray( C8, in, nspecies );
    }

}

prec U_ij( prec r2, int spc_id, M_atom *atom, M_system* sys) {
  prec deltaU = 0; 
  prec r_r2, r_r6, r_r8, r_r12;
  prec r = 0; 

  if ( B[spc_id] ) {
    if ( abm[spc_id] ) {
      r = sqrt(r2);
      deltaU += B[spc_id] * exp(-1.0 * abm[spc_id] * r);
    } else {
      deltaU += B[spc_id];
    }    
  }
  if ( B2[spc_id] ) {
    if ( abm2[spc_id] ) {
      r = sqrt(r2);
      deltaU += B2[spc_id] * r * exp(-1.0 * abm2[spc_id] * r);
    } else {
      deltaU += B2[spc_id];
    }    
  }

  if ( atom->spec->charge ) {
    if ( ! r ) {
      r = sqrt(r2);
    }    
    M_ewald* new = m_ewald_calc_del(sys, atom);
    prec U_isolated = m_ewald_isolated(sys, atom->spec->charge);
    prec deltaU_coulomb = (new->U_coulomb + U_isolated) - sys->ewald->U_coulomb;
    deltaU += deltaU_coulomb;
    m_ewald_free(new);
  }

  r_r2 = 1.0/r2;
  r_r6 = pow(r_r2, 3);
  r_r8 = r_r2 * r_r6;
  r_r12 = r_r6 * r_r6; 

  deltaU += ss12[spc_id] * r_r12 - C6[spc_id] * r_r6 - C8[spc_id] * r_r8;

  if ( deltaU < -100 ) {
      DEBUG("%lf\t%lf\n",sqrt(r2),deltaU);
  }

  return deltaU;
}

void frame_routine(M_system* sys) {

    calc_centers(sys);

    int i;
    for (i=0; i<c_pool->count; i++ ) {

        M_molec *c_mol = (M_molec *) c_pool->data[i];
        prec U_i = 0;
        prec pr = 1.0;

        int j;
        for ( j=0; j<sys->atoms->count; j++ ) {
            M_atom *a = m_sys_atom(sys, j);
            if ( a->molec->id == c_mol->id ) continue;

            prec r2 = m_vector_mag2(m_disp(a->pos,c_mol->center));
            if ( r2 < exclusion_r2[a->spec->id] ) {
                pr = 0;
                U_i = NAN;
                break;
            } else if ( r2 < sys->rcut2 ) {
                U_i += U_ij(r2, a->spec->id, a, sys);
            }
        }

        if ( pr > 0 ) {
            pr = exp(-1.0*beta*U_i);
        }

        fprintf(outfile,"%d\t%.12e\t%.12e\n",c_mol->id,pr,U_i*beta);
        fflush(outfile);

    }

}

void frame_finalise(M_system* sys) {

    if ( outfile != stdout && outfile != NULL ) {
        fclose(outfile);
    }
    m_parray_free(c_pool);
    free(smallsig);
    free(ss12);
    free(B);
    free(abm);
    free(C6);
    free(C8);
    free(exclusion_r);
    free(exclusion_r2);
}

