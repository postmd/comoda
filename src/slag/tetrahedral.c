#include "comoda.h"

char *description \
 = "frame.tetrahedral\n\
    \n\
      calculates the distribution of the tetrahedral order parameter before and after\n\
      solute insertion. The order parameter about molecule j is given by:\n\
    \n\
          q_j = 1 - 3/8 Sum_{i=1}^{3} Sum_{k=i+1}^{4} ( Cos(Theta_ijk) + 1/3 )^2\n\
    \n\
";

void print_routine_example() {
    char *text \
= "[tetrahedral]\n\
dq              = 0.01      ; bin size for tetrahedral order parameter\n\
center_spec     = silicon   ; name of the molecular species for 'j'\n\
cage_spec       = oxide     ; name of the molecular species for 'i' and 'k'\n\
";

    printf("%s\n",text);
}

M_n_aggregate *tetrahedral;

M_molec_spec *center_mspec, *cage_mspec;

M_neighbours ** nearest_neighbours;

void frame_init(M_system* sys, dictionary* dict) {

    prec *width = malloc(sizeof(prec) * 1);
    prec *min = malloc(sizeof(prec) * 1);
    prec *max = malloc(sizeof(prec) * 1);

    // The tetrahedral order parameter is summed over 6 pairs of nearest neighbours
    // q has a range of [-3.0,1.0]
    min[0] = -3.0;
    max[0] = 1.0;
    width[0] = iniparser_getdouble(dict, "tetrahedral:dq", 0.01);

    tetrahedral = m_n_aggregate_new(1, min, max, width, 1);

    free(min);
    free(max);
    free(width);

    char *cage_name;
    cage_name = iniparser_getstring(dict, "tetrahedral:cage_spec", NULL );
    if ( cage_name != NULL ) {
        cage_mspec = m_molec_spec_find( sys, cage_name );
        if ( cage_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",cage_name);
            exit(1);
        }
    } else {
        FATAL("Must supply cage_spec in the configuration file.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "tetrahedral:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * center_mspec->count);

    sys->calc_centers = true;

}

void frame_routine(M_system* sys) {

    int i,j,k;
    M_molec *molec_i, *molec_j, *molec_k;

    // clear nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 4, sys->rcut2 );
    }

    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<cage_mspec->count; j++) {
            molec_j = cage_mspec->molecs->data[j];

            if ( molec_i != molec_j ) {
              prec r2 = m_vector_mag2(m_radius(molec_i->center, molec_j->center));
              m_nearest_neighbours_add( nearest_neighbours[i], molec_j, r2);
            }
        }
    }

    // save the tetrahedral parameter into molec->dummy
    for( j=0; j<center_mspec->count; j++) {
        molec_j = center_mspec->molecs->data[j];

        prec q = 0.0;

        for( i=0; i<3; i++) {
            molec_i = nearest_neighbours[j]->molecs->data[i];

            M_vector dr_ij_hat = m_vector_unit(m_radius(molec_i->center,molec_j->center));

            for( k=i+1; k<=3; k++) {
                molec_k = nearest_neighbours[j]->molecs->data[k];

                M_vector dr_kj_hat = m_vector_unit(m_radius(molec_k->center,molec_j->center));

                //  the tetrahedral order parameter is defined as:
                //  q_j = 1 - 3/8 Sum_{i=1}^{3} Sum_{k=i+1}^{4} ( Cos(Theta_ijk) + 1/3 )^2

                prec cos_theta = m_vector_dot( dr_ij_hat, dr_kj_hat );
                q += pow( cos_theta + 1.0/3.0, 2.0 );
            }
        }

        q = 1.0 - 3.0/8.0*q;
        molec_j->dummy = q;
    }

    // free the nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }

    // aggregate p(q)
    for ( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        tetrahedral->key[0]=molec_i->dummy;
        tetrahedral->data[0]=1.0;
        m_n_aggregate(&tetrahedral);
    }
}


void frame_finalise(M_system* sys) {

  // scale as a probability density function
  //     sum p(q_i) * \delta_q = 1
  prec dE = tetrahedral->width[0];
  prec norm = sum_farray(tetrahedral->values,tetrahedral->total_length);
  m_n_aggregate_col_scale(tetrahedral, 0, (prec) 1.0/(dE * norm));

  m_n_aggregate_print(tetrahedral);
  
  m_n_aggregate_free(tetrahedral);
  
  free( nearest_neighbours );

}
