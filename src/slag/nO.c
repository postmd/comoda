#include "comoda.h"

char *description \
 = "slag.nO\n\
gives the bonding environment for oxides\n\
";

void print_routine_example() {
    char *text \
= "";
    printf("%s\n",text);
}

M_molec_spec *center_mspec;
M_neighbours ** nearest_neighbours;
prec *shell_r;
M_n_aggregate *nO;

void frame_init(M_system* sys, dictionary* dict) {

    shell_r = malloc(sizeof(prec)*sys->molec_specs->count);

    char *in = iniparser_getstring(dict, "shell:cutoff_list", 0);
    if ( in != NULL ) {
        parse_farray(shell_r,in,sys->molec_specs->count);
    } else {
        FATAL("Must supply a cutoff_list.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "shell:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * sys->molecs->count);

    sys->calc_centers = true;

    prec min[sys->molec_specs->count+1];
    prec max[sys->molec_specs->count+1];
    prec width[sys->molec_specs->count+1];

    // the first key is the coordination number
    min[0] = -0.5;
    max[0] = 4.5;
    width[0] = 1;

    // the keys are for the count of other species
    int i;
    for (i=0; i<sys->molec_specs->count; i++) {
        min[i+1] = -0.5;
        max[i+1] = 4.5;
        width[i+1] = 1;
    }

    nO = m_n_aggregate_new(sys->molec_specs->count+1, min, max, width, 1);
}

void frame_routine(M_system* sys) {

    int i,j,k;
    M_molec *molec_i, *molec_j;

    // clear nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }

    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<sys->molecs->count; j++) {
            molec_j = m_sys_molec(sys,j);

            if (molec_j->spec == center_mspec) continue;

            prec r = m_vector_mag(m_radius(molec_i->center, molec_j->center));

            if ( r < shell_r[molec_j->spec->id] ) {

                m_nearest_neighbours_append( nearest_neighbours[molec_i->id], molec_j);
                k = nearest_neighbours[molec_i->id]->molecs->count;
                nearest_neighbours[molec_i->id]->dummy[k-1] = r;

                m_nearest_neighbours_append( nearest_neighbours[molec_j->id], molec_i);
                k = nearest_neighbours[molec_j->id]->molecs->count;
                nearest_neighbours[molec_j->id]->dummy[k-1] = r;
            }
        }
    }

    // on each specie's site
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];
        M_neighbours *nn_i = nearest_neighbours[molec_i->id];

        nO->key[0] = nn_i->molecs->count;

        for (j=0; j<nn_i->molecs->count; j++) {
            molec_j = nn_i->molecs->data[j];
            nO->key[molec_j->spec->id+1]++;
        }
        nO->data[0] = 1;

        m_n_aggregate(&nO);
    }

    // free the nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

    nO->print_count = 0;
    nO->print_empty = 0;

    m_n_aggregate_print(nO);
    m_n_aggregate_free(nO);

    free( nearest_neighbours );

}
