#include "comoda.h"
#include "frame_main.h"
char *description \
 = "Calculates the dipole moment of a neutral cell:\n\
\n\
\t ~         ~\n\
\t M = Sum   x_i q_i\n\
\t      i\n\
\n\
\tOutput format:\n\
\t M.x \t M.y \t M.z \t |M|\t (all in units of bohr.charge)\n";

void print_routine_example() {
};

void frame_init(M_system* sys, dictionary* dict) {
}

void frame_routine(M_system* sys) {

    M_vector M = M_ZERO_VEC;
    int i;

    for( i=0; i<sys->atoms->count; i++) {
        M_atom *atom = m_sys_atom(sys,i);
        M = m_vector_add(M,
                         m_vector_scale(m_point_to_vector(&(atom->pos)),
                                        atom->spec->charge
                                       )
                        );
    }

    printf("%lf\t%lf\t%lf\t%lf\n",M.x,M.y,M.z,m_vector_mag(M));
}

void frame_finalise(M_system* sys) {
}
