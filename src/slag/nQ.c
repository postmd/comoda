#include "comoda.h"

char *description \
 = "slag.nQ\n\
";

void print_routine_example() {
    char *text \
= "";
    printf("%s\n",text);
}

M_molec_spec *center_mspec;
M_neighbours ** nearest_neighbours;
prec *shell_r;
M_n_aggregate *nQ;

void frame_init(M_system* sys, dictionary* dict) {

    shell_r = malloc(sizeof(prec)*sys->molec_specs->count);

    char *in = iniparser_getstring(dict, "shell:cutoff_list", 0);
    if ( in != NULL ) {
        parse_farray(shell_r,in,sys->molec_specs->count);
    } else {
        FATAL("Must supply a cutoff_list.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "shell:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * sys->molecs->count);

    sys->calc_centers = true;

    prec min[sys->molec_specs->count+2];
    prec max[sys->molec_specs->count+2];
    prec width[sys->molec_specs->count+2];

    // the first key is for the species
    min[0] = -0.5;
    max[0] = sys->molec_specs->count-0.5;
    width[0] = 1;

    // second key is for the number of bonded oxygens
    min[1] = -0.5;
    max[1] = 4.5;
    width[1] = 1;

    // the rest of the keys are for the count of other species
    int i;
    for (i=0; i<sys->molec_specs->count; i++) {
        min[i+2] = -0.5;
        max[i+2] = 4.5;
        width[i+2] = 1;
    }

    nQ = m_n_aggregate_new(sys->molec_specs->count+2, min, max, width, 1);
}

void frame_routine(M_system* sys) {

    int i,j,k;
    M_molec *molec_i, *molec_j, *molec_k;

    // clear nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }

    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<sys->molecs->count; j++) {
            molec_j = m_sys_molec(sys,j);

            if (molec_j->spec == center_mspec) continue;

            prec r = m_vector_mag(m_radius(molec_i->center, molec_j->center));

            if ( r < shell_r[molec_j->spec->id] ) {

                m_nearest_neighbours_append( nearest_neighbours[molec_i->id], molec_j);
                k = nearest_neighbours[molec_i->id]->molecs->count;
                nearest_neighbours[molec_i->id]->dummy[k-1] = r;

                m_nearest_neighbours_append( nearest_neighbours[molec_j->id], molec_i);
                k = nearest_neighbours[molec_j->id]->molecs->count;
                nearest_neighbours[molec_j->id]->dummy[k-1] = r;
            }
        }
    }

    // on each specie's site
    for ( j=0; j<sys->molecs->count; j++) {
        molec_j = m_sys_molec(sys,j);
        M_neighbours *nn_j = nearest_neighbours[molec_j->id];

        // pick out only the tetrahedral units
        if (molec_j->spec == center_mspec) continue;
        if (nn_j->molecs->count != 4) continue;

        nQ->key[0] = molec_j->spec->id;
        nQ->data[0] = 1;

        for (i=0; i<4; i++ ) {
            molec_i = nn_j->molecs->data[i];
            M_neighbours *nn_i = nearest_neighbours[molec_i->id];

            if (nn_i->molecs->count > 1) {
                // if molec_i has other nearest neighbours, it must be bridging
                nQ->key[1]++;

                // loop over its neighbours
                for (k=0; k<nn_i->molecs->count; k++) {
                    molec_k = nn_i->molecs->data[k];
                    if (molec_k->id == molec_j->id) continue;

                    // count what the secondary neighbours are
                    nQ->key[molec_k->spec->id+2]++;
                }
            }
        }
        m_n_aggregate(&nQ);
    }

    // free the nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

    nQ->print_count = 0;
    nQ->print_empty = 0;

    m_n_aggregate_print(nQ);
    m_n_aggregate_free(nQ);

    free( nearest_neighbours );

}
