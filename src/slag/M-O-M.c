#include "comoda.h"

char *description \
 = "slag.M-O-M\n\
gives the distribution of the M-O-M bonding angles\n\
";

void print_routine_example() {
    char *text \
= "";
    printf("%s\n",text);
}

M_molec_spec *center_mspec;
M_neighbours ** nearest_neighbours;
prec *shell_r;
M_n_aggregate *angles;

void frame_init(M_system* sys, dictionary* dict) {

    prec dq = iniparser_getdouble(dict, "shell:dq", 0.01);

    shell_r = malloc(sizeof(prec)*sys->molec_specs->count);

    char *in = iniparser_getstring(dict, "shell:cutoff_list", 0);
    if ( in != NULL ) {
        parse_farray(shell_r,in,sys->molec_specs->count);
    } else {
        FATAL("Must supply a cutoff_list.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "shell:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * sys->molecs->count);

    sys->calc_centers = true;

    prec width[2], min[2], max[2];

    // the first key is the bond angle cosine
    min[0] = -1.0;
    max[0] = 1.0;
    width[0] = dq;

    // out of nspecies, 1 is O, n-1 is M
    // the combinations are then (n-1)+(n-2)+....+1=n(n-1)/2
    int nspecies = sys->molec_specs->count;
    int n2 = (nspecies-1)*nspecies/2;

    // second key is for species
    min[1] = -0.5;
    max[1] = n2 - 0.5;
    width[1] = 1;

    angles = m_n_aggregate_new(2, min, max, width, 1);
}

int get_column_id(i,j,nspecies,ref_id) {
    if (i>ref_id) {
      i--;
    }
    if (j>ref_id) {
      j--;
    }
    if (i>j) {
      int temp = i;
      i = j;
      j = temp;
    }
    nspecies--;

    int out = 0;
    while (i>0) {
      out+=nspecies;
      nspecies--;
      i--;
      j--;
    }
    out+=j;

    return out;
}

void frame_routine(M_system* sys) {

    int i,j,k;
    M_molec *molec_i, *molec_j, *molec_k;

    // clear nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }

    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<sys->molecs->count; j++) {
            molec_j = m_sys_molec(sys,j);

            if (molec_j->spec == center_mspec) continue;

            prec r = m_vector_mag(m_radius(molec_i->center, molec_j->center));

            if ( r < shell_r[molec_j->spec->id] ) {

                m_nearest_neighbours_append( nearest_neighbours[molec_i->id], molec_j);
                k = nearest_neighbours[molec_i->id]->molecs->count;
                nearest_neighbours[molec_i->id]->dummy[k-1] = r;

                m_nearest_neighbours_append( nearest_neighbours[molec_j->id], molec_i);
                k = nearest_neighbours[molec_j->id]->molecs->count;
                nearest_neighbours[molec_j->id]->dummy[k-1] = r;
            }
        }
    }

    // on each specie's site
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];
        M_neighbours *nn_i = nearest_neighbours[molec_i->id];

        if ( nn_i->molecs->count < 2 ) continue;

        for (j=0; j<nn_i->molecs->count-1; j++) {
            molec_j = nn_i->molecs->data[j];

            M_vector dr_ij = m_vector_unit(
                                m_radius(molec_i->center, molec_j->center));

            for (k=j+1; k<nn_i->molecs->count; k++) {
                molec_k = nn_i->molecs->data[k];

                M_vector dr_ik = m_vector_unit(
                                    m_radius(molec_i->center, molec_k->center));

                angles->key[0] = m_vector_dot(dr_ij, dr_ik);
                angles->key[1] = get_column_id(molec_j->spec->id,
                                               molec_k->spec->id,
                                               sys->molec_specs->count,
                                               center_mspec->id);
                angles->data[0] = 1;
                m_n_aggregate(&angles);
            }
        }
    }

    // free the nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

    angles->print_count = 0;
    angles->print_0_count = 1;
    angles->print_2d_values = 1;

    m_n_aggregate_print(angles);
    m_n_aggregate_free(angles);

    free( nearest_neighbours );

}
