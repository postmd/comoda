#include "comoda.h"

char *description \
 = "slag.coordination\n\
";

void print_routine_example() {
    char *text \
= "[shell]\n\
cage_spec      = oxide\n\ 
shell_cutoff    = 4.35      ; bohr, the cutoff distance for the 1st shell\n\
                              ; usually taken as the first minimum in the RDF\n\
";

    printf("%s\n",text);
}
M_molec_spec *cage_mspec;
M_neighbours ** nearest_neighbours;
prec *shell_r;
M_uint *chained, *chains, *visit_mask, *add_mask;
M_uint *chain_specs;
M_uint *backbone_specs;

void frame_init(M_system* sys, dictionary* dict) {

    shell_r = malloc(sizeof(prec)*sys->molec_specs->count);

    char *in = iniparser_getstring(dict, "shell:cutoff_list", 0);
    if ( in != NULL ) {
        parse_farray(shell_r,in,sys->molec_specs->count);
    } else {
        FATAL("Must supply a cutoff_list.\n");
        exit(1);
    }

    char *cage_name;
    cage_name = iniparser_getstring(dict, "shell:cage_spec", NULL );
    if ( cage_name != NULL ) {
        cage_mspec = m_molec_spec_find( sys, cage_name );
        if ( cage_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",cage_name);
            exit(1);
        }
    } else {
        FATAL("Must supply cage_spec in the configuration file.\n");
        exit(1);
    }

    // a boolean list to control which species can be part of the backbone
    backbone_specs = malloc(sizeof(M_uint) * sys->molec_specs->count);
    in = iniparser_getstring(dict, "shell:backbone_specs", NULL);
    if ( in != NULL ) {
        parse_uiarray(backbone_specs, in, sys->molec_specs->count);
    } else {
        set_uiarray(backbone_specs, 1, sys->molec_specs->count);
    }
    // the "cage" spec is never counted as the backbone
    backbone_specs[cage_mspec->id] = 0;

    nearest_neighbours = malloc(sizeof(M_neighbours *) * (sys->molecs->count));

    chained = malloc(sizeof(M_uint) * cage_mspec->count);
    chains  = malloc(sizeof(M_uint) * sys->molecs->count);
    visit_mask = malloc(sizeof(M_uint) * sys->molecs->count);
    add_mask = malloc(sizeof(M_uint) * sys->molecs->count);

    sys->calc_centers = true;

}

// append some neighbours to the list
int get_chain_el(M_molec *start, M_neighbours *neighbours,
                 M_uint* visit_mask, M_uint* add_mask, M_parray *molec_list) {
    if ( visit_mask[start->id] ) {
        return 0;
    }

    visit_mask[start->id] = 1;
    int added = 0;
    int i;
    for(i=0; i<neighbours->molecs->count; i++) {
        M_molec *n = neighbours->molecs->data[i];

        if ( ! add_mask[n->id] ) {
            add_mask[n->id] = 1;
            m_parray_push(molec_list, n);
            added += 1;
        }
    }
    return added;
}

// mark a list of molecules to a particular chain_id
void mask_chain_el(M_uint chain_id, M_uint* chains, M_parray *molec_list) {
    int i = 0;
    while ( i < molec_list->count ) {
        M_molec *mol = molec_list->data[i];

        if (visit_mask[mol->id]) {
            chains[mol->id] = chain_id;
            m_parray_delete(molec_list, i);
        } else {
            i++;
        }
    }
}

void do_chain(M_molec *start, M_neighbours ** nearest_neighbours,
              M_uint chain_id, M_uint* chains, M_uint* visit_mask, M_uint* add_mask) {

    M_parray *to_add = m_parray_new(0);
    m_parray_push(to_add, start);
    add_mask[start->id] = 1;
    
    while ( to_add->count > 0 ) {

        int added = 0;
        int to_do = to_add->count;
        int i;
        for (i=0; i<to_do; i++) {
            M_molec *m = to_add->data[i];
            added += get_chain_el(m, nearest_neighbours[m->id], visit_mask, add_mask, to_add);
        }

        //DEBUG("%d\n",to_add->count);
        mask_chain_el(chain_id, chains, to_add);
    }
    //DEBUG("got here\n");

    m_parray_free(to_add);

}

void frame_routine(M_system* sys) {

    int i,j;
    M_molec *molec_i, *molec_j;

    // clear nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }
    clear_uiarray(chained,cage_mspec->count);
    set_uiarray(chains,-1,sys->molecs->count);
    clear_uiarray(visit_mask,sys->molecs->count);
    clear_uiarray(add_mask,sys->molecs->count);
    int n_chains = -1;

    // loop over backbones
    for( i=0; i<sys->molecs->count; i++) {
        molec_i = m_sys_molec(sys,i);
        if (! backbone_specs[molec_i->spec->id]) continue;

        // loop over cages
        for( j=0; j<cage_mspec->count; j++) {
            molec_j = cage_mspec->molecs->data[j];

            prec r = m_vector_mag(m_radius(molec_i->center, molec_j->center));

            // add this pair to the nearest_neighbour list
            if ( r < shell_r[molec_i->spec->id] ) {
                m_nearest_neighbours_append( nearest_neighbours[i], molec_j);
                int k = nearest_neighbours[i]->molecs->count;
                nearest_neighbours[i]->dummy[k-1] = r;

                m_nearest_neighbours_append( nearest_neighbours[molec_j->id], molec_i );
                k = nearest_neighbours[molec_j->id]->molecs->count;
                nearest_neighbours[molec_j->id]->dummy[k-1] = r;

                chained[molec_j->id_in_spec] = 1;
            }
        }
    }

    // walk over the nearest_neighbour lists to assemble a chain
    // loop over backbones
    for( i=0; i<sys->molecs->count; i++) {
        molec_i = m_sys_molec(sys,i);
        if (! backbone_specs[molec_i->spec->id]) continue;

        // if it has not been visited before, then this is a new chain
        if (visit_mask[i]) continue;
        n_chains += 1;
        do_chain(molec_i, nearest_neighbours, n_chains, chains, visit_mask, add_mask);
    }

    // first column is how many of the cage molecules participated in chains
    printf("%d",sum_uiarray(chained,cage_mspec->count));
    for (i=-1;i<=n_chains;i++) {
        M_uint count = 0;
        for (j=0; j<sys->molecs->count; j++) {
            if (chains[j]==i) {
                count+=1;
            }
        }
        printf("\t%d",count);
    }
    printf("\n");

    // free the nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

    free( nearest_neighbours );
    free(chained);
    free(chains);
    free(visit_mask);
    free(add_mask);
  
}
