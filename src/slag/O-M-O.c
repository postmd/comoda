#include "comoda.h"

char *description \
 = "slag.O-M-O\n\
gives the distribution of the O-M-O bonding angles\n\
";

void print_routine_example() {
    char *text \
= "";
    printf("%s\n",text);
}

M_molec_spec *cage_mspec;
M_neighbours ** nearest_neighbours;
prec *shell_r;
M_n_aggregate *angles;

void frame_init(M_system* sys, dictionary* dict) {

    prec dq = iniparser_getdouble(dict, "shell:dq", 0.01);

    shell_r = malloc(sizeof(prec)*sys->molec_specs->count);

    char *in = iniparser_getstring(dict, "shell:cutoff_list", 0);
    if ( in != NULL ) {
        parse_farray(shell_r,in,sys->molec_specs->count);
    } else {
        FATAL("Must supply a cutoff_list.\n");
        exit(1);
    }

    char *cage_name;
    cage_name = iniparser_getstring(dict, "shell:cage_spec", NULL );
    if ( cage_name != NULL ) {
        cage_mspec = m_molec_spec_find( sys, cage_name );
        if ( cage_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",cage_name);
            exit(1);
        }
    } else {
        FATAL("Must supply cage_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * sys->molecs->count);

    sys->calc_centers = true;

    prec width[2], min[2], max[2];

    // the first key is the bond angle cosine
    min[0] = -1.0;
    max[0] = 1.0;
    width[0] = dq;

    // second key is for species
    min[1] = -0.5;
    max[1] = sys->molec_specs->count - 0.5;
    width[1] = 1;

    angles = m_n_aggregate_new(2, min, max, width, 1);
}

void frame_routine(M_system* sys) {

    int i,j,k;
    M_molec *molec_i, *molec_j, *molec_k;

    // clear nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }

    // populate nearest neighbour
    for( i=0; i<cage_mspec->count; i++) {
        molec_i = cage_mspec->molecs->data[i];

        for( j=0; j<sys->molecs->count; j++) {
            molec_j = m_sys_molec(sys,j);

            if (molec_j->spec == cage_mspec) continue;

            prec r = m_vector_mag(m_radius(molec_i->center, molec_j->center));

            if ( r < shell_r[molec_j->spec->id] ) {

                m_nearest_neighbours_append( nearest_neighbours[molec_i->id], molec_j);
                k = nearest_neighbours[molec_i->id]->molecs->count;
                nearest_neighbours[molec_i->id]->dummy[k-1] = r;

                m_nearest_neighbours_append( nearest_neighbours[molec_j->id], molec_i);
                k = nearest_neighbours[molec_j->id]->molecs->count;
                nearest_neighbours[molec_j->id]->dummy[k-1] = r;
            }
        }
    }

    // on each specie's site
    for( i=0; i<sys->molecs->count; i++) {
        molec_i = m_sys_molec(sys,i);
        if (molec_i->spec == cage_mspec) continue;
        M_neighbours *nn_i = nearest_neighbours[molec_i->id];

        if ( nn_i->molecs->count < 2 ) continue;

        for (j=0; j<nn_i->molecs->count-1; j++) {
            molec_j = nn_i->molecs->data[j];

            M_vector dr_ij = m_vector_unit(
                                m_radius(molec_i->center, molec_j->center));

            for (k=j+1; k<nn_i->molecs->count; k++) {
                molec_k = nn_i->molecs->data[k];

                M_vector dr_ik = m_vector_unit(
                                    m_radius(molec_i->center, molec_k->center));

                angles->key[0] = m_vector_dot(dr_ij, dr_ik);
                angles->key[1] = molec_i->spec->id;
                angles->data[0] = 1;
                m_n_aggregate(&angles);
            }
        }
    }

    // free the nearest neighbour list
    for (i=0; i<sys->molecs->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

    angles->print_count = 0;
    angles->print_0_count = 1;
    angles->print_2d_values = 1;

    m_n_aggregate_print(angles);
    m_n_aggregate_free(angles);

    free( nearest_neighbours );

}
