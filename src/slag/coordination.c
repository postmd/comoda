#include "comoda.h"

char *description \
 = "slag.coordination\n\
";

void print_routine_example() {
    char *text \
= "[shell]\n\
center_spec      = oxide\n\ 
shell_cutoff    = 4.35      ; bohr, the cutoff distance for the 1st shell\n\
                              ; usually taken as the first minimum in the RDF\n\
";

    printf("%s\n",text);
}
M_molec_spec *center_mspec;
M_neighbours ** nearest_neighbours;
prec *shell_r;

void frame_init(M_system* sys, dictionary* dict) {

    shell_r = malloc(sizeof(prec)*sys->molec_specs->count);

    char *in = iniparser_getstring(dict, "shell:cutoff_list", 0);
    if ( in != NULL ) {
        parse_farray(shell_r,in,sys->molec_specs->count);
    } else {
        FATAL("Must supply a cutoff_list.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "shell:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * center_mspec->count);

    sys->calc_centers = true;

}

void frame_routine(M_system* sys) {

    int i,j;
    M_molec *molec_i, *molec_j;

    // clear nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }

    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<sys->molecs->count; j++) {
            molec_j = m_sys_molec(sys,j);

            if ( molec_i != molec_j ) {
                prec r = m_vector_mag(m_radius(molec_i->center, molec_j->center));

                if ( r < shell_r[molec_j->spec->id] ) {
                    m_nearest_neighbours_append( nearest_neighbours[i], molec_j);
                    int k = nearest_neighbours[i]->molecs->count;
                    nearest_neighbours[i]->dummy[k-1] = r;
                }
            }
        }
    }

    M_uint spec_count[sys->molec_specs->count];
    for (i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];
        printf("%d\t%d",molec_i->id,nearest_neighbours[i]->molecs->count);

        clear_uiarray(spec_count,sys->molec_specs->count);
        for (j=0; j<nearest_neighbours[i]->molecs->count; j++) {
            molec_j = nearest_neighbours[i]->molecs->data[j];
            spec_count[molec_j->spec->id]++;
        }
        for (j=0; j<sys->molec_specs->count; j++) {
            printf("\t%d",spec_count[j]);
        }
        printf("\n");
    }
        

    // free the nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

  free( nearest_neighbours );

}
