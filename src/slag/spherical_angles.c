#include "comoda.h"

char *description \
 = "slag.spherical_angles\n\
    \n\
    prints the spherical angles for all particles in the first shell\n\
";

void print_routine_example() {
    char *text \
= "[shell]\n\
center_spec     = silicon   ; name of the molecular species for 'j'\n\
cage_spec       = oxide     ; name of the molecular species for 'i' and 'k'\n\
shell_cutoff    = 4.35      ; bohr, the cutoff distance for the 1st shell\n\
                              ; usually taken as the first minimum in the RDF\n\
";

    printf("%s\n",text);
}

M_molec_spec *center_mspec, *cage_mspec;

M_neighbours ** nearest_neighbours;
prec shell_r2,shell_recip;

void frame_init(M_system* sys, dictionary* dict) {

    prec shell = iniparser_getdouble(dict, "shell:shell_cutoff", 0);
    if ( ! shell > 0 ) {
        FATAL("Must spply a shell_cutoff > 0.");
        exit(1);
    }
    shell_r2 = shell*shell;
    shell_recip = 1.0/shell;

    char *cage_name;
    cage_name = iniparser_getstring(dict, "shell:cage_spec", NULL );
    if ( cage_name != NULL ) {
        cage_mspec = m_molec_spec_find( sys, cage_name );
        if ( cage_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",cage_name);
            exit(1);
        }
    } else {
        FATAL("Must supply cage_spec in the configuration file.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "shell:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * center_mspec->count);

    sys->calc_centers = true;

}

void frame_routine(M_system* sys) {

    int i,j;
    M_molec *molec_i, *molec_j;

    // clear nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }

    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<cage_mspec->count; j++) {
            molec_j = cage_mspec->molecs->data[j];

            if (molec_j->spec == molec_i->spec) continue;

            if ( molec_i != molec_j ) {
                prec r2 = m_vector_mag2(m_radius(molec_i->center, molec_j->center));

                if ( r2 < shell_r2 ) {
                    m_nearest_neighbours_append( nearest_neighbours[i], molec_j);
                }
            }
        }
    }

    // save the distortion parameter into molec->dummy
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        M_neighbours *n = nearest_neighbours[i];

        printf("%d",n->molecs->count);

        for( j=0; j<n->molecs->count; j++) {
            molec_j = n->molecs->data[j];

            M_vector dr_ij = m_radius(molec_i->center,molec_j->center);
            M_vector dr_ij_hat = m_vector_unit(dr_ij);
            // z = zenith
            // a = azimuth
            // \hat{dr} = { sin(z)cos(a), sin(z)sin(a), cos(z)
            prec zenith  = acos(dr_ij_hat.z);
            prec azimuth = atan2(dr_ij_hat.y,dr_ij_hat.x);

            printf("\t%lf\t%lf", azimuth, zenith);
        }

        printf("\n");
    }

    // free the nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

  free( nearest_neighbours );

}
