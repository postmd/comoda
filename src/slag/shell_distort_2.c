#include "comoda.h"

char *description \
 = "slag.shell_distort_2\n\
    \n\
    Calculates the distribution of |q_i|.\n\
";

void print_routine_example() {
    char *text \
= "[shell]\n\
dq              = 0.01      ; bin size for the distortion parameter\n\
center_spec     = silicon   ; name of the molecular species for 'j'\n\
cage_spec       = oxide     ; name of the molecular species for 'i' and 'k'\n\
shell_cutoff    = 4.35      ; bohr, the cutoff distance for the 1st shell\n\
                              ; usually taken as the first minimum in the RDF\n\
";

    printf("%s\n",text);
}

M_n_aggregate *distortion;

M_molec_spec *center_mspec, *cage_mspec;

M_neighbours ** nearest_neighbours;
prec shell_r2,shell_recip;

void frame_init(M_system* sys, dictionary* dict) {

    prec shell = iniparser_getdouble(dict, "shell:shell_cutoff", 0);
    if ( ! shell > 0 ) {
        FATAL("Must spply a shell_cutoff > 0.");
        exit(1);
    }
    shell_r2 = shell*shell;
    shell_recip = 1.0/shell;

    prec *width = malloc(sizeof(prec) * 2);
    prec *min = malloc(sizeof(prec) * 2);
    prec *max = malloc(sizeof(prec) * 2);

    // The first key is the distortion parameter q
    min[0] = 0;
    max[0] = 5;
    width[0] = iniparser_getdouble(dict, "shell:dq", 0.01);

    // the second key is the number of neighbours
    min[1] = 0.5;
    max[1] = 2.5;
    width[1] = 1;

    // m_n_aggregate_new( 2 keys, min, max, width, 1 data )
    distortion = m_n_aggregate_new(2, min, max, width, 1);

    free(min);
    free(max);
    free(width);

    char *cage_name;
    cage_name = iniparser_getstring(dict, "shell:cage_spec", NULL );
    if ( cage_name != NULL ) {
        cage_mspec = m_molec_spec_find( sys, cage_name );
        if ( cage_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",cage_name);
            exit(1);
        }
    } else {
        FATAL("Must supply cage_spec in the configuration file.\n");
        exit(1);
    }

    char *center_name;
    center_name = iniparser_getstring(dict, "shell:center_spec", NULL );
    if ( center_name != NULL ) {
        center_mspec = m_molec_spec_find( sys, center_name );
        if ( center_mspec == NULL ) {
            FATAL("No molecules in the system matches %s.\n",center_name);
            exit(1);
        }
    } else {
        FATAL("Must supply center_spec in the configuration file.\n");
        exit(1);
    }

    nearest_neighbours = malloc(sizeof(M_neighbours *) * center_mspec->count);

    sys->calc_centers = true;

}

void frame_routine(M_system* sys) {

    int i,j,k;
    M_molec *molec_i, *molec_j, *molec_k;

    // clear nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        nearest_neighbours[i] = m_nearest_neighbours_new( 0, 0 );
    }


    // populate nearest neighbour
    for( i=0; i<center_mspec->count; i++) {
        molec_i = center_mspec->molecs->data[i];

        for( j=0; j<cage_mspec->count; j++) {
            molec_j = cage_mspec->molecs->data[j];

            if ( molec_i != molec_j ) {
                prec r2 = m_vector_mag2(m_radius(molec_i->center, molec_j->center));

                if ( r2 < shell_r2 ) {
                    m_nearest_neighbours_append( nearest_neighbours[i], molec_j);
                }
            }
        }
    }

    // save the distortion parameter into molec->dummy
    for( j=0; j<center_mspec->count; j++) {
        molec_j = center_mspec->molecs->data[j];

        M_neighbours *n = nearest_neighbours[j];

        prec q = 0.0;

        for( i=0; i<n->molecs->count-1; i++) {
            molec_i = n->molecs->data[i];

            M_vector dr_ij_hat = m_vector_unit(m_radius(molec_i->center,molec_j->center));

            for( k=i+1; k<n->molecs->count; k++ ) {
                molec_k = n->molecs->data[k];

                M_vector dr_kj_hat = m_vector_unit(m_radius(molec_k->center,molec_j->center));

                // the new order parameter is defined as:
                // q_j = 2/(N(N-1)) Sum_{i<k}^N (Cos(Theta_ijk) + 1)^2
                // where N(N-1)/2 is the total number of pairs
                // and 0<q<4

                prec cos_theta = m_vector_dot(dr_ij_hat, dr_kj_hat);
                //q += pow( cos_theta + 1.0, 2.0 );
                //q += cos_theta + 1.0;
                q += acos( cos_theta )/pi;

            }
        }

        q = q * 2.0/(n->molecs->count * (n->molecs->count-1));
        molec_j->dummy = q;

        distortion->key[0]=q;
        distortion->key[1]=(prec)n->molecs->count;
        distortion->data[0]=1.0;
        m_n_aggregate(&distortion);
    }

    // free the nearest neighbour list
    for (i=0; i<center_mspec->count; i++) {
        m_nearest_neighbours_free(nearest_neighbours[i]);
    }
}


void frame_finalise(M_system* sys) {

  distortion->print_count = 0;
  distortion->print_0_count = 1;
  distortion->print_2d_values = 1;

  m_n_aggregate_print(distortion);
  
  m_n_aggregate_free(distortion);
  
  free( nearest_neighbours );

}
