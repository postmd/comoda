#include "comoda.h"
#include "math.h"

int main () {

  M_system *s = m_system_new();

  M_molec_spec *A, *B, *C, *D;
  
  A = m_system_add_molec_spec (s, "A", 250);
  m_system_add_atom_spec (s, "A", "A", "A", 1, 0, 0, 2);
  B = m_system_add_molec_spec (s, "B", 250);
  m_system_add_atom_spec (s, "B", "B", "B", 1, 0, 0, 1);
  C = m_system_add_molec_spec (s, "C", 250);
  m_system_add_atom_spec (s, "C", "C", "C", 1, 0, 0, -2);
  D = m_system_add_molec_spec (s, "D", 250);
  m_system_add_atom_spec (s, "D", "D", "D", 1, 0, 0, -1);

  m_sys_build(s);

  M_atom *atom_i, *atom_j;
  M_vector dipole, M, M1, M2;
  
  int i;
  //prec M2onN;

  while ( m_read_frame_stdin_xyz(s) ) {

    M.x=0;
    M.y=0;
    M.z=0;

    M1.x=0;
    M1.y=0;
    M1.z=0;

    for (i=0; i<A->molecs->count; i++) {
      atom_i = m_spec_molec(A,i)->atoms->data[0];
      atom_j = m_spec_molec(C,i)->atoms->data[0];
      dipole = m_vector_scale(m_radius(atom_j->pos,atom_i->pos), atom_i->spec->charge);

      M1 = m_vector_add( M1, dipole );
    }

    M2.x=0;
    M2.y=0;
    M2.z=0;

    for (i=0; i<B->molecs->count; i++) {
      atom_i = m_spec_molec(B,i)->atoms->data[0];
      atom_j = m_spec_molec(D,i)->atoms->data[0];
      dipole = m_vector_scale(m_radius(atom_j->pos,atom_i->pos), atom_i->spec->charge);

      M2 = m_vector_add( M2, dipole );
    }
    M = m_vector_add( M1, M2 );

    //M2onN = m_vector_mag2( M )/(A->molecs->count + B->molecs->count);
    printf("%f\t%f\t%f\n",m_vector_mag2(M), m_vector_mag2(M1), m_vector_mag2(M2));
  }

  return 0;
}
