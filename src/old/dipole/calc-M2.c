#include "comoda.h"
#include "math.h"

int main () {

  M_molec_spec *oxygens, *h1s, *h2s, *phobe;

  M_system *s = m_system_new();

  oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  
  m_sys_build(s);

  s->rcut = 20.0; // in bohrs

  M_atom *oxygen, *ha, *hb;
  M_vector dipole, M;
  
  int frames;
  int i,j;
  prec M2onN;

  frames = 0;

  while ( m_read_frame_stdin_xyz(s) ) {

    M.x=0;
    M.y=0;
    M.z=0;

    for (i=0; i<oxygens->molecs->count; i++) {
      oxygen = m_spec_molec(oxygens,i)->atoms->data[0];
      ha = m_spec_molec(h1s,i)->atoms->data[0];
      hb = m_spec_molec(h2s,i)->atoms->data[0];
      dipole = m_vector_unit( m_vector_add( m_radius(oxygen->pos,ha->pos), m_radius(oxygen->pos,hb->pos) ) ); // -- --> ++

      M = m_vector_add( M, dipole );
    }

    M2onN = m_vector_mag2( M )/511;
    printf("%f\n",M2onN);

    frames += 1;
    //DEBUG("frame %d done\n",frames);
  }

  return 0;
}
