#include "comoda.h"
int num_mols;
int num_atoms;
int matrix_size;
double bin_size;
double r_cut;
vector dims;
dipole_vector matrix[20];

void calc_dipole(point *atoms1, point *atoms2) {
  int i = 0;
  vector dipole1,mu1hat,mu1p,mu1phat;
  vector dipole2,mu2hat,mu2p,mu2phat;
  vector r,rhat;
  double d;
  double alpha,alphac,alpha2,alpha3,alpha4,alpha5,alpha6,alpha7,alpha8,alpha9,
         beta,betac,beta2,
         gamma,gammac,
         ab,ab2,a2b2,acbcg,aacbbcg,ac2bc2gc,
         phi110,phi112,phi121,phi123,phi220,phi222,phi224,
         phi101,phi202,phi303,phi404,phi505,phi606,phi707,phi808,phi909;

  // get the displacement vector
  r = new_vector(atoms1[0],atoms2[0],dims);
  d = magnitude(r);
  if ( d < r_cut ) {
  
    d = floor(magnitude(r)/bin_size)*bin_size;
    
    dipole1.x = 0.0;
    dipole1.y = 0.0;
    dipole1.z = 0.0;
    dipole2.x = 0.0;
    dipole2.y = 0.0;
    dipole2.z = 0.0;
    for( i = 0; i < matrix_size; i++ ) {
      vector v;
      v = new_vector(atoms1[matrix[i].a],atoms1[matrix[i].b], dims);
      dipole1.x += v.x*matrix[i].scale;
      dipole1.y += v.y*matrix[i].scale;
      dipole1.z += v.z*matrix[i].scale;

      vector w;
      w = new_vector(atoms2[matrix[i].a],atoms2[matrix[i].b], dims);
      dipole2.x += w.x*matrix[i].scale;
      dipole2.y += w.y*matrix[i].scale;
      dipole2.z += w.z*matrix[i].scale;
    }

    rhat = unit(r);

    // calculate the zenith cosines alpha=cos(theta1), beta=cos(theta2)
    mu1hat = unit(dipole1);
    mu2hat = unit(dipole2);
    alpha = dot(rhat,mu1hat);
    beta = dot(rhat,mu2hat);    
    
    // project the dipole moments perpendicular to the displacement
    // mu_p = mu_hat - (mu_hat . r_hat) * r_hat
    mu1p.x = mu1hat.x-rhat.x*alpha;
    mu1p.y = mu1hat.y-rhat.y*alpha;
    mu1p.z = mu1hat.z-rhat.z*alpha;
    mu2p.x = mu2hat.x-rhat.x*beta;
    mu2p.y = mu2hat.y-rhat.y*beta;
    mu2p.z = mu2hat.z-rhat.z*beta;
    mu1phat = unit(mu1p);
    mu2phat = unit(mu2p);
    
    // calculate the azimuthal cosine gamma=cos(phi1-phi2)
    gamma = dot(mu1phat,mu2phat);

    // A few common expressions
    alpha2 = alpha * alpha;
    alphac = pow((1.0 - alpha2),0.5);
    beta2 = beta * beta;
    betac = pow((1.0-beta2),0.5);
    gammac = 2.0*gamma*gamma - 1.0;

    // A few more complex quantities
    ab=alpha*beta;
    ab2=ab*beta;
    a2b2=ab2*alpha;
    acbcg=alphac*betac*gamma;
    aacbbcg=ab*acbcg;
    ac2bc2gc=(1.0-alpha2)*(1.0-beta2)*gammac;

    alpha3=alpha2*alpha;
    alpha4=alpha3*alpha;
    alpha5=alpha4*alpha;
    alpha6=alpha5*alpha;
    alpha7=alpha6*alpha;
    alpha8=alpha7*alpha;
    alpha9=alpha8*alpha;

    // Generate the rotational invariants
    phi110=-ab-acbcg;
    phi112=2.0*ab-acbcg;
    phi121=alpha-3.0*ab2-3.0*beta*acbcg;
    phi123=-alpha+3.0*ab2-2.0*beta*acbcg;
    phi220=1.0-3.0*alpha2-3.0*beta2+9.0*a2b2+12.0*aacbbcg+3.0*ac2bc2gc;
    phi222=1.0-3.0*beta2+alpha2*(-3.0+9.0*beta2)+6.0*aacbbcg-3.0*ac2bc2gc;
    phi224=1.0-3.0*alpha2-3.0*beta2+9.0*a2b2-8.0*aacbbcg+0.5*ac2bc2gc;

    phi101=alpha;
    phi202=-1.0+3.0*alpha2;
    phi303=-3.0*alpha+5.0*alpha3;
    phi404=3.0-30.0*alpha2+35.0*alpha4;
    phi505=15.0*alpha-70.0*alpha3+63.0*alpha5;
    phi606=-5.0+105*alpha2-315.0*alpha4+231.0*alpha6;
    phi707=-35.0*alpha+315.0*alpha3-693.0*alpha5+429.0*alpha7;
    phi808=35.0-1260.0*alpha2+6930.0*alpha4-12012.0*alpha6+6435.0*alpha8;
    phi909=315.0*alpha-4620.0*alpha3+18018.0*alpha5-25740.0*alpha7+12155.0*alpha9;
    
    printf("%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",
      d,1.0,phi110,phi112,phi121,phi123,phi220,phi222,phi224,
      phi101,phi202,phi303,phi404,phi505,phi606,phi707,phi808,phi909);
  }
}

int main(int argc, char **argv) {
  if ( !get_num_mols(&num_mols) ){ return 1; }
  if ( !get_box_dim(&dims) ){ return 1; }
  if ( !get_num_atoms(&num_atoms) ) { return 1; }
  if ( !get_bin_size(&bin_size) ) { return 1; }
  if ( !get_r_cut(&r_cut) ) { return 1; }
  matrix_size = get_dipole_matrix( matrix);
  if ( matrix_size == 0) { return 1; }

  //total number of solvents
  int solv_atoms = 0;
  
  // counters and temporary variables
  double x,y,z;
  int atoms = 0;
  int frames = 0;
  int dummy = 0;

  // some indices used in loops
  int i = 0;
  int j = 0;
  int O,H1,H2 ;
  point water1[3];
  point water2[3];

  // array of positions
  point p[1536];

  // numsites
  solv_atoms = num_atoms * num_mols;

  // read input
  while(scanf("x\t%d\t%lf\t%lf\t%lf\n",&dummy,&x,&y,&z) == 4 ) {
    // store data
    p[atoms].x = x;
    p[atoms].y = y;
    p[atoms].z = z;
    atoms++;
    // a whole frame has been read
    if( atoms == solv_atoms ) {
      // loop over atoms
      for (i=0;i<num_mols;i++) {
        O = i;
        H1 = i+num_mols;
        H2 = H1+num_mols;
        water1[0] = p[O];
        water1[1] = p[H1];
        water1[2] = p[H2];
        // loop over every other atom (unique pairs)
        for (j=i+1;j<num_mols;j++) {
          O = j;
          H1 = j+num_mols;
          H2 = H1+num_mols;
          water2[0] = p[O];
          water2[1] = p[H1];
          water2[2] = p[H2];

          // calculate dipolar correlations
          calc_dipole(water1,water2);
        }
      }
      frames++;
      atoms=0;
      fprintf(stderr,"stderr - Progress: %d frames done.\n",frames);

    }
  }
  return 1;
}
