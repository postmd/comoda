#include "comoda.h"
#include "math.h"

int main () {

  M_molec_spec *oxygens, *h1s, *h2s, *phobe;

  M_system *s = m_system_new();

  oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  
  phobe = m_system_add_molec_spec (s, "phobe", 1);
  m_system_add_atom_spec (s, "phobe", "phobe", "p", 1, 0, 0, 0);

  m_sys_build(s);

  s->rcut = 20.0; // in bohrs

  M_atom *oxygen1, *ha1, *hb1, *oxygen2, *ha2, *hb2, *solute;
  M_vector dipole1, dipole2, rvec, rhat;

  M_aggregate *agg = m_aggregate_new(0.0, s->rcut, 0.1, 2);
  
  int frames;
  int i,j;
  prec r, D, prefactor;

  frames = 0;
  prec sum_rho_w = 0;
  prec sum_rho_h = 0;

  while ( m_read_frame_stdin_xyz(s) ) {

    sum_rho_w += oxygens->density;
    sum_rho_h += phobe->density;

    solute = m_spec_molec(phobe,0)->atoms->data[0];

    for (i=0; i<oxygens->molecs->count; i++) {
      oxygen1 = m_spec_molec(oxygens,i)->atoms->data[0];
      ha1 = m_spec_molec(h1s,i)->atoms->data[0];
      hb1 = m_spec_molec(h2s,i)->atoms->data[0];
      dipole1 = m_vector_unit( m_vector_add( m_radius(oxygen1->pos,ha1->pos), m_radius(oxygen1->pos,hb1->pos) ) ); // -- --> ++

      for (j=i+1;j<oxygens->molecs->count; j++) {
        oxygen2 = m_spec_molec(oxygens,j)->atoms->data[0];
        ha2 = m_spec_molec(h1s,j)->atoms->data[0];
        hb2 = m_spec_molec(h2s,j)->atoms->data[0];
        dipole2 = m_vector_unit( m_vector_add( m_radius(oxygen2->pos,ha2->pos), m_radius(oxygen2->pos,hb2->pos) ) ); // -- --> ++

        rvec = m_radius(oxygen2->pos,oxygen1->pos); // points from oxygen1-->oxygen2
        rhat = m_vector_unit(rvec);
        r = m_vector_length(rvec);

        D = 3*m_vector_dot( dipole1, rhat )*m_vector_dot( dipole2, rhat) - m_vector_dot(dipole1,dipole2);
        if (r<s->rcut) {
          agg->row[0] = r;
          agg->row[1] = D;
          m_aggregate(agg);
        }
      }
    }

    frames += 1;
    DEBUG("frame %d done\n",frames);
  }

  prefactor=2.0/( ((float) oxygens->molecs->count) * sum_rho_w ) ;
  m_aggregate_col_scale(agg,1,1.5*prefactor); // extra factor of 1.5 from Hansen & MacDonald 2nd ed, Eq (12.4.6)
  m_aggregate_col_scale_dV(agg,1);
  m_aggregate_print(agg);

  return 0;
}
