#include "comoda.h"
int num_mols;
int num_atoms;
int matrix_size;
vector dims;
dipole_vector matrix[20];
point phobe;

void calc_dipole(point *atoms) {
  int i = 0;
  vector dipole;
  vector rhat;
  vector muhat;
  vector r;
  double mod_r;
  double alpha;

  dipole.x = 0.0;
  dipole.y = 0.0;
  dipole.z = 0.0;
  for( i = 0; i < matrix_size; i++ ) {
    vector v;
    v = new_vector(atoms[matrix[i].a],atoms[matrix[i].b], dims);
    dipole.x += v.x*matrix[i].scale;
    dipole.y += v.y*matrix[i].scale;
    dipole.z += v.z*matrix[i].scale;
  }
  r = new_vector(phobe,atoms[0],dims);
  rhat = unit(r);
  muhat = unit(dipole);
  mod_r = magnitude(r);
  alpha = dot(rhat,muhat);
  
  //printf("%lf,%lf,%lf\n",dipole.x,dipole.y,dipole.z);
  printf("%lf\t%lf\n",mod_r,alpha);
}
int main(int argc, char **argv) {
  if ( !get_num_mols(&num_mols) ){ return 1; }
  if ( !get_box_dim(&dims) ){ return 1; }
  if ( !get_num_atoms(&num_atoms) ) { return 1; }
  matrix_size = get_dipole_matrix( matrix);
  if ( matrix_size == 0) { return 1; }
  double x,y,z;
  int atoms = 0;
  int i = 0;
  int solv_atoms = 0;
  int O,H1,H2 ;
  int dummy ;

  point p[1536];
  point temp_mol[3];
  
  solv_atoms = num_atoms * num_mols;
  
  scanf("x\t%d\t%lf\t%lf\t%lf\n",&dummy,&x,&y,&z); 
  phobe.x = x;
  phobe.y = y;
  phobe.z = z;
  
  while(scanf("x\t%d\t%lf\t%lf\t%lf\n",&dummy,&x,&y,&z) == 4 ) {
    p[atoms].x = x;
    p[atoms].y = y;
    p[atoms].z = z;
    atoms++;
    if( atoms == solv_atoms ) {
      atoms=0;
      for (i=0;i<num_mols;i++) {
        O = i;
        H1 = i+num_mols;
        H2 = H1+num_mols;
        temp_mol[0] = p[O];
        temp_mol[1] = p[H1];
        temp_mol[2] = p[H2];

        calc_dipole(temp_mol);
      }

    }
    //if ( atoms % num_atoms == 0 ) {
     //mol++;
     //calc_dipole(p);
     //atoms = 0;
    //}
  }
  return 1;
}
