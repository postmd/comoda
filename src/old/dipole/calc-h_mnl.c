#include "comoda.h"
#include "math.h"

int main () {

  M_molec_spec *water;

  M_system *s = m_system_new();

  water = m_system_add_molec_spec (s, "water", 511);
  m_system_add_atom_spec (s, "water", "oxygen", "O", 1, 16, 1, 0 );
  m_system_add_atom_spec (s, "water", "hydrogen", "H", 2, 1, 1, 1 );
  //m_system_add_atom_spec (s, "water", "lp", "LP", 1, 1, 1, -2 );
  water->position = SEPARATED;

  m_sys_build(s);

  s->rcut = 20.0; // in bohrs

  prec dr = s->rcut/512;

  M_atom *oxygen1, *ha1, *hb1, *lp1;
  M_atom *oxygen2, *ha2, *hb2, *lp2;
  M_vector dipole1,mu1hat,mu1p,mu1phat;
  M_vector dipole2,mu2hat,mu2p,mu2phat;
  M_vector r,rhat;
  prec d;

  prec alpha,alphac,alpha2,alpha3,alpha4,alpha5,alpha6,alpha7,alpha8,alpha9,
       beta,betac,beta2,
       gamma,gammac,
       ab,ab2,a2b2,acbcg,aacbbcg,ac2bc2gc,
       phi110,phi112,phi121,phi123,phi220,phi222,phi224,
       phi101,phi202,phi303,phi404,phi505,phi606,phi707,phi808,phi909;

  prec basis_norm[18];

  basis_norm[0]=1.0;                      // r
  basis_norm[1]=1.0;                      // g(r)
  basis_norm[2]=pow(3,0.5);               // 110
  basis_norm[3]=pow(1.5,0.5);             // 112
  basis_norm[4]=pow(1.5,0.5);             // 121
  basis_norm[5]=1.5;                      // 123
  basis_norm[6]=pow(5.0,0.5)/4.0;         // 220
  basis_norm[7]=5.0/(2.0*pow(14.0,0.5));  // 222
  basis_norm[8]=1.5*pow(5.0/14.0,0.5);    // 224
  basis_norm[9]=pow(3.0,0.5);             // 101
  basis_norm[10]=pow(5.0,0.5)/2.0;         // 202
  basis_norm[11]=pow(7.0,0.5)/2.0;        // 303
  basis_norm[12]=3.0/8.0;                 // 404
  basis_norm[13]=pow(11.0,0.5)/8.0;       // 505
  basis_norm[14]=pow(13.0,0.5)/16.0;      // 606
  basis_norm[15]=pow(15.0,0.5)/16.0;      // 707
  basis_norm[16]=pow(17.0,0.5)/128.0;     // 808
  basis_norm[17]=pow(19.0,0.5)/128.0;     // 909

  M_aggregate *agg = m_aggregate_new(0.0, s->rcut, dr, 18);
  
  int frames;
  int i,j;

  prec prefactor = 0;
  prec sum_rho_w = 0;
  prec sum_rho_h = 0;

  frames = 0;
  while ( m_read_frame_stdin_xyz(s) ) {

    sum_rho_w += water->density;

    for (i=0; i<water->molecs->count; i++) {
      oxygen1 = m_spec_molec(water,i)->atoms->data[0];
      ha1 = m_spec_molec(water,i)->atoms->data[1];
      hb1 = m_spec_molec(water,i)->atoms->data[2];
      //lp1 = m_spec_molec(water,i)->atoms->data[3];

      dipole1 = m_vector_add( m_radius(oxygen1->pos,ha1->pos), m_radius(oxygen1->pos,hb1->pos) ); // -- --> ++
      //dipole1 = m_vector_add( m_radius(lp1->pos,ha1->pos), m_radius(lp1->pos,hb1->pos) ); // -- --> ++
      mu1hat = m_vector_unit(dipole1);

      for (j=i+1;j<water->molecs->count; j++) {
        oxygen2 = m_spec_molec(water,j)->atoms->data[0];
        ha2 = m_spec_molec(water,j)->atoms->data[1];
        hb2 = m_spec_molec(water,j)->atoms->data[2];
        //lp2 = m_spec_molec(water,j)->atoms->data[3];

        r = m_radius(oxygen2->pos,oxygen1->pos); // points from oxygen1-->oxygen2
        d = m_vector_length(r);

        if ( d < s->rcut) {
          rhat = m_vector_unit(r);

          dipole2 = m_vector_add( m_radius(oxygen2->pos,ha2->pos), m_radius(oxygen2->pos,hb2->pos) ); // -- --> ++
          //dipole2 = m_vector_add( m_radius(lp2->pos,ha2->pos), m_radius(lp2->pos,hb2->pos) ); // -- --> ++
          mu2hat = m_vector_unit(dipole2);

          // calculate the zenith cosines alpha=cos(theta1), beta=cos(theta2)
          alpha = m_vector_dot(rhat,mu1hat);
          beta = m_vector_dot(rhat,mu2hat);

          // project the dipole moments perpendicular to the displacement
          // mu_p = mu_hat - (mu_hat . r_hat) * r_hat
          mu1p.x = mu1hat.x-rhat.x*alpha;
          mu1p.y = mu1hat.y-rhat.y*alpha;
          mu1p.z = mu1hat.z-rhat.z*alpha;
          mu2p.x = mu2hat.x-rhat.x*beta;
          mu2p.y = mu2hat.y-rhat.y*beta;
          mu2p.z = mu2hat.z-rhat.z*beta;
          mu1phat = m_vector_unit(mu1p);
          mu2phat = m_vector_unit(mu2p); 

          // calculate the azimuthal cosine gamma=cos(phi1-phi2)
          gamma = m_vector_dot(mu1phat,mu2phat);

          // A few common expressions
          alpha2 = alpha * alpha;
          alphac = pow((1.0 - alpha2),0.5);
          beta2 = beta * beta;
          betac = pow((1.0-beta2),0.5);
          gammac = 2.0*gamma*gamma - 1.0;

          // A few more complex quantities
          ab=alpha*beta;
          ab2=ab*beta;
          a2b2=ab2*alpha;
          acbcg=alphac*betac*gamma;
          aacbbcg=ab*acbcg;
          ac2bc2gc=(1.0-alpha2)*(1.0-beta2)*gammac;

          alpha3=alpha2*alpha;
          alpha4=alpha3*alpha;
          alpha5=alpha4*alpha;
          alpha6=alpha5*alpha;
          alpha7=alpha6*alpha;
          alpha8=alpha7*alpha;
          alpha9=alpha8*alpha;

          // Generate the rotational invariants
          phi110=-ab-acbcg;
          phi112=2.0*ab-acbcg;
          phi121=alpha-3.0*ab2-3.0*beta*acbcg;
          phi123=-alpha+3.0*ab2-2.0*beta*acbcg;
          phi220=1.0-3.0*alpha2-3.0*beta2+9.0*a2b2+12.0*aacbbcg+3.0*ac2bc2gc;
          phi222=1.0-3.0*beta2+alpha2*(-3.0+9.0*beta2)+6.0*aacbbcg-3.0*ac2bc2gc;
          phi224=1.0-3.0*alpha2-3.0*beta2+9.0*a2b2-8.0*aacbbcg+0.5*ac2bc2gc;

          phi101=alpha;
          phi202=-1.0+3.0*alpha2;
          phi303=-3.0*alpha+5.0*alpha3;
          phi404=3.0-30.0*alpha2+35.0*alpha4;
          phi505=15.0*alpha-70.0*alpha3+63.0*alpha5;
          phi606=-5.0+105*alpha2-315.0*alpha4+231.0*alpha6;
          phi707=-35.0*alpha+315.0*alpha3-693.0*alpha5+429.0*alpha7;
          phi808=35.0-1260.0*alpha2+6930.0*alpha4-12012.0*alpha6+6435.0*alpha8;
          phi909=315.0*alpha-4620.0*alpha3+18018.0*alpha5-25740.0*alpha7+12155.0*alpha9;
        
          agg->row[0] = d;
          agg->row[1] = 1.0;
          agg->row[2] = phi110;
          agg->row[3] = phi112;
          agg->row[4] = phi121;
          agg->row[5] = phi123;
          agg->row[6] = phi220;
          agg->row[7] = phi222;
          agg->row[8] = phi224;
          agg->row[9] = phi101;
          agg->row[10] = phi202;
          agg->row[11] = phi303;
          agg->row[12] = phi404;
          agg->row[13] = phi505;
          agg->row[14] = phi606;
          agg->row[15] = phi707;
          agg->row[16] = phi808;
          agg->row[17] = phi909;

          m_aggregate(agg);
        }
      }
    }

    frames += 1;
    DEBUG("frame %d done\n",frames);
  }

  prefactor=2.0/( ((float) water->molecs->count) * sum_rho_w ) ;
  for (i=1;i<18;i++) {
    m_aggregate_col_scale(agg,i,prefactor*basis_norm[i]);
    m_aggregate_col_scale_dV(agg,i);
  }
  m_aggregate_print_all(agg);

  return 0;
}
