#include "comoda.h"
#include "math.h"

int main () {

  M_molec_spec *oxygens, *h1s, *h2s, *phobe;

  M_system *s = m_system_new();

  oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  
  phobe = m_system_add_molec_spec (s, "phobe", 1);
  m_system_add_atom_spec (s, "phobe", "phobe", "p", 1, 0, 0, 0);

  m_sys_build(s);

  s->rcut = 20.0; // in bohrs

  M_atom *oxygen1, *ha1, *hb1, *oxygen2, *ha2, *hb2, *solute;
  M_vector dipole1, dipole2, rvec, rhat;

  M_aggregate *agg = m_aggregate_new(0.0, s->rcut, 0.1, 11);
  
  int frames;
  int i,j;
  prec r, g_ww_prefactor;
  prec phi101, phi202, phi303, phi404, phi505, phi606, phi707, phi808, phi909;
  prec c101, c202, c303, c404, c505, c606, c707, c808, c909;
  prec alpha, alpha2, alpha3, alpha4, alpha5, alpha6, alpha7, alpha8, alpha9;

  frames = 0;
  prec sum_rho_w = 0;
  prec sum_rho_h = 0;

  c101 = pow(3,0.5);
  c202 = pow(5,0.5)/2.0;
  c303 = pow(7,0.5)/2.0;
  c404 = 3.0/8.0;
  c505 = pow(11,0.5)/8.0;
  c606 = pow(13,0.5)/16.0;
  c707 = pow(15,0.5)/16.0;
  c808 = pow(17,0.5)/128.0;
  c909 = pow(19,0.5)/128.0;

  while ( m_read_frame_stdin_xyz(s) ) {

    sum_rho_w += oxygens->density;
    sum_rho_h += phobe->density;

    solute = m_spec_molec(phobe,0)->atoms->data[0];

    for (i=0; i<oxygens->molecs->count; i++) {
      oxygen1 = m_spec_molec(oxygens,i)->atoms->data[0];
      ha1 = m_spec_molec(h1s,i)->atoms->data[0];
      hb1 = m_spec_molec(h2s,i)->atoms->data[0];
      dipole1 = m_vector_unit( m_vector_add( m_radius(oxygen1->pos,ha1->pos), m_radius(oxygen1->pos,hb1->pos) ) ); // -- --> ++

      for (j=i+1;j<oxygens->molecs->count; j++) {
        oxygen2 = m_spec_molec(oxygens,j)->atoms->data[0];
        ha2 = m_spec_molec(h1s,j)->atoms->data[0];
        hb2 = m_spec_molec(h2s,j)->atoms->data[0];
        dipole2 = m_vector_unit( m_vector_add( m_radius(oxygen2->pos,ha2->pos), m_radius(oxygen2->pos,hb2->pos) ) );

        rvec = m_radius(oxygen2->pos,oxygen1->pos); // points from oxygen1-->oxygen2
        rhat = m_vector_unit(rvec);
        r = m_vector_length(rvec);

        alpha = m_vector_dot( dipole1, rhat );
        alpha2=alpha*alpha;
        alpha3=alpha2*alpha;
        alpha4=alpha3*alpha;
        alpha5=alpha4*alpha;
        alpha6=alpha5*alpha;
        alpha7=alpha6*alpha;
        alpha8=alpha7*alpha;
        alpha9=alpha8*alpha;

        phi101=alpha;
        phi202=-1.0+3.0*alpha2;
        phi303=-3.0*alpha+5.0*alpha3;
        phi404=3.0-30.0*alpha2+35.0*alpha4;
        phi505=15.0*alpha-70.0*alpha3+63.0*alpha5;
        phi606=-5.0+105*alpha2-315.0*alpha4+231.0*alpha6;
        phi707=-35.0*alpha+315.0*alpha3-693.0*alpha5+429.0*alpha7;
        phi808=35.0-1260.0*alpha2+6930.0*alpha4-12012.0*alpha6+6435.0*alpha8;
        phi909=315.0*alpha-4620.0*alpha3+18018.0*alpha5-25740.0*alpha7+12155.0*alpha9;

        if (r<s->rcut) {
          agg->row[0] = r; // this is the key
          agg->row[1] = 1.0; // this is the RDF
          agg->row[2] = phi101; // h101
          agg->row[3] = phi202; // h202
          agg->row[4] = phi303; // h303
          agg->row[5] = phi404; // h404
          agg->row[6] = phi505; // h505
          agg->row[7] = phi606; // h606
          agg->row[8] = phi707; // h707
          agg->row[9] = phi808; //h808
          agg->row[10] = phi909; //h909
        m_aggregate(agg);
        }

        alpha = -1.0 * m_vector_dot( dipole2, rhat );
        alpha2=alpha*alpha;
        alpha3=alpha2*alpha;
        alpha4=alpha3*alpha;
        alpha5=alpha4*alpha;
        alpha6=alpha5*alpha;
        alpha7=alpha6*alpha;
        alpha8=alpha7*alpha;
        alpha9=alpha8*alpha;

        phi101=alpha;
        phi202=-1.0+3.0*alpha2;
        phi303=-3.0*alpha+5.0*alpha3;
        phi404=3.0-30.0*alpha2+35.0*alpha4;
        phi505=15.0*alpha-70.0*alpha3+63.0*alpha5;
        phi606=-5.0+105*alpha2-315.0*alpha4+231.0*alpha6;
        phi707=-35.0*alpha+315.0*alpha3-693.0*alpha5+429.0*alpha7;
        phi808=35.0-1260.0*alpha2+6930.0*alpha4-12012.0*alpha6+6435.0*alpha8;
        phi909=315.0*alpha-4620.0*alpha3+18018.0*alpha5-25740.0*alpha7+12155.0*alpha9;

        if (r<s->rcut) {
          agg->row[0] = r; // this is the key
          agg->row[1] = 1.0; // this is the RDF
          agg->row[2] = phi101; // h101
          agg->row[3] = phi202; // h202
          agg->row[4] = phi303; // h303
          agg->row[5] = phi404; // h404
          agg->row[6] = phi505; // h505
          agg->row[7] = phi606; // h606
          agg->row[8] = phi707; // h707
          agg->row[9] = phi808; //h808
          agg->row[10] = phi909; //h909
        m_aggregate(agg);
        }

      }
    }

    frames += 1;
    DEBUG("frame %d done\n",frames);
  }
  
  m_aggregate_col_scale(agg,2,c101);
  m_aggregate_col_scale(agg,3,c202);
  m_aggregate_col_scale(agg,4,c303);
  m_aggregate_col_scale(agg,5,c404);
  m_aggregate_col_scale(agg,6,c505);
  m_aggregate_col_scale(agg,7,c606);
  m_aggregate_col_scale(agg,8,c707);
  m_aggregate_col_scale(agg,9,c808);
  m_aggregate_col_scale(agg,10,c909);

  g_ww_prefactor=1.0/( ((float) oxygens->molecs->count) * sum_rho_w );

  m_aggregate_col_scale(agg,1,g_ww_prefactor);
  m_aggregate_col_scale(agg,2,g_ww_prefactor);
  m_aggregate_col_scale(agg,3,g_ww_prefactor);
  m_aggregate_col_scale(agg,4,g_ww_prefactor);
  m_aggregate_col_scale(agg,5,g_ww_prefactor);
  m_aggregate_col_scale(agg,6,g_ww_prefactor);
  m_aggregate_col_scale(agg,7,g_ww_prefactor);
  m_aggregate_col_scale(agg,8,g_ww_prefactor);
  m_aggregate_col_scale(agg,9,g_ww_prefactor);
  m_aggregate_col_scale(agg,10,g_ww_prefactor);

  m_aggregate_col_scale_dV(agg,1);
  m_aggregate_col_scale_dV(agg,2);
  m_aggregate_col_scale_dV(agg,3);
  m_aggregate_col_scale_dV(agg,4);
  m_aggregate_col_scale_dV(agg,5);
  m_aggregate_col_scale_dV(agg,6);
  m_aggregate_col_scale_dV(agg,7);
  m_aggregate_col_scale_dV(agg,8);
  m_aggregate_col_scale_dV(agg,9);
  m_aggregate_col_scale_dV(agg,10);

  m_aggregate_print(agg);

  return 0;
}
