#include "../../comoda.h"
#include "math.h"
#include <stdbool.h>

// a modulo routine, for getting array indices with periodic boundary convention
int mod(int x, int m) {
  return (x % m + m) % m;
}

// clear a boolean array to all T
void clear_bool_array( bool *bool_array, int len) {
  int i;
  for (i=0; i<len; i++) {
        bool_array[i]=1;
  }
}

// print a boolean array in dim1 blocks of dim2xdim3 matrices
void print_bool_array( bool *bool_array, int dim1, int dim2, int dim3) {
  int i,j,k;

  for (i=0; i<dim1; i++) {
    printf("block %i\n",i);
    for (j=0; j<dim2; j++) {
      for (k=0; k<dim3; k++) {
        printf("%i\t",bool_array[k+dim3*j+dim3*dim2*i]);
      }
      printf("\n");
    }
    printf("\n");
  }
}

// add an index to an integer list, where the first element of the list is len(list)-1
int* add_occupancy( int* occupancy_at_grid, int new_oxygen) {
  int i,original_size;

  original_size=occupancy_at_grid[0]+1;

  int * new_occupancy = malloc(sizeof (int)*(original_size+1));

  for (i=0; i<original_size; i++) {
    new_occupancy[i] = occupancy_at_grid[i];
  }

  new_occupancy[original_size] = new_oxygen;
  new_occupancy[0] += 1;

  return new_occupancy;
}


// main execution block
int main ( int argc, char *argv[] ) {

  // generic iterators
  int i,j,k,l,n;
  int ii,jj,kk;
  int iii,jjj,kkk;

  // -------------------
  // LJ potentials and thermodynamic parameters
  // -------------------
  prec sspot, sspot12, c6pot, sigma, kT, beta;

  if ( argc<3 ) {
    FATAL("Missing command line arguments. 3 expected, %i supplied\n",argc-1);
    exit(1);
  }

  sscanf(argv[1],"%lf",&sspot);
  sscanf(argv[2],"%lf",&c6pot);
  sscanf(argv[3],"%lf",&kT);
  DEBUG("LJ parameters: smallsig=%lf, c6=%lf\n",sspot,c6pot);
  DEBUG("Temperature: kT=%lf\n",kT);

  // LJ potentials for Xe in SPC/E, in toyMD units
  //sspot=3.9552445663902933;
  //c6pot=155.10448037374562;
  sspot12=pow(sspot,12);
  sigma=pow(sspot12/c6pot,1.0/6.0);
  
  //kT=0.000944;
  beta=1.0/kT;

  M_molec_spec *oxygens, *h1s, *h2s;

  M_system *s = m_system_new();

  oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  
  m_sys_build(s);



  // -------------------
  // We divide the box into smaller boxes of ngrid x ngrid x ngrid
  // -------------------
  int ngrid = 50;
  int ngridpts = pow(ngrid,3);;
  M_vector dgrid;
  prec dV;


  // -------------------
  // initialize variables for excluded volume
  // -------------------
  // a boolean array with excluded volumes
  // excluded volume is defined as volume that is OUTSIDE of a certain radius of oxygen
  // excluded_v==1: the volume is part of the excluded volume
  // excluded_v==0: the volume is NOT part of the excluded volume
  bool excluded_v[ngridpts];
  // r-cutoff for excluded volume
  prec excluded_v_cutoff=0.7*sigma;
  prec excluded_v_cutoff2=pow(excluded_v_cutoff,2);;
  // size of the excluded volume, in number of grid points
  int excluded_v_ngrid[3];

  // -------------------
  // use the cell method for loop optimisation
  // -------------------
  // an array of integer arrays, each integer array containing
  // the indices of oxygen atoms located inside a grid-box
  //  
  // note that the size of 'occupancy' is ngrid^3
  // it is the 'flattened' array of a 3D matrix
  //
  // also note that the number of grids in occpancy is different to number of grids for insertion
  // it is much coarser
  int occ_ngrid = 16;
  int occ_ngridpts = pow(occ_ngrid,3);
  M_vector occ_dgrid;
  int ** occupancy = malloc(sizeof (int*)*occ_ngridpts);
  // initialize array
  for (i=0; i<occ_ngridpts; i++) {
    occupancy[i] = malloc(sizeof (int)*1);
  }
  // rcut in bohrs
  s->rcut = 20.0; 
  // size of rcut volume, in number of grid points
  int rcut_ngrid[3]; 

  // -------------------
  // some variables we will need
  // -------------------
  // we will either work with a grid point, or an oxygen atom.
  // these are the two kinds of probes.
  //
  // this is the oxygen probe
  M_atom *oxygen;
  int oxygen_array_i,occ_oxygen_array_i;
  int oxygen_grid_i[3],occ_oxygen_grid_i[3];
  // 
  // this is the grid probe
  int probe_array_i,occ_probe_array_i;
  int probe_grid_i[3],occ_probe_grid_i[3];
  M_point ri;
  // 
  // a variable to store displacement vectors
  M_vector dr;
  prec r2,r6,r12,r6_t,r12_t;
  //
  // variables to save potential energies, pr, etc.
  prec dU,pr,sum_pr,total_sum_pr;
  //
  // some counters that are incremented over the analysis
  int frames = 0;
  int n_inserts = 0;
  int n_inserts_frame;

  // -------------------
  // some variables to keep the g(r)
  // -------------------
  prec delta_r = s->rcut/512;
  M_aggregate *temp_gr = m_aggregate_new(0.0, s->rcut, delta_r, 2);
  M_aggregate *all_gr = m_aggregate_new(0.0, s->rcut, delta_r, 2);
  prec g_hw_prefactor;

  m_aggregate_reset(all_gr);

  total_sum_pr=0;

  while ( m_read_frame_stdin_xyz(s) ) {

    // initialize some variables
    n_inserts_frame = 0;
    sum_pr = 0;
    // clear the occupancy matrix
    for (i=0; i<occ_ngridpts; i++) {
      occupancy[i][0] = 0;
    }

    // prefactor for g(r)
    //  for g_{ij}(r), the prefactor is 1/(rho_i rho_j V dV nframes)
    //  in the case of phobe-oxygen
    //    rho_phobe*V = n_phobe = 1
    //    nframes = 1 (we calculate a g(r) for each insertion)
    //    ==> prefactor = 1/(rho_oxygen dV)
    g_hw_prefactor=1.0/(oxygens->density); // dV is handled by another routine

    // set the dims of the box to the dims of the grid probe
    ri.dims=&s->dims;

    // get the size of a grid
    dgrid.x=s->dims.x/ngrid;
    dgrid.y=s->dims.y/ngrid;
    dgrid.z=s->dims.z/ngrid;
    dV = dgrid.x * dgrid.y * dgrid.z;
    occ_dgrid.x=s->dims.x/occ_ngrid;
    occ_dgrid.y=s->dims.y/occ_ngrid;
    occ_dgrid.z=s->dims.z/occ_ngrid;

    // get the size of excluded volume of a oxygen, in number of grids
    excluded_v_ngrid[0]=ceil(excluded_v_cutoff/dgrid.x);
    excluded_v_ngrid[1]=ceil(excluded_v_cutoff/dgrid.y);
    excluded_v_ngrid[2]=ceil(excluded_v_cutoff/dgrid.z);

    // get the size of rcut, in number of grids
    rcut_ngrid[0]=ceil(s->rcut/occ_dgrid.x); 
    rcut_ngrid[1]=ceil(s->rcut/occ_dgrid.y); 
    rcut_ngrid[2]=ceil(s->rcut/occ_dgrid.z); 

    //DEBUG("rcut_ngrid: %i, %i, %i\n",rcut_ngrid[0],rcut_ngrid[1],rcut_ngrid[2]);

    // get the excluded volume array
    clear_bool_array(excluded_v,ngridpts);
    //print_bool_array(excluded_v,ngrid,ngrid,ngrid);

    // -------------------
    // build excluded volume and occupancy
    // -------------------
    // for each oxygen:
    //    save its occupancy
    //    subtract its excluded volume
    for (n=0; n<oxygens->molecs->count; n++) {
      oxygen = m_spec_molec(oxygens,n)->atoms->data[0];
      
      oxygen_grid_i[0] = floor(oxygen->pos.x/dgrid.x);
      oxygen_grid_i[1] = floor(oxygen->pos.y/dgrid.y);
      oxygen_grid_i[2] = floor(oxygen->pos.z/dgrid.z);
      
      //oxygen_array_i=(oxygen_grid_i[0]*ngrid*ngrid)+(oxygen_grid_i[1]*ngrid)+oxygen_grid_i[2];
      
      occ_oxygen_grid_i[0] = floor(oxygen->pos.x/occ_dgrid.x);
      occ_oxygen_grid_i[1] = floor(oxygen->pos.y/occ_dgrid.y);
      occ_oxygen_grid_i[2] = floor(oxygen->pos.z/occ_dgrid.z);

      occ_oxygen_array_i=(occ_oxygen_grid_i[0]*occ_ngrid*occ_ngrid)+(occ_oxygen_grid_i[1]*occ_ngrid)+occ_oxygen_grid_i[2];

      occupancy[occ_oxygen_array_i] = add_occupancy( occupancy[occ_oxygen_array_i], n);


      for (i=0; i < excluded_v_ngrid[0]*2+1; i++) {
        ii = mod((i + oxygen_grid_i[0] - excluded_v_ngrid[0]) , ngrid);
        ri.x = dgrid.x*(ii+0.5);
        for (j=0; j < excluded_v_ngrid[1]*2+1; j++) {
          jj = mod((j + oxygen_grid_i[1] - excluded_v_ngrid[1]) , ngrid);
          ri.y = dgrid.y*(jj+0.5);
          for (k=0; k < excluded_v_ngrid[2]*2+1; k++) {
            kk = mod((k + oxygen_grid_i[2] - excluded_v_ngrid[2]) , ngrid);
            ri.z = dgrid.z*(kk+0.5);

            probe_array_i=(ii*ngrid*ngrid)+(jj*ngrid)+kk;
            
            dr = m_radius(oxygen->pos,ri);
            r2 = m_vector_mag2(dr);

            if ( r2 < excluded_v_cutoff2) {
              excluded_v[probe_array_i]=0;
            }
          }
        }
      }
    }

   // for (j=0; j<occ_ngridpts; j++) {
   //   // print occupancy array for debugging purposes
   //   for (i=0; i<occupancy[j][0]+1; i++) {
   //     printf("%d\t",occupancy[j][i]);
   //   }
   //   printf("\n");
   //   //printf("---\n");
   // }
   // DEBUG("rcut_ngrid: %i, %i, %i\n",rcut_ngrid[0],rcut_ngrid[1],rcut_ngrid[2]);
   // return 0;
    
    // -------------------
    // perform insertions
    // -------------------
    // for each grid point:
    //    if the point is included:
    //        make insertion, calculate LJ dU, sum
    for (i=0; i<ngrid; i++) {
      ri.x = dgrid.x*(i+0.5);
      occ_probe_grid_i[0] = floor(ri.x/occ_dgrid.x);
      for (j=0; j<ngrid; j++) {
        ri.y = dgrid.y*(j+0.5);
        occ_probe_grid_i[1] = floor(ri.y/occ_dgrid.y);
        for (k=0; k<ngrid; k++) {
          ri.z = dgrid.z*(k+0.5);
          occ_probe_grid_i[2] = floor(ri.z/occ_dgrid.z);
          
          probe_array_i=i*ngrid*ngrid+j*ngrid+k;

          // excluded_v == 0 : this is NOT excluded volume. do not make insertion
          // excluded_v == 1 : this is excluded volume. make insertion here
          if ( ! excluded_v[probe_array_i] ) {

            pr = 0;

          } else {

            m_aggregate_reset(temp_gr);

            r6=0;
            r12=0;

            // loop over oxygens within the r_cut cube
            for (ii=0; ii < rcut_ngrid[0]*2+1; ii++ ) {
              iii = mod( (ii + occ_probe_grid_i[0] - rcut_ngrid[0]), occ_ngrid);
              for (jj=0; jj < rcut_ngrid[1]*2+1; jj++ ) {
                jjj = mod( (jj + occ_probe_grid_i[1] - rcut_ngrid[1]), occ_ngrid);
                for (kk=0; kk < rcut_ngrid[2]*2+1; kk++ ) {
                  kkk = mod( (kk + occ_probe_grid_i[2] - rcut_ngrid[2]), occ_ngrid);

                  //DEBUG("%i\t%i\t%i\n",iii,jjj,kkk);
                  
                  // the array index of this grid is:
                  occ_probe_array_i = (iii*occ_ngrid*occ_ngrid) + (jjj*occ_ngrid) + kkk;

                  //DEBUG("l: %i\n",occupancy[occ_probe_array_i][0]);

                  // occupancy[i]={ count, i1, i2, i3, ... }
                  for (l=1; l <= occupancy[occ_probe_array_i][0]; l++) {
                    n = occupancy[occ_probe_array_i][l];
                    oxygen = m_spec_molec(oxygens,n)->atoms->data[0];

                    dr = m_radius(oxygen->pos,ri);
                    
                    r2 = m_vector_mag2(dr);
                    r6_t = pow(r2,-3);
                    r12_t = pow(r6_t,2);

                    r6 += r6_t;
                    r12 += r12_t;

                    temp_gr->row[0] = sqrt(r2);
                    temp_gr->row[1] = 1.0;
                    m_aggregate(temp_gr);

                  }
                  
                }
              }
            }
            //DEBUG("noxygens: %i\n",noxygens);
            //DEBUG("occ_dngrid: %e, %e, %e\n",occ_dgrid.x,occ_dgrid.y,occ_dgrid.z);
            //return 0;

            //DEBUG("r6: %e, r12: %e\n",r6,r12);
            // tally up the energy
            dU = sspot12 * r12 - c6pot * r6;
            pr = exp(-1.0*beta*dU);

            // g(r) = prefactor * 1/dV * n(r)
            m_aggregate_col_scale_dV(temp_gr,1);
            m_aggregate_col_scale(temp_gr,1,g_hw_prefactor*pr);

            // <g(r)> = sum(g(r,U) * exp(-beta U))/(sum(exp(-beta U)))
            // all_gr = sum(g(r,U) * exp(-beta U))
            m_aggregate_add(all_gr,temp_gr);

            n_inserts_frame += 1;
            //return 0;
          }

          // add the pr to the pr of the frame
          sum_pr += pr;
        }
      }
    }
    //return 0;

    // keep track of sum(exp(-beta U))
    total_sum_pr += sum_pr;

    //printf("frame %d:\t sum_pr:%e,\tn_inserts_frame:%d\tn_inserts:%d\n",frames,sum_pr,n_inserts_frame,n_inserts);
    DEBUG("%d\t%e\t%e\t%d\n",frames,sum_pr*dV,total_sum_pr*dV/(frames+1),n_inserts_frame);
    fflush(stdout);

    n_inserts += n_inserts_frame;
    
    frames += 1;
  }

  m_aggregate_col_scale(all_gr,1,1.0/total_sum_pr);
  m_aggregate_print_all(all_gr);
  return 0;
}

