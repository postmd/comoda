#include "../../comoda.h"
#include "math.h"
#include <stdbool.h>

// a modulo routine, for getting array indices with periodic boundary convention
int mod(int x, int m) {
  return (x % m + m) % m;
}

// clear a boolean array to all T
void clear_bool_array( bool *bool_array, int len) {
  int i;
  for (i=0; i<len; i++) {
        bool_array[i]=1;
  }
}

// print a boolean array in dim1 blocks of dim2xdim3 matrices
void print_bool_array( bool *bool_array, int dim1, int dim2, int dim3) {
  int i,j,k;

  for (i=0; i<dim1; i++) {
    printf("block %i\n",i);
    for (j=0; j<dim2; j++) {
      for (k=0; k<dim3; k++) {
        printf("%i\t",bool_array[k+dim3*j+dim3*dim2*i]);
      }
      printf("\n");
    }
    printf("\n");
  }
}

// add an index to an integer list, where the first element of the list is len(list)-1
int* add_occupancy( int* occupancy_at_grid, int new_oxygen) {
  int i,original_size;

  original_size=occupancy_at_grid[0]+1;

  int * new_occupancy = malloc(sizeof (int)*(original_size+1));

  for (i=0; i<original_size; i++) {
    new_occupancy[i] = occupancy_at_grid[i];
  }

  new_occupancy[original_size] = new_oxygen;
  new_occupancy[0] += 1;

  return new_occupancy;
}

prec interpolate_fn( prec *data, int rows, int cols, prec dr, prec r, int out_col) {
  prec x0,x1,y0,y1;

  int i=floor(r/dr-0.5);

  if (i<rows-1 && i>=0) {
  
    y0=data[i*cols+out_col];
    y1=data[(i+1)*cols+out_col];

    prec m=(y1-y0)/dr;

    return m*(r-dr*(i+0.5))+y0;
  }
  else if (i>=rows-1 || i==-1) {
    return 0.0; // soft r-cut for r>r_max
  }
  else {
    FATAL("Requested bin %d is out of bounds: [0,%d]",i,rows-1);
    FATAL("You have requested to interpolate a function at %lf when it is defined only for %lf<x<%lf.%d\n",r,0.5*dr,(rows-0.5)*dr,i);
    exit(1);
  }
}

int read_fn_data( char *fname, prec *data, int rows, int cols ) {
  int i,j;
  prec temp;
  
  FILE *inputf = fopen(fname,"r");
  if(inputf == NULL) {
    FATAL("The input file %s does not exist.\n",fname);
    exit(1);
  }
  else {
    for (i=0; i<rows; i++ ) {
      for (j=0; j<cols-1; j++ ) {
        fscanf(inputf,"%lf\t",&temp);
        data[i*cols+j]=temp;
      }
      fscanf(inputf,"%lf\n",&temp);
      data[i*cols+j]=temp;
    }
    
    return 0;
  }
  
  return 1;
}

void print_fn_data( prec *data, int rows, int cols ) {
  int i,j;

  for (i=0; i<rows; i++ ) {
    for (j=0; j<cols-1; j++ ) {
      printf("%lf\t",data[i*cols+j]);
    }
    printf("%lf\n",data[i*cols+j]);
  }
}

// main execution block
int main ( int argc, char *argv[] ) {

  // generic iterators
  int i,j,k,l,n;
  int ii,jj,kk;
  int iii,jjj,kkk;

  // -------------------
  // LJ potentials and thermodynamic parameters
  // -------------------
  prec sspot, sspot12, c6pot, sigma, kT, beta;

  // -------------------
  // tabulated phi(r) data
  // -------------------
  char *fname = malloc(256);
  int rows = 512;
  int cols = 4;
  int out_col = 2; // third column, index=2
  prec phi_data_dr = 20.0/512;
  prec *phi_data = malloc(sizeof (prec*)*rows*cols);

  if ( argc<4 ) {
    FATAL("Missing command line arguments. 3 expected, %i supplied\n",argc-1);
    exit(1);
  }

  sscanf(argv[1],"%lf",&sspot);
  sscanf(argv[2],"%lf",&c6pot);
  sscanf(argv[3],"%lf",&kT);
  sscanf(argv[4],"%s",fname);
  DEBUG("LJ parameters: smallsig=%lf, c6=%lf\n",sspot,c6pot);
  DEBUG("Temperature: kT=%lf\n",kT);
  DEBUG("phi(r) file: %s\n",fname);

  if ( read_fn_data(fname,phi_data,rows,cols) != 0 ) {
    FATAL("An error has occurred while reading %s.\n",fname);
    exit(1); 
  }
  //print_fn_data(phi_data,rows,cols);
  prec temp_x;
  //for (i=0;i<2000;i++) {
  //  temp_x = i/100.0;
  //  DEBUG("%lf\t%lf\n",temp_x,interpolate_fn(phi_data,rows,cols,phi_data_dr,temp_x,out_col));
  //}
  //return 0; 

  // LJ potentials for Xe in SPC/E, in toyMD units
  //sspot=3.9552445663902933;
  //c6pot=155.10448037374562;
  sspot12=pow(sspot,12);
  sigma=pow(sspot12/c6pot,1.0/6.0);
  
  //kT=0.000944;
  beta=1.0/kT;

  M_molec_spec *oxygens, *h1s, *h2s;

  M_system *s = m_system_new();

  oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  
  m_sys_build(s);

  // -------------------
  // We divide the box into smaller boxes of ngrid x ngrid x ngrid
  // -------------------
  int ngrid = 50;
  int ngridpts = pow(ngrid,3);;
  M_vector dgrid;
  prec dV;

  // -------------------
  // initialize variables for excluded volume
  // -------------------
  // a boolean array with excluded volumes
  // excluded volume is defined as volume that is OUTSIDE of a certain radius of oxygen
  // excluded_v==1: the volume is part of the excluded volume
  // excluded_v==0: the volume is NOT part of the excluded volume
  bool excluded_v[ngridpts];
  // r-cutoff for excluded volume
  prec excluded_v_cutoff=0.7*sigma;
  prec excluded_v_cutoff2=pow(excluded_v_cutoff,2);;
  // size of the excluded volume, in number of grid points
  int excluded_v_ngrid[3];

  // -------------------
  // use the cell method for loop optimisation
  // -------------------
  // an array of integer arrays, each integer array containing
  // the indices of oxygen atoms located inside a grid-box
  //  
  // note that the size of 'occupancy' is ngrid^3
  // it is the 'flattened' array of a 3D matrix
  //
  // also note that the number of grids in occpancy is different to number of grids for insertion
  // it is much coarser
  int occ_ngrid = 16;
  int occ_ngridpts = pow(occ_ngrid,3);
  M_vector occ_dgrid;
  int ** occupancy = malloc(sizeof (int*)*occ_ngridpts);
  // initialize array
  for (i=0; i<occ_ngridpts; i++) {
    occupancy[i] = malloc(sizeof (int)*1);
  }
  // rcut in bohrs
  s->rcut = 20.0; 
  // size of rcut volume, in number of grid points
  int rcut_ngrid[3]; 

  // -------------------
  // some variables we will need
  // -------------------
  // we will either work with a grid point, or an oxygen atom.
  // these are the two kinds of probes.
  //
  // this is the oxygen probe
  M_atom *oxygen;
  int oxygen_array_i,occ_oxygen_array_i;
  int oxygen_grid_i[3],occ_oxygen_grid_i[3];
  // 
  // this is the grid probe
  int probe_array_i,occ_probe_array_i;
  int probe_grid_i[3],occ_probe_grid_i[3];
  M_point ri;
  // 
  // a variable to store displacement vectors
  M_vector dr;
  prec r2,r6,r12,r6_t,r12_t;
  //
  // variables to save potential energies, pr, etc.
  prec dU,pr,sum_pr;
  prec total_sum_pr = 0;
  //
  // g-force mu
  M_atom *oxygen1, *oxygen2;
  prec mu_pure,mu_solute;
  prec total_mu_pure = 0;
  prec total_mu_solute = 0;
  prec a_mu_pure,a_mu_solute,dmu;
  //
  // some counters that are incremented over the analysis
  int frames = 0;
  int total_inserts = 0;
  int n_inserts_frame;

  while ( m_read_frame_stdin_xyz(s) ) {

    // initialize some variables
    n_inserts_frame = 0;
    sum_pr = 0;
    mu_pure = 0;
    mu_solute = 0;
    // clear the occupancy matrix
    for (i=0; i<occ_ngridpts; i++) {
      occupancy[i][0] = 0;
    }

    // set the dims of the box to the dims of the grid probe
    ri.dims=&s->dims;

    // get the size of a grid
    dgrid.x=s->dims.x/ngrid;
    dgrid.y=s->dims.y/ngrid;
    dgrid.z=s->dims.z/ngrid;
    dV = dgrid.x * dgrid.y * dgrid.z;
    occ_dgrid.x=s->dims.x/occ_ngrid;
    occ_dgrid.y=s->dims.y/occ_ngrid;
    occ_dgrid.z=s->dims.z/occ_ngrid;

    // get the size of excluded volume of a oxygen, in number of grids
    excluded_v_ngrid[0]=ceil(excluded_v_cutoff/dgrid.x);
    excluded_v_ngrid[1]=ceil(excluded_v_cutoff/dgrid.y);
    excluded_v_ngrid[2]=ceil(excluded_v_cutoff/dgrid.z);

    // get the size of rcut, in number of grids
    rcut_ngrid[0]=ceil(s->rcut/occ_dgrid.x); 
    rcut_ngrid[1]=ceil(s->rcut/occ_dgrid.y); 
    rcut_ngrid[2]=ceil(s->rcut/occ_dgrid.z); 

    // get the excluded volume array
    clear_bool_array(excluded_v,ngridpts);
    //print_bool_array(excluded_v,ngrid,ngrid,ngrid);

    // -------------------
    // build excluded volume and occupancy
    // -------------------
    // for each oxygen:
    //    save its occupancy
    //    subtract its excluded volume
    for (n=0; n<oxygens->molecs->count; n++) {
      oxygen = m_spec_molec(oxygens,n)->atoms->data[0];
      
      oxygen_grid_i[0] = floor(oxygen->pos.x/dgrid.x);
      oxygen_grid_i[1] = floor(oxygen->pos.y/dgrid.y);
      oxygen_grid_i[2] = floor(oxygen->pos.z/dgrid.z);
      
      //oxygen_array_i=(oxygen_grid_i[0]*ngrid*ngrid)+(oxygen_grid_i[1]*ngrid)+oxygen_grid_i[2];
      
      occ_oxygen_grid_i[0] = floor(oxygen->pos.x/occ_dgrid.x);
      occ_oxygen_grid_i[1] = floor(oxygen->pos.y/occ_dgrid.y);
      occ_oxygen_grid_i[2] = floor(oxygen->pos.z/occ_dgrid.z);

      occ_oxygen_array_i=(occ_oxygen_grid_i[0]*occ_ngrid*occ_ngrid)+(occ_oxygen_grid_i[1]*occ_ngrid)+occ_oxygen_grid_i[2];

      occupancy[occ_oxygen_array_i] = add_occupancy( occupancy[occ_oxygen_array_i], n);


      for (i=0; i < excluded_v_ngrid[0]*2+1; i++) {
        ii = mod((i + oxygen_grid_i[0] - excluded_v_ngrid[0]) , ngrid);
        ri.x = dgrid.x*(ii+0.5);
        for (j=0; j < excluded_v_ngrid[1]*2+1; j++) {
          jj = mod((j + oxygen_grid_i[1] - excluded_v_ngrid[1]) , ngrid);
          ri.y = dgrid.y*(jj+0.5);
          for (k=0; k < excluded_v_ngrid[2]*2+1; k++) {
            kk = mod((k + oxygen_grid_i[2] - excluded_v_ngrid[2]) , ngrid);
            ri.z = dgrid.z*(kk+0.5);

            probe_array_i=(ii*ngrid*ngrid)+(jj*ngrid)+kk;
            
            dr = m_radius(oxygen->pos,ri);
            r2 = m_vector_mag2(dr);

            if ( r2 < excluded_v_cutoff2) {
              excluded_v[probe_array_i]=0;
            }
          }
        }
      }
    }

    // -------------------
    // calculate g-force 
    // -------------------
    // for each pair of waters
    //  - calculate \sum 1/r^6 for the frame
    //  \mu_ex = 1/2 \int \int dr dr' \rho_w(r) \rho_w(r') \phi(r-r')
    //         = 1/2 \sum_{i,j} \chi/|r-r'|^6
    //         = \sum_{i<j} \chi/|r-r'|^6
    //  mu/chi = \sum_{i<j} 1/dr^6
    for (i=0; i<oxygens->molecs->count; i++) {
      oxygen1 = m_spec_molec(oxygens,i)->atoms->data[0];
      for (j=i+1; j<oxygens->molecs->count; j++) {
        oxygen2 = m_spec_molec(oxygens,j)->atoms->data[0];

        dr = m_radius(oxygen1->pos,oxygen2->pos);
        
        mu_pure += interpolate_fn(phi_data,rows,cols,phi_data_dr,m_vector_mag(dr),out_col);
      }
    }

    // -------------------
    // perform insertions
    // -------------------
    // for each grid point:
    //    if the point is included:
    //        make insertion, calculate LJ dU, sum
    for (i=0; i<ngrid; i++) {
      ri.x = dgrid.x*(i+0.5);
      occ_probe_grid_i[0] = floor(ri.x/occ_dgrid.x);
      for (j=0; j<ngrid; j++) {
        ri.y = dgrid.y*(j+0.5);
        occ_probe_grid_i[1] = floor(ri.y/occ_dgrid.y);
        for (k=0; k<ngrid; k++) {
          ri.z = dgrid.z*(k+0.5);
          occ_probe_grid_i[2] = floor(ri.z/occ_dgrid.z);
          
          probe_array_i=i*ngrid*ngrid+j*ngrid+k;

          // excluded_v == 0 : this is NOT excluded volume. do not make insertion
          // excluded_v == 1 : this is excluded volume. make insertion here
          if ( ! excluded_v[probe_array_i] ) {

            pr = 0;

          } else {

            r6=0;
            r12=0;

            // loop over oxygens within the r_cut cube
            for (ii=0; ii < rcut_ngrid[0]*2+1; ii++ ) {
              iii = mod( (ii + occ_probe_grid_i[0] - rcut_ngrid[0]), occ_ngrid);
              for (jj=0; jj < rcut_ngrid[1]*2+1; jj++ ) {
                jjj = mod( (jj + occ_probe_grid_i[1] - rcut_ngrid[1]), occ_ngrid);
                for (kk=0; kk < rcut_ngrid[2]*2+1; kk++ ) {
                  kkk = mod( (kk + occ_probe_grid_i[2] - rcut_ngrid[2]), occ_ngrid);

                  // the array index of this grid is:
                  occ_probe_array_i = (iii*occ_ngrid*occ_ngrid) + (jjj*occ_ngrid) + kkk;

                  //DEBUG("l: %i\n",occupancy[occ_probe_array_i][0]);

                  // occupancy[i]={ count, i1, i2, i3, ... }
                  for (l=1; l <= occupancy[occ_probe_array_i][0]; l++) {
                    n = occupancy[occ_probe_array_i][l];
                    oxygen = m_spec_molec(oxygens,n)->atoms->data[0];

                    dr = m_radius(oxygen->pos,ri);
                    
                    r2 = m_vector_mag2(dr);
                    r6_t = pow(r2,-3);
                    r12_t = pow(r6_t,2);

                    r6 += r6_t;
                    r12 += r12_t;

                  }
                  
                }
              }
            }

            // tally up the energy
            dU = sspot12 * r12 - c6pot * r6;
            pr = exp(-1.0*beta*dU);

            // add the pr to the pr of the frame
            sum_pr += pr;

            mu_solute += mu_pure*pr;

            n_inserts_frame += 1;
            //return 0;
          }

        }
      }
    }

    // keep track of sum(exp(-beta U))
    total_sum_pr += sum_pr;
    total_mu_solute += mu_solute;
    total_mu_pure += mu_pure;
    total_inserts += n_inserts_frame;
    frames += 1;

    // averages and dmu
    a_mu_solute = total_mu_solute/total_sum_pr;
    a_mu_pure = total_mu_pure/frames;
    dmu = a_mu_pure-a_mu_solute;

    //printf("frame %d:\t sum_pr:%e,\tn_inserts_frame:%d\tn_inserts:%d\n",frames,sum_pr,n_inserts_frame,n_inserts);
    printf("%d\t%e\t%e\t%e\t%e\t%d\n",frames,dmu,mu_pure,mu_solute,sum_pr,n_inserts_frame);
    fflush(stdout);

    
  }

  return 0;
}

