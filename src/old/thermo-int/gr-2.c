#include "../../comoda.h"
#include "math.h"

int main () {

  //M_molec_spec *oxygens, *h1s, *h2s, *phobe;
  M_molec_spec *waters;

  M_system *s = m_system_new();

  waters = m_system_add_molec_spec(s, "water", 512);
  m_system_add_atom_spec(s, "water", "threesite", "O", 3, 0,0,0);

  //oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  //m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  //h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  //m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  //h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  //m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  //
  //phobe = m_system_add_molec_spec (s, "phobe", 1);
  //m_system_add_atom_spec (s, "phobe", "phobe", "p", 1, 0, 0, 0);

  m_sys_build(s);

  s->rcut = 20.0; // in bohrs

  M_atom *oxygen, *solute, *oxygen2;

  M_aggregate *agg = m_aggregate_new(0.0, s->rcut, 0.1, 3);
  
  int frames;
  int i,j;
  prec r;
  prec g_hw_prefactor, g_ww_prefactor;

  frames = 0;
  prec sum_rho_w = 0;
  prec sum_rho_h = 0;

  while ( m_read_frame_stdin_xyz(s) ) {

    sum_rho_w += waters->density;
    //sum_rho_h += phobe->density;

    //solute = m_spec_molec(phobe,0)->atoms->data[0];

    //for (i=0; i<oxygens->molecs->count; i++) {
    //  oxygen = m_spec_molec(oxygens,i)->atoms->data[0];
    //  r = m_vector_length(m_radius(oxygen->pos,solute->pos));
    //  if (r<s->rcut) {
    //    agg->row[0] = r;
    //    agg->row[1] = 1.0;
    //    agg->row[2] = 0.0;
    //    m_aggregate(agg);
    //  }
    //}

    for (i=0; i<waters->molecs->count; i++) {
      oxygen = m_spec_molec(waters,i)->atoms->data[0];
      for (j=i+1;j<waters->molecs->count; j++) {
        oxygen2 = m_spec_molec(waters,j)->atoms->data[0];
        r = m_vector_length(m_radius(oxygen->pos,oxygen2->pos));
        if (r<s->rcut) {
          agg->row[0] = r;
          agg->row[1] = 0.0;
          agg->row[2] = 1.0;
          m_aggregate(agg);
        }
      }
    }

    frames += 1;
    DEBUG("frame %d done\n",frames);
  }

  //g_hw_prefactor=1.0/( ((float) phobe->molecs->count) * sum_rho_w ) ;
  g_ww_prefactor=2.0/( ((float) waters->molecs->count) * sum_rho_w ) ;
  //m_aggregate_col_scale(agg,1,g_hw_prefactor);
  m_aggregate_col_scale(agg,2,g_ww_prefactor);
  //m_aggregate_col_scale_dV(agg,1);
  m_aggregate_col_scale_dV(agg,2);
  m_aggregate_print(agg);

  return 0;
}
