#include "comoda.h"

int main () {

  M_molec_spec *oxygens, *h1s, *h2s, *phobe;

  M_system *s = m_system_new();

  oxygens = m_system_add_molec_spec (s, "oxygen", 511);
  m_system_add_atom_spec (s, "oxygen", "oxygen", "o", 1, 0, 0, 0);

  h1s = m_system_add_molec_spec (s, "hydrogen1", 511);
  m_system_add_atom_spec (s, "hydrogen1", "hydrogen", "h", 1, 0, 0, 0);

  h2s = m_system_add_molec_spec (s, "hydrogen2", 511);
  m_system_add_atom_spec (s, "hydrogen2", "hydrogen", "h", 1, 0, 0, 0);
  
  phobe = m_system_add_molec_spec (s, "phobe", 1);
  m_system_add_atom_spec (s, "phobe", "phobe", "p", 1, 0, 0, 0);

  m_sys_build(s);

  s->rcut = 17.0; // in bohrs

  M_atom *oxygen, *solute;

  //M_aggregate *agg = m_aggregate_new(0.0, s->rcut, s->rcut, 5);
  
  int frames;
  int i;
  double r;
  double r6,r12;
  frames = 0;
  while ( m_read_frame_stdin_xyz(s) ) {
    r6 = 0;
    r12 = 0;

    solute = m_spec_molec(phobe,0)->atoms->data[0];

    for (i=0; i<oxygens->molecs->count; i++) {
      oxygen = m_spec_molec(oxygens,i)->atoms->data[0];

      r = m_vector_length(m_radius(oxygen->pos,solute->pos));
      //DEBUG("%f\n",r);

      if (r<s->rcut) {
        r6 += pow(r,-6);
        r12 += pow(r,-12);
        //agg->row[0] = 0.0;
        //agg->row[1] = pow(r,-6);
        //agg->row[2] = pow(r,-12);
        //m_aggregate(agg);
      }

    }
    printf("%e\t%e\n",r6,r12);
    frames += 1;
    DEBUG("frame %d done\n",frames);
  }

  //m_aggregate_print_raw(agg);

  return 0;
}
