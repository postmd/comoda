#!/usr/bin/env python3

import os,sys,string
import argparse
import configparser

def main():
    get_config()
    setup_rdf()

    sys.stderr.write("r"+"\t"+"combined"+"\t"+"\t".join([str(c) for c in combos])+"\n")

    with open(rdf,"r") as fp:
        for line in fp:
            values=line.rstrip().split()
            rdf_d=dict(zip(columns,values))

            rdf_out=rdf_d["r"]+"\t"
            g=[]

            for combo in combos:
                (atom_A,atom_B)=combo
                g.extend([float(rdf_d[atom_A,atom_B])*weights[atom_A,atom_B]])
            rdf_out+=str(sum(g))+"\t"
            rdf_out+="\t".join([str(v) for v in g])
            print(rdf_out)

def setup_rdf():
    global weights
    weights = {}

    global columns
    columns = ["r"]

    global combos
    combos = []

    n_atom_A = 0
    for atom_A in atoms_A:
        n_atom_A += n_atom[atom_A]

    n_atom_B = 0
    for atom_B in atoms_B:
        n_atom_B += n_atom[atom_B]

    for i in range(len(atoms)):
        atom_A = atoms[i]
        for j in range(i,len(atoms)):
            atom_B = atoms[j]
            columns.extend([(atom_A,atom_B)])

    for atom_A in atoms_A:
        for atom_B in atoms_B:
            combo = (atom_A, atom_B)
            if not(combo in columns):
                combo = (atom_B, atom_A)
            if not(combo in columns):
                raise Exception(str(combo)+" is not a valid pair of atomic species.")
            weight = float(n_atom[atom_A]*n_atom[atom_B])/(n_atom_A*n_atom_B)
            if combo in combos:
                weights[combo] += weight
            else:
                combos.extend([combo])
                weights[combo] = weight

def setup_cmdline_opts():
    parser = argparse.ArgumentParser(description="Combine RDFs")
    parser.add_argument('-c','--config',nargs=1,type=str,required=True,
        help="path to the configuration file (*.ini)",dest="config")
    parser.add_argument('-r','--rdf',nargs=1,type=str,required=True,
        help="path to the rdf file",dest="rdf")
    parser.add_argument('atoms_A',nargs=1,type=str,
        help="comma separated list of atoms by their symbols")
    parser.add_argument('atoms_B',nargs=1,type=str,
        help="comma separated list of atoms by their symbols")
    return parser

def save_config(config):
    global atoms
    molecules = config["molecules"].keys()
    keys = config.keys()
    atoms = []
    
    global n_mol, n_atom, n_total
    n_mol = {}
    n_atom = {}
    for m in molecules:
        n_mol[m]=int(config["molecules"][m])
        for k in keys:
            k_split = k.split("#")
            if (len(k_split)==2 and k_split[0]==m):
                atom_name = config[k]["symbol"]
                atoms.extend([atom_name])
                n_atom[atom_name] = n_mol[m]*int(config[k]["count"])

def get_config():
    parser = setup_cmdline_opts()
    args = parser.parse_args()
    
    global atoms_A, atoms_B
    atoms_A = args.atoms_A[0].split(',')
    atoms_B = args.atoms_B[0].split(',')

    global rdf
    rdf = args.rdf[0]

    #if not(atoms_A.isdisjoint(atoms_B) or atoms_A==atoms_B):
    #    raise Exception("cannot combine sets of atoms "+",".join(atoms_A)+" and "+",".join(atoms_B))

    config = configparser.ConfigParser(inline_comment_prefixes=(';','#'))
    config.read(args.config[0])
    save_config(config)

if __name__ == "__main__":
    main()
