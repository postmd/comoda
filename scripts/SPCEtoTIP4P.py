#!/usr/bin/python


import os
os.system('clear')
import sys
import numpy as np
from math import *

c = cos(104.52/2 * pi/180)
s = sin(104.52/2 * pi/180)
rb = 0.9572
rm = 0.1546

box = [ 24.856541, 24.856541, 24.856541 ]

def get_new_water(O,H1,H2):
	def dot(a,b):
		return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]

	def scale(a,k):
		return [a[0]*k, a[1]*k, a[2]*k]

	def unit(a):
		return scale(a,dot(a,a)**-0.5)

	def add(a,b):
		return [a[0]+b[0], a[1]+b[1], a[2]+b[2]]

	def subtract(a,b):
		new = [a[0]-b[0], a[1]-b[1], a[2]-b[2]]
		for i in range(3):
			if new[i] > box[i]/2:
				new[i] -= box[i]
			if new[i] < -box[i]/2:
				new[i] += box[i]
		return new

	OH1 = subtract(H1, O)
	OH2 = subtract(H2, O)

	if ( dot(OH1,OH1) > 1.1 or dot(OH2, OH2) > 1.1):
		print "Something bad has happened"

	alpha = add(OH1,OH2)
	alphahat = unit(alpha)

	OM = scale(alphahat, rm)
	M = add(O, OM)

	HH = subtract(H2, H1)

	beta = subtract(OH1 , scale(alphahat, dot(OH1,alphahat)))
	betahat = unit(beta)

	OH1p = scale( add( scale(alphahat, c), scale(betahat, s) ), rb)
	OH2p = scale( subtract( scale(alphahat, c), scale(betahat, s) ), rb)

	H1p = add( OH1p, O )
	H2p = add( OH2p , O )

	return [H1p, H2p, M]

def coords_dump_xyz(l,symb):
	for i in range(len(l)):
		#print "\t".join([str(x/0.529177) for x in l[i]])
		print symb+"\t"+"\t".join([str(x) for x in l[i]])

def coords_dump_toy(l,symb):
	for i in range(len(l)):
		print "\t".join([str(x/0.529177) for x in l[i]])

data = sys.stdin.readlines()
natoms = int(data[0])
nmol = natoms/3
coords=[]
for i in range(2,2+natoms):
	str_coords=data[i].split()
	coords.extend([[float(str_coords[i]) for i in range(1,4)]])

new_O=[]
new_H1p=[]
new_H2p=[]
new_M=[]
for i in range(nmol):
	O=coords[i]
	H1=coords[i+nmol]
	H2=coords[i+2*nmol]
	new_atoms=get_new_water(O,H1,H2)

	H1p=new_atoms[0]
	H2p=new_atoms[1]
	M=new_atoms[2]

	new_O.extend([O])
	new_H1p.extend([H1p])
	new_H2p.extend([H2p])
	new_M.extend([M])

#print str(nmol*4)
#print "Converted from SPCE"
coords_dump_toy(new_O, "O")
coords_dump_toy(new_H1p, "H")
coords_dump_toy(new_H2p, "H")
coords_dump_toy(new_M, "M")
