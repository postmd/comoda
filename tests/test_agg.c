#include "../comoda.h"
#include "math.h"

int main() {

    M_molec_spec *waters;

    M_system *s = m_system_new();
    
    waters = m_system_add_molec_spec(s, "water", 512); 
    m_system_add_atom_spec(s, "water", "spce", "O", 3, 0,0,0); 

    m_sys_build(s);
    
    s->rcut = 20.0; // in bohrs

    M_molec *water1, *water2;
    M_atom *oxy1, *oxy2;

    M_aggregate *agg = m_aggregate_new(0.0, 0.0, 0.1, 4);

    int frames=0;
    prec r;
    prec avg_rho=0.0;
    while (m_read_frame_stdin_xyz(s)) {
      frames ++;
      avg_rho += waters->density;
      int i,j;

      for (i=0; i<waters->molecs->count; i++) {
        water1 = m_spec_molec(waters,i);
        oxy1 = water1->atoms->data[0];

        for (j=i+1; j<waters->molecs->count; j++) {
          water2 = m_spec_molec(waters,j);
          oxy2 = water2->atoms->data[0];

          r = m_vector_length(m_radius(oxy1->pos,oxy2->pos));
          
          if (r<s->rcut) {

              if ( r > agg->max || r < agg->min ) {
                agg = m_aggregate_extend(agg, fmin(agg->min, r), fmax(agg->max, r));
              }

              agg->row[0] = r;
              agg->row[1] = 1.0;
              agg->row[2] = 1.0;
              agg->row[3] = 1.0;
              m_aggregate(agg);
          }
        }
      }

    }
    //printf("This is what the aggregate table looks like (col 2-4 are identical):\n");
    //m_aggregate_print_val(agg);

    //m_aggregate_col_scale(agg,2,2.0);
    //printf("Column 3 (col_index=2) is multiplied by 2:\n");
    //m_aggregate_print_val(agg);
  
    avg_rho = avg_rho/((float)frames);
    m_aggregate_col_scale_dV(agg,3);
    prec prefactor;
    // the prefactor is 2/(n * rho * nframes), where n is the number of molecules, rho is the number density
    prefactor = 2.0/((float)frames)/avg_rho/((float)waters->molecs->count);
    m_aggregate_col_scale(agg,3,prefactor);
    //printf("Column 4 (col_index=3) is converted to g(r):\n");
    m_aggregate_print_all(agg);

    return 0;
}


