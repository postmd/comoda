#include "../comoda.h"
#include "math.h"

int main() {

    M_molec_spec *waters;

    M_system *s = m_system_new();
    
    waters = m_system_add_molec_spec(s, "water", 512); 
    m_system_add_atom_spec(s, "water", "spce", "O", 3, 0,0,0); 

    m_sys_build(s);
    
    s->rcut = 20.0; // in bohrs

    M_molec *water1, *water2;
    M_atom *oxy1, *oxy2;

    M_point p1, p2;
    M_vector v1, v2;

    int frames=0;
    prec r;
    while (m_read_frame_stdin_xyz(s)) {
      frames ++;
      int i,j;

      printf("%d\n",frames);

      water1 = m_spec_molec(waters,5);
      oxy1 = water1->atoms->data[0];

      p1 = oxy1->pos;
      printf("Point1:\t\t %f, %f, %f ... remember that M_point has dims: %f, %f, %f\n",p1.x,p1.y,p1.z,p1.dims->x,p1.dims->y,p1.dims->z);

      water2 = m_spec_molec(waters,10);
      oxy2 = water2->atoms->data[0];

      p2 = oxy2->pos;
      printf("Point2:\t\t %f, %f, %f\n",p2.x,p2.y,p2.z);

      v1 = m_radius(p2,p1);
      printf("v=p2-p1:\t %f, %f, %f\n",v1.x,v1.y,v1.z);
      r = m_vector_length(v1);
      printf("|v|= \t\t %f\n",r);

      m_point_trans( &p1, v1);
      printf("Move p1 by v:\t %f, %f, %f\n",p1.x,p1.y,p1.z);
      printf("Note that the original atom is not moved: %f, %f, %f, p1 is copied (not a reference)\n", oxy1->pos.x,oxy1->pos.y,oxy1->pos.z);

      v2 = m_vector_add(v1,v1);
      printf("v+v:\t\t %f, %f, %f\n",v2.x,v2.y,v2.z);
      
      v2 = m_vector_subtract(v1,v1);
      printf("v-v:\t\t %f, %f, %f\n",v2.x,v2.y,v2.z);

      v2 = m_vector_scale(v1,-0.2);
      printf("-0.2*v:\t\t %f, %f, %f\n",v2.x,v2.y,v2.z);

      v2 = m_vector_unit(v1);
      printf("v_hat:\t\t %f, %f, %f\n",v2.x,v2.y,v2.z);
      printf("v_hat.v_hat:\t %f\n",m_vector_dot(v2,v2));
      return 0;
    }

    return 0;
}


