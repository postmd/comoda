#include "../comoda.h"

int main() {

    M_molec_spec *waters;

    M_system *s = m_system_new();
    
    waters = m_system_add_molec_spec(s, "water", 511); 
    m_system_add_atom_spec(s, "water", "spce", "O", 1, 16, 0, -0.84); 
    m_system_add_atom_spec(s, "water", "spce", "2", 2, 1, 0, 0.42); 
    waters->position = SEPARATED;
    waters->center = geom_center;

    m_sys_build(s);
    
    s->rcut = 20.0; // in bohrs

    M_molec *water1, *water2;
    M_atom *oxy1, *oxy2;

    while (m_read_frame_stdin_xyz(s)) {
      calc_dipoles(s);

      water1 = m_sys_molec(s,0);

      DEBUG("%lf\n",water1->spec->mass);

      DEBUG("%lf\n",water1->dip_mag);
      m_puts_vector(water1->dipole);
      m_puts_point(water1->center);
      m_puts_point(m_molec_atom(water1,0)->pos);
      m_puts_point(m_molec_atom(water1,1)->pos);
      m_puts_point(m_molec_atom(water1,2)->pos);

      exit(0);
      

    }

    return 0;
}


