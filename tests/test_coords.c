#include "../lib/headers/comoda.h"

int main () {
    // Define molecule species pointers which will point to our species
    // arrays of our different species.
    M_molec_spec *polymer;

    // Create a system object. This holds all atoms and molecules as well as
    // information such as box length;
    M_system *s = m_system_new();

    // Add a polymer molecular species and get a pointer to it
    polymer = m_system_add_molec_spec (s, "polymer", 1);

    // Add 50 monomer species to the polymer
    m_system_add_atom_spec (s, "polymer", "monomer",  "Mo", 10, 1, 1, 0);

    // Build the systems mappings and relations

    // Set our box size
    s->dims = (M_vector){.x = 10, .y = 10, .z = 10};
    m_sys_build(s);


    /* Set up our polymer to test flattining of coords.
       Half the plymer will cross the box in the x direction

       The head of the polymer is marked H, the tail T.
       The head is the first atom in the list. The tail is the
       last atom listed.

       +------------------------------------+
       |                                    |
       |                                    |
       |                                    |
       |                                    |
       |                                    |
       |              H      T              |
       ~--0--0--0--0--0      O--0--0--0--0--~
       |                                    |
       |                                    |
       |                                    |
       |                                    |
       |                                    |
       |                                    |
       |                                    |
       +-----------------|------------------+
       0                 5                  10

       After flattening:

                           +------------------------------------+
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
         T                 |              H                     |
         0--0--0--0--0--0--0--0--0--0--0--0                     |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           +-----------------|------------------+
                           0                 5                  10
    */

    M_atom *atom;
    atom = m_array_elt_pos(s->atoms,0);
    m_sys_atom(s, 0)->pos = (M_point) {.x = 5, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 1)->pos = (M_point) {.x = 4, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 2)->pos = (M_point) {.x = 3, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 3)->pos = (M_point) {.x = 2, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 4)->pos = (M_point) {.x = 1, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 5)->pos = (M_point) {.x = 0, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 6)->pos = (M_point) {.x = 9, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 7)->pos = (M_point) {.x = 8, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 8)->pos = (M_point) {.x = 7, .y = 0, .z = 0, .dims = &s->dims };
    m_sys_atom(s, 9)->pos = (M_point) {.x = 6, .y = 0, .z = 0, .dims = &s->dims };
    M_molec *p = molecule(polymer, 0);

    // This creates a new molecule which is flattened
    M_molec *flat =  m_molec_flatten_new(p);
    int i;
    for ( i = 0; i < flat->atoms->count; i++ ) {
        M_point pos = m_molec_atom(flat, i)->pos;
        printf("%e %e %e\n", pos.x, pos.y, pos.z); 
    }
    // free the molecule
    m_molec_free(flat);
    free(flat);

    // This flattens the actual molecule. Cannot be undone.
    m_molec_flatten(p);
    for ( i = 0; i < p->atoms->count; i++ ) {
        M_point pos = m_molec_atom(p, i)->pos;
        printf("%e %e %e\n", pos.x, pos.y, pos.z); 
    }
    // No freeing is necessary. Freeing the system will free this.

    // Free the system object
    m_system_free(s);
    return 0;
}
