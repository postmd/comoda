#include "comoda.h"
#include <stdio.h>
#include <stdlib.h>

void print_routine_example() {
}

void frame_init(M_system* sys, dictionary* dict) {
    sys->read_dip = 1;
    sys->read_vel = 1;
    sys->read_force = 1;
}

void frame_routine(M_system* sys) {
    DEBUG("%d\n",sys->current_frame_id);

    M_atom *atom_i = m_sys_atom(sys,3);
    DEBUG("%lf\t%lf\t%lf\n",atom_i->dip.x, atom_i->dip.y, atom_i->dip.z);
    DEBUG("%lf\t%lf\t%lf\n",atom_i->vel.x, atom_i->vel.y, atom_i->vel.z);
    DEBUG("%lf\t%lf\t%lf\n",atom_i->force.x, atom_i->force.y, atom_i->force.z);
}

void frame_finalise(M_system* sys) {
}
