#include "../lib/headers/marray.h"

void dub(int *ans, int *elt ) {
    *ans = 2*(*elt);
}

int main () {
    int i;

    M_array *x = m_array_new(100, sizeof(int), NULL);

    for ( i = 0; i  < 100; i++ ) {
        m_array_push_nofree(x, &i, 1);
    }

    M_array *y = m_array_map(x, sizeof(int), (M_map_fn) &dub);

    for ( i = 0; i < 100; i++ ) {
        int *a = m_array_elt_pos(y, i);
        printf("c = %d\n", *a);
    }

    return 0;
}
