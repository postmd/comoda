#include "comoda.h"

void frame_init(M_system *sys, dictionary *dict) {
}

void frame_routine(M_system *sys) {

    //M_molec *mol = m_sys_molec(sys,0);
    //printf("%lf\t%lf\t%lf\n",mol->vel.x,mol->vel.y,mol->vel.z);
    //m_puts_point(mol->center);

    //m_puts_matrix(m_matrix_scale( m_vector_outer(mol->vel, mol->vel),
    //                              mol->spec->mass ) );

    //printf("%lf\n",mol->spec->mass);

    //printf("%.12E\t%.12E\n",sys->ewald->U_recip,sys->ewald->U_coulomb);
    //m_stress_output(sys->stress);
    //m_puts_diag(*sys->stress->total);
    //m_puts_diag(*sys->stress->electro);
    //m_puts_diag(*sys->stress->lj);
    //m_puts_diag(*sys->stress->kinetic);
    prec p_total = m_matrix_trace(*sys->stress->total)/3;
    prec p_electro = m_matrix_trace(*sys->stress->electro)/3;
    prec p_lj = m_matrix_trace(*sys->stress->lj)/3;
    prec p_kinetic = m_matrix_trace(*sys->stress->kinetic)/3;
    prec gamma = ((sys->stress->total->xx+sys->stress->total->yy)*-0.5
                 + sys->stress->total->zz)*0.5*sys->dims.z;
    printf("%.12E\t%.12E\t%.12E\t%.12E\t",p_total,p_electro,p_lj,p_kinetic);
    printf("%.12E",sys->ewald->U_coulomb/(3*sys->volume));
    printf("\n");

}

void frame_finalise(M_system *sys) {

}

void print_routine_example() {
    return;
}
