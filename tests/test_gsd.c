#include "../lib/headers/comoda.h"

void map_fn ( double *ans, M_molec *m ) {
    // Calculate the surface area by putting  10000 points on the surface of
    // each sphere and using a solvent radis of 1.0
    *ans = m_gsd_sasa2(m, 100, 2.00);
}

int main () {
    // Define molecule species pointers which will point to our species
    // arrays of our different species.
    M_molec_spec *water, *polymer, *fluid, *crap;

    // Create a system object. This holds all atoms and molecules as well as
    // information such as box length;
    M_system *s = m_system_new();

    // Add a polymer molecular species and get a pointer to it
    polymer = m_system_add_molec_spec (s, "polymer", 150);

    // Add 50 monomer species to the polymer
    m_system_add_atom_spec (s, "polymer", "monomer",  "Mo", 50, 1, 0.5, 0);

    // Add a fluid species to the system
    fluid = m_system_add_molec_spec(s, "fluid", 36250);

    // Add a solvent species to the fluid molecule
    m_system_add_atom_spec (s, "fluid", "solvent",  "S", 1, 1, 0.05, 0);

    // Build the systems mappings and relations
    m_sys_build(s);

    // Read in a frame
    m_read_frame(s);

    // Get surface area of polymer 1
    //M_array *sasa = m_thparray_map(polymer->molecs, sizeof(double), (M_map_fn)map_fn);
    //M_array *sasa = m_parray_map(polymer->molecs, sizeof(double), (M_map_fn)map_fn);

    M_molec *m = molecule(polymer, 149);
    m_gsd_sasa2(m, 10000, 0.5);

    int i;
    //for ( i = 0; i < sasa->count; i++ ) {
        //double *sa = m_array_elt_pos(sasa,i);
        //printf("sasa[%d] = %lf\n", i, *sa);
    //}

    //m_array_free(sasa);

    // Free the system
    m_system_free(s);
    return 0;
}
