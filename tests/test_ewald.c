#include "comoda.h"

void frame_init(M_system *sys, dictionary *dict) {
    //sys->eta = 0.17346;
    //sys->eta = 0.18;
    //sys->eta = 0.17;
    //sys->eta = 0.13734;
    //set_uiarray(sys->nk, 25, 3);
    
}

void frame_routine(M_system *sys) {

    //sys->ewald = m_ewald_calc_frame(sys);

    //printf("%d\t%.10E\t%.10E\t%.10E\t%.10E\t%.10E\n",sys->current_frame_id,
    //       sys->ewald->U_self, sys->ewald->U_real, sys->ewald->U_recip,
    //       sys->ewald->U_mol_self, sys->ewald->U_coulomb);
    printf("%.12E\t%.12E\t%.12E\t%.12E\t%.12E\n",
           sys->ewald->U_self, sys->ewald->U_real, sys->ewald->U_recip,
           sys->ewald->U_mol_self, sys->ewald->U_coulomb);

    prec U_ext = 0;
    U_ext -= sys->ewald->eta * 0.945 * 0.945 / sqrt_pi;
    
    int i;
    for (i=0; i<sys->ewald->n_k; i++) {
        U_ext += 0.945 * 0.945 / (2.0 * sys->volume) * sys->ewald->greenfn[i];
    }
    printf("%.12E\t%.12E\t%.12E\n", sys->ewald->U_coulomb, U_ext, m_ewald_isolated(sys, -0.945));

    //M_point origin;
    //origin.x = 0;
    //origin.y = 0;
    //origin.z = 0;
    //origin.dims = &(sys->dims);
    //M_ewald *new = m_ewald_calc_add(sys, origin, 1);

    //M_atom *Na = m_sys_atom(sys,1);
    //M_ewald *new = m_ewald_calc_del(sys, Na);
    //printf("%.12E\t%.12E\t%.12E\t%.12E\t%.12E\n",
    //       new->U_self, new->U_real, new->U_recip,
    //       new->U_mol_self, new->U_coulomb);
    //m_ewald_free(new);
    


    //m_ewald_free(sys->ewald);
    //sys->ewald = NULL;

}

void frame_finalise(M_system *sys) {

}

void print_routine_example() {
    return;
}
