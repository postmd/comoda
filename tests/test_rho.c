#include "../comoda.h"
#include "math.h"

int main() {

    M_molec_spec *waters1, *waters2, *waters3;

    M_system *s = m_system_new();
    
    waters1 = m_system_add_molec_spec(s, "water1", 100); 
    m_system_add_atom_spec(s, "water1", "spce", "O", 3, 0,0,0); 

    waters2 = m_system_add_molec_spec(s, "water2", 411); 
    m_system_add_atom_spec(s, "water2", "spce", "O", 3, 0,0,0); 

    waters3 = m_system_add_molec_spec(s, "water3", 1); 
    m_system_add_atom_spec(s, "water3", "spce", "O", 3, 0,0,0); 

    m_sys_build(s);
    
    s->rcut = 16.0; // in bohrs

    while (m_read_frame_stdin_xyz(s)) {
      printf("%lf\t%lf\t%lf\t%lf\n",waters1->density,waters2->density,waters3->density,waters1->density+waters2->density+waters3->density);
      
    }
    
    return 0;
}


