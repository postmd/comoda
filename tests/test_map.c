#include "../lib/headers/comoda.h"

int main () {
    M_molec_spec *water, *polymer, *benzene;

    M_system *s = m_system_new();

    //polymer = m_system_add_molec_spec (s, "polymer", 3);
    //m_system_add_atom_spec (s, "polymer", "monomer",  "Mo", 10, 1, 1, 0);

    water = m_system_add_molec_spec(s, "water", 10);
    m_system_add_atom_spec (s, "water", "oxygen",  "O", 1, 1, 1, 0);
    m_system_add_atom_spec (s, "water", "hydrogen",  "H", 2, 1, 1, 0);
    m_system_add_atom_spec (s, "water", "lp",  "LP", 1, 1, 1, 0);
    water->position = SEPERATED;

    benzene = m_system_add_molec_spec(s, "benzene", 1);
    m_system_add_atom_spec (s, "benzene", "carbon",  "C", 6, 1, 1, 0);
    m_system_add_atom_spec (s, "benzene", "hydrogen",  "H", 6, 1, 1, 0);

    m_sys_build(s);

    // Print the mappings out
    m_print_mapping(s);

    m_system_free(s);
    return 0;
}
