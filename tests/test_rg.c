#include "../comoda.h"

void map_fn ( double *ans, M_molec *m ) {
    *ans = rg(m);
}

int main () {
    // Define molecule species pointers which will point to our species
    // arrays of our different species.
    M_molec_spec *water, *polymer, *fluid, *crap;

    // Create a system object. This holds all atoms and molecules as well as
    // information such as box length;
    M_system *s = m_system_new();

    // Add a polymer molecular species and get a pointer to it
    polymer = m_system_add_molec_spec (s, "polymer", 150);

    // Add 50 monomer species to the polymer
    m_system_add_atom_spec (s, "polymer", "monomer",  "Mo", 50, 1, 1, 0);

    // Add a fluid species to the system
    fluid = m_system_add_molec_spec(s, "fluid", 36250);

    // Add a solvent species to the fluid molecule
    m_system_add_atom_spec (s, "fluid", "solvent",  "S", 1, 1, 1, 0);

    // Build the systems mappings and relations
    m_sys_build(s);

    // Read in a frame
    m_read_frame(s);

    // Map an array of molecule pointers to an array of rg values
    M_array *rgs = m_parray_map(polymer->molecs, sizeof(double), (M_map_fn) &map_fn);
    M_array *thrgs = m_thparray_map(polymer->molecs, sizeof(double), (M_map_fn) &map_fn);

    int i;
    for (i = 0; i < thrgs->count; i++ ) {
        printf("rg(i) = %lf, %lf\n", *(double*)m_array_elt_pos(thrgs,i), *(double*)m_array_elt_pos(rgs,i));
    }

    // Free the rg array
    m_array_free(rgs);
    m_array_free(thrgs);

    // Free the system
    m_system_free(s);
    return 0;
}
