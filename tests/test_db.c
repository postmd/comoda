#include "../comoda.h"

void rdf(M_aggregate *agg, M_atom *a, M_atom *b) {
    prec r = m_vector_length(m_radius(a->pos, b->pos));
    agg->row[0] = r;
    m_aggregate(agg);
}

int main () {
    // Define molecule species pointers which will point to our species
    // arrays of our different species.
    M_molec_spec *water, *polymer, *fluid;

    // Create a system object. This holds all atoms and molecules as well as
    // information such as box length;
    M_system *s = m_system_new();

    // Add a polymer molecular species and get a pointer to it
    polymer = m_system_add_molec_spec (s, "polymer", 150);

    // Add 50 monomer species to the polymer
    m_system_add_atom_spec (s, "polymer", "monomer",  "Mo", 50, 1, 1, 0);

    // Add a fluid species to the system
    //fluid = m_system_add_molec_spec(s, "fluid", 36250);

    // Add a solvent species to the fluid molecule
    //m_system_add_atom_spec (s, "fluid", "solvent",  "S", 1, 1, 1, 0);

    //fluid = m_system_add_molec_spec(s, "water", 512);
    //m_system_add_atom_spec (s, "water", "oxygen",  "S", 1, 1, 1, 0);
    //m_system_add_atom_spec (s, "water", "hydrogen",  "S", 1, 1, 1, 0);

    // Build the systems mappings and relations
    m_sys_build(s);


    M_molec *old, *new;

    old = m_sys_molec(s,5);
    new = m_molec_copy(old);

    DEBUG("hello!\n");
    M_atom *this, *that;

    this = m_molec_atom(old,2);
    that = m_molec_atom(new,2);

    that->pos.x = 1.0;
    DEBUG("%lf\t%lf\n",this->pos.x,that->pos.x);
    
    new->center.x = 1.0;
    DEBUG("%lf\t%lf\n",old->center.x,new->center.x);

    s->dims.x=1.0;
    DEBUG("%lf\t%lf\n",old->center.dims->x,new->center.dims->x);

    m_molec_free(new);

    //m_print_mapping(s);

    // Read in a frame
    //m_read_frame(s);
    
    // Create an aggregate structure to hold an rdf data
    //M_aggregate *rdf_agg = m_aggregate_new(0.0, 100.0, 1.0, 1);

    // Call the aggregate pair function using the rdf function
    //m_thaggregate_pair(rdf_agg, s->atoms, (M_aggregate_pair_fn) rdf);

    // Print the rdf aggregate
    //m_aggregate_print_raw(rdf_agg);

    // Free the aggregate object
    //m_aggregate_free(rdf_agg);

    // Free the system object
    m_system_free(s);
    return 0;
}
