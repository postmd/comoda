#include "../lib/headers/fft.h"

int main() {

    prec dx = 1619.8705143626489;
    prec dq = 0.0000096458327140720790;

    FILE *fp;

    fp = fopen("fort.100","r");

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    M_complex *rho = malloc(sizeof(M_complex)*64);
    M_complex *rhohat_ref = malloc(sizeof(M_complex)*64);
    M_uint nn[3];
    nn[0]=4;
    nn[1]=4;
    nn[2]=4;

    int counter = 0;
    while( (read = getline(&line, &len, fp)) != -1 ) {

      prec a,b;

      sscanf(line,"%lf\t%lf\n",&a,&b);
      rho[counter] = a + b*I;
      //printf("%d\t%lf\t%lf\n",counter,creal(rho[counter]),cimag(rho[counter]));
      counter++;

    }

    fclose(fp);


    fp = fopen("fort.200","r");
    counter = 0;
    while( (read = getline(&line, &len, fp)) != -1 ) {

      prec a,b;

      sscanf(line,"%lf\t%lf\n",&a,&b);
      rhohat_ref[counter] = a + b*I;

      counter++;

    }

    fclose(fp);
    free(line);

    M_complex *rhohat = fft(rho, nn, 3, dx);
    M_complex *rho_rev = ifft(rhohat_ref, nn, 3, dq);


    int i;
    for (i=0; i<64; i++) {
      prec a = creal(rho_rev[i]);
      prec b = cimag(rho_rev[i]);
      prec c = a-creal(rho[i]);
      prec d = b-cimag(rho[i]);
      printf("%d\t%e\t%e\t%e\t%e\n",i,a,b,c,d);
    }
    




}
