#include "../comoda.h"
#include "math.h"

int main() {

    M_molec_spec *waters;

    M_system *s = m_system_new();
    
    waters = m_system_add_molec_spec(s, "water", 512); 
    m_system_add_atom_spec(s, "water", "spce", "O", 3, 0,0,0); 

    m_sys_build(s);
    
    s->rcut = 16.0; // in bohrs

    M_molec *water1, *water2;
    M_atom *oxy1, *oxy2;

    M_aggregate *agg = m_aggregate_new(0.0, s->rcut, 0.5, 3);

    prec r;
    while (m_read_frame_stdin_xyz(s)) {
      
      int i,j;

      for (i=0; i<waters->molecs->count; i++) {
        water1 = m_spec_molec(waters,i);
        oxy1 = water1->atoms->data[0];

        for (j=i+1; j<waters->molecs->count; j++) {
          water2 = m_spec_molec(waters,j);
          oxy2 = water2->atoms->data[0];

          r = m_vector_length(m_radius(oxy1->pos,oxy2->pos));
          
          if (r<s->rcut) {
              agg->row[0] = r;
              agg->row[1] = pow(r,-6);
              agg->row[2] = pow(r,-12);
              m_aggregate(agg);
          }
        }
      }

    }
    m_aggregate_print_raw(agg);

    return 0;
}


