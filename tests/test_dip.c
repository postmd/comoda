#include "../lib/headers/comoda.h"

int main () {
    M_molec_spec *stuff;
    M_atom *atom1;

    M_system *s = m_system_new();

    stuff = m_system_add_molec_spec(s, "stuff", 3);
    m_system_add_atom_spec (s, "stuff", "oxygen",  "O", 1, 1, 1, 0);

    m_sys_build(s);

    // Print the mappings out
    m_print_mapping(s);
    
    while (m_read_frame_stdin_xyzpol(s)) {
	int i;
	for (i=0; i<s->atoms->count; i++ ) {
		atom1 = m_sys_atom(s,i);
		
		printf("dip.x %f\tdip.y %f\tdip.z %f\n",atom1->dip.x,atom1->dip.y,atom1->dip.z);
	}
    }


    m_system_free(s);
    return 0;
}
